@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection


@section('content')
<div class="content-wrapper">

<section class="content-header">
    <h1>
      Produits

    </h1>

</section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>


<section class="content">
    <div class="row">
        <div class="col-xs-12">



          <div class="box">
            <div class="box-header with-border">
            @if ( $userProfile->type == "Admin" )

              <a href="{{ url('/produits/create') }}" class="btn btn-success btn-sm" title="Add New Ville">
                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Produit
                </a>

               <a href="{{ url('/produits/importcsv') }}" class="btn bg-red btn-sm pull-right" title="Add New Ville">
                  <i class="fa fa-plus" aria-hidden="true"></i> Importer depuis CSV
              </a>
            @endif
              <hr>
              <div class="box-body">
                  <div >
                    <div class="row">

                        <form  method="POST"  action="{{ url('/produits/search') }}" accept-charset="UTF-8" style="display:inline">
                          {{ csrf_field() }}
                          <div class="col-sm-10">
                          <div class="form-group">
                            <input class="form-control"  name="search" type="text" id="search" value="" >
                         </div>
                          </div>

                          <div class="col-sm-2">
                              <button type="submit" class="btn btn-primary">Chercher</button>
                          </div>

                          <div class="col-sm-6">
                            <div class="form-group" style="display: none" id="filterList">
                              <label class="form-group">Prix vente</label>
                               <input class="form-control"  name="prix_vente" type="text" id="prix_vente" value="" disabled >
                            </div>
                          </div>
                          <div class="col-sm-6">

                            </div>


                        </form>

                      </div>
                      <button type="button" id="filters" class="btn btn-small bg-green">Filters</button>

                   </div>
              </div>

              @if(count($produits) >0 )

              <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                  <div class="row">
                    <div class="col-sm-12">
                      <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1">#</th><th>Nom</th><th>Marque</th><th>Prix</th><th  class="col" >Actions</th>


                          </tr>
                        </thead>
                        <tbody>

                        @foreach($produits as $item)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->nom }}</td><td>{{ $item->marque }}</td><td>{{ $item->prix_vente }} DH</td>
                                <td>
                                    <!--<a href="{{ url('/produits/' . $item->id) }}" title="View Produit"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>-->
                                    @if ( $userProfile->type == "Admin" )

                                    <a href="{{ url('/produits/' . $item->id . '/edit') }}" title="Modifier Produit"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                                    @endif

                                    <form method="POST" action="{{ url('/produits' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <!-- !!! CASCADE DELETE ISSUE WITH STOCK !!! -->
                                        <!-- <button type="submit" class="btn btn-danger btn-sm" title="Supprimer Produit" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button> -->
                                    </form>
                                </td>
                            </tr>
                        @endforeach




                        </tbody>

                      </table>
                    </div>
                  </div>

                </div>
              </div>
              @endif
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif


            </div>

          </div>


            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>




    $(function() {

      $('#filters').on('click',function(){
         $('#filters').hide();
         $('#prix_vente').prop('disabled',false);
         $('#filterList').show();

      });


      $('#example1').DataTable({
        searching:false
      })

    })
  </script>
@endsection
