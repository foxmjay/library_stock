@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')
<div class="content-wrapper">

<section class="content-header">
    <h1>
      Produits

    </h1>

</section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    @if(Session::has('error_message'))
        <div class="alert alert-danger">
            <span class="fa fa-remove"></span>
            {!! session('error_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

</section>


<section class="content">
    <div class="row">
        <div class="col-xs-12">



          <div class="box">
            <div class="box-header with-border">

              <hr>
              <div class="box-body">
                  <div >
                    <div class="row">
                       
                        <form  method="POST"  action="{{ url('/produits/storecsv') }}" accept-charset="UTF-8" style="display:inline" enctype="multipart/form-data" >
                          {{ csrf_field() }}
                          <div class="col-sm-10">
                          <div class="form-group">
                            <input class="form-control"  name="csv_file" type="file" id="csv_file" value="" accept=".csv" >
                         </div>
                          </div>
                                                    
                          <div class="col-sm-2">
                              <button type="submit" class="btn btn-primary">Importer</button>
                          </div>     
                        </form>

     
                      </div>
                      <a href="{{ URL::asset('assets/admin/docs/Produits_exemple.csv')}}" download>Telecharger example fichier</a>


                   </div>
              </div>
            </div>

          </div>


            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>


    
    
    $(function() {

      $('#filters').on('click',function(){
         $('#filters').hide();
         $('#filterList').show();

      });


      $('#example1').DataTable({
        searching:false
      })

    })
  </script>
@endsection
