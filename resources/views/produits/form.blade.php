
<div class="box-body">

    <div class="col-md-6">


        <div class="form-group ">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" value="{{ old('nom', optional($produit)->nom) }}" required>
            {!! $errors->first('nom', '<p class="text-danger">:message</p>') !!}
        </div>

        <div class="form-group ">
            <label for="marque">Marque</label>
            <input type="text" class="form-control" id="marque" name="marque" placeholder="Marque" value="{{ old('marque', optional($produit)->marque) }}">
            {!! $errors->first('marque', '<p class="text-danger">:message</p>') !!}
        </div>
        @php $userProfile = \App\UserProfile::where('user_id',Auth::user()->id)->first(); @endphp
        @if ($userProfile->type == "Admin")
        <div class="form-group ">
            <label for="prix_vente">Prix De Vente</label>
            <input type="number"   step=any class="form-control" id="prix_vente" name="prix_vente" placeholder="Prix De Vente" value="{{ old('prix_vente', optional($produit)->prix_vente) }}">
            {!! $errors->first('prix_vente', '<p class="text-danger">:message</p>') !!}
        </div>
        @endif
         


        <div class="form-group ">
            <label for="prix_min">Prix Min</label>
            <input type="number"   step=any class="form-control" id="prix_min" name="prix_min" placeholder="prix min" value="{{ old('prix_min', optional($produit)->prix_min) }}" readonly>
            {!! $errors->first('prix_min', '<p class="text-danger">:message</p>') !!}
        </div>

        <div class="form-group ">
            <label for="ref_interne">Ref Interne</label>
            <input type="text" class="form-control" id="ref_interne" name="ref_interne" placeholder="ref interne" value="{{ old('ref_interne', optional($produit)->ref_interne) }}" required>
            {!! $errors->first('ref_interne', '<p class="text-danger">:message</p>') !!}
        </div>

        <div class="form-group ">
            <label for="ref_interne">Code bar</label>
            <input type="text" class="form-control" id="barcode" name="barcode" placeholder="Code bar" value="{{ old('barcode', optional($produit)->barcode) }}" required>
            {!! $errors->first('barcode', '<p class="text-danger">:message</p>') !!}
        </div>



    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="{{ old('description', optional($produit)->description) }}">
            {!! $errors->first('description', '<p class="text-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="ref_constructeur">Ref Constructeur</label>
            <input type="text" class="form-control" id="ref_constructeur" name="ref_constructeur" placeholder="ref_constructeur" value="{{ old('ref_constructeur', optional($produit)->ref_constructeur) }}">
            {!! $errors->first('description', '<p class="text-danger">:message</p>') !!}
        </div>

        <div class="form-group ">
            <label for="categorie_id">Categorie</label>
            @if (!empty($produit))
                <select class="form-control" name="categorie_id" id="categorie_id">
                @foreach ($categories as $object)
                @if ($object->id === $produit->categorie_id)
                <option value="{{$object->id}}" selected>{{$object->categorie}} -- {{$object->s_categorie}}</option>
                @else
                <option value="{{$object->id}}">{{$object->categorie}} -- {{$object->s_categorie}}</option>
                @endif
                @endforeach
                </select>
            @else
                <select class="form-control" name="categorie_id" id="categorie_id">
                @foreach ($categories as $object)
                    <option value="{{$object->id}}">{{$object->categorie}} -- {{$object->s_categorie}}</option>
                @endforeach
                </select>
            @endif

            {!! $errors->first('categorie_id', '<p class="text-danger">:message</p>') !!}

        </div>
       

        <div class="form-group ">
            <label for="prix_max">Prix max</label>
            <input type="number"   step=any class="form-control" id="prix_max" name="prix_max" placeholder="prix max" value="{{ old('prix_max', optional($produit)->prix_max) }}" readonly>
            {!! $errors->first('prix_max', '<p class="text-danger">:message</p>') !!}
        </div>

        <div class="form-group ">
            <label for="garantie">Garantie</label>
            <input type="number"   step=any class="form-control" id="garantie" name="garantie" placeholder="garantie" value="{{ old('garantie', optional($produit)->garantie) }}">
        </div>



    </div>

  
</div>


