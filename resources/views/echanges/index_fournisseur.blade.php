
@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

<div class="content-wrapper">
<section class="content-header">
    <h1>
      Echanges

    </h1>

</section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

         
          <div class="box">
            <div class="box-header with-border">
              
              <hr>
              <div class="box-body">
                  <div >
                    <div class="row">
                       
                        <form  method="POST"  action="{{ url('/echange/search') }}" accept-charset="UTF-8" style="display:inline">
                          {{ csrf_field() }}
                          <div class="col-sm-10">
                          <div class="form-group">
                            <input class="form-control"  name="search" type="text" id="search" value="" >
                         </div>
                          </div>
                          <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary">Chercher</button>
                          </div>
      
                        </form>

                      </div>
                   </div>
              </div>
              @if(count($echanges) >0 )
              <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  
                  <div class="row">
                    <div class="col-sm-12">
                   
                      <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <!--<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1">#</th>-->
                            <th>ID </th>
                            <th>Ref Interne </th>
                            <th>Produit </th>
                            <th>Marque</th>
                            <th>Fournisseur </th>
                            <th>Numero serie </th>
                            
                            <th  class="col" >Actions</th>


                          </tr>
                        </thead>
                        <tbody>
                        
                          @foreach($echanges as $item)
                              @php
                                $color = 'white';
                                if( $item->etat == 'Gone')
                                  $color = 'rgb(130,255,130)';
                               

                              @endphp
                              <tr style="background-color: {{ $color}}">
                                  <!--<td class="sorting_1">{{ $loop->iteration }}</td> -->
                                  <td>{{ $item->id }}</td>
                                  <td>{{ $item->ref_interne }}</td>
                                  <td>{{ $item->nom }}</td>
                                  <td>{{ $item->marque }}</td> 
                                  <td>{{ $item->raison_sociale }}</td>
                                  <td>{{ $item->num_serie }}</td>

                                  <td >
                                     @if($item->etat == 'Echange')
                                      <a href="{{ url('echange/fournisseur/' . $item->id) }}" title="Changer"><button class="btn btn-primary btn-sm"><i class="fa fa-exchange" aria-hidden="true"></i> Changer</button></a>
                                     @endif
                                  </td>
                              </tr>
                          @endforeach

                        </tbody>

                      </table>

                    </div>
                  </div>
                
                </div>

              </div>

            @endif


              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

            </div>
          </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        
    </div>
    <!-- /.row -->
</section>
</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable({
        searching:false
      })

    })
  </script>
@endsection
