@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Echange
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Echange</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Date achat</th>
                        <th>Garantie (Mois)</th>
                       
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td><input  type="radio" name="checked" v-bind:id="stock.id" v-bind:value="stock.id" v-model="checkedProduit" v-on:change="selectProduit(stock)"><br></td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td> 
                    <td>@{{stock.date_achat}}</td>  
                    <td>@{{stock.garantie}}</td>  
                    
                </tr>
  
                </table>

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->

        <div class="col-xs-12" v-if="checkedProduit != ''">

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Action</h3>
                </div> 
                <div  class="box-body">
                    
                      <div class="col-xs-5"></div>

                      <div class="col-xs-2">
                        <div class="form-group">
                            <label for="categorie">Action</label>
                            <select name="action" id="action" v-model="action">
                                @foreach (['Echange','Aucune'] as $object)
                                <option value="{{$object}}">{{$object}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>

                     <div class="col-xs-5"></div>
                </div>


           </div>

    </div>


    <div class="col-xs-12" v-show="action == 'Echange'">

        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Stock</h3>
            </div> 
            <div  class="box-body">

            <div  class="box-body table-responsive ">
                    <table  id="stockList" class="table ">
                    <thead>
                    <tr>
                        <th>Ref fournisseur</th>
                        <th>Numero de serie</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text"   class="form-control" id="ref_fournisseur" name="ref_fournisseur" placeholder="ref fournisseur" v-model="ref_fournisseur"></td>
                        <td><input type="text"   class="form-control" id="num_serie" name="num_serie" placeholder="numero serie" v-model="num_serie"></td>

                    </tr>
                  </tbody>
      
                    </table>
                    
                </div>
                
            </div>


        </div>

        </div>


        <div class="col-xs-12">

            <div class="box">
                <div class="form-group">
             
                  <div  class="box-body table-responsive">
                      <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                          <i class="fa fa-save"></i>Confirmer
                      </button> 
                      <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                          <i class="fa fa-close"></i>Annuler
                      </button>     
                  </div>
                </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var echange = {!! json_encode($echange) !!};



var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {

    selectedStocks: echange,
    checkedProduit : '',
    action : '',
    ref_fournisseur : '',
    num_serie : ''

   },

 
    
  methods: {


    selectProduit: function selectProduit(stock){
       this.checkedProduit = stock;
       
    },


    annuler: function annuler(){
      //history.back();
      window.location.href ="/echange/fournisseur";
    },

    confirmer: function(){

        //console.log(this.selectedProduit);
        enableSpinner();


        var _this = this;
        let formData = new FormData();

        //var stocksdata = JSON.stringify( { 'stock' : _this.checkedStock});

        formData.append('id',this.checkedProduit.id);
        formData.append('action',_this.action);
        formData.append('ref_fournisseur', _this.ref_fournisseur);
        formData.append('num_serie', _this.num_serie);


        axios.post('/echange/fournisseur/store',formData).then(function (response){
          //console.log(response);
          if(response.data['result'] == "OK"){
            window.location.href ="/echange/fournisseur";
          }

          if(response.data['result'] == "KO"){
            disableSpinner();

            var stock=response.data['stock']
            alert("Le produit avec  numero de serie : "+stock+" exist deja dans le stock");
          }

        }).catch(function (error){
          console.log(error)
        });


},

    
  }
});



</script>

@endsection
