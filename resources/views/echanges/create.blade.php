@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Echange
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Echange</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Date vente</th>
                        <th>Garantie (Mois)</th>
                       
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td><input  type="radio" name="checked" v-bind:id="stock.id" v-bind:value="stock.id" v-model="checkedProduit" v-on:change="selectProduit(stock)"><br></td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td> 
                    <td>@{{stock.date_vente}}</td>  
                    <td>@{{stock.garantie}}</td>  
                    
                </tr>
  
                </table>

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->

        <div class="col-xs-12" v-if="checkedProduit != ''">

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Action</h3>
                </div> 
                <div  class="box-body">
                    
                      <div class="col-xs-5"></div>

                      <div class="col-xs-2">
                        <div class="form-group">
                            <label for="categorie">Action</label>
                            <select name="action" id="action" v-model="action">
                                @foreach (['Echange','Aucune'] as $object)
                                <option value="{{$object}}">{{$object}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>

                     <div class="col-xs-5"></div>
                </div>


           </div>

    </div>


    <div class="col-xs-12" v-show="action == 'Echange'">

        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Stock</h3>
            </div> 
            <div  class="box-body">

                <table class="table">
                    <tr>
                    
                        <td> 
                            <label for="categorie">Type</label>
                            <select name="type_search" id="type_search" v-model="type_search">
                                @foreach (['Serialisable','Non Seralisable'] as $object)
                                <option value="{{$object}}">{{$object}}</option>
                                @endforeach
                            </select>
                      </td>
                        <td> 
                            <input type="text" name="keyword" id="keyword" v-model="keyword"  v-on:keyup.enter="getStock()" required>

                            <button  type="button" class="btn btn-success" title="Chercher"@click.prevent="getStock()">
                                <span class="fa fa-search" aria-hidden="true"></span>
                            </button>
                            
                        </td>
                    </tr>
                  </table>
                
            </div>


            <div  class="box-body table-responsive no-padding">
                <table  id="stockList" class="table ">
                <thead>
                <tr>
                    <th></th>
                    <th>Produit</th>
                    <th>Marque</th>
                    <th>Ref interne</th>
                    <th>Numero serie</th>
                    <th>Fournisseur</th>

                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(stock,index) in stocks" >
                    <td><input  type="radio" name="selectedProduit" v-bind:id="stock.id" v-bind:value="stock.id" v-model="selectedProduit"></td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td>                                  
                    <td>@{{stock.raison_sociale}}</td> 
                    <td>

                    </td>
                </tr>
              </tbody>
  
                </table>
                
            </div>


        </div>

        </div>


        <div class="col-xs-12">

            <div class="box">
                <div class="form-group">
             
                  <div  class="box-body table-responsive">
                      <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                          <i class="fa fa-save"></i>Confirmer
                      </button> 
                      <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                          <i class="fa fa-close"></i>Annuler
                      </button>     
                  </div>
                </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var bl_ventes = {!! json_encode($ventes) !!};
var bl_id = {!! json_encode($bl->id) !!};
var client_id = {!! json_encode($client_id) !!};
var devis_id = {!! json_encode($devis_id) !!};


var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {

    selectedStocks: bl_ventes,
    checkedProduit : '',
    selectedProduit : '',
    action : '',
    stocks : [],
    type_search : 'Serialisable',
    keyword : '',

   },

 
    
  methods: {


    selectProduit: function selectProduit(stock){
       this.checkedProduit = stock;
       
    },

    getStock: function getStock(){
      _this = this;
      var data = JSON.stringify( {'vente_id':this.checkedProduit.vente_id, 'type_search' : this.type_search ,'keyword': this.keyword});
      axios.post('/produits/getstock',{data: data}).then(function (response) {
         _this.stocks = response.data;
      });

      this.selectedProduit = '';
    
    },


    annuler: function annuler(){
      //history.back();
      window.location.href ="/clients/"+client_id;
    },

    confirmer: function(){

        //console.log(this.selectedProduit);

        enableSpinner();

        var _this = this;
        let formData = new FormData();

        //var stocksdata = JSON.stringify( { 'stock' : _this.checkedStock});

        formData.append('devis_id',devis_id);
        formData.append('client_id',client_id);
        formData.append('bl_id',bl_id);
        formData.append('vente_id',_this.checkedProduit.vente_id);
        formData.append('stockOrig', _this.checkedProduit.id);
        formData.append('stockEchange', _this.selectedProduit);
        formData.append('action', _this.action);


        axios.post('/echange/store',formData).then(function (response){
          //console.log(response);
          if(response.data['result'] == "OK"){
            window.location.href ="/clients/"+client_id;
          }

        }).catch(function (error){
          console.log(error)
        });


},

    
  }
});



</script>

@endsection
