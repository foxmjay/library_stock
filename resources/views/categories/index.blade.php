@extends('layouts.admin')





@section('content')
<div class="content-wrapper">

    <section class="content-header">
        <h1>
           Categories
        </h1>

    </section>

    <section>
        <br>
        @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
        @endif
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">



                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ url('/categories/create') }}" class="btn btn-success btn-sm" title="Add New Ville">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Categorie
                        </a>
                    </div>

                        @if(count($categories) >0 )

                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-bordered " role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th>ID</th>
                                                    <th>Categorie</th>
                                                    <th>Sous Categorie</th>
                                                    <th class="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($categories as $item)

                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{{ $item->categorie }}</td>
                                                    <td>{{ $item->s_categorie }}</td>
                                                    <td>
                                                        <a href="{{ url('/categories/' . $item->id . '/edit') }}" title="Modifier">
                                                             <button  class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button>
                                                        </a>
                                                         <form method="POST" action="{{ url('/categories' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                            {{ method_field('DELETE') }}
                                                            {{ csrf_field() }}
                                                        
                                                            <button type="submit" class="btn btn-danger btn-sm" title="Supprimer" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button> 
                                                        </form> 
                                                    </td>
                                                </tr>
                                                @endforeach




                                            </tbody>

                                        </table>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>

                        @endif


                    
                    </div>

                </div>


                <!-- /.box -->
            </div>
            <!-- /.col -->

    </section>
</div>

@endsection
