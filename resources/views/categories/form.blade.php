<div class="box-body">


    <div class="col-md-6">


        <div class="form-group">
            <label for="categorie">Categorie</label>
            <input type="text" class="form-control" id="categorie" name="categorie" placeholder="categorie"
                value="{{ old('categorie', optional($categorie)->categorie) }}" required>
            {!! $errors->first('categorie', '<p class="text-danger ">:message</p>') !!}
        </div>

        
    </div>

    <div class="col-md-6">
        
        <div class="form-group" v-if="justif">
            <label for="s_categorie">Sous Categorie</label>
            <input type="text" class="form-control" id="s_categorie" name="s_categorie" placeholder=" sous categorie"
                value="{{ old('s_categorie', optional($categorie)->s_categorie) }}">
            {!! $errors->first('s_categories', '<p class="text-danger ">:message</p>') !!}

        </div>

    
    </div>


</div>