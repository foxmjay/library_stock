@extends('layouts.admin')

@section('content')

                <div class="card">
                    <div class="card-header">ChiffreAffaire {{ $chiffreaffaire->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/chiffre-affaires') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/chiffre-affaires/' . $chiffreaffaire->id . '/edit') }}" title="Edit ChiffreAffaire"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('chiffreaffaires' . '/' . $chiffreaffaire->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete ChiffreAffaire" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $chiffreaffaire->id }}</td>
                                    </tr>
                                    <tr><th> Chiffreannuel </th><td> {{ $chiffreaffaire->chiffreannuel }} </td></tr><tr><th> Objectif </th><td> {{ $chiffreaffaire->objectif }} </td></tr><tr><th> Commerciale Id </th><td> {{ $chiffreaffaire->commerciale_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                </div>

@endsection
