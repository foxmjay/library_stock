<div class="box-body" id="chiffreaffaire">
    <div class="form-group ">
        <label for="type">Type</label>
        <select class="form-control" name="type" id="type" v-on:change="annuel=!annuel;">
            @if (!empty($chiffreaffaire) == 1)
            @if (is_null($chiffreaffaire->commerciale_id) )
            <option value="0">par Commerciale </option>
            <option value="1" selected>Annuel </option>
            @else
            <option value="0" selected>par Commerciale </option>
            <option value="1">Annuel </option>
            @endif
            @else
            <option value="0">par Commerciale </option>
            <option value="1">Annuel </option>
            @endif

        </select>
    </div>

    <div class="form-group {{ $errors->has('chiffreannuel') ? 'has-error' : ''}}" v-if="annuel">
        <label for="chiffreannuel" class="control-label">{{ 'Chiffreannuel' }}</label>
        <input class="form-control" name="chiffreannuel" type="number" id="chiffreannuel"
            value="{{ $formMode === 'edit' ? $chiffreaffaire->chiffreannuel : ''}}" required>

        {!! $errors->first('chiffreannuel', '<p class="help-block">:message</p>') !!}
    </div>



    <div class="form-group" v-if="!annuel">
        <label for="commerciale_id">Commerciale</label>
        @if (!empty($chiffreaffaire) == 1)
        <select class="form-control" name="commerciale_id" id="commerciale_id">
            @foreach ($commerciales as $object)
            @if ($object->id === $chiffreaffaire->commercial_id)
            <option value="{{$object->id}}" selected="selected">{{$object->nom}} {{$object->prenom}}</option>
            @else
            <option value="{{$object->id}}">{{$object->nom}} {{$object->prenom}} </option>
            @endif
            @endforeach
        </select>
        @else
        <select class="form-control" name="commerciale_id" commerciale_id="etat">
            @foreach ($commerciales as $object)
            <option value="{{$object->id}}">{{$object->nom}} {{$object->prenom}} </option>
            @endforeach
        </select>
        @endif
    </div>

    <div class="form-group {{ $errors->has('objectif') ? 'has-error' : ''}}" v-if="!annuel">
        <label for="objectif" class="control-label">{{ 'Objectif' }}</label>
        <input class="form-control" name="objectif" type="number" id="objectif"
            value="{{ $formMode === 'edit' ? $chiffreaffaire->objectif : ''}}" required>

        {!! $errors->first('objectif', '<p class="help-block">:message</p>') !!}
    </div>



    <div class="form-group">
        <input class="btn btn-primary" type="submit" onSubmit="enableSpinner();" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
    </div>
</div>
@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

@if (!empty($chiffreaffaire) == 1)
@if (is_null($chiffreaffaire->commerciale_id) )
<script>
    var app = new Vue({
          el: '#chiffreaffaire',

          data: {
          annuel : true,


           },
        });

</script>
@else
<script>
    var app = new Vue({
          el: '#chiffreaffaire',

          data: {
          annuel : false,


           },
        });

</script>
@endif
@endif
@endsection
