
<div class="box-body" >


    <div class="col-md-12">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
            <tr>
                <td> <input type="text" class="form-control" id="nom" name="nom" placeholder="nom" v-model="contactModel.nom"  ></td>
                <td> <input type="text" class="form-control" id="poste" name="poste" placeholder="poste" v-model="contactModel.poste" ></td>
                <td> <input type="text" class="form-control" id="telephone" name="telephone" placeholder="telephone" v-model="contactModel.telephone"  ></td>
                <td> <input type="text" class="form-control" id="gsm" name="gsm" placeholder="gsm" v-model="contactModel.gsm" ></td>
                <td> <input type="text" class="form-control" id="email" name="email" placeholder="email" v-model="contactModel.email" ></td>
                <td> 
                    <button type="button" class="btn btn-success" title="Ajouter" @click.prevent="addItem()">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </button>
                </td>
            </tr>
            </table>
        </div>
        <!-- /.box-body -->

        

    </div>

    

    <div class="col-md-12">

            <hr> 
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
            <tr>
                <th>Nom</th>
                <th>Poste</th>
                <th>Telephone</th>
                <th>GSM</th>
                <th>Email</th>
                <th></th>
            </tr>
            <tr v-for="item in items">
                <td>@{{item.nom}}</td>
                <td>@{{item.poste}}</td>
                <td>@{{item.telephone}}</td>
                <td>@{{item.gsm}}</td>
                <td>@{{item.email}}</td>
                <td> 
                    <button type="button" class="btn btn-danger" title="Supprimer" @click.prevent="deleteItem(item)"> 
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </td>
            </tr>
        
            </table>
        </div>
        <!-- /.box-body -->

    

    </div>

</div>

