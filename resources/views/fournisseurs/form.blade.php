
              <div class="box-body">

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="raison_sociale">Raison Sociale</label>
                    <input type="text" class="form-control" id="raison_sociale" name="raison_sociale" placeholder="raison sociale" value="{{ old('raison_sociale', optional($fournisseur)->raison_sociale) }}" required>
                    {!! $errors->first('raison_sociale', '<p class="text-danger ">:message</p>') !!}
                  </div>

                  <div class="form-group">
                    <label for="ice">ICE</label>
                    <input type="text" class="form-control" id="ice" name="ice" placeholder="ice" value="{{ old('ice', optional($fournisseur)->ice) }}">
                  </div>

                  <div class="form-group">
                    <label for="categorie">Categorie</label>
                    <input type="text" class="form-control" id="categorie" name="categorie" placeholder="categorie" value="{{ old('categorie', optional($fournisseur)->categorie) }}">
                  </div>

                  <div class="form-group">
                      <label for="secteur_activite">Secteur d'activite</label>
                      <input type="text" class="form-control" id="secteur_activite" name="secteur_activite" placeholder="secteur activitee" value="{{ old('secteur_activite', optional($fournisseur)->secteur_activite) }}">
                    </div>

                   <div class="form-group">
                    <label for="forme_juridique">Forme juridique</label>
                    <input type="text" class="form-control" id="forme_juridique" name="forme_juridique" placeholder="forme_juridique" value="{{ old('forme_juridique', optional($fournisseur)->forme_juridique) }}">
                  </div>

                  <div class="form-group">
                    <label for="identifient_fiscal">Identifient fiscal</label>
                    <input type="text" class="form-control" id="identifient_fiscal" name="identifient_fiscal" placeholder="identifient_fiscal" value="{{ old('identifient_fiscal', optional($fournisseur)->identifient_fiscal) }}">
                  </div>
                
                
                  <div class="form-group">
                    <label for="n_patente">Numero patente</label>
                    <input type="text" class="form-control" id="n_patente" name="n_patente" placeholder="n_patente" value="{{ old('n_patente', optional($fournisseur)->n_patente) }}">
                  </div>
                  
                  <div class="form-group">
                      <label for="n_reg_commerce">Numero Registre Commerce</label>
                      <input type="text" class="form-control" id="n_reg_commerce" name="n_reg_commerce" placeholder="Numero Registre Commerce" value="{{ old('n_reg_commerce', optional($fournisseur)->n_reg_commerce) }}">
                    </div>

                   <div class="form-group">
                    <label for="ville_reg_commerce">Ville registre Commerce</label>
                    <input type="text" class="form-control" id="ville_reg_commerce" name="ville_reg_commerce" placeholder="Ville registre Commerce" value="{{ old('ville_reg_commerce', optional($fournisseur)->ville_reg_commerce) }}">
                  </div>

                  <div class="form-group">
                    <label for="adresse_facturation">Adresse facturation</label>
                    <input type="text" class="form-control" id="adresse_facturation" name="adresse_facturation" placeholder="Adresse facturation" value="{{ old('adresse_facturation', optional($fournisseur)->adresse_facturation) }}">
                  </div>
                


                </div>
                <div class="col-md-6">

                <div class="form-group">
                    <label for="telephone">telephone</label>
                    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="telephone" value="{{ old('telephone', optional($fournisseur)->telephone) }}">
                  </div>

                  <div class="form-group">
                    <label for="adresse">Adresse</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" placeholder="adresse" value="{{ old('adresse', optional($fournisseur)->adresse) }}">
                  </div>

                    <div class="form-group">
                    <label for="gsm">gsm</label>
                    <input type="text" class="form-control" id="gsm" name="gsm" placeholder="gsm" value="{{ old('gsm', optional($fournisseur)->gsm) }}">
                    </div>

                    <div class="form-group">
                      <label for="email">email</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="email" value="{{ old('email', optional($fournisseur)->email) }}">
                    </div>

                    <div class="form-group">
                      <label for="ville_id">ville</label>
                      @if (!empty($fournisseur) == 1)
                        <select class="form-control" name="ville_id">
                        @foreach ($villes as $object)
                          @if ($object->id === $fournisseur->ville_id)
                          <option value="{{$object->id}}" selected="selected">{{$object->name}}</option>
                          @else
                          <option value="{{$object->id}}">{{$object->name}} </option>
                          @endif
                        @endforeach
                        </select>
                      @else
                        <select class="form-control" name="ville_id" id="ville_id">
                        @foreach ($villes as $object)
                          <option value="{{$object->id}}">{{$object->name}}</option>
                        @endforeach
                        </select>
                      @endif
                    </div>

                    <div class="form-group">
                      <label for="pays_id">Pays</label>
                      @if (!empty($fournisseur))
                        <select class="form-control" name="pays_id">
                        @foreach ($pays as $object)
                          @if ($object->id === $fournisseur->pays_id)
                          <option value="{{$object->id}}" selected>{{$object->name}}</option>
                          @else
                          <option value="{{$object->id}}">{{$object->name}}</option>
                          @endif
                        @endforeach
                        </select>
                      @else
                        <select class="form-control" name="pays_id" id="pays_id">
                        @foreach ($pays as $object)
                          <option value="{{$object->id}}">{{$object->name}}</option>
                        @endforeach
                        </select>
                      @endif
                    </div>

                    <div class="form-group">
                      <label for="region_id">Regions</label>
                      @if (!empty($fournisseur))
                      <select class="form-control" name="region_id" id="region_id">
                      @foreach ($regions as $object)
                        @if ($object->id === $fournisseur->region_id)
                        <option value="{{$object->id}}" selected>{{$object->name}}</option>
                        @else
                        <option value="{{$object->id}}">{{$object->name}}</option>
                        @endif
                      @endforeach
                      </select>
                      @else
                        <select class="form-control" name="region_id" id="region_id">
                        @foreach ($regions as $object)
                          <option value="{{$object->id}}">{{$object->name}}</option>
                        @endforeach
                        </select>
                      @endif
                    </div>

                    <div class="form-group">
                      <label for="langue_id">Langue</label>
                      @if (!empty($fournisseur))
                      <select class="form-control" name="langue_id" id="langue_id">
                      @foreach ($langues as $object)
                        @if ($object->id === $fournisseur->langue_id)
                        <option value="{{$object->id}}" selected>{{$object->name}}</option>
                        @else
                        <option value="{{$object->id}}">{{$object->name}}</option>
                        @endif
                      @endforeach
                      </select>
                        @else
                        <select class="form-control" name="langue_id" id="langue_id">
                        @foreach ($langues as $object)
                          <option value="{{$object->id}}">{{$object->name}}</option>
                        @endforeach
                        </select>
                      @endif
                    </div>


                    <div class="form-group">
                      <label for="devise_id">Devise</label>
                      @if (!empty($fournisseur))
                      <select class="form-control" name="devise_id" id="devise_id">
                      @foreach ($devises as $object)
                        @if ($object->id === $fournisseur->devise_id)
                        <option value="{{$object->id}}" selected>{{$object->name}}</option>
                        @else
                        <option value="{{$object->id}}">{{$object->name}}</option>
                        @endif
                      @endforeach
                      </select>
                          @else
                        <select class="form-control" name="devis_id" id="devis_id">
                        @foreach ($devises as $object)
                          <option value="{{$object->id}}">{{$object->name}}</option>
                        @endforeach
                        </select>
                      @endif
                    </div>

                    <div class="form-group">
                      <label for="note">Note</label>
                      <textarea  class="form-control" id="note" name="note" >{{ old('mode_paiement', optional($fournisseur)->note) }}</textarea>
                    </div>

                    <!--<div class="form-group">
                      <label for="telephone">mode paiement</label>
                      <input type="text" class="form-control" id="mode_paiement" name="mode_paiement" placeholder="mode_paiement" value="{{ old('mode_paiement', optional($fournisseur)->mode_paiement) }}">
                    </div>


                    <div class="form-group">
                    <label for="echeance_paiement">echeance paiement</label>
                    <input type="text" class="form-control" id="echeance_paiement" name="echeance_paiement" placeholder="echeance_paiement" value="{{old('echeance_paiement', optional($fournisseur)->echeance_paiement)}}">
                    </div> -->


                </div>

            
              </div>

          