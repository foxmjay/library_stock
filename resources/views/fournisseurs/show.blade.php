@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$fournisseur->raison_sociale}}
      </h1>

    </section>

<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>


    <!-- Main content -->
    <section class="content" id="fournisseur_show">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h1 class="text-center"><b>{{$fournisseur->raison_sociale}}</b></h1>
              <hr>

              <p class="text-muted text-center">{{$fournisseur->forme_juridique}} , {{$fournisseur->categorie}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Nombre de bon commandes:</b> <a class="pull-right">{{count($boncommands)}}</a>
                </li>
                <!--<li class="list-group-item">
                  <b>Nombre facture : </b> <a class="pull-right"></a>
                </li> -->
                <li class="list-group-item">
                  <b>totale ventes :</b> <a class="pull-right">{{$total[0]->som}} Dh</a>
                </li>
              </ul>

              <a href="{{ url('/boncommands/'.$fournisseur->id.'/create') }}" class="btn btn-success btn-block"><b>Ajouter un Bon de commande</b></a>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Plues d'info</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-fw fa-envelope margin-r-5"></i> Email : {{$fournisseur->email}}</strong>





              <hr>

              <strong><i class="fa fa-fw fa-mobile margin-r-5"></i> GSM : {{$fournisseur->gsm}}</strong>



              <hr>

              <strong><i class="fa fa-fw fa-phone margin-r-5"></i> Fixe :  {{$fournisseur->telephone}}</strong>



              <hr>

              <strong><i class="fa fa-fw fa-map-pin margin-r-5"></i> Ville : {{$fournisseur->telephone}}</strong>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li id="tabBoncommands" class="active"><a href="#boncommands" data-toggle="tab">Bons de commande</a></li>
              <li id="tabBonlivraisons"  ><a href="#bonlivraisons" data-toggle="tab">bons de livraison</a></li>


            </ul>
            <div class="tab-content">

              <div class="active tab-pane" id="boncommands">

                <div class="box-body table-responsive">

                    @if(count($boncommands) == 0)
                    @else


                    <table id="tableBoncommands" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Reference boncommand</th>
                      <th>N Devis</th>
                      <th>Date</th>
                      <th>Montant HT</th>
                      <th>Date livraison</th>
                      <th>Cree par</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($boncommands as $boncommand)
                        @if($boncommand->etat == "En cours")
                        <tr>
                          <td>{{ $boncommand->id }}</td>
                          <td>{{ $boncommand->ref_bc }}</td>
                          <td>{{ $boncommand->ref_devis }}</td>
                          <td>{{ $boncommand->created_at->format('d/m/Y') }}</td>
                          <td>{{ $boncommand->prix_total }} DH</td>
                          <td>{{ $boncommand->date_livraison }}</td>
                          <td>{{ $boncommand->nom }} {{ $boncommand->prenom }}</td>

                          <td  style="min-width: 100px">

                              <form method="POST" action="{{ url('/boncommands' . '/' . $boncommand->id) }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/boncommands/' . $boncommand->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                                    <!--<a href="" class="btn btn-primary" title="Edit Boncommand">
                                        <span class="icon-pencil" aria-hidden="true"></span>
                                    </a> -->

                                    <!--<button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Supprimer Boncommand?&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button>-->

                                    <a href="{{ url('/boncommands/' . $boncommand->id . '/print') }}" target="_blank" class="btn btn-warning" title="Imprimer">
                                        <span class="fa fa-print" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{ url('/echeances/' . $boncommand->id . '/index') }}"  class="btn btn-success" title="Echeance Paiement">
                                        <span class="fa fa-tags" aria-hidden="true"></span>
                                    </a>

                                </div>
                              </form>

                          </td>
                        </tr>
                        @endif
                        @endforeach

                    </tbody>

                  </table>
                    @endif
                  </div>

              </div>

              <div class="tab-pane" id="bonlivraisons">

                  <div class="box-body table-responsive">
  
                      @if(count($boncommands) == 0)
                      @else
  
  
                      <table id="tableBonlivraisons" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>ID</th>
                        <th>Reference boncommand</th>
                        <th>Date</th>
                        <th>Montant HT</th>
                        <th>Cree par</th>
                        <th></th>
                      </tr>
                      </thead>
                      <tbody>
                          @foreach($boncommands as $boncommand)
                          @if($boncommand->etat == "Termine")
                          <tr>
                            <td>{{ $boncommand->id }}</td>
                            <td>{{ $boncommand->ref_bc }}</td>
                            <td>{{ $boncommand->created_at->format('d/m/Y') }}</td>
                            <td>{{ $boncommand->prix_total }}Dh</td>
                            <td>{{ $boncommand->nom }} {{ $boncommand->prenom }}</td>
                            <td  style="min-width: 100px">
  
                                <form method="POST" action="{{ url('/boncommands' . '/' . $boncommand->id) }}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
                                  <div class="btn-group btn-group-sm" role="group">
                                      <a href="{{ url('/boncommands/' . $boncommand->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                          <span class="fa fa-edit" aria-hidden="true"></span>
                                      </a>
       
                                      <a href="{{ url('/boncommands/' . $boncommand->id . '/createbl') }}"  class="btn btn-success" title="Creer Bon de livraison"><span class="fa fa-clone" aria-hidden="true"></span></a>

                                      <button type="button" class="btn btn-active" title="Liste Bons livraison" v-on:click="getBls({{$boncommand->id}})" ><span class="fa fa-eye" aria-hidden="true"></span></button>

                                      <a href="{{ url('/boncommands/' . $boncommand->id . '/print') }}" target="_blank" class="btn btn-warning" title="Imprimer">
                                          <span class="fa fa-print" aria-hidden="true"></span>
                                      </a>
                                      <a href="{{ url('/echeances/' . $boncommand->id . '/index') }}"  class="btn btn-success" title="Echeance Paiement">
                                          <span class="fa fa-tags" aria-hidden="true"></span>
                                      </a>
  
                                  </div>
                                </form>
  
                            </td>
                          </tr>
                          @endif
                          @endforeach
  
                      </tbody>
                    
                    </table>
                      @endif
                    </div>
  
                </div>
      
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->


        <div id="bllie" class="col-md-12">
            <div class="box box-primary" >
  
            <div class="box-header">
                <h3 class="box-title">Bons de livraison</h3>      
            </div>
    
              <div class="box-body box-profile">
  
                      <div class="box-body table-responsive no-padding">
                              <table class="table table-hover">
                              <tr>
                                      <th>Reference bon livraison</th>
                                      <th>Type facture</th>
                                      <th>Date</th>
                                      <th>Montant HT</th>
                                      <th></th>
                              </tr>
                              <tr v-for="bl in bls">
                                  <td>@{{bl.ref_bl}}</td>
                                  <td>@{{bl.type}}</td>
                                  <td>@{{bl.created_at}}</td>
                                  <td>@{{bl.prix_total}} Dh</td>
  
                                  <td>
                                      <a v-bind:href="'/boncommandbls/'+bl.id+'/show'"  class="btn btn-success" title="Affichier Bon de livraison">
                                                <span class="fa fa-tv" aria-hidden="true"></span>
                                      </a>
      
                                  </td>
                                  
                              </tr>
  
                              </table>
                          </div>
  
              </div>
              <!-- /.box-body -->
        </div>
            
      </div>
      
      

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>


@endsection

@section('js')


   <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

  <script>
    $(function() {
      $('#tableBoncommands').DataTable();
      $('#tableBonlivraisons').DataTable();

    })
  </script>



<script>

    $('#bllie').hide();

    $('document').ready(function() {

          $('#tabBoncommands').on('click', function(event){
            $('#bllie').hide();
          });

          $('#tabBonlivraisons').on('click', function(event){
            $('#bllie').show();
          });

    })


    var app = new Vue({
      el: '#fournisseur_show',

      data: {

        items: [],
        bls: [],
        //showed:false,

       },

      mounted: function mounted() {
        //this.getVueItems();
        //this.prix_total = this.calcPrixTotal();
      },
      methods: {
     

        getBls:  function getBls(bc_id){
          var _this = this;
          axios.get('/boncommandbls/'+bc_id+'/perbc').then(function (response) {
                _this.bls = response.data;
          });

        },

      }

    });

    </script>


@endsection
