
              <div class="box-body">

              <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email', optional($user)->email) }}" required>
                  </div>

                  <div class="form-group">
                    <label for="name">Nom utilisateur</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="name" value="{{ old('name', optional($user)->name) }}" required>
                  </div>

                </div>
                <div class="col-md-6">

                    <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="text" class="form-control" id="password" name="password" placeholder="Mot de passe" value="" required>
                    </div>

                </div>

                <div class="col-md-12">
                  <hr>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="nom" value="{{ old('nom', optional($user)->nom) }}" required>
                  </div>

                  <div class="form-group">
                    <label for="tel">telephone</label>
                    <input type="text" class="form-control" id="tel" name="tel" placeholder="telephone" value="{{ old('tel', optional($user)->tel) }}">
                  </div>

                </div>
                <div class="col-md-6">

                    <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="prenom" value="{{ old('prenom', optional($user)->prenom) }}">
                    </div>

                      <div class="form-group">
                        <label for="etat">Type</label>
                        @if (!empty($user))
                          
                          <select class="form-control" name="type" id="type">
                          @foreach (['Admin','Commercial', 'Teleoperateur','Assistant'] as $object)
                            @if ($object === $user->type)
                            <option value="{{$object}}" selected="selected">{{$object}}</option>
                            @else
                            <option value="{{$object}}">{{$object}} </option>
                            @endif
                          @endforeach
                          </select>
                        @else

                        <select class="form-control" name="type" id="type">
                        @foreach (['Admin','Commercial', 'Teleoperateur','Assistant'] as $object)
                            <option value="{{$object}}">{{$object}} </option>
                          @endforeach
                          </select>

                      @endif

                    </div>



                </div>
                <div class="col-md-12">
                  <hr>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="remise_min">Remise min</label>
                    <input type="text" class="form-control" id="remise_min" name="remise_min" placeholder="Remise min" value="{{ old('remise_min', optional($user)->remise_min) }}">
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="remise_max">Remise max</label>
                    <input type="text" class="form-control" id="remise_max" name="remise_max" placeholder="Remise Max" value="{{ old('remise_max', optional($user)->remise_max) }}">
                    </div>

                </div>
            
              </div>

          