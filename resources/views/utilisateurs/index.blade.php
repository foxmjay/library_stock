@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="comptashow">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>gestion des users</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List des users</h3>
              <a href="{{ url('/utilisateurs/create') }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Ajouter
              </a>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($users) == 0)
              @else


                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nom</th>
                      <th>Email</th>
                      <th>Remise min</th>
                      <th>Remise max</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                          <td>{{ $user->id }}</td>
                          <td>{{ $user->nom }} {{ $user->prenom }}</td>
                          <td>{{ $user->email }}</td>
                          <td>{{ $user->remise_min }}</td>
                          <td>{{ $user->remise_max }}</td>
                          <td>

                              <form method="POST" action="{{ url('/utilisateurs' . '/' . $user->id) }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/utilisateurs/' . $user->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                                    <!--<a href="" class="btn btn-primary" title="Edit Client">
                                        <span class="icon-pencil" aria-hidden="true"></span>
                                    </a> -->
                                    <button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Supprimer Client?&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button>
                                    <button type="button" class="btn btn-active" title="Compta" v-on:click="getStat({{$user->id}})" >
                                        <span class="fa fa-eye" aria-hidden="true"></span>
                                    </button>
                                </div>
                              </form>

                          </td>
                        </tr>
                        @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                    <th>ID</th>
                      <th>Nom</th>
                      <th>Email</th>
                      <th>Remise min</th>
                      <th>Remise max</th>
                        <th></th>
                    </tr>
                    </tfoot>
                  </table>
              @endif
            </div>
            <div class="box box-primary" v-show="showed">
                <div class="box-body box-profile">

                        <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                <tr>
                                        <th>Mois</th>
                                        <th>Gain</th>


                                </tr>
                                <tr v-for="item in items">
                                    <td>@{{item.mois}}</td>
                                    <td>@{{item.prix}}</td>



                                </tr>

                                </table>
                            </div>

                </div>
                <!-- /.box-body -->
              </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
  <script>
    $(function() {
      $('#example1').DataTable()

    })
  </script>


<script>


    var app = new Vue({
      el: '#comptashow',

      data: {

        items: [],
        showed:false,

       },

      mounted: function mounted() {
        //this.getVueItems();
        //this.prix_total = this.calcPrixTotal();
      },
      methods: {
        getStat: function (id) {

                this.showed = true;
                var _this = this;

                axios.get('/compta/'+id).then(function (response) {
                    _this.items = response.data;
                });

    }

      }

    });

    </script>
@endsection
