@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

   .description-block h5 {
     font-size: 26px !important;
   }
</style>

@endsection

@section('content')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="vue-wrapper">
    <!-- Content Header (Page header) -->

    @include ('pointventes.modal_remise')
    @include ('pointventes.modal_espece')
    @include ('pointventes.modal_clients')
    @include ('pointventes.modal_cheque')

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box">
                    <!-- <div class="box-header">
                      <h3 class="box-title">Stock</h3>
                    </div>  -->
                    <div  class="box-body">


                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-3 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-percentage text-green"><i class="fa fa-money"></i></span>
                                <h5 class="description-header"  >@{{ remise}} %</h5>
                                <span class="description-text">Remise(%)</span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-percentage text-yellow"><i class="fa fa-bank"></i></span>
                                <h5 class="description-header">@{{recu}} DH</h5>
                                <span class="description-text">Recu</span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-percentage text-green"><i class="fa fa-money"></i></span>
                                <h5 class="description-header" :style="{color: rendu_calc >= 0 ? 'green' : 'red'}" >@{{rendu_calc}} DH</h5>
                                <span class="description-text">Rendu</span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-percentage text-red"><i class="fa fa-bank"></i></span>
                                <h5 class="description-header">@{{ totalPrice }} DH</h5>
                                <span class="description-text">Total</span>
                              </div>
                              <!-- /.description-block -->
                            </div>

                          </div>
                          <!-- /.row -->
                        </div>



                        <table class="table" style="min-height: 100px;">
                            <tr>
                                <td>
                                  <!-- <input style="width: 100%;" type="text"  v-model="barcode" placeholder="Code barre"  v-on:keyup.enter="getStock()" @change="getStock()"> -->
                                  <input id="debounceSearch" style="width: 100%;" type="text" @input="debounceSearch" placeholder="Code barre">
                                </td>

                                <td>
                                  <div class="form-group has-error">
                                    <input style="width: 100%;" type="text"  v-model="selectedProduit.nom" placeholder="Article">
                                    <span class="help-block" v-if="selectedProduit.quantity"> Prix : @{{ selectedProduit.prix_vente }} DH </span>
                                  </div>

                                </td>
                                <td>
                                    <input style="width: 100%;" type="text"  v-model="quantity" placeholder="Quantite" >
                                    <span class="help-block" > Quantite en stock: @{{ selectedProduit.quantity }} </span>

                                </td>
                                <td>

                                    <button  type="button" class="btn btn-success" title="Ajouter" @click.prevent="addProduit()" :disabled="!selectedProduit.quantity || valide">
                                        <span class="fa fa-plus-circle" aria-hidden="true"></span>
                                    </button>

                                </td>
                            </tr>
                          </table>


                    </div>


                    <div  class="box-body table-responsive no-padding" style="min-height: 300px;">
                        <table  id="stockList" class="table table-bordered ">
                        <thead>
                        <tr>
                            <th style="width: 2%;">N</th>
                            <th>Code Barre</th>
                            <th>Ref interne</th>
                            <th>Designation</th>
                            <th>Quantite</th>
                            <th>P.U.V</th>
                            <th>Remise</th>
                            <th>TVA</th>
                            <th>Total</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody  >

                          <tr v-for="prod,index in listProduits">
                            <td>@{{ index+1 }}</td>
                            <td>@{{prod.barcode}}</td>
                            <td>@{{prod.ref_interne}}</td>
                            <td>@{{prod.nom}}</td>
                            <td v-if="prod.quantity_stock">@{{prod.quantity}} (@{{prod.quantity_stock}})</td>
                            <td v-else>@{{prod.quantity}}</td>
                            <td>@{{prod.price}}</td>
                            <td>@{{remise}}%</td>
                            <td></td>
                            <td>@{{ prod.total - ( prod.total * remise / 100 ) }}</td>
                            <td >
                              <button v-if="!valide"  type="button" class="btn-xs btn-danger" title="Supprimer" @click.prevent="removeProduit(index)" >
                                <span class="fa fa-minus-circle" aria-hidden="true"></span>
                            </button>
                            </td>
                          </tr>


                      </tbody>

                        </table>

                    </div>

                    <div class="box-footer">
                        <div class="col-xs-6">
                            <h4>Client : @{{ client.raison_sociale }}</h4>
                        </div>
                        <div class="col-xs-6" v-if="pv">
                            <h4 class="pull-right">Ref : @{{ pv.ref}}</h4>
                        </div>



                    </div>

                </div>

                </div>


        <div class="col-xs-12">

            <div class="box">

            <div  class="box-body table-responsive">

                <button type="button" class="btn btn-app bg-money" data-toggle="modal" data-target="#modalClients" :disabled="valide">
                  <i class="fa fa-user"></i>CLIENT
                </button>
                <button type="button" class="btn btn-app bg-money" data-toggle="modal" data-target="#modalEspece" :disabled="valide">
                    <i class="fa fa-save"></i>ESPECE
                </button>
                <button type="button" class="btn btn-app bg-default" data-toggle="modal" data-target="#modalCheque" :disabled="valide">
                    <i class="fa fa-newspaper-o"></i>CHEQUE
                </button>

                <button type="button" class="btn btn-app bg-money" data-toggle="modal" data-target="#modalRemise" :disabled="valide">
                  <i class="fa fa-money"></i>REMISE
                </button>

                <button type="button" class="btn btn-app bg-default">
                  <i class="fa fa-book"></i>EN COMPTE
                </button>

                <button type="button" class="btn btn-app bg-default" @click.prevent="clearAll()" :disabled="valide">
                 <i class="fa fa-trash"></i>VIDER
                </button>
                <button type="button" class="btn btn-app bg-default">
                  <i class="fa fa-briefcase"></i>CAISSE
                </button>
                <button type="button" class="btn btn-app bg-default">
                 <i class="fa fa-calendar"></i>RAPPEL
                </button>

                <button type="button" class="btn btn-app bg-default" @click.prevent="fermer()">
                    <i class="fa fa-times-circle"></i>FERMER
                </button>



                <!-- <button type="button" class="btn btn-app bg-default pull-right">
                  <i class="fa fa-star"></i>Button
                 </button>

                 <button type="button" class="btn btn-app bg-default pull-right">
                  <i class="fa fa-star"></i>Button
                 </button> -->


                 <a :href="'/pointventes/printpvticket/'+pv.id" target="_blank" class="btn btn-app bg-default pull-right"  :disabled="!valide">
                  <i class="fa fa-print"></i>Imprimer
                 </a>

                <button v-if="pv && !edit" type="button" class="btn btn-app bg-default pull-right" @click="edit=true ; valide=false" :disabled="!valide">
                    <i class="fa fa-edit"></i>Modifier
                </button>

                <button v-if="edit" type="button" class="btn btn-app bg-default pull-right" @click="updatePV()" :disabled="valide">
                  <i class="fa fa-check"></i>Valider
              </button>


          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="{{ URL::asset('assets/admin/dist/js/vue.js')}}"></script>
<script src="{{ URL::asset('assets/admin/dist/js/axios.js')}}"></script>
<script src="{{ URL::asset('assets/admin/dist/js/vue-numeral-filter.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>

var pv_id = {!! json_encode($pv_id) !!};


var app = new Vue({
  el: '#vue-wrapper',

  data: {
    listProduits: [],
    selectedProduit: [],
    remise : 0,
    barcode : '',
    quantity:1,
    debounce: null,
    remise : 0,
    recu : 0,
    rendu : 0,
    keyword_client : '',
    clientList: [],
    client:{id:null},
    type_paiement:'',
    image_cheque : '',
    valide: true,
    pv : '',
    edit : false,
   },

   computed: {
    // a computed getter
    totalPrice: function () {
      var price=0;
      this.listProduits.forEach(function(element,index){
        price +=parseInt(element.total);
      });

      return price -  (price * this.remise / 100 );

    },

    rendu_calc: function () {

      return this.recu - this.totalPrice ;

    }
  },

  mounted: function mounted() {
    this.getPVData();
  },

  methods: {

    handleFileUpload: function handleFileUpload(){
        var _this = this;
        var image_cheque = this.$refs.image_cheque.files[0];
        var reader = new FileReader();
        reader.onloadend = function(e) {
            _this.image_cheque=e.target.result;
        };
        reader.readAsDataURL(image_cheque);

    },

    debounceSearch(event) {
      clearTimeout(this.debounce)
        this.debounce = setTimeout(() => {
        this.barcode = event.target.value;
        this.getStock();
      }, 600)
    },

    debounceSearchClient(event) {
      clearTimeout(this.debounce)
        this.debounce = setTimeout(() => {
        this.keyword_client = event.target.value;
        this.getClients();
      }, 600)
    },

    clearAll: function (){

      this.listProduits=[];
      this.selectedProduit=[];
      this.remise = 0;
      this.remise = 0;
      this.recu=0;
      this.rendu=0;

    },

    selectClient: function(client){
        this.client = client;
    },


    getStock: function getStock(){


       _this = this;
       var data = JSON.stringify( {'barcode': this.barcode});
       axios.post('/pointventes/getstock',{data: data}).then(function (response) {
          //_this.stocks = response.data;
          if("Error" in response.data){
              //console.log("Error")
              _this.selectedProduit=JSON.parse('{"nom":"Produit non trouve","quantity":0}');
          }else {

            _this.selectedProduit=response.data;
            //console.log(_this.selectedProduit);

          }
       });

    },

    getClients: function(){

        var _this = this;
        var data = JSON.stringify( {'keyword': this.keyword_client});
        axios.post('/clients/get_clients',{data: data}).then(function (response) {

            if("Error" in response.data){
                //console.log("Error")
                //_this.selectedProduit=JSON.parse('{"nom":"Produit non trouve","quantity":0}');
                _this.clientList=[];
            }else {

                _this.clientList=response.data;
                //console.log(_this.selectedProduit);

            }
        });

    },

    getPVData: function(){
        var _this =this
        axios.get('/pointventes/getpv/'+pv_id).then(function (response) {
                  //console.log(response.data);
                  if("error" in response.data){
                      alert(response.data['error']);
                  }else {

                      _this.pv=response.data['pv'];
                      _this.recu=response.data['pv'].recu;
                      _this.rendu=response.data['pv'].rendu;
                      _this.remise=response.data['pv'].remise;
                      _this.type_paiement=response.data['pv'].type_paiement;
                      _this.listProduits=response.data['listProduits'];

                      if(response.data['client'])
                        _this.client=response.data['client'];
                  }
          });
    },

    updatePV: function(){

          //enableSpinner();

          var _this = this;
          var data = JSON.stringify( {
              'pv_id': this.pv.id,
              'client_id':this.client.id,
              'prix_total':this.totalPrice,
              'remise' : this.remise,
              'type_paiement' : this.type_paiement,
              'montant_recu' : this.recu,
              'montant_rendu' : this.rendu_calc,
              'listProduits': this.listProduits,
              'image_cheque': this.image_cheque,
          });
          axios.post('/pointventes/update_pv',{data: data}).then(function (response) {
                  //console.log(response.data);
                  if("error" in response.data){
                      alert(response.data['error']);
                  }else {
                      _this.valide=true;
                      _this.edit=false;
                      //_this.pv=response.data['pv_new'];
                  }
          });

          //disableSpinner();

      },




    addProduit: function addProduit(){
    var _this = this;
     var exists = false;
     var existsIndex=null
     this.listProduits.forEach(function(element,index){
        if(element.produit_id == _this.selectedProduit.id){
          existsIndex=index;
          exists=true;
          return
        }
     });

     var prix_total = this.selectedProduit.prix_vente * this.quantity ;

     if(exists == true){

      this.listProduits[existsIndex].total+=parseFloat(prix_total);
      this.listProduits[existsIndex].quantity+=parseInt(this.quantity);
      this.listProduits[existsIndex].quantity_stock=parseInt(this.selectedProduit.quantity);

     }else{

        produit = {
            "produit_id": this.selectedProduit.id,
            "barcode":this.selectedProduit.barcode,
            "nom" : this.selectedProduit.nom,
            "ref_interne" : this.selectedProduit.ref_interne,
            "quantity": parseInt(this.quantity),
            "price" : this.selectedProduit.prix_vente,
            "total": parseFloat( prix_total ),
            "quantity_stock": this.selectedProduit.quantity
            };
        this.listProduits.push(produit);
     }

     this.selectedProduit=[];
     this.quantity=1;
     this.barcode='';
     $('#debounceSearch').val('');

    },

    removeProduit: function removeProduit(index){
      this.listProduits.splice(index,1);
    },

    fermer: function(){
      window.location.href ="/pointventes/"
    },

  }
});


</script>

@endsection
