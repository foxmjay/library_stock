


<div class="modal fade" id="modalRemise" tabindex="-1" role="dialog" aria-labelledby="modalRemiseLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">

           <div class="form-group ">
             <input type="text" class="form-control" v-model="remise" placeholder="Remise" >
           </div>

          <div class="box-body table-responsive no-padding">
              <button type="button" class="btn btn-success pull-right" title="sélectionner" data-dismiss="modal" > Valider </button>
          </div>
        </div>

      </div>
    </div>
  </div>
