


<div class="modal fade" id="modalClients" tabindex="-1" role="dialog" aria-labelledby="modalClientsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">

           <div class="form-group ">
             <input id="debounceSearchClient" style="width: 100%;" type="text" @input="debounceSearchClient" placeholder="Client">
           </div>


          <div class="box-body table-responsive no-padding">

          <table  id="stockList" class="table table-bordered ">
                <thead>
                <tr>
                    <th style="width: 2%;">N</th>
                    <th>Raison sociale</th>
                    <th></th>
                    <th></th>
                    <th></th>

                </tr>
                </thead>
                <tbody  >

                    <tr v-for="client,index in clientList">
                    <td>@{{ index+1 }}</td>
                    <td>@{{client.raison_sociale}}</td>
                    <td>-</td>
                    <td>-</td>
                    <td>
                        <button  type="button" class="btn btn-success" title="Selectioner" data-dismiss="modal" @click.prevent="selectClient(client)">
                        <span class="fa fa-check" aria-hidden="true"></span>
                    </button>
                    </td>
                    </tr>


                </tbody>

            </table>


              <button type="button" class="btn btn-default pull-left" title="Annuler" data-dismiss="modal"> Annuler </button>
          </div>
        </div>

      </div>
    </div>
  </div>
