


<div class="modal fade" id="modalCheque" tabindex="-1" role="dialog" aria-labelledby="modalChequeLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">

          <div class="form-group ">
           <label class="control-label">Montant du cheque</label>
            <input type="text" class="form-control" v-model="recu" placeholder="Montant" >
        </div>

        <div class="form-group ">
            <label class="control-label">Image Cheque</label>
            <input type="file" class="form-control" ref="image_cheque" id="image_cheque" name="image_cheque" placeholder="Image cheque" v-on:change="handleFileUpload()" accept="image/*">
        </div>

          <div class="box-body table-responsive no-padding">
              <button type="button" class="btn btn-success pull-right" title="sélectionner" data-dismiss="modal" @click="type_paiement='Cheque'"> Valider </button>
          </div>
        </div>

      </div>
    </div>
  </div>
