@extends('layouts.admin')





@section('content')
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Encaissements/Déceiassements

        </h1>

    </section>

    <section>
        <br>
        @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
        @endif
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">



                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ url('/depenses/create') }}" class="btn btn-success btn-sm" title="Add New Ville">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter un Encaissements/Déceiassements
                        </a>


                        <hr>
                        <div class="box-body">

                            <div class="row">


                                <form method="POST" action="{{ url('/depenses/search') }}" accept-charset="UTF-8"
                                    style="display:inline">
                                    {{ csrf_field() }}
                                    <div class="box-tools ">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <div class="input-group margin">
                                                <input class="form-control" name="search" type="text" id="search"
                                                    value="">
                                                <span class="input-group-btn">
                                                    <button type="submit"
                                                        class="btn btn-primary btn-flat">Chercher</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>


                                    </div>


                                </form>

                            </div>

                        </div>

                        @if(count($depenses) >0 )

                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                                <div class="row">
                                    <div class="col-sm-6">
                                        @if(count($gain) >0 )
                                        <h3 align="center"><b>Décaissements</b></h3>
                                        @endif
                                        <table class="table table-bordered " role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th>#</th>
                                                    <th>Nom Prenom</th>
                                                    <th>Date</th>
                                                    <th>Montant HT</th>
                                                    <th>motif</th>
                                                    <th class="col">Actions</th>


                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($depenses as $item)

                                                @if ($item->type == 'Depense')
                                                <tr class="bg-danger">
                                                    @else
                                                <tr class="bg-success">
                                                    @endif


                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->nom }} {{ $item->prenom }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item->date)) }}</td>
                                                    <td>{{ $item->montant }} DH</td>
                                                    <td>{{ !empty($item->motif) ? $item->motif : $item->autremotif }}
                                                    </td>
                                                    <td>
                                                        <!--<a href="{{ url('/depenses/' . $item->id) }}" title="View Depense"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>-->
                                                        {{-- @if (date("Y-m-d") == $item->created_at->format("Y-m-d") ) --}}
                                                        <a href="{{ url('/depenses/' . $item->id . '/edit') }}"
                                                            title="Modifier Depense"><button
                                                                class="btn btn-primary btn-sm"><i
                                                                    class="fa fa-pencil-square-o"
                                                                    aria-hidden="true"></i> Modifier</button></a>
                                                        {{-- @endif --}}


                                                        {{-- <form method="POST" action="{{ url('/depenses' . '/' . $item->id) }}"
                                                        accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <!-- !!! CASCADE DELETE ISSUE WITH STOCK !!! -->
                                                        <!-- <button type="submit" class="btn btn-danger btn-sm" title="Supprimer Depense" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button> -->
                                                        </form> --}}
                                                    </td>
                                                </tr>
                                                @endforeach




                                            </tbody>

                                        </table>
                                        {{ $depenses->links() }}
                                    </div>
                                    @endif
                                    @if(count($gain) >0 )
                                    <div class="col-sm-6">
                                        <h3 align="center"><b>Encaissements</b></h3>
                                        <table class="table table-bordered " role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th>#</th>
                                                    <th>Nom Prenom</th>
                                                    <th>Date</th>
                                                    <th>Montant HT</th>
                                                    <th>Jutifiée</th>
                                                    <th>RefFacture</th>
                                                    <th>Image</th>
                                                    <th>Actions</th>


                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($gain as $item)

                                                <tr class="bg-success">
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->nom }} {{ $item->prenom }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($item->date)) }}</td>
                                                    <td>{{ $item->montant }} DH</td>
                                                    <td>{{ $item->justifie }}</td>
                                                    <td>{{ $item->factureref }}</td>
                                                    <td><a href="/storage/{{ $item->imagesfacture }}"
                                                            target="_blank"><img height="50px"
                                                                src="/storage/{{ $item->imagesfacture }}"
                                                                alt="image" /></a></td>
                                                    <td>
                                                        <!--<a href="{{ url('/depenses/' . $item->id) }}" title="View Depense"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>-->
                                                        {{-- @if (date("Y-m-d") == $item->created_at->format("Y-m-d") ) --}}
                                                        <a href="{{ url('/depenses/' . $item->id . '/edit') }}"
                                                            title="Modifier Depense"><button
                                                                class="btn btn-primary btn-sm"><i
                                                                    class="fa fa-pencil-square-o"
                                                                    aria-hidden="true"></i> Modifier</button></a>
                                                        {{-- @endif --}}


                                                        {{-- <form method="POST" action="{{ url('/depenses' . '/' . $item->id) }}"
                                                        accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <!-- !!! CASCADE DELETE ISSUE WITH STOCK !!! -->
                                                        <!-- <button type="submit" class="btn btn-danger btn-sm" title="Supprimer Depense" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button> -->
                                                        </form> --}}
                                                    </td>
                                                </tr>



                                                @endforeach




                                            </tbody>

                                        </table>
                                        {{ $gain->links() }}
                                    </div>
                                </div>
                                @endif

                            </div>
                        </div>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif


                    </div>

                </div>


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
</div>

@endsection
