<div class="box-body" id="depenseview">

    <div class="col-md-12">
        <div class="form-group ">
            <label for="type">Type</label>


            @if (!empty($depense))
            <select class="form-control" name="type" id="type">
                @if ('Gain' === $depense->type)
                <option value="Gain" selected>Encaissement </option>
                <option value="Depense">Decaissement </option>
                @else
                <option value="Gain">Encaissement </option>
                <option value="Depense" selected>Decaissement </option>
                @endif

            </select>
            @else
            <select class="form-control" name="type" id="type" v-on:change="depensetype=!depensetype;">

                <option value="Depense">Decaissement </option>
                <option value="Gain">Encaissement </option>




            </select>
            @endif

        </div>

    </div>

    <div class="col-md-6">


        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom"
                value="{{ old('nom', optional($depense)->nom) }}" required>
            {!! $errors->first('nom', '<p class="text-danger ">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="prenom">Prenom</label>
            <input required type="text" class="form-control" id="prenom" name="prenom" placeholder="prenom"
                value="{{ old('prenom', optional($depense)->prenom) }}">
            {!! $errors->first('prenom', '<p class="text-danger ">:message</p>') !!}

        </div>

        <div class="form-group ">
            <label for="montant">Montant HT</label>
            <input required type="number" step=any class="form-control" id="montant" name="montant"
                placeholder="montant" value="{{ old('montant', optional($depense)->montant) }}">
            {!! $errors->first('montant', '<p class="text-danger ">:message</p>') !!}

        </div>

        <div class="form-group ">
            <label for="date">Date</label>
            <input required type="date" step=any class="form-control" id="date" name="date" placeholder="date"
                value="{{ old('date', optional($depense)->date) }}">
            {!! $errors->first('date', '<p class="text-danger ">:message</p>') !!}

        </div>


        <div class="form-group " v-if="!depensetype">
            <label for="motif">Motif</label>
            <select class="form-control" name="motif" id="motif" v-model="typemotif" required>
                <option value="carburant">Carburant </option>
                <option value="repas">Repas </option>
                <option value="achat">Achat </option>
                <option value="auto">Auto </option>
                <option value="hebergement">Hebergement </option>
                <option value="remboursement">Remboursement </option>
                <option value="transportscdm">Transport SCDM </option>
                <option value="autre">Autre... </option>
            </select>
        </div>
        <div class="form-group " v-if="typemotif == 'autre'">
            <label for="autremotif">Autre</label>
            <textarea required class="form-control" id="autremotif" name="autremotif" placeholder="Autre"
                value="{{ old('autremotif', optional($depense)->autremotif) }}"></textarea>
            {!! $errors->first('motif', '<p class="text-danger ">:message</p>') !!}

        </div>



    </div>

    <div class="col-md-6" v-if="depensetype">
        <div class="form-group ">
            <label for="justifie">Justifiée</label>
            <select required class="form-control" name="justifie" id="justifie" v-on:change="justif=!justif;">
                <option value="oui">oui </option>

                <option value="non" selected>non </option>
            </select>
        </div>
        <div class="form-group" v-if="justif">
            <label for="factureref">Ref.Facture</label>
            <input required type="text" class="form-control" id="factureref" name="factureref"
                value="{{ old('factureref', optional($depense)->factureref) }}">
            {!! $errors->first('factureref', '<p class="text-danger ">:message</p>') !!}

        </div>

        <div class="form-group" v-if="justif">
            <label for="imagesfacture">Images</label>
            <input required type="file" class="form-control" id="imagesfacture" name="imagesfacture"
                value="{{ old('imagesfacture', optional($depense)->imagesfacture) }}">
            {!! $errors->first('imagesfacture', '<p class="text-danger ">:message</p>') !!}

        </div>



    </div>


</div>
@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>
    var app = new Vue({
          el: '#depenseview',

          data: {
          depensetype : false,
          justif : false,
          autre : false,
          typemotif : '',

           },
        });

</script>
@endsection
