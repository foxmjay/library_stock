@extends('layouts.admin')

@section('css')
<!-- <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Depenses
        <small>gestion des depenses</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Depenses</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Creation nouveau depense</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" method="POST" action="{{ url('/depenses') }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample" enctype="multipart/form-data">
             {{ csrf_field() }}

                @include ('depenses.form', ['depense' => null,])

              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Ajouter</button>
              </div>

            </form>

          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('scripts')

<!-- <script src="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script> -->



<script>
  /*$('document').ready(function() {

      $('#echeance_paiement').datepicker({
      language: 'fr',
      autoclose: true,
      todayHighlight: true,
      format: "dd-mm-yyyy",
    })


  });*/
</script>

@endsection
