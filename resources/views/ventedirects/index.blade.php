
@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

<div class="content-wrapper">
<section class="content-header">
    <h1>
      Vente Direct

    </h1>

</section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

         
          <div class="box">
            <div class="box-header with-border">
              <a href="{{ url('/ventedirect/create') }}" class="btn btn-success btn-sm" title="Add New Ville">
                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter 
                </a>

              <hr>
              <div class="box-body">
                  <div >
                    <div class="row">
                       
                        <form  method="POST"  action="{{ url('/ventedirect/search') }}" accept-charset="UTF-8" style="display:inline">
                          {{ csrf_field() }}
                          <div class="col-sm-10">
                          <div class="form-group">
                            <input class="form-control"  name="search" type="text" id="search" value="" >
                         </div>
                          </div>
                          <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary">Chercher</button>
                          </div>
      
                        </form>

                      </div>
                   </div>
              </div>
              @if(count($vds) >0 )
              <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  
                  <div class="row">
                    <div class="col-sm-12">
                   
                      <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <!--<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1">#</th>-->
                            <th>ID </th>
                            <th>Ref </th>
                            <th>nom </th>
                            <th>objet </th>
                            <th>Montant</th>
                            
                            <th  class="col" >Actions</th>


                          </tr>
                        </thead>
                        <tbody>
                        
                          @foreach($vds as $item)
                              <tr role="row" class="odd">
                                  <!--<td class="sorting_1">{{ $loop->iteration }}</td> -->
                                  <td>{{ $item->id }}</td>
                                  <td>{{ $item->ref }}</td>
                                  <td>{{ $item->nom }}</td>
                                  <td>{{ $item->objet }}</td> 
                                  <td>{{ $item->prix_total }}</td>

                                  <td>

                                      <a href="{{ url('/ventedirect/' . $item->id . '/show') }}" target="_blank" class="btn btn-success" title="Imprimer Devis"><span class="fa fa-tv" aria-hidden="true"></span></a>
                                      <a href="{{ url('/ventedirect/' . $item->id . '/printbl') }}" target="_blank" class="btn btn-warning" title="Imprimer Devis"><span class="fa fa-print" aria-hidden="true"></span></a>
                                      
                                  </td>
                              </tr>
                          @endforeach

                        </tbody>

                      </table>

                    </div>
                  </div>
                
                </div>

              </div>

            @endif


              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

            </div>
          </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        
    </div>
    <!-- /.row -->
</section>
</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable({
        searching:false
      })

    })
  </script>
@endsection
