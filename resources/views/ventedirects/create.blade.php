@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="vue-wrapper">
    <!-- Content Header (Page header) -->



    <section class="content-header">
      <h1>
        Bon de livraison
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de livraison</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Details</h3>
                      </div> 
                      <div class="box-body">

                        <div class="col-md-6">
        
                          <div class="form-group">
                            <label for="client_id">nom</label>
                            <input type="text" class="form-control" id="nom" name="nom" v-model="nom" >
                          </div>
  
        
                            <div class="form-group">
                                <label for="prix_total">Adresse</label>
                                <input type="text" class="form-control" id="adresse" name="adresse"  v-model="adresse">
                            </div>
        
                            <div class="form-group">
                                <label for="objet">Objet</label>
                                <input type="text" class="form-control" id="objet" name="objet"  v-model="objet" >
                            </div>

                        </div>
                        <div class="col-md-6">
        
                            <div class="form-group">
                            <label for="ref">Reference</label>
                            <input type="text" class="form-control" id="ref" name="ref"  v-model="ref" readonly>
                            </div>
        
                            <div class="form-group">
                                <label for="delai_livraison">Prix total</label>
                                <input type="text" class="form-control" id="prix_total" name="prix_total" v-model="prix_total" readonly>
                            </div>
        
        
                           <div class="form-group">
                                <label for="garantie">Garantie (Mois)</label>
                                <input type="number" class="form-control" id="garantie" name="garantie" v-model="garantie">
                            </div>
        
        
                        </div>
        
        
                      </div>

    
                 </div>
    
          </div>
              <!-- /.col -->



              <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Stock</h3>
                    </div> 
                    <div  class="box-body">
        
                        <table class="table">
                            <tr>
                            
                                <td> 
                                    <label for="categorie">Type</label>
                                    <select name="type_search" id="type_search" v-model="type_search">
                                        @foreach (['Serialisable','Non Seralisable'] as $object)
                                        <option value="{{$object}}">{{$object}}</option>
                                        @endforeach
                                    </select>
                              </td>
                                <td> 
                                    <input type="text" name="keyword" id="keyword" v-model="keyword"  v-on:keyup.enter="getStock()" required>
        
                                    <button  type="button" class="btn btn-success" title="Chercher"@click.prevent="getStock()">
                                        <span class="fa fa-search" aria-hidden="true"></span>
                                    </button>
                                    
                                </td>
                            </tr>
                          </table>
                        
                    </div>
        
        
                    <div  class="box-body table-responsive no-padding">
                        <table  id="stockList" class="table ">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Produit</th>
                            <th>Marque</th>
                            <th>Ref interne</th>
                            <th>Numero serie</th>
                            <th>Fournisseur</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(stock,index) in stocks" >
                            <td><input  type="radio" name="selectedProduit" v-bind:id="stock.id" v-bind:value="stock.id" @change="selectProduit(stock)"></td>
                            <td>@{{stock.nom}}</td>
                            <td>@{{stock.marque}}</td>
                            <td>@{{stock.ref_interne}}</td>
                            <td>@{{stock.num_serie}}</td>                                  
                            <td>@{{stock.raison_sociale}}</td> 

                        </tr>
                      </tbody>
          
                        </table>
                        
                    </div>

                    <br>
                    <br>
                    <br>

                    <div  class="box-body table-responsive no-padding" v-if="selectedProduit != ''">
                        <table   class="table ">
                        <thead>
                        <tr>
                            <th>Produit</th>
                            <th>Numero de serie</th>
                            <th>Prix vente</th>
                            <th>Remise(%)</th>
                            <th>Prix</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr  >
                            <td>@{{selectedProduit.nom}}</td>
                            <td>@{{selectedProduit.num_serie}}</td>
                            <td>@{{selectedProduit.prix_vente}}</td>
                            <td><input  type="text" name="remise" id="remise" v-model="remise" @input="marge"></td>                                  
                            <td><input  type="text" name="prix" id="prix" v-model="prix" readonly></td> 
                            <td>
                                <button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="addStock()">
                                    <span class="fa fa-arrow-circle-down" aria-hidden="true"></span>
                                </button>
                            </td>

                        </tr>
                      </tbody>
          
                        </table>
                        
                    </div>
        
        
                </div>
        
                </div>


          
          <div class="col-xs-12" >

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits selectiones</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Prix vente</th>
                        <th>remise</th>
                        <th>Prix</th>
                        
                        <th></th>
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td>   
                    <td>@{{stock.prix_vente}}</td>
                    <td>@{{stock.remise}}%</td>
                    <td>@{{stock.prix}} DH</td>
   
                    <td>
                            <button type="button" class="btn btn-danger" title="sélectionner" data-dismiss="modal" v-on:click="removeStock(index)">
                                <span class="fa fa-trash" aria-hidden="true"></span>
                            </button>
                    </td>
                </tr>
  
                </table>

                <br>
                <br>
                <hr>
                  <h2 class="text-right" style="padding-right: 5%"><b>Prix total : </b>  @{{ prix_total | numeral('0.2') }} DH</h2>
                <hr> 

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->
       

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Confirmer
                </button> 
                <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                    <i class="fa fa-close"></i>Annuler
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>

var max = {!! json_encode($max ) !!}; 
var min = {!! json_encode( $min ) !!};
var ref = {!! json_encode( $ref ) !!};

var app = new Vue({
  el: '#vue-wrapper',

  data: {
    nom : '',
    adresse : '',
    objet : '',
    ref : ref,
    garantie : 0,
    type_search : 'Serialisable',
    keyword : '',
    selectedStocks: [],
    selectedProduit:'',
    stocks : [],
    prix_total : 0.0,
    remise : 0,
    prix : 0.0,

   },


  methods: {

    marge:function() {
      
      if (this.remise > max) {
        this.remise = max;
      }else if (this.remise < min) {
        this.remise = min;
      }

      this.prix = this.selectedProduit.prix_vente*(1-this.remise/100);

    },



    getStock: function getStock(){
      _this = this;
      var data = JSON.stringify( {'type_search' : this.type_search ,'keyword': this.keyword});
      axios.post('/ventedirect/getstock',{data: data}).then(function (response) {
         _this.stocks = response.data;
      });
    
    },


    selectProduit: function selectProduit(stock){
       this.selectedProduit = stock;
       this.prix = stock.prix_vente;
       setTimeout(function() {  $("#remise").focus(); }, 300);       
    },


    addStock: function addStock(){
     var _this = this;
     exists = false;
     this.selectedStocks.forEach(function(element){
        if(element.num_serie == _this.selectedProduit.num_serie)
          exists=true;

     });

     if(exists == true){
       alert('Produit deja ajoute!');
       return;
     }

     var tmp =  Object.assign({}, this.selectedProduit); // STUPID  JAVASCRIPT CLONNING VARIABLE !!!!
     tmp['remise'] = this.remise;
     tmp['prix'] = this.prix;

     this.selectedStocks.push(tmp);

     this.prix_total += this.prix;

     this.selectedProduit = ''
     this.remise = 0;
     this.prix = 0;
    },

    removeStock: function removeStock(index){

      this.prix_total -= this.selectedStocks[index].prix;
      this.selectedStocks.splice(index,1);
    },

    annuler: function annuler(){
      //history.back();
      window.location.href ="/ventedirect/"
    },

    confirmer: function confirmer(){

      enableSpinner();


      var _this = this;
      //var data = this.echeanceModel;

      if(this.selectedStocks.length <= 0 ){
        alert("Selectioner au moin un produit");
        return;
      }


      var data = JSON.stringify( { 'stocks' : this.selectedStocks ,'nom': this.nom, 'adresse':this.adresse, 'objet':this.objet, 'ref':this.ref, 'prix_total':this.prix_total, 'garantie':this.garantie });
      
      axios.post('/ventedirect/store',{data: data}).then(function (response){
        //console.log(response.data);
        if(response.data['result'] == "OK"){
          console.log(response.data['result']);
          window.location.href ="/ventedirect/"
        }

        if(response.data['result'] == "KO"){
          //console.log(response.data['result']);
          disableSpinner();

          var stock=response.data['stock']
          alert("Le produit "+stock.nom+" numero serie : "+stock.num_serie+" est non disponible dans le stock");
          //console.log(stock);
          _this.selectedStocks.forEach(function(element,index){
            //console.log(index);
            if(element.id == stock.id){
              _this.selectedStocks.splice(index,1);
            }

          });
         // _this.getStock(stock.vente_id);
        }

        
      }).catch(function (error){
        console.log(error)
      })

   }

  }
});


</script>

@endsection
