@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Rdvs
        <small>gestion des rdvs</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Rdvs</li>
      </ol>
    </section>

    <section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List des rdvs</h3>
              <a href="{{ url('/rdvs/create') }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Ajouter
              </a>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($rdvs) == 0)
              @else


                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Nom Prenom</th>
                      <th>Application</th>
                      <th>Telephone</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Etat</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($rdvs as $rdv)
                        @php
                          //$editDate= \Carbon\Carbon::parse($rdv->created_at);
                          //$now = \Carbon\Carbon::now();
                          //$diff = $editDate->diffInHours($now);
                          //if( $diff <= 24 
                        @endphp
                        <tr>
                          <td>{{ $rdv->nom." ".$rdv->prenom }}</td>
                          <td>{{ $rdv->application }}</td>
                          <td>{{ $rdv->tel }}</td>
                          <td>{{ $rdv->date }} {{ $rdv->heur }}</td>
                          <td>{{ $rdv->etat }}</td>
                          <td>{{ $rdv->action }}</td>
                          <td>

                              <form method="POST" action="{{ url('/rdvs' . '/' . $rdv->id) }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/rdvs/' . $rdv->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                       

                                    <a href="{{ url('/rdvs/' . $rdv->id) }}" class="btn btn-warning" title="Visualiser">
                                        <span class="fa fa-eye" aria-hidden="true"></span>
                                    </a>

                                    <!--<a href="" class="btn btn-primary" title="Edit Rdv">
                                        <span class="icon-pencil" aria-hidden="true"></span>
                                    </a> -->

                                    <!--<button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Supprimer Rdv?&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button> -->

                                    <!--<a href="{{ url('/contacts/' . $rdv->id) .'/rdv' }}" class="btn btn-warning" title="Contacts">
                                        <span class="fa fa-user" aria-hidden="true"></span>
                                    </a> -->

                                    <!--<a href="{{ url('/rdvs/' . $rdv->id)}}" title="Info's" type="button" class="btn btn-success">
                                        <span class="fa fa-align-center" aria-hidden="true"></span>
                                    </a> -->


                                </div>
                              </form>

                          </td>
                        </tr>
                        @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                    <th>Nom Prenom</th>
                      <th>Application</th>
                      <th>Telephone</th>
                      <th>Date</th>
                      <th>Etat</th>
                      <th></th>
                    </tr>
                    </tfoot>
                  </table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable()

    })
  </script>
@endsection
