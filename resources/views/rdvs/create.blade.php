@extends('layouts.admin')

@section('css')
<!-- <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Rdvs
        <small>gestion des rdvs</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Rdvs</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Creation nouveau rdv</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" method="POST" action="{{ url('/rdvs') }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample">
             {{ csrf_field() }}

                @include ('rdvs.form', ['rdv' => null,])

              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Ajouter</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script>
  $('document').ready(function() {

    $(".time-selector").datetimepicker({
      datepicker:false,
      format:'H:i',
      step:5
    });

  });
</script> 

@endsection
