
              <div class="box-body">

                <div class="col-md-6">

                  <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="nom" value="{{ old('nom', optional($rdv)->nom) }}" required>
                    {!! $errors->first('nom', '<p class="text-danger ">:message</p>') !!}
                  </div>



                  <div class="form-group">
                    <label for="societe">Nom societe</label>
                    <input type="text" class="form-control" id="societe" name="societe" placeholder="societe" value="{{ old('societe', optional($rdv)->societe) }}" required>
                    {!! $errors->first('nom', '<p class="text-danger ">:message</p>') !!}
                  </div>

                   <div class="form-group">
                    <label for="adresse">Adresse</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" placeholder="adresse" value="{{ old('adresse', optional($rdv)->adresse) }}">
                  </div>



                  <div class="form-group">
                    <label for="profession">Profession</label>
                    <input type="text" class="form-control" id="profession" name="profession" placeholder="profession" value="{{ old('profession', optional($rdv)->profession) }}">
                  </div>


                  <div class="form-group">
                    <label for="date">Date</label>
                    <input  class="form-control timepicker"  type="date" id="date" name="date" placeholder="date" value="{{old('date', optional($rdv)->date)}}">
                  </div>
                

                  <div class="form-group">
                    <label for="date_rappel">Date Rappel</label>
                    <input  class="form-control timepicker"  type="date" id="date_rappel" name="date_rappel" placeholder="date_rappel" value="{{old('date_rappel', optional($rdv)->date_rappel)}}">
                  </div>
                 
                 
                  <div class="form-group">
                      <label for="commercial">Commercial</label>
                      @if (!empty($rdv) == 1)
                        <select class="form-control" name="commercial" id="commercial">
                        <option value="">Aucun</option>
                        @foreach ($commercials as $object)
                          @if ($object->id === $rdv->commercial)
                          <option value="{{$object->id}}" selected="selected">{{$object->nom}} {{$object->prenom}}</option>
                          @else
                          <option value="{{$object->id}}">{{$object->nom}} {{$object->prenom}} </option>
                          @endif
                        @endforeach
                        </select>
                      @else
                        <select class="form-control" name="commercial" id="commercial">
                        <option value="">Aucun</option>
                        @foreach ($commercials as $object)
                          <option value="{{$object->id}}">{{$object->nom}} {{$object->prenom}}</option>
                        @endforeach
                        </select>
                      @endif
                    </div>


                 

                    <div class="form-group">
                        <label for="etat">Status</label>
                        @if (!empty($rdv) == 1)
                        <select class="form-control" name="etat" id="etat">
                        @foreach (['Excellent','Bien','Moyen','mediocre'] as $object)
                          @if ($object === $rdv->etat)
                          <option value="{{$object}}" selected="selected">{{$object}}</option>
                          @else
                          <option value="{{$object}}">{{$object}} </option>
                          @endif
                        @endforeach
                        </select>
                        @else
                        <select class="form-control" name="etat" id="etat">
                        @foreach (['Excellent','Bien','Moyen','mediocre'] as $object)
                          <option value="{{$object}}">{{$object}} </option>
                        @endforeach
                        </select>
                      @endif
                  </div>  


                </div>
                <div class="col-md-6">

                
                    <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="prenom" value="{{ old('prenom', optional($rdv)->prenom) }}">
                    </div>

                    <div class="form-group">
                      <label for="email">email</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="email" value="{{ old('email', optional($rdv)->email) }}">
                    </div>



                  <div class="form-group">
                      <label for="tel">Telephone</label>
                      <input type="text" class="form-control" id="tel" name="tel" placeholder="Telephone" value="{{ old('tel', optional($rdv)->tel) }}">
                    </div>


                    
                    <div class="form-group">
                        <label for="application">Application</label>
                        @if (!empty($rdv) == 1)
                        <select class="form-control" name="application" id="application">
                        @foreach (['OneTeeth','OneCar','OneDoc','Autre'] as $object)
                          @if ($object === $rdv->application)
                          <option value="{{$object}}" selected="selected">{{$object}}</option>
                          @else
                          <option value="{{$object}}">{{$object}} </option>
                          @endif
                        @endforeach
                        </select>
                        @else
                        <select class="form-control" name="application" id="application">
                        @foreach (['OneTeeth','OneCar','OneDoc','Autre'] as $object)
                          <option value="{{$object}}">{{$object}} </option>
                        @endforeach
                        </select>
                      @endif
                  </div>  


                    <div class="form-group">
                      <label>Heure</label>
                        <input type="text" id="heur" name="heur" class="time-selector form-control" placeholder="Heur" value="{{ old('heur', optional($rdv)->heur) }}">
                    </div>

                    <div class="form-group">
                        <label>Heure Rappel</label>
                        <input type="text" id="heur_rappel" name="heur_rappel" class="time-selector form-control" placeholder="heur_rappel" value="{{ old('heur_rappel', optional($rdv)->heur_rappel) }}">
                    </div>

                    

                  <div class="form-group">
                    <label for="note">Note</label>
                    <input  class="form-control"  type="text" id="note" name="note" placeholder="note" value="{{old('note', optional($rdv)->note)}}">
                  </div>
                       

                  <div class="form-group">
                        <label for="etat_appel">Etat Appel</label>
                        @if (!empty($rdv) == 1)
                        <select class="form-control" name="etat_appel" id="etat_appel">
                        @foreach (['Non Appele','Appele'] as $object)
                          @if ($object === $rdv->etat_appel)
                          <option value="{{$object}}" selected="selected">{{$object}}</option>
                          @else
                          <option value="{{$object}}">{{$object}} </option>
                          @endif
                        @endforeach
                        </select>
                        @else
                        <select class="form-control" name="etat_appel" id="etat_appel">
                        @foreach (['Non Appele','Appele'] as $object)
                          <option value="{{$object}}">{{$object}} </option>
                        @endforeach
                        </select>
                      @endif
                  </div>  




                </div>

            
              </div>

          