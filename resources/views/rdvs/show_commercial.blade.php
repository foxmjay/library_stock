@extends('layouts.admin')


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="devisref">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Rendez-vous
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">


          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <div class="col-md-6">

                    <strong><i class="fa fa-fw fa-user margin-r-5"></i> Nom : </strong> {{$rdv->nom}}
                    <hr>
      

                    <strong><i class="fa fa-fw fa-user margin-r-5"></i> Nom Societe : </strong> {{$rdv->societe}}
                    <hr>

                    <strong><i class="fa fa-fw fa-phone margin-r-5"></i> Telephone : </strong>  {{$rdv->tel}}
                    <hr>
      
                    <strong><i class="fa fa-fw fa-phone margin-r-5"></i> Professions : </strong>  {{$rdv->profession}}
      
                    <hr>    
                    <strong><i class="fa fa-fw fa-circle margin-r-5"></i> Etat : </strong> {{$rdv->etat}}

                    <hr>
                      @php $commercial = \App\UserProfile::find($rdv->commercial)  @endphp
                    <strong><i class="fa fa-fw fa-user margin-r-5"></i> Commercial : </strong> {{$commercial->nom." ".$commercial->prenom}}
      
      
                  </div>

                  <div class="col-md-6">

                      <strong><i class="fa fa-fw fa-user margin-r-5"></i> Prenom : </strong> {{$rdv->prenom}}
                      <hr>
        
                      <strong><i class="fa fa-fw fa-map-pin margin-r-5"></i> Adresse : </strong> {{$rdv->adresse}}
                      <hr>
        
                      <strong><i class="fa fa-fw fa-clone margin-r-5"></i> Application : </strong> {{$rdv->application}}
        
                      <hr>
                      <strong><i class="fa fa-fw fa-clock-o margin-r-5"></i> Date et Heur : </strong> {{$rdv->date." ".$rdv->heur}}
                      <hr>

                      <strong><i class="fa fa-fw fa-clock-o margin-r-5"></i> Date et Heur Rappel : </strong> {{$rdv->date_rappel." ".$rdv->heur_rappel}}
                      <hr>

                      <strong><i class="fa fa-fw  fa-sticky-note-o margin-r-5"></i> Note : </strong> {{$rdv->note}}
        
        
                    </div>
                    
                    <div class="col-md-12">
                        
                        <br>
                        <br>
                        <br>
                        <br>
                        <hr>

                        <!-- form start -->
                          <form role="form" method="POST" action="{{ url('/rdvs/' . $rdv->id) }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample">
                            <input name="_method" type="hidden" value="PUT">
                          {{ csrf_field() }}

                          <div class="form-group">
                              <label for="action">Action</label>
                              <select class="form-control" name="action" id="action" {{ $rdv->action != "Aucune" ? 'disabled' : '' }} >
                              @foreach (['Aucune','Accepter','Rejeter', 'Rediriger'] as $object)
                                @if ($object === $rdv->action)
                                <option value="{{$object}}" selected="selected">{{$object}}</option>
                                @else
                                <option value="{{$object}}">{{$object}} </option>
                                @endif
                              @endforeach
                              </select>
                          </div>
                          
                          
                          <div class="form-group" id="etat_retour_div">
                              <label for="etat_retour">Etat</label>
                              <select class="form-control" name="etat_retour" id="etat_retour">
                              @foreach (['Aucun','Bien','Mauvais'] as $object)
                                @if ($object === $rdv->etat_retour)
                                <option value="{{$object}}" selected="selected">{{$object}}</option>
                                @else
                                <option value="{{$object}}">{{$object}} </option>
                                @endif
                              @endforeach
                              </select>
                          </div> 
                          
                          <div class="form-group" id="feedback_div">
                              <label for="feedback">Note</label>
                              <textarea id="feedback" name="feedback" style="width: 100%">{{old('feedback', optional($rdv)->feedback)}}</textarea>
                          </div> 
                          
                          <div class="form-group" id="redirect_div">
                              <label for="commercial">Rediriger</label>
                              @if (!empty($rdv) == 1)
                                <select class="form-control" name="commercial" id="commercial">
                                @foreach ($commercials as $object)
                                  @if ($object->id === $rdv->commercial)
                                  @else
                                  <option value="{{$object->id}}">{{$object->nom}} </option>
                                  @endif
                                @endforeach
                                </select>
                              @else
                                <select class="form-control" name="commercial" id="commercial">
                                @foreach ($commercials as $object)
                                  <option value="{{$object->id}}">{{$object->nom}}</option>
                                @endforeach
                                </select>
                              @endif
                            </div>

                          
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Valider</button>
                            </div>

                          </form>
                    </div>
                 

            </div>
            <!-- /.box-body -->
           
          </div>
          <!-- /.box -->
        </div>
       
      </div>
      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>


@endsection

@section('js')

<script>

$('document').ready(function() {

  function setactiondiv(){

    if( $('#action').val() == "Aucune" ){
      $('#feedback_div').hide()
      $('#etat_retour_div').hide();
      $('#redirect_div').hide();
      $('#commercial').prop('disabled', true);
      $('#etat_retour').prop('disabled', true);
      $('#feedback').prop('disabled', true);
    }

    if( $('#action').val() == "Accepter" ){

       $('#feedback_div').hide()
       $('#etat_retour_div').show();
       $('#redirect_div').hide();
       $('#commercial').prop('disabled', true);
      $('#etat_retour').prop('disabled', false);
      $('#feedback').prop('disabled', true);

    }
    if( $('#action').val() == "Rejeter" ){
      $('#feedback_div').show()
      $('#etat_retour_div').hide();
      $('#redirect_div').hide();
      $('#commercial').prop('disabled', true);
      $('#etat_retour').prop('disabled', true);
      $('#feedback').prop('disabled', false);
    }
    if( $('#action').val() == "Rediriger" ){
      $('#feedback_div').hide()
      $('#etat_retour_div').hide();
      $('#redirect_div').show();
      $('#commercial').prop('disabled', false);
      $('#etat_retour').prop('disabled', true);
      $('#feedback').prop('disabled', true);
    }

  }

  $("#action").on('change',function() {
    
     setactiondiv();

  });

  setactiondiv();

});

</script>

@endsection
