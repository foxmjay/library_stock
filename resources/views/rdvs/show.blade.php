@extends('layouts.admin')


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="devisref">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Rendez-vous
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">


          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <div class="col-md-6">

                    <strong><i class="fa fa-fw fa-user margin-r-5"></i> Nom : </strong>{{$rdv->nom}}
                    <hr>

                    <strong><i class="fa fa-fw fa-user margin-r-5"></i> Nom Societe : </strong>{{$rdv->societe}}
                    <hr>
      
                    <strong><i class="fa fa-fw fa-phone margin-r-5"></i> Telephone : </strong> {{$rdv->tel}}
                    <hr>
      
                    <strong><i class="fa fa-fw fa-phone margin-r-5"></i> Professions :  </strong>{{$rdv->profession}}
      
                    <hr>    
                    <strong><i class="fa fa-fw fa-circle margin-r-5"></i> Status : </strong>{{$rdv->etat}}

                    <hr>
                    @php $commercial = \App\UserProfile::find($rdv->commercial)  @endphp
                    <strong><i class="fa fa-fw fa-user margin-r-5"></i> Commercial : </strong> {{$commercial->nom." ".$commercial->prenom}}
                    <hr>
      
      
                  </div>

                  <div class="col-md-6">

                      <strong><i class="fa fa-fw fa-user margin-r-5"></i> Prenom :  </strong>{{$rdv->prenom}}
                      <hr>
        
                      <strong><i class="fa fa-fw fa-map-pin margin-r-5"></i> Adresse : </strong> {{$rdv->adresse}}
                      <hr>
        
                      <strong><i class="fa fa-fw fa-clone margin-r-5"></i> Application : </strong>{{$rdv->application}}
        
                      <hr>
                      <strong><i class="fa fa-fw fa-clock-o margin-r-5"></i> Date et Heur : </strong> {{$rdv->date." ".$rdv->heur}}
                      <hr>
                      <strong><i class="fa fa-fw fa-clock-o margin-r-5"></i> Date et Heur Rappel : </strong> {{$rdv->date_rappel." ".$rdv->heur_rappel}}
                      <hr>
                      <strong><i class="fa fa-fw  fa-sticky-note-o margin-r-5"></i> Note : </strong>{{$rdv->note}}
                      <hr>
        
        
                    </div>
                    <div class="col-md-6">
                        

                        <br>
                        <br>

                        
                          <strong><i class="fa fa-fw  fa-sticky-note-o margin-r-5"></i> Etat :  </strong>{{$rdv->action}}
                          <hr>
                       
                          
                    </div>

                    <div class="col-md-6">
                        

                      <br>
                      <br>
                      @if( $rdv->action === "Rediriger")
                        <strong><i class="fa fa-fw  fa-sticky-note-o margin-r-5"></i> Commercial :  </strong>  {{$commercial->nom." ".$commercial->prenom}}
                        <hr>
                      @endif

                      @if( $rdv->action === "Accepter")
                      <strong><i class="fa fa-fw  fa-sticky-note-o margin-r-5"></i> Etat retour :  </strong>  {{$rdv->etat_retour}}
                      <hr>
                      @endif

                      @if( $rdv->action === "Rejeter")
                      <strong><i class="fa fa-fw  fa-sticky-note-o margin-r-5"></i> Note :  </strong> <textarea  style="width: 100%; height: 110px" readonly> {{$rdv->feedback}}</textarea>

                      <hr>
                      @endif
                        
                    </div>

                    <div class="col-md-12" >
                      <!-- form start -->
                      <form role="form" method="POST" action="{{ url('/rdvs/' . $rdv->id) }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample">
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}

                        <div class="form-group" id="redirect_div">
                            <label for="commercial">Rediriger</label>
                            @if (!empty($rdv) == 1)
                              <select class="form-control" name="commercial" id="commercial" >
                              @foreach ($commercials as $object)
                                @if ($object->id === $rdv->commercial)
                                <option value="{{$object->id}}" selected="selected">{{$object->nom}}</option>
                                @else
                                <option value="{{$object->id}}">{{$object->nom}} </option>
                                @endif
                              @endforeach
                              </select>
                            @else
                              <select class="form-control" name="commercial" id="commercial">
                              @foreach ($commercials as $object)
                                <option value="{{$object->id}}">{{$object->nom}}</option>
                              @endforeach
                              </select>
                            @endif
                          </div>

                          <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Valider</button>
                          </div>

                        </form>
                    </div>
                 

            </div>
            <!-- /.box-body -->
           
          </div>
          <!-- /.box -->
        </div>
       
      </div>
      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>


@endsection

@section('js')

<script>

$('document').ready(function() {

  function setactiondiv(){

    if( $('#action').val() == "Aucune" ){
      $('#feedback_div').hide()
      $('#etat_retour_div').hide();
      $('#redirect_div').hide();

    }

    if( $('#action').val() == "Accepter" ){

       $('#feedback_div').hide()
       $('#etat_retour_div').show();
       $('#redirect_div').hide();


    }
    if( $('#action').val() == "Rejeter" ){
      $('#feedback_div').show()
      $('#etat_retour_div').hide();
      $('#redirect_div').hide();

    }
    if( $('#action').val() == "Rediriger" ){
      $('#feedback_div').hide()
      $('#etat_retour_div').hide();
      $('#redirect_div').show();

    }

  }

  $("#action").on('change',function() {
    
     setactiondiv();

  });

  setactiondiv();

});

</script>

@endsection