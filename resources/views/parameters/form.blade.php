
              <div class="box-body">

              <div class="col-md-6">
                  <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="nom" value="{{$parameter->nom}}">
                  </div>

                  <div class="form-group">
                      <label for="email">Email</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="email" value="{{ $parameter->email }}">
                    </div>

                  <div class="form-group">
                      <label for="adresse">Adresse</label>
                      <textarea  class="form-control" id="adresse" name="adresse"> {{ $parameter->adresse }} </textarea>
                  </div>


                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="tel">Telephone</label>
                        <input type="text" class="form-control" id="telephone" name="telephone" placeholder="telephone" value="{{ $parameter->telephone}}">
                      </div>

                      <div class="form-group ">
                          <label for="logo" >Logo</label>
                          <input name="logo" type="file" id="logo" value="" >
                          @if(!empty($parameter->logo))
                          <a  href="{{ URL::asset(  'storage/'.$parameter->logo  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$parameter->logo  ) }}" alt=""/></a>    
                          @endif
                      
                      </div>
                    

                </div>

                
            
              </div>

          