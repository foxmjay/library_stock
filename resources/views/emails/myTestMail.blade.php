<!DOCTYPE html>
<html>

<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<h3>Decaissement</h3>
<table class="table no-margin">
    <thead>
        <tr>
            <th>Date Cheque</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($echeances as $echeance)
        @if ($echeance->bc_id)
        <tr class="bg-danger">
            <td>{{$echeance->date_cheque}}</td>
            <td> <a href="http://gestionstock.stepone.ma/echeances/{{$echeance->bc_id}}/indexdevis/bc">
                    plus d'infos
                </a></td>

        </tr>
        @endif
        @endforeach

    </tbody>
</table>

<h3>Encaissement</h3>
<table class="table no-margin">
    <thead>
        <tr>
            <th>Date Cheque</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($echeances as $echeance)
        @if ($echeance->devis_id)
        <tr class="bg-success">
            <td>{{$echeance->date_cheque}}</td>
            <td> <a href="http://gestionstock.stepone.ma/echeances/{{$echeance->devis_id}}/indexdevis/devis">
                    plus d'infos
                </a></td>
        </tr>
        @endif
        @endforeach

    </tbody>
</table>
</body>

</html>
