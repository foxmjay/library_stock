@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Boncommands
        <small>gestion des devis</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Devis</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List des devis</h3>
              <a href="{{ url('/devis/create') }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Ajouter
              </a>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($devis) == 0)
              @else


                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Client</th>
                      <th>Reference devis</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($devis as $devi)
                        <tr>
                          <td>{{ $devi->id }}</td>
                          <td >{{ $devi->client->raison_sociale }}</td>
                          <td>{{ $devi->ref_devis }}</td>
                          <td >

                              <form method="POST" action="{{ url('/devis' . '/' . $devi->id) }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/devis/' . $devi->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                                    <!--<a href="" class="btn btn-primary" title="Edit Boncommand">
                                        <span class="icon-pencil" aria-hidden="true"></span>
                                    </a> -->

                                    <button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Supprimer Devis?&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button>
                                </div>
                              </form>

                          </td>
                        </tr>
                        @endforeach

                    </tbody>

                  </table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    /*$('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })*/
  })
</script>

<!-- <script src="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script>
  $('document').ready(function() {
    //Date picker
    //Date picker
    $('#rdv_datepicker').datepicker({
      autoclose: true,
      format: "dd-mm-yyyy",
    }).on("changeDate", function(e) {
      //console.log(e.format('yyyy-mm-dd'));
      //var month = e['date'].getMonth() + 1;
      //var day = e['date'].getDate();
      //var year = e['date'].getFullYear();
      window.location.href = window.location.origin + "?date=" + e.format('yyyy-mm-dd');
      //$("#dateSearch").attr("href","/compta/"+e.format('yyyy-mm-dd'));
    });

  })
</script>
<script>
  ;
  (function($) {
    $.fn.datepicker.dates['fr'] = {
      days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
      daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
      daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
      months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
      monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
      today: "Aujourd'hui",
      monthsTitle: "Mois",
      clear: "Effacer",
      weekStart: 1,
      format: "dd/mm/yyyy"
    };
  }(jQuery));

  $('.datepicker').datepicker({
    language: 'fr',
    autoclose: true,
    todayHighlight: true
  })
</script> --->
@endsection
