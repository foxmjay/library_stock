
<div class="box-body" >


    @if($devis->valider == false)

            <div class="col-md-12">

                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <div class="col-md-4">&nbsp</div>
                    <div class="input-group margin col-md-4 " align="center"  v-show="showsearch">
                            <input type="text" class="form-control" id="nomproduit" name="nomproduit" placeholder="nom produit" v-model="nomproduit" v-on:keyup.enter="searchProduct('enter')" >
                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" v-on:click="searchProduct('none')">
                                                Chercher
                                            </button>
                                </span>
                    </div>



                    <table class="table table-hover" v-show="showadd">
                    <tr>
                    
                        <td> Produit<input type="text" class="form-control" id="nom" name="nom" placeholder="nom" v-model="selectedProduitName" readonly ></td>
                        <td> Quantite<input type="text" class="form-control" id="quantite" name="quantite" placeholder="quantite"  v-on:input="marge" v-model="venteModel.quantite" ></td>
                        <td> Prix Vente HT<input type="text" class="form-control" id="prix_vente" name="prix_vente" placeholder="Prix  Vente" v-model="venteModel.prix_vente" readonly></td>
                        <td> Remise <div class="input-group">
                            <input class="form-control" id="remise"  type="number" :min="min" :max="max" v-on:input="marge" v-model="venteModel.remise">
                            <span class="input-group-addon">%</span>
                        </div></td>
                        
                        <td> <div class="form-group has-success">
                            Prix Total HT<input type="text" class="form-control" id="prix_total" name="prix_total" placeholder="Prix final" v-model="venteModel.prix_total" readonly>
                            @if ( $userProfile->type == "Admin" )
                            <span class="help-block">Marge Prix : @{{prix_marge.toFixed(2) }} DH</span>
                            <span class="help-block">Marge Pourcentage : @{{percent_marge.toFixed(2)}}%</span>
                            @endif
                             </div>
                        </td>


                        <td> <br>
                            <button type="button" class="btn btn-success" title="Ajouter" @click.prevent="addItem()">
                                <span class="fa fa-plus" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-danger" title="Annuler" @click.prevent="showadd=false ; showsearch = true">
                                    <span class="fa fa-remove" aria-hidden="true"></span>
                                </button>
                        </td>
                    </tr>
                    </table>
                </div>
                <!-- /.box-body -->



            </div>
        @endif



    <div class="col-md-12" >

            <hr>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
            <tr>
                <th>Produit</th>
                <th>Ref interne</th>
                <th>Quantite</th>
                <th>Prix unitaire HT</th>
                <th>Remise</th>
                <th>Prix total HT</th>
                @if ( $userProfile->type == "Admin" )
                <th>Marge</th>
                @endif
                <th></th>
            </tr>
            <tr v-for="item in items">
                <td>@{{item.nom}}</td>
                <td>@{{item.ref_interne}}</td>
                <td>@{{item.quantite}}</td>
                <td>@{{item.prix_unitaire}}</td>
                <td>@{{item.remise}} %</td>
                <td>@{{item.prix_total}}</td>
                @if ( $userProfile->type == "Admin" )
                <td>@{{ (item.prix_total - item.prix_max* item.quantite).toFixed(2)  }} DH <br>
                    @{{  ((item.prix_total - item.prix_max* item.quantite) *100 / item.prix_total).toFixed(2) }} %
                </td>
                @endif
                <td>
                    @if($devis->valider == false)
                        <button type="button" class="btn btn-danger" title="Supprimer" @click.prevent="deleteItem(item)">
                            <span class="fa fa-trash" aria-hidden="true"></span>
                        </button>
                    @endif
                </td>
            </tr>

            </table>
        </div>
        <!-- /.box-body -->



    </div>

</div>

