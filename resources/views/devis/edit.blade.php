@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

@php
    $userProfile =  \App\UserProfile::where('user_id',Auth::user()->id)->first();
 @endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="vente-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de Devis
        <small>gestion des Devis</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Etat</a></li>
        <li class="active"><font color="green">{{$devis->etat}}</font></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification Devis</h3>
               

            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/devis/' . $devis->id) }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('devis.form_edit', ['devis' => $devis,])

            <div class="box-footer">
              <div class="col-xs-6">
                    <button type="button" onclick="window.location.href='/clients/{{$devis->client_id}}'" class="btn btn-danger">Annuler</button>
              </div>
              
              <div class="col-xs-6">
                  @if($devis->user_id != Auth::id() )
                    <button type="submit"  onclick="return confirm('Ce devis appartient a un autre utilisateur, la poursuite du sauvegarde créera une nouvelle version .');" class="btn btn-primary pull-right">Sauvgarder</button>
                  @else

                    @if($devis->valider == false)
                      <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Sauvgarder</button>
                    @else
                      <button type="submit"  onclick="return confirm('Ce devis est validé, la poursuite du sauvegarde créera une nouvelle version .');" class="btn btn-primary pull-right">Sauvgarder</button>
                    @endif

                  @endif
              </div>
            </div>

            </form>
            <div class="box-footer">
              
            </div>


          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

     <!-- /.content -->
     <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Produit</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Ref Interne</th>
                                    <th>Produit</th>
                                    <th>Marque</th>
                                    <th>Quantite</th>
                                    <th>Prix vente HT</th>
                                    <th>Quantite reserve</th>

                                    <th></th>
                                </tr>
                                <tr v-for="produit in produits">
                                    <td>@{{produit.id}}</td>
                                    <td>@{{produit.ref_interne}}</td>
                                    <td>@{{produit.nom}}</td>
                                    <td>@{{produit.marque}}</td>
                                    <td>@{{produit.quantite}}</td>
                                    <td>@{{produit.prix_vente}}</td>
                                    <td>@{{produit.reserve}}</td>

                                    
                                    <td>
                                            <button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="getventeitem(produit.id,produit.nom,produit.prix_vente, produit.prix_max)">
                                                <span class="fa fa-arrow-circle-down" aria-hidden="true"></span>
                                            </button>
                                    </td>
                                </tr>

                                </table>
                            </div>
                </div>

              </div>
            </div>
          </div>
  <!-- Main content -->




    <!-- /.content -->
    <div class="modal fade" id="ModalImagebc" tabindex="-1" role="dialog" aria-labelledby="ModalImagebcLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body">
                  <div  class="box-body table-responsive no-padding">
                    <div class="form-group ">
                        <label for="image_bc" >Image bon de commande: </label>
                        <input name="image_bc" class="form-control"  type="file" id="image_bc" ref="image_bc" v-on:change="handleFileUpload()" required>
                    </div>

                    <div class="form-group ">
                        <label for="num_bc" >Numero bon de commande: </label>
                        <input name="num_bc" class="form-control" type="text" id="num_bc" v-model="num_bc" required>
                    </div>

                      <div class="form-group">
                        <label for="date_bc">Date bon de commande: </label>
                        <input type="date" class="form-control" id="date_bc" name="date_bc" placeholder="Date bon de commande" v-model="date_bc" required>
                      </div>

                    <div class="box-footer">
                        <button type="button" v-on:click="confirmerDevis()" class="btn btn-primary pull-right">Confirmer</button>
      
                    </div>

                  </div>
                    
          </div>

        </div>
      </div>
    </div>
<!-- Main content -->

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Produits</h3>
          </div>
              @include ('devis.form_vente')
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->



  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body" >
              <div class="col-md-12">
        
              @if ($devis->etat == 'En cours')
              <!--<a class="btn btn-app pull-right bg-green" href="{{ url('/devis/' . $devis->id.'/confirm') }}">
                <i class="fa fa-save"></i> Confirmer Devis
              </a>-->
              @if($devis->valider == true)
              <button type="button" class="btn btn-app pull-right   bg-green" data-toggle="modal" data-target="#ModalImagebc">
                  <i class="fa fa-thumbs-up"></i>Confirmer Devis
              </button> 
              @else
                <button type="button" class="btn btn-app pull-right  bg-blue" v-on:click="validerDevis()" >
                    <i class="fa fa-check"></i>Valider Devis
                </button> 
              @endif

            @endif

          </div>

        </div>
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')

<!--<script src="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>-->

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script>


var devis_id = {!! json_encode($devis->id) !!};
var delai_livraison = {!! json_encode($devis->delai_livraison) !!};
var objet = {!! json_encode($devis->objet) !!};
var garantie = {!! json_encode($devis->garantie) !!};
var client_id = {!! json_encode($devis->client_id) !!};


var max = {!! json_encode($max ) !!}; 
var min = {!! json_encode( $min ) !!};

var app = new Vue({
  el: '#vente-vue-wrapper',

  data: {
    items: [],
    produits: [],
    venteModel : {'devis_id':devis_id,'produit_id':'','quantite': 0 ,'prix_vente': 0.0 ,'remise':0.0 ,'prix_total':0.0 },
    prix_total:0,
    selectedProduitName:'',
    max:max,
    min:min,
    showadd:false,
    showsearch:true,
    nomproduit: '',
    prixvente: 0,
    loading : false,
    image_bc : '',
    num_bc : '',
    date_bc : '',
    objet: objet,
    delai_livraison: delai_livraison,
    garantie : garantie,
    percent_marge : 0,
    prix_marge: 0,
    prix_max : 0,

   },

  mounted: function mounted() {
    this.getVueItems();
    //this.prix_total = this.calcPrixTotal();
  },
  computed:{



    calcPrixTotal : function calcPrixTotal(){
      var _this = this;
      var prix_total=0;

      _this.items.forEach(function(element){
          prix_total += element.prix_total;
      });
      this.prix_total = prix_total;
      return prix_total;
    }
  },

  methods: {

    handleFileUpload: function handleFileUpload(){
      this.image_bc = this.$refs.image_bc.files[0];
    },

    marge:function() {
      if (this.venteModel.remise == 0) {
        this.venteModel.remise = 0;
      }else if (this.venteModel.remise > this.max) {
        this.venteModel.remise = this.max;
      }else if (this.venteModel.remise < this.min) {
        this.venteModel.remise = this.min;
      }

      this.venteModel.prix_total = this.venteModel.quantite*this.venteModel.prix_vente*(1-this.venteModel.remise/100);

      this.prix_marge = this.venteModel.prix_total - this.prix_max* this.venteModel.quantite
      this.percent_marge = this.prix_marge * 100 / this.venteModel.prix_total;

    },

    getVueItems: function getVueItems() {
      var _this = this;
      axios.get('/ventes/'+devis_id+'/perdevis').then(function (response) {
        _this.items = response.data;
        
      });
    },
    
    getventeitem: function getventeitem(id,nom, prix_vente, prix_max) {

        //console.log(nom);
        var _this = this;
        this.showadd = true;
        this.showsearch = false;
        this.nomproduit = "";


        this.venteModel.produit_id = id;
        this.venteModel.prix_vente = prix_vente;
        this.venteModel.quantite = 1;
        this.venteModel.remise = 0;
        this.selectedProduitName = nom;

        this.venteModel.prix_total = this.venteModel.prix_vente*(1-this.venteModel.remise/100);

        this.prix_max = prix_max;
        this.prix_marge = this.venteModel.prix_total - prix_max;
        this.percent_marge = this.prix_marge * 100 / this.venteModel.prix_total;


    },

    searchProduct: function searchProduct(action) {
      var _this = this;
      if(action == 'enter'){
        $('#exampleModal').modal('show');
      }

      axios.get('/getproduct/'+this.nomproduit).then(function (response) {
        _this.produits = response.data;
      });
    },


    /*getproduct: function getproduct() {
        this.loading = false;

      var _this = this;

      axios.get('/getproduct/'+this.nomproduit).then(function (response) {
        _this.stock = response.data;
        _this.loading = true;
      });

    },*/

    /*updatePrix: function(e){
      var _this = this;
      _this.venteModel.prix_total = _this.prixvente * _this.venteModel.quantite;
      _this.calcPrixTotal();
    },*/

    addItem: function(){

      var _this = this;
      var data = this.venteModel;
      
      axios.post('/ventes',data).then(function (response){
        console.log(response.data)
        _this.getVueItems();
      })

      this.showadd = false;
      this.showsearch = true;

    },

    deleteItem: function deleteItem(item){
      var _this = this;
      axios.post('/ventes/'+item.id+'/delete').then(function(response){
        _this.getVueItems();
      });
      //_this.calcPrixTotal();
    },

    confirmerDevis: function(){

      enableSpinner();

        var _this = this;
        let formData = new FormData();
       
        formData.append('image_bc',this.image_bc);
        formData.append('num_bc',this.num_bc);
        formData.append('date_bc',this.date_bc);
        formData.append('prix_total',this.prix_total);
        formData.append('delai_livraison',this.delai_livraison);
        formData.append('objet',this.objet);
        formData.append('garantie',this.garantie);


        axios.post('/devis/'+devis_id+'/confirm',formData,{
          headers: {'Content-Type': 'multipart/form-data' }

        }).then(function (response){
          window.location.href = "/clients/"+client_id;
        });

    },


    validerDevis: function(){

      enableSpinner();

      var _this = this;
      let formData = new FormData();

      formData.append('prix_total',this.prix_total);
      formData.append('delai_livraison',this.delai_livraison);
      formData.append('objet',this.objet);
      formData.append('garantie',this.garantie);


      axios.post('/devis/'+devis_id+'/valider',formData).then(function (response){
        window.location.href = "/clients/"+client_id;
      });

      },
  }
});

</script>

@endsection
