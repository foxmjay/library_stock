@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="vente-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de Devis
        <small>gestion des Devis</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Etat</a></li>
        <li class="active"><font color="yellow">{{$devis->etat}}</font></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification Devis</h3>
               

            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/devis/' . $devis->id) }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('devis.form_edit2', ['devis' => $devis,])
                
                <div class="col-xs-6">
                    <button type="button" onclick="window.location.href='/clients/{{$devis->client_id}}'" class="btn btn-danger">Annuler</button>
              </div>

              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Sauvgarder</button>

              </div>



            </form>


          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Produits</h3>
          </div>
          <div class="box-body" >
              <div class="col-md-12" >

                    <hr>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                    <tr>
                        <th>Produit</th>
                        <th>Ref interne</th>
                        <th>Quantite</th>
                        <th>Prix unitaire HT</th>
                        <th>Remise</th>
                        <th>Prix total HT</th>
                    </tr>
                    <tr v-for="item in items">
                        <td>@{{item.nom}}</td>
                        <td>@{{item.ref_interne}}</td>
                        <td>@{{item.quantite}}</td>
                        <td>@{{item.prix_unitaire}}</td>
                        <td>@{{item.remise}} %</td>
                        <td>@{{item.prix_total}}</td>

                    </tr>

                    </table>
                    </div>
                    <!-- /.box-body -->



                    </div>
            </div>
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')

<!--<script src="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>-->

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>


var devis_id = {!! json_encode($devis->id) !!};


var app = new Vue({
  el: '#vente-vue-wrapper',

  data: {
    items: [],
   },

  mounted: function mounted() {
    this.getVueItems();
  },

  methods: {


    getVueItems: function getVueItems() {
      var _this = this;
      axios.get('/ventes/'+devis_id+'/perdevis').then(function (response) {
        _this.items = response.data;
        
      });
    },
    
  }
});

</script>

@endsection
