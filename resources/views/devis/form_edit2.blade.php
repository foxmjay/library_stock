
              <div class="box-body">

                <div class="col-md-6">

                  <div class="form-group">
                    <label for="client_id">Client</label>
                    <input type="text" class="form-control" id="client_id" name="client_id"  value="{{ $client->raison_sociale }}" disabled>
                    <input type="hidden" class="form-control" id="client_id" name="client_id"  value="{{ $client->id }}">
                  </div>




                    <div class="form-group">
                        <label for="prix_total">Prix total HT</label>
                        <input type="text" class="form-control" id="prix_total" name="prix_total" placeholder="prix total"  value="{{$devis->prix_total}}" disabled>
                    </div>

                    <div class="form-group">
                        <label for="objet">Objet</label>
                        <input type="text" class="form-control" id="objet" name="objet" placeholder="objet" value="{{ old('objet', optional($devis)->objet) }}">
                    </div>


                </div>
                <div class="col-md-6">

                    <div class="form-group">
                    <label for="ref_devis">Reference Devis</label>
                    <input type="text" class="form-control" id="ref_devis" name="ref_devis" placeholder="reference Devis" value="{{ old('ref_devis', optional($devis)->ref_devis) }}" disabled>
                    </div>

                    <div class="form-group">
                        <label for="delai_livraison">Delai livraison</label>
                        <input type="text" class="form-control" id="delai_livraison" name="delai_livraison" placeholder="delai livraison" value="{{ old('delai_livraison', optional($devis)->delai_livraison) }}">
                    </div>


                   <div class="form-group">
                        <label for="garantie">Garantie (Mois)</label>
                        <input type="number" class="form-control" id="garantie" name="garantie" placeholder="garantie" value="{{ old('garantie', optional($devis)->garantie) }}">
                    </div>




                </div>


              </div>

