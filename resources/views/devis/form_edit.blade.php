
              <div class="box-body">

                <div class="col-md-6">

                  <div class="form-group">
                    <label for="client_id">Client</label>
                    <input type="text" class="form-control" id="client_id" name="client_id"  value="{{ $client->raison_sociale }}" disabled>
                    <input type="hidden" class="form-control" id="client_id" name="client_id"  value="{{ $client->id }}">
                  </div>




                    <div class="form-group">
                        <label for="prix_total">Prix total HT</label>
                        <input type="text" class="form-control" id="prix_total" name="prix_total" placeholder="prix total" v-model="calcPrixTotal" readonly>
                    </div>

                    <div class="form-group">
                        <label for="objet">Objet</label>
                        <input type="text" class="form-control" id="objet" name="objet" placeholder="objet" v-model="objet" >
                    </div>


                    <!--<div class="form-group">
                        <label for="etat">Etat</label>
                        <select class="form-control" name="etat" id="etat">
                        @foreach (['En cours','Termine'] as $object)
                          @if ($object === $devis->etat)
                          <option value="{{$object}}" selected="selected">{{$object}}</option>
                          @else
                          <option value="{{$object}}">{{$object}} </option>
                          @endif
                        @endforeach
                        </select>
                    </div>-->



                </div>
                <div class="col-md-6">

                    <div class="form-group">
                    <label for="ref_devis">Reference Devis</label>
                    <input type="text" class="form-control" id="ref_devis" name="ref_devis" placeholder="reference Devis" value="{{ old('ref_devis', optional($devis)->ref_devis) }}-{{$devis->version}}" disabled>
                    </div>

                    <div class="form-group">
                        <label for="delai_livraison">Delai livraison</label>
                        <input type="text" class="form-control" id="delai_livraison" name="delai_livraison" v-model="delai_livraison" placeholder="delai livraison">
                    </div>


                   <div class="form-group">
                        <label for="garantie">Garantie (Mois)</label>
                        <input type="number" class="form-control" id="garantie" name="garantie" placeholder="garantie" v-model="garantie">
                    </div>




                </div>


              </div>

