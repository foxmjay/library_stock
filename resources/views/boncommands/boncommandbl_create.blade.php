@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->



    <section class="content-header">
      <h1>
        Bon de livraison
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de livraison</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Details</h3>
                      </div> 
                      <div  class="box-body">
                          

                            <div class="col-xs-4">
                              <div class="form-group">
                                  <label for="categorie">Type</label>
                                    <select class="form-control" name="bl_type" id="bl_type" v-model="bl_type" :disabled="!isNewBl">
                                        <option v-for="bl in bl_typeList" v-bind:value="bl.type">@{{bl.type}}</option>
                                    </select>
                              </div>
                            </div>

                            <div class="col-xs-4">
                                <div class="form-group">
                                <label for="categorie">Reference bon de livraison</label>
                                <input type="text" class="form-control" id="ref_bl" name="ref_bl" v-model="ref_bl" >
                                </div>
                            </div>

                            <div class="col-xs-4">
                                <div class="form-group">
                                <label for="categorie">Image bon de livriason</label>
                                <input type="file" class="form-control" id="image_bl" name="image_bl" ref="image_bl" v-on:change="handleFileUpload()">
                                </div>
                            </div>


                      </div>

                      <div  class="box-body">
                          
                        <div class="col-xs-5"></div>
                        <div class="col-xs-2">
                            <div class="form-group">
                            <label for="categorie">Reference Devis :</label>
                              <span>{{$boncommand->ref_devis}}</span>
                            </div>
                        </div>
                        <div class="col-xs-5"></div>

                      </div>
    
                 </div>
    
          </div>
              <!-- /.col -->



          <div class="col-xs-12">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Produits</h3>
              </div>        

            <div  class="box-body table-responsive no-padding">
              <table id="produitList" class="table">
              <tr>
                <th>ID</th>
                <th>Produit</th>
                <th>Ref interne</th>
                <th>Quantite</th>
                <th>Prix unitaire HT</th>
                <th>Prix total HT</th>
                <th>Quantite sortie</th>
                <th></th>
              </tr>
              <tr v-for="(achat,index) in achats"  v-bind:id="'produitid'+achat.id" >
                    <td>@{{achat.id}}</td>
                    <td>@{{achat.nom}}</td>
                    <td>@{{achat.ref_interne}}</td>
                    <td>@{{achat.quantite}}</td>
                    <td>@{{achat.prix_unitaire}}</td>
                    <td>@{{achat.prix_total}}</td>
                    <td>@{{achat.left_quantity}}</td>
                  <td v-if="achat.left_quantity < achat.quantite">
                         <button type="button" class="btn btn-success" title="sélectionner"  v-on:click="selectedProduct(achat)">
                              <span class="fa fa-plus" aria-hidden="true"></span>
                          </button>
                  </td>
              </tr>

              </table>
          </div>

        </div>

          </div>
          <!-- /.col -->



          <div class="col-xs-12" id="stockSection">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Stock</h3>
                </div>       

  
                <div  class="box-body table-responsive ">
                    <table  id="stockList" class="table ">
                    <thead>
                    <tr>
                        <th>Produit</th>
                        <th>Ref fournisseur</th>
                        <th>Numero de serie</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> <span>@{{ selectedProduit.nom }}</span></td>
                        <td><input type="text"   class="form-control" id="ref_fournisseur" name="ref_fournisseur" placeholder="ref fournisseur" v-model="ref_fournisseur"></td>
                        <td><input type="text"   class="form-control" id="num_serie" name="num_serie" placeholder="numero serie" v-model="num_serie"  v-on:keyup.enter="addItem()"></td>
                        <td>
                            <button type="button" class="btn btn-success btn-flat"    @click.prevent="addItem()"> <span class="fa fa-plus" aria-hidden="true"></span></button>

                        </td>
                    </tr>
                  </tbody>
      
                    </table>
                    
                </div>
  
          </div>
  
       </div>
            <!-- /.col -->

          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits selectiones</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Produit</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Ref fournisseur</th>
                        <th>Prix unitaire HT</th>
                        
                        <th></th>
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td>@{{stock.id}}</td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td>  
                    <td>@{{stock.ref_fournisseur}}</td> 
                    <td>@{{stock.prix_unitaire}}</td>
   
                    <td>
                            <button type="button" class="btn btn-danger" title="Supprimer"  v-on:click="removeStock(index)">
                                <span class="fa fa-trash" aria-hidden="true"></span>
                            </button>
                    </td>
                </tr>
  
                </table>

                <br>
                <br>
                <hr>
                  <h2 class="text-right" style="padding-right: 5%"><b>Prix total HT : </b>  @{{ prix_total | numeral('0.2') }} DH</h2>
                <hr> 

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->
       

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Confirmer
                </button> 
                <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                    <i class="fa fa-close"></i>Annuler
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var bc_id = {!! json_encode($boncommand->id) !!};
var fournisseur_id = {!! json_encode($fournisseur->id) !!};
var bl_type = {!! json_encode($boncommand->type ) !!};

var isNewBl = false;
if(bl_type == null){
  isNewBl = true;
  bl_type = 'Complet';
}else{
  isNewBl = false;
}

$('#stockSection').hide();

var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {
    isNewBl : isNewBl,
    bl_type : bl_type,
    type_search : 'Serialisable',
    keyword : '',
    bl_typeList : [{'type':'Partiel'},{'type':'Complet'}],
    achats: [],
    selectedStocks: [],
    items: [],
    stocks: [],
    prix_total : 0.0,
    selectedProduit : '',
    image_bl : '',
    ref_bl : '',
    num_serie: '',
    ref_fournisseur: '',
   },

  mounted: function mounted() {
    this.getVueItems();
  },
    
  methods: {

    handleFileUpload: function handleFileUpload(){
      this.image_bl = this.$refs.image_bl.files[0];
    },

    ifSelected: function ifSelected(stockid){

      var exists = true;
      this.selectedStocks.forEach(function(element){
         if(element.id == stockid)
          exists = false;
      })
      
       return exists;

    },

    selectedProduct: function selectedProduct(selected){
      $('#produitList tr').removeClass('highlight');
      $('#produitid'+selected.id).addClass('highlight');
      $("#ModalSearchStock").modal('show');
      this.selectedProduit = selected;
      //this.getQuantite(id, produit_id);
      $('#stockSection').show();
      $("#num_serie").focus();
      //this.getStock(id);
    },


    /*getQuantite: function getQuantite(vente_id,produit_id){
      _this = this;

      var data = JSON.stringify( { 'vente_id' : vente_id ,'devis_id': devis_id ,'produit_id': produit_id});
      axios.post('/ventes/getquantite',{data: data}).then(function (response){
        if(response.data['result'] == "OK"){
          //console.log(response.data);
          //if( response.data['quantite'] <= this.selectedProduitQuantite)
           _this.selectedProduitQuantite = _this.selectedProduitQuantite - response.data['quantite'];
           //console.log(_this.selectedProduitQuantite);
                        
        }
        
      }).catch(function (error){
        console.log(error)
      })
    
    },*/

    getVueItems: function getVueItems() {
      var _this = this;
      axios.get('/achats/'+bc_id+'/perbc').then(function (response) {
        _this.achats = response.data;
        
      });

    },

    addItem: function addItem(){
     
     var _this = this;
     
     
     produitCount = 0;
     this.selectedStocks.forEach(function(element){
        if(element.produit_id == _this.selectedProduit.produit_id)
         produitCount+=1;

     });

     if(produitCount >= this.selectedProduit.quantite){
       alert('Quantite atteinte pour ce produit');
       return;
     }

     exists = 0;
     this.selectedStocks.forEach(function(element){
        if(element.num_serie == _this.num_serie)
              exists = 1;
     });

     if(exists == 1 ){
       alert('Numero de serie deja existant');
       return;
     }

     var tmp =  Object.assign({}, this.selectedProduit); // STUPID  JAVASCRIPT CLONNING VARIABLE !!!!
     tmp['num_serie'] = this.num_serie;
     tmp['ref_fournisseur'] = this.ref_fournisseur;
     
     this.selectedStocks.push(tmp);
     this.prix_total += this.selectedProduit.prix_unitaire;
     this.num_serie='';
    },

    removeStock: function removeStock(index){

      this.prix_total -= this.selectedProduit.prix_unitaire;
      this.selectedStocks.splice(index,1);
    },

    annuler: function annuler(){
      //history.back();
      window.location.href ="/fournisseurs/"+fournisseur_id
    },


    confirmer: function confirmer(){

      var _this = this;
      //var data = this.echeanceModel;

      if(this.selectedStocks.length <= 0 ){
        alert("Selectioner au moin un produit");
        return;
      }

      if(this.bl_type == ""){
        alert("Selectioner le type du bon du livraison");
        return;
      }

 

      var data = JSON.stringify( {'stocks' : this.selectedStocks ,'bl_type': this.bl_type, 'ref_bl':this.ref_bl ,'bc_id':bc_id ,'isNewBl' : this.isNewBl ,'fournisseur_id' : fournisseur_id });

      let formData = new FormData();
      formData.append('jsondata',data);
      formData.append('image_bl',this.image_bl);
      
      axios.post('/boncommandbls/store',formData ,{
          headers: {'Content-Type': 'multipart/form-data'}
      } ).then(function (response){
        //console.log(response.data);
        if(response.data['result'] == "OK"){
          console.log(response.data['result']);
          window.location.href = "/fournisseurs/"+fournisseur_id;
        }

        if(response.data['result'] == "KO"){
          console.log(response.data['result']);
          var stock=response.data['stock']
          alert("Le produit "+stock.nom+" numero serie : "+stock.num_serie+" exist deja dans le stock");
          //console.log(stock);
          _this.selectedStocks.forEach(function(element,index){
            //console.log(index);
            if(element.id == stock.id){
              _this.selectedStocks.splice(index,1);
            }

          });
          //_this.getStock(stock.vente_id);
        }

        
      }).catch(function (error){
        console.log(error)
      })

   }

  }
});



</script>

@endsection
