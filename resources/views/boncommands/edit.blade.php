@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="achat-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de commandes
        <small>gestion des boncommandes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Boncommands</li>
      </ol>
    </section>

    <!-- /.content -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Produit</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                <tr>
                                    <th>Ref Interne</th>
                                    <th>Produit</th>
                                    <th>Marque</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                                <tr v-for="product in product">
                                    <td>@{{product.ref_interne}}</td>
                                    <td>@{{product.nom}}</td>
                                    <td>@{{product.marque}}</td>
                                    <td>@{{product.description}}</td>
                                    
                                    <td>
                                            <button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="getAchatItem(product.id ,product.nom, product.prix_vente)">
                                                <span class="fa fa-arrow-circle-down" aria-hidden="true"></span>
                                            </button>
                                    </td>
                                </tr>

                                </table>
                            </div>
                </div>

              </div>
            </div>
          </div>
  <!-- Main content -->

  <div class="modal fade" id="nouveauProduitModal" tabindex="-1" role="dialog" aria-labelledby="nouveauProduitModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nouveauProduitModal">Produit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>


          <div class="modal-body">
                  <div class="box-body table-responsive no-padding">
                      @include ('boncommands.form_produit')
                      <button type="button" class="btn btn-success pull-right" title="sélectionner" data-dismiss="modal" v-on:click="addNewProduct()">
                          Ajouter
                      </button>
                   </div>
          </div>

        </div>
      </div>
    </div>
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification bon de commande</h3>
            
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/boncommands/' . $boncommand->id) }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('boncommands.form_edit', ['boncommand' => $boncommand,])
            
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Sauvgarder</button>
              </div>

            </form>
            
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Produits</h3>
          </div>

                            
                  <div class="box-body" >

                  @if ($boncommand->etat == 'En cours')

                  <div class="col-md-12">

                          <!-- /.box-header -->
                          <div class="box-body table-responsive no-padding">
                              <div class=" col-md-3"></div>
                              <div v-show="showsearch" align="center" class=" col-md-4"> <input type="text" class="form-control" id="nomproduit" name="nomproduit" placeholder="nom produit" v-model="nomproduit" >
                              </div>
                                  <div v-show="showsearch"  class=" col-md-2">
                                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" v-on:click="searchProduct()">
                                              Chercher
                                      </button> 
                                  </div>
                                  <div v-show="showsearch"  class=" col-md-2">
                                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nouveauProduitModal">
                                              Nouveau Produit
                                      </button> 
                                  </div>
                                  
                                        <div class=" col-md-3"></div>



                              <table class="table table-hover" v-show="showadd">
                              <tr>

                                  <td> Produit<input type="text" class="form-control" id="nom" name="nom" placeholder="nom" v-model="selectedProduitName" readonly ></td>
                                  <td>Quantite<input type="text" class="form-control" id="quantite" name="quantite" placeholder="Quantite" v-on:input="marge" v-model="achatModel.quantite" ></td>
                                  <td>Prix unitaire<input type="text" class="form-control" id="prix_unitaire" name="prix_unitaire" v-on:input="marge" placeholder="Prix unitaire" v-model="achatModel.prix_unitaire" ></td>
                                  <td> Prix Total<input type="text" class="form-control" id="prix_total" name="prix_total" placeholder="Prix total" v-model="achatModel.prix_total" readonly></td>


                                  <td> <br>
                                      <button type="button" class="btn btn-success" title="Ajouter" @click.prevent="addItem()">
                                          <span class="fa fa-plus" aria-hidden="true"></span>
                                      </button>
                                      <button type="button" class="btn btn-danger" title="Annuler" @click.prevent="showadd=false ; showsearch = true">
                                              <span class="fa fa-remove" aria-hidden="true"></span>
                                          </button>
                                  </td>
                              </tr>
                              </table>
                          </div>
                          <!-- /.box-body -->



                      </div>

                      @endif

                  <div class="col-md-12">

                  <hr> 
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">
                      <table class="table table-hover">
                      <tr>
                          <th>Produit</th>
                          <th>Ref interne</th>
                          <th>Prix unitaire HT</th>
                          <th>Quantite</th>
                          <th>Prix total HT</th>
                          <th></th>
                      </tr>
                      <tr v-for="item in items">
                          <td>@{{item.nom}}</td>
                          <td>@{{item.ref_interne}}</td>
                          <td>@{{item.prix_unitaire}}</td>
                          <td>@{{item.quantite}}</td>
                          <td>@{{item.prix_total}}</td>
                          <td> 
                                  @if ($boncommand->etat == 'En cours')
                                  <button type="button" class="btn btn-danger" title="Supprimer" @click.prevent="deleteItem(item)"> 
                                      <span class="fa fa-trash" aria-hidden="true"></span>
                                  </button>
                                  @endif
                          </td>
                      </tr>

                      </table>
                  </div>
                  <!-- /.box-body -->



                  </div>

                  </div>


          
          
        </div>
        <!-- /.box -->
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

  @if ($boncommand->etat == 'En cours')

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body" >
              <div class="col-md-12">
        
              
  
              <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmerBC()">
                  <i class="fa fa-save"></i>Confirmer BC
              </button> 
            

          </div>

        </div>
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  @endif
  <!-- /.content -->



  </div>
  <!-- /.content-wrapper -->

  


@endsection

@section('js')

<!--<script src="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>-->

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>

var bc_id = {!! json_encode($boncommand->id) !!};
var date_bc = {!! json_encode($boncommand->date_bc) !!};
var tva = {!! json_encode($boncommand->tva) !!};
var date_livraison = {!! json_encode($boncommand->date_livraison) !!};
var image_devis = {!! json_encode($boncommand->image_devis) !!};
var ref_devis = {!! json_encode($boncommand->ref_devis) !!};

var fournisseur_id = {!! json_encode($boncommand->fournisseur_id) !!};


var app = new Vue({
  el: '#achat-vue-wrapper',

  data: {
    items: [],
    product: [],
    achatModel : {'bc_id':bc_id,'fournisseur_id':fournisseur_id,'produit_id':'','quantite': 0, 'prix_unitaire': 0, 'prix_total' :0 },
    produitModel : {'nom':'','marque':'','description': '' ,'ref_constructeur' : '','categorie_id': 0 ,'ref_interne':'','garantie': 0},
    selectedProduitName:'',
    selectedProduitPrix_vente: 0,
    prix_total:0,
    count: 0,
    showadd:false,
    showsearch:true,
    nomproduit: '',
    date_bc : date_bc,
    date_livraison : date_livraison,
    image_devis : '',
    tva: tva,
    ref_devis : ref_devis
   },

  mounted: function mounted() {
    this.getVueItems();
  },

  computed:{
    calcPrixTotal : function calcPrixTotal(){
      var _this = this;
      var prix_total=0;

      _this.items.forEach(function(element){
          prix_total += element.prix_unitaire * element.quantite;
      });
      this.prix_total = prix_total;
      return prix_total;
    }
  },

  methods: {

    handleFileUpload: function handleFileUpload(){
      this.image_devis = this.$refs.image_devis.files[0];
    },


    marge:function() {
     
      this.achatModel.prix_total = this.achatModel.quantite*this.achatModel.prix_unitaire;
    },


    searchProduct: function searchProduct() {
      var _this = this;

      axios.get('/searchproduct/'+this.nomproduit).then(function (response) {
        _this.product = response.data;
      });
    },

    getAchatItem: function getAchatItem(id, nom, prix) {
        var _this = this;
        this.showadd = true;
        this.showsearch = false;
        this.achatModel.produit_id = id;
        this.selectedProduitName = nom;
        this.selectedProduitPrix_vente = prix;
    },
    
    getVueItems: function getVueItems() {
      var _this = this;
     
      axios.get('/achats/'+bc_id+'/perbc').then(function (response) {
        _this.items = response.data;
      });
      
    },
    /*updatePrix: function(e){
      var _this = this;
      _this.achatModel.prix_total = _this.achatModel.prix_unitaire * _this.achatModel.quantite;
    },*/
    addItem: function(){
     
      var _this = this;
      var data = this.achatModel;
      
      axios.post('/achats',data).then(function (response){
 
          _this.showadd = false;
          _this.showsearch = true;
          _this.getVueItems();
   
      }).catch(function (error){
        console.log(error)
      });
    },

    addNewProduct: function(){
     
     var _this = this;
     var data = this.produitModel;
     this.showadd = true;
     this.showsearch = false;
     axios.post('/produits/api/create',data).then(function (response){
       console.log(response.data.id);
       _this.achatModel.produit_id = response.data.id
       _this.selectedProduitName = response.data.nom
     }).catch(function (error){
        console.log(error)
    });
   },

    deleteItem: function deleteItem(item){
      var _this = this;
      axios.post('/achats/'+item.id+'/delete').then(function(response){
        _this.getVueItems();
      });
     // _this.calcPrixTotal();
    },

    confirmerBC: function(){

      enableSpinner();


      var _this = this;
      let formData = new FormData();

      formData.append('bc_id',bc_id);
      formData.append('tva',this.tva);
      formData.append('date_bc',this.date_bc);
      formData.append('date_livraison',this.date_livraison);
      formData.append('prix_total',this.prix_total);
      formData.append('ref_devis',this.delai_livraison);
      formData.append('image_devis',this.image_devis);
      formData.append('ref_devis',this.ref_devis);


      axios.post('/boncommands/confirmer',formData,{
          headers: {'Content-Type': 'multipart/form-data' }

        }).then(function (response){
        console.log(response.data['result'])
        if(response.data['result'] == "OK"){
            window.location.href= "/fournisseurs/"+fournisseur_id
        }

      });


      },

  }
});

</script>

@endsection
