
<div class="box-body" >

        @if ($boncommand->etat == 'En cours')

        <div class="col-md-12">

                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <div class=" col-md-3"></div>
                    <div v-show="showsearch" align="center" class=" col-md-4"> <input type="text" class="form-control" id="nomproduit" name="nomproduit" placeholder="nom produit" v-model="nomproduit" >
                    </div>
                        <div v-show="showsearch"  class=" col-md-2">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" v-on:click="searchProduct()">
                                    Chercher
                            </button> 
                        </div>
                        <div v-show="showsearch"  class=" col-md-2">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nouveauProduitModal">
                                    Nouveau Produit
                            </button> 
                         </div>
                        
                              <div class=" col-md-3"></div>
        
        
        
                    <table class="table table-hover" v-show="showadd">
                    <tr>

                        <td> Produit<input type="text" class="form-control" id="nom" name="nom" placeholder="nom" v-model="selectedProduitName" readonly ></td>

                        <td> Num serie<input type="text" class="form-control" id="num_serie" name="num_serie" placeholder="num serie" v-model="achatModel.num_serie" ></td>
                        <td> Ref fournisseur <input type="text" class="form-control" id="ref_fournisseur" name="ref_fournisseur" placeholder="ref fournisseur" v-model="achatModel.ref_fournisseur"></td>
                        <td> Prix Unitaire HT<input type="text" class="form-control" id="prix_unitaire" name="prix_unitaire" placeholder="Prix unitaire" v-model="achatModel.prix_unitaire"  v-on:input="checkprix" >
                             <p id="erreur_prix" style="display: none ; color:red"></p>
                        </td>
                        <td> <br>
                            <button type="button" class="btn btn-success" title="Ajouter" @click.prevent="addItem()">
                                <span class="fa fa-plus" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-danger" title="Annuler" @click.prevent="showadd=false ; showsearch = true">
                                    <span class="fa fa-remove" aria-hidden="true"></span>
                                </button>
                        </td>
                    </tr>
                    </table>
                </div>
                <!-- /.box-body -->
        
        
        
            </div>

            @endif

    <div class="col-md-12">

        <hr> 
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
            <tr>
                <th>Produit</th>
                <th>Ref interne</th>
                <th>Numero serie</th>
                <th>Prix unitaire HT</th>
                <th></th>
            </tr>
            <tr v-for="item in items">
                <td>@{{item.nom}}</td>
                <td>@{{item.ref_interne}}</td>
                <td>@{{item.num_serie}}</td>
                <td>@{{item.prix_unitaire}}</td>
                <td> 
                        @if ($boncommand->etat == 'En cours')
                        <button type="button" class="btn btn-danger" title="Supprimer" @click.prevent="deleteItem(item)"> 
                            <span class="fa fa-trash" aria-hidden="true"></span>
                        </button>
                        @endif
                </td>
            </tr>
        
            </table>
        </div>
        <!-- /.box-body -->

    

    </div>

</div>

