@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Boncommands
        <small>gestion des boncommands</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Boncommands</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List des boncommands</h3>
              <a href="{{ url('/boncommands/create') }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Ajouter
              </a>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($boncommands) == 0)
              @else


                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Fournisseur</th>
                      <th>Reference boncommand</th>
                      <th>Date</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($boncommands as $boncommand)
                        <tr>
                          <td>{{ $boncommand->id }}</td>
                          <td>{{ $boncommand->fournisseur->raison_sociale }}</td>
                          <td>{{ $boncommand->ref_bc }}</td>
                          <td>{{ $boncommand->date_bc }}</td>
                          <td >

                              <form method="POST" action="{{ url('/boncommands' . '/' . $boncommand->id) }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/boncommands/' . $boncommand->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                                    <!--<a href="" class="btn btn-primary" title="Edit Boncommand">
                                        <span class="icon-pencil" aria-hidden="true"></span>
                                    </a> -->

                                    <button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Supprimer Boncommand?&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button>

                                    <a href="{{ url('/boncommands/' . $boncommand->id . '/print') }}" target="_blank" class="btn btn-warning" title="Imprimer">
                                        <span class="fa fa-print" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{ url('/echeances/' . $boncommand->id . '/index') }}" target="_blank" class="btn btn-warning" title="Imprimer">
                                        <span class="fa fa-print" aria-hidden="true"></span>
                                    </a>

                                </div>
                              </form>

                          </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Fournisseur</th>
                      <th>Reference boncommand</th>
                      <th></th>
                    </tr>
                    </tfoot>
                  </table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->

@endsection


@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable()

    })
  </script>
@endsection
