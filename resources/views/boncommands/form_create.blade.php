
              <div class="box-body">

                <div class="col-md-6">

                  <div class="form-group">
                    <label for="gsm">Fournisseur</label>
                    <input type="text" class="form-control" id="fournisseur" name="fournisseur"  value="{{ $fournisseur->raison_sociale }}" readonly>
                    <input type="hidden" class="form-control" id="fournisseur_id" name="fournisseur_id"  value="{{ $fournisseur->id }}">
                  </div>


                
                  <div class="form-group ">
                      <label for="image_devis" >Image Devis</label>
                      <input name="image_devis" type="file" id="image_devis" value="" >
                      @if(!empty($boncommand->image_devis))
                      <a  href="{{ URL::asset(  'storage/'.$boncommand->image_devis  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$boncommand->image_devis  ) }}" alt=""/></a>    
                      @endif
                  </div>

                <div class="form-group">
                    <label for="gsm">TVA (%)</label>
                    <input type="number"  step=any  class="form-control" id="tva" name="tva" placeholder="TVA">
                      {!! $errors->first('tva', '<p class="text-danger ">:message</p>') !!}

                    </div>

               </div>

               <div class="col-md-6">

                  <div class="form-group">
                      <label for="gsm">Reference bon de command</label>
                      <input type="text" class="form-control" id="ref_bc" name="ref_bc" placeholder="reference bon de command" value="{{ $ref_bc }}">
                      {!! $errors->first('ref_bc', '<p class="text-danger ">:message</p>') !!}
                   </div>

                 
                  
                   <div class="form-group">
                      <label for="ref_devis">Reference Devis</label>
                      <input type="text" class="form-control" id="ref_devis" name="ref_devis" placeholder="Reference Devis" value="">
                      {!! $errors->first('ref_bc', '<p class="text-danger ">:message</p>') !!}
                  </div>
    
   

                   

                </div>

            
              </div>

          