
              <div class="box-body">

                <div class="col-md-6">

                    <div class="form-group">
                        <label for="prix_total">Fournisseur</label>
                        <input type="text" class="form-control" id="fournisseur_id" name="fournisseur_id"  value="{{$boncommand->fournisseur->raison_sociale}}" disabled>
                    </div>
                   

                    <div class="form-group">
                        <label for="date_bc">Date bon de commande</label>
                        <input type="date" class="form-control" id="date_bc" name="date_bc" placeholder="Date bon de commande" v-model="date_bc" required>
                    </div>

                    <div class="form-group ">
                        <label for="image_devis" >Image Devis</label>
                        <input name="image_devis" type="file" id="image_devis" ref="image_devis" v-on:change="handleFileUpload()" {{empty($boncommand->image_devis) ? 'required' : ''}}>
                        @if(!empty($boncommand->image_devis))
                        <a  href="{{ URL::asset(  'storage/'.$boncommand->image_devis  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$boncommand->image_devis  ) }}" alt=""/></a>
                        @endif

                    </div>

 

                </div>
                <div class="col-md-6">

                    <div class="form-group">
                    <label for="ref_bc">Reference bon de commande</label>
                    <input type="text" class="form-control" id="ref_bc" name="ref_bc" placeholder="reference bon de command" value="{{ old('ref_bc', optional($boncommand)->ref_bc) }}" disabled>
                    </div>

                    <div class="form-group">
                      <label for="ref_devis">Reference Devis</label>
                      <input type="text" class="form-control" id="ref_devis" name="ref_devis" placeholder="Reference Devis" v-model="ref_devis">
                      {!! $errors->first('ref_devis', '<p class="text-danger ">:message</p>') !!}
                  </div>

                </div>


              </div>

