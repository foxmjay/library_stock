
<div class="box-body">

    <div class="col-md-6">


        <div class="form-group ">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" v-model="produitModel.nom">
        </div>

        <div class="form-group ">
            <label for="marque">Marque</label>
            <input type="text" class="form-control" id="marque" name="marque" placeholder="Marque" v-model="produitModel.marque">
        </div>


        <div class="form-group ">
            <label for="ref_interne">Ref interne</label>
            <input type="text" class="form-control" id="ref_interne" name="ref_interne" placeholder="ref_interne" v-model="produitModel.ref_interne">
        </div>


        <div class="form-group ">
            <label for="ref_constructeur">Ref constructeur</label>
            <input type="text" class="form-control" id="ref_constructeur" name="ref_constructeur" placeholder="ref_constructeur" v-model="produitModel.ref_constructeur">
        </div>


    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Description" v-model="produitModel.description">
        </div>

        <div class="form-group ">
            <label for="categorie_id">Categorie</label>
            <select class="form-control" name="categorie_id" id="categorie_id" v-model="produitModel.categorie_id" >
                @foreach ($categories as $object)
                <option value="{{$object->id}}">{{$object->categorie}} -- {{$object->s_categories}} </option>
                @endforeach
            </select>


        </div>

        <div class="form-group">
            <label for="garantie">Garantie</label>
            <input type="number" class="form-control" id="garantie" name="garantie" placeholder="Garantie" v-model="produitModel.garantie">
        </div>


        

    </div>


</div>


