@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->



    <section class="content-header">
      <h1>
        Bon de livraison
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de livraison</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Details</h3>
                      </div> 
                      <div  class="box-body">
                          

                            <div class="col-xs-4">
                              <div class="form-group">
                                  <label for="categorie">Type</label>
                                    <select class="form-control" name="bl_type" id="bl_type" v-model="bl_type" disabled>
                                        <option v-for="bl in bl_typeList" v-bind:value="bl.type">@{{bl.type}}</option>
                                    </select>
                              </div>
                            </div>

                            <div class="col-xs-4">
                                <div class="form-group">
                                <label for="categorie">Reference bon de livraison</label>
                                <input type="text" class="form-control" id="ref_bl" name="ref_bl" v-model="ref_bl" disabled >
                                </div>
                            </div>

                            <div class="col-xs-4">
                                <div class="form-group">
                                <label for="categorie">Image bon de livriason</label>

                                <br>
                                  <a v-bind:href="image_bl" > <img v-bind:src="image_bl" width="40" /> </a>
                                </div>
                            </div>


                      </div>

                      <div  class="box-body">
                          
                        <div class="col-xs-5"></div>
                        <div class="col-xs-2">
                            <div class="form-group">
                            <label for="categorie">Reference Devis :</label>
                              <span>{{$boncommand->ref_devis}}</span>
                            </div>
                        </div>
                        <div class="col-xs-5"></div>

                      </div>
    
                 </div>
    
          </div>
              <!-- /.col -->



          <div class="col-xs-12">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Produits</h3>
              </div>        

            <div  class="box-body table-responsive no-padding">
              <table id="produitList" class="table">
              <tr>
                <th>ID</th>
                <th>Produit</th>
                <th>Ref interne</th>
                <th>Quantite</th>
                <th>Prix unitaire HT</th>
                <th>Prix total HT</th>
              </tr>
              <tr v-for="(achat,index) in achats"  v-bind:id="'produitid'+achat.id">
                    <td>@{{achat.id}}</td>
                    <td>@{{achat.nom}}</td>
                    <td>@{{achat.ref_interne}}</td>
                    <td>@{{achat.quantite}}</td>
                    <td>@{{achat.prix_unitaire}}</td>
                    <td>@{{achat.prix_total}}</td>
                 
              </tr>

              </table>
          </div>

        </div>

          </div>
          <!-- /.col -->
         
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits selectiones</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Produit</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Ref fournisseur</th>
                        <th>Prix unitaire HT</th>
                        
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td>@{{stock.id}}</td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td>  
                    <td>@{{stock.ref_fournisseur}}</td> 
                    <td>@{{stock.prix_unitaire}}</td>
   
                    
                </tr>
  
                </table>

                <br>
                <br>
                <hr>
                  <h2 class="text-right" style="padding-right: 5%"><b>Prix total HT: </b>  @{{ prix_total | numeral('0.2') }} DH</h2>
                <hr> 

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->
       

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
              
                <button type="button" class="btn btn-app pull-left bg-yellow" v-on:click="annuler()">
                    <i class="fa fa-close"></i>Retour
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var bc_id = {!! json_encode($boncommand->id) !!};
var bl = {!! json_encode($bl) !!};
var fournisseur_id = {!! json_encode($fournisseur->id) !!};
var bl_type = {!! json_encode($boncommand->type ) !!};

var stocks = {!! json_encode($stocks) !!};

$('#stockSection').hide();

var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {
    bl_type : bl_type,
    type_search : 'Serialisable',
    keyword : '',
    bl_typeList : [{'type':'Partiel'},{'type':'Complet'}],
    achats: stocks,
    selectedStocks: stocks,
    items: [],
    stocks: [],
    prix_total : bl.prix_total,
    selectedProduit : '',
    image_bl : '/storage/'+bl.image_bl,
    ref_bl : bl.ref_bl ,
    num_serie: '',
    ref_fournisseur: '',
   },

  mounted: function mounted() {
    this.getVueItems();
  },
    
  methods: {

    




    getVueItems: function getVueItems() {
      var _this = this;
      axios.get('/achats/'+bc_id+'/perbc').then(function (response) {
        _this.achats = response.data;
        
      });

    },

    

    annuler: function annuler(){
      //history.back();
      window.location.href ="/fournisseurs/"+fournisseur_id
    },

  }
});



</script>

@endsection
