@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->


    <section class="content-header">
      <h1>
        Bon de livraison
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Numero de facture</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12 col-md-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Confirmation</h3>
                      </div> 
                      <div  class="box-body">
                          <div class="row" >

                              <div class="col-xs-2"></div>
   
                               <div class="col-xs-4">
                                 <div class="form-group">
                                 <label for="categorie">Type reference facture</label>
                                   <select class="form-control" name="type_ref_facture" id="type_ref_facture" v-model="type_ref_facture" >
                                       @foreach (['Automatique','Manuel'] as $object)
                                       <option value="{{$object}}">{{$object}}</option>
                                       @endforeach
                                   </select>
                                 </div>
                               </div>
   
                               <div class="col-xs-6">
                                 <div v-if="type_ref_facture == 'Manuel'" class="col-xs-4">
                                   <div class="form-group">
                                   <label for="ref_facture">Numero de facture</label>
                                     <input style="border-color: red" class="form-control" type="text" style="min-width: 200px" id="ref_facture" v-model="ref_facture" name="ref_facture" placeholder="2020-00018" v-on:keyup="verifRef">
                                   </div>
                               </div>
                             </div>
   
   
                           </div>

                      </div>
                 </div>
    
          </div>
              <!-- /.col -->

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Confirmer
                </button> 
               
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var devis_id = {!! json_encode($bl->devis_id) !!};
var prix_total = {!! json_encode($bl->prix_total) !!};
var client_id = {!! json_encode($client_id) !!};


var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {
    devis_id : devis_id,
    prix_total : prix_total,
    type_ref_facture : 'Automatique',
    ref_facture: '',
   },

    
  methods: {

    verifRef: function  verifRef(){
      if( this.ref_facture.match(/^\d{4}-\d{5}$/) !=null){
        $('#ref_facture').css('border-color','green')
      }else{
        $('#ref_facture').css('border-color','red')
      }
    },

    confirmer: function confirmer(){

      var _this = this;
      //var data = this.echeanceModel;

      if(this.type_ref_facture == 'Manuel'){
        if( this.ref_facture.match(/^\d{4}-\d{5}$/)==null){
          alert("Numero de facture est invalide");
          return;
        }
      }

      enableSpinner();

      var data = JSON.stringify( {'devis_id':this.devis_id,'prix_total':this.prix_total,'type_ref_facture': this.type_ref_facture, 'ref_facture': this.ref_facture});
      
      axios.post('/bonlivraison/storeFacture',{data: data}).then(function (response){
        //console.log(response.data);
        if(response.data['result'] == "OK"){
          console.log(response.data['result']);
          //disableSpinner();
          window.location.href = "/clients/"+client_id;
          
        }

        if(response.data['result'] == "KO"){
          //console.log(response.data['result']);
          disableSpinner();
          if(response.data.hasOwnProperty('facture')){
            alert("Le numero de facture "+response.data['facture']+" existe deja .");
          }

        }

        
      }).catch(function (error){
        console.log(error)
      })

   }

  }
});

</script>

@endsection
