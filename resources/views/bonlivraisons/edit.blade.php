@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de livraison
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de livraison</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Type</h3>
                      </div> 
                      <div  class="box-body">
                          
                            <div class="col-xs-5"></div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                  <select name="bl_type" id="bl_type" v-model="bl_type">
                                      @foreach (['Normal','Teste','Provisoire','Cadeau'] as $object)
                                      <option value="{{$object}}">{{$object}}</option>
                                      @endforeach
                                  </select>
                                </div>
                            </div>
                            <div class="col-xs-5"></div>


                  
                      </div>

                      <div  class="box-body">
                          
                        <div class="col-xs-5"></div>
                        <div class="col-xs-2">
                            <div class="form-group">
                            <label for="categorie">N Bon commande :</label>
                              <input type="text" class="form-control" id="num_bc" name="num_bc" value="" v-model="num_bc">
                            </div>
                        </div>
                        <div class="col-xs-5"></div>

                      </div>
    
    
                 </div>
    
          </div>
              <!-- /.col -->



          <div class="col-xs-12">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Produits</h3>
              </div>        

            <div  class="box-body table-responsive no-padding">
              <table id="produitList" class="table">
              <tr>
                <th>ID</th>
                <th>Produit</th>
                <th>Ref interne</th>
                <th>Quantite</th>
                <th>Prix unitaire HT</th>
                <th>Remise</th>
                <th>Prix total HT</th>
                <th></th>
              </tr>
              <tr v-for="(vente,index) in ventes" v-on:click="selectedProduct(vente.id, vente.quantite, vente.prix_unitaire, vente.remise, vente.produit_id)" v-bind:id="'produitid'+vente.id">
                    <td>@{{vente.id}}</td>
                    <td>@{{vente.nom}}</td>
                    <td>@{{vente.ref_interne}}</td>
                    <td>@{{vente.quantite}}</td>
                    <td>@{{vente.prix_unitaire}}</td>
                    <td>@{{vente.remise}}</td>
                    <td>@{{vente.prix_total}}</td>
                  <td>
                          <!--<button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="selectVente(index)">
                              <span class="fa fa-arrow-circle-down" aria-hidden="true"></span>
                          </button>-->
                  </td>
              </tr>

              </table>
          </div>

        </div>

          </div>
          <!-- /.col -->



          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Stock</h3>
                </div>       

  
                <div  class="box-body table-responsive no-padding">
                    <table  id="stockList" class="table ">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Fournisseur</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(stock,index) in stocks" v-if="ifSelected(stock.id)" >
                        <td>@{{stock.id}}</td>
                        <td>@{{stock.nom}}</td>
                        <td>@{{stock.marque}}</td>
                        <td>@{{stock.ref_interne}}</td>
                        <td>@{{stock.num_serie}}</td>                                  
                        <td>@{{stock.raison_sociale}}</td> 
                        <td>
                                <button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="selectStock(index,stock.produit_id)">
                                    <span class="fa fa-arrow-circle-down" aria-hidden="true"></span>
                                </button>
                        </td>
                    </tr>
                  </tbody>
      
                    </table>
                    
                </div>
  
          </div>
  
            </div>
            <!-- /.col -->

          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits selectiones</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Prix vente</th>
                        <th></th>
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td>@{{stock.id}}</td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td>   
                    <td>@{{stock.prix_vente}}</td>   
                    <td>
                            <button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="removeStock(index , stock.produit_id)">
                                <span class="fa fa-arrow-circle-up" aria-hidden="true"></span>
                            </button>
                    </td>
                </tr>
  
                </table>

                <br>
                <br>
                <hr>
                  <h2 class="text-right" style="padding-right: 5%"><b>Prix total HT : </b>  @{{ prix_total | numeral('0.2') }} DH</h2>
                <hr> 

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Confirmer
                </button> 
                <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                    <i class="fa fa-close"></i>Annuler
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var devis_id = {!! json_encode($devis_id) !!};
var bl_ventes = {!! json_encode($ventes) !!};
var bl_id = {!! json_encode($bl->id) !!};
var bl_type = {!! json_encode($bl->type) !!};
var num_bc = {!! json_encode($bl->num_bc) !!};
var prix_total = {!! json_encode($bl->prix_total) !!};
var client_id = {!! json_encode($client_id) !!};

var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {
    ventes: [],
    selectedStocks: bl_ventes,
    items: [],
    stocks: [],
    prix_total : prix_total,
    bl_type : bl_type,
    selectedVenteId : 0,
    selectedProduitId : 0,
    selectedProduitQuantite : 0,
    selectedProduitPrixVente : 0,
    selectedProduitRemise : 0,
    num_bc: num_bc,
   },

  mounted: function mounted() {
    this.getVueItems();
  },
    
  methods: {

    ifSelected: function ifSelected(stockid){

      var exists = true;
      this.selectedStocks.forEach(function(element){
         
         if(element.id == stockid)
          exists = false;
      })
      
       return exists;

    },

    selectedProduct: function selectedProduct(id, quantite, prixVente, remise, produit_id){
      $('#produitList tr').removeClass('highlight');
      $('#produitid'+id).addClass('highlight');
      this.selectedVenteId = id;
      this.selectedProduitId = produit_id;
      this.selectedProduitQuantite = quantite;
      this.getQuantite(id, produit_id);
      this.selectedProduitPrixVente = prixVente;
      this.selectedProduitRemise = remise;
      this.getStock(id);
    },

    getStock: function getStock(vente_id){
      _this = this;
      axios.get('/produits/'+vente_id+'/getstock').then(function (response) {
         _this.stocks = response.data;
      });
    
    },

    getQuantite: function getQuantite(vente_id,produit_id){
      _this = this;

      var data = JSON.stringify( { 'vente_id' : vente_id ,'devis_id': devis_id ,'produit_id': produit_id});
      axios.post('/ventes/getquantite',{data: data}).then(function (response){
        if(response.data['result'] == "OK"){
          //console.log(response.data);
          //if( response.data['quantite'] <= this.selectedProduitQuantite)
           _this.selectedProduitQuantite = _this.selectedProduitQuantite - response.data['quantite'];
           //console.log(_this.selectedProduitQuantite);
                        
        }
        
      }).catch(function (error){
        console.log(error)
      })
    
    },

    getVueItems: function getVueItems() {
      var _this = this;
      axios.get('/ventes/'+devis_id+'/perdevis').then(function (response) {
        _this.ventes = response.data;
        
      });

    },

    selectStock: function selectStock(stockid,produit_id){

     
     //console.log(this.selectedProduitQuantite);
     produitCount = 0;
     this.selectedStocks.forEach(function(element){
        if(element.produit_id == produit_id)
         produitCount+=1;

     });

     if(produitCount >= this.selectedProduitQuantite){
       alert('Quantite atteinte pour ce produit');
       return;
     }

     this.stocks[stockid]['vente_id'] = this.selectedVenteId;
     this.stocks[stockid]['remise'] = this.selectedProduitRemise;

     index = this.selectedStocks.push(this.stocks[stockid]);

     this.prix_total += this.selectedProduitPrixVente - (  this.selectedProduitPrixVente * this.selectedProduitRemise /100);;
     this.stocks.splice(stockid,1);
    },

    removeStock: function removeStock(index, produit_id){

      var prix_vente =0;
      var remise = 0
      this.ventes.forEach(function(element){
         
           if(produit_id == element.produit_id){
             prix_vente = element.prix_unitaire;
             remise = element.remise;
           }
      });
      if(this.selectedProduitId == produit_id )
        this.stocks.push(this.selectedStocks[index]);
      this.prix_total -= prix_vente - (  prix_vente * remise/100);
      this.selectedStocks.splice(index,1);
    },

    annuler: function annuler(){
      //history.back();
      window.location.href ="/clients/"+client_id;
    },

    confirmer: function confirmer(){

      //var _this = this;
      //var data = this.echeanceModel;

      if(this.selectedStocks.length <= 0 ){
        alert("Selectioner au moin un produit");
        return;
      }

      if(this.bl_type == ""){
        alert("Selectioner le type du bon du livraison");
        return;
      }

      var data = JSON.stringify( { 'stocks' : this.selectedStocks ,'bl_type': this.bl_type, 'num_bc': this.num_bc });

      axios.post('/bonlivraison/'+bl_id+'/update',{data: data}).then(function (response){
        if(response.data['result'] == "OK"){
          window.location.href = "/clients/"+client_id;
        }

        if(response.data['result'] == "KO"){
          var stock=response.data['stock']
          alert("Le produit "+stock.nom+" numero serie : "+stock.num_serie+" est non disponible dans le stock");
          //console.log(stock);
          _this.selectedStocks.forEach(function(element,index){
            //console.log(index);
            if(element.id == stock.id){
              _this.selectedStocks.splice(index,1);
            }

          });
          _this.getStock(stock.vente_id);
        }
        
      }).catch(function (error){
        console.log(error)
      })

   }

  }
});



</script>

@endsection
