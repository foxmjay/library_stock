@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de livraison
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de livraison</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Details</h3>
                      </div> 
                      <div  class="box-body">
                          
                            <div class="col-xs-5"></div>
                            <div class="col-xs-2">
                                <div class="form-group">
                                  <label for="categorie">Type : </label>
                                  <span >@{{bl_type}}</span>
                                 
                                </div>
                            </div>
                            <div class="col-xs-5"></div>


                  
                      </div>

                      <div  class="box-body">
                          
                        <div class="col-xs-5"></div>
                        <div class="col-xs-2">
                            <div class="form-group">
                            <label for="categorie">N Bon commande :</label>
                            <span >{{$num_bc}}</span>
                            </div>
                        </div>
                        <div class="col-xs-5"></div>

                      </div>
    
    
                 </div>
    
          </div>
              <!-- /.col -->

          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Prix vente HT</th>
                        <th>remise</th>
                        <th>Prix HT</th>
                       
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td> 
                    <td>@{{stock.prix_vente}}</td>  
                    <td>@{{stock.remise}} %</td>  
                    <td>@{{ stock.prix_vente - (stock.prix_vente*stock.remise/100) }} DH</td>
                       
                    
                </tr>
  
                </table>

                <br>
                <br>
                <hr>
                  <h2 class="text-right" style="padding-right: 5%"><b>Prix total HT : </b>  @{{ prix_total | numeral('0.2') }} DH</h2>
                <hr> 

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-left bg-orange" v-on:click="annuler()">
                    <i class="fa fa-arrow-circle-o-left "></i>Retour
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var devis_id = {!! json_encode($devis_id) !!};
var bl_ventes = {!! json_encode($ventes) !!};
var bl_id = {!! json_encode($bl->id) !!};
var bl_type = {!! json_encode($bl->type) !!};
var prix_total = {!! json_encode($bl->prix_total) !!};
var client_id = {!! json_encode($client_id) !!};

var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {
 
    selectedStocks: bl_ventes,
    prix_total : prix_total,
    bl_type : bl_type,
   },

 
    
  methods: {


    annuler: function annuler(){
      //history.back();
      window.location.href ="/clients/"+client_id;
    },

    
  }
});



</script>

@endsection
