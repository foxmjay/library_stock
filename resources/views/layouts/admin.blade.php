<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Advanced</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="{{ URL::asset('assets/admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/skins/_all-skins.min.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/morris.js/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
    <!-- Date Picker
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
    <!-- Daterange picker
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">-->
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
        href="{{ URL::asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <link rel="stylesheet"
        href="{{ URL::asset('assets/admin/bower_components/datetimepicker/jquery.datetimepicker.min.css')}}" />

    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png')}}" />

    <link rel="stylesheet" href="{{ URL::asset('assets/admin/plugins/loader/loader-default.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .table td {
            text-align: center;
        }

        .table th {
            text-align: center;
        }
    </style>
    @yield('css')

</head>

<body class="skin-blue sidebar-collapse sidebar-mini" style="height: auto; min-height: 100%;">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="/" class="logo" style="background-color: white">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>G</b>/S</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"> <img src="{{ URL::asset('assets/images/advnced_alpha.png')}}"> </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        @php $mgs = DB::select("select users.name,messages.* FROM messages,users WHERE WEEK(SYSDATE()) =
                        WEEK(messages.created_at) and id_distinataire = ".Auth::id()." and users.id =
                        messages.id_createur ORDER BY messages.created_at desc"); @endphp
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success">{{count($mgs)}}</span>
                            </a>
                            <ul class="dropdown-menu">


                                <li class="header">Messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        @foreach ($mgs as $item)
                                        <li>
                                            <!-- start message -->
                                            <a href="{{ url('/messages/' . $item->id) }}">
                                                <div class="pull-left">
                                                    {{$item->name}}
                                                </div>
                                                <h4>
                                                    {{str_limit($item->objet,10)}}

                                                </h4>
                                                <p>{{str_limit(strip_tags($item->message),20)}}</p>
                                            </a>
                                        </li>
                                        @endforeach

                                        <!-- end message -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <!-- <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li> -->
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!-- <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                                <li>
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Design some buttons
                                                    <small class="pull-right">20%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-aqua" style="width: 20%"
                                                        role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                        <span class="sr-only">20% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li> -->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ URL::asset('assets/admin/dist/img/user.png')}}" class="user-image"
                                    alt="User Image">
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ URL::asset('assets/admin/dist/img/user.png')}}" class="img-circle"
                                        alt="User Image">

                                    <p>
                                        {{ Auth::user()->name }}

                                        @php $userProfile =
                                        \App\UserProfile::where('user_id',Auth::user()->id)->first(); @endphp

                                        <small> {{  $userProfile->type }} </small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                        @if ( $userProfile->type == "Admin" )
                                        <div class="col-xs-4 text-center">
                                            <a href="{{ url('/parameters/edit') }}">Parameters</a>
                                        </div>
                                        @else
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Options</a>
                                        </div>
                                        @endif
                                        <div class="col-xs-4 text-center">
                                            @if ($userProfile->type == "SuperAdmin")
                                            <a href="/backup">Sql BackUp</a>
                                            @endif

                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Options</a>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                                            <i class="icon-logout menu-icon"></i>
                                            <span class="menu-title">Deconnexion</span>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{ URL::asset('assets/admin/dist/img/user.png')}}" class="img-circle"
                            alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>{{ Auth::user()->name }}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> {{ $userProfile->type }} </a>
                    </div>
                </div>
                <!-- search form
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU</li>
                    <li class="treeview">
                        <!--<a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>-->



                @if ($userProfile->type != "Teleoperateur" )
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">
                       <i class="fa fa-street-view"></i>  <span>Accueil</span></a>
                    </li>

                    <li class="{{ Request::is('clients*') ? 'active' : '' }}"><a href="{{ route('clients.index') }}">
                       <i class="fa fa-street-view"></i> <span>Clients</span></a>
                    </li>

                    <li class="{{ Request::is('ventedirect*') ? 'active' : '' }}"><a href="{{ url('ventedirect') }}">
                        <i class="fa fa-street-view"></i> <span>Vente direct</span></a>
                    </li>

                    <li class="{{ Request::is('pointventes*') ? 'active' : '' }}"><a href="{{ url('pointventes') }}">
                        <i class="fa fa-street-view"></i> <span>Point de vente</span></a>
                    </li>



                        @if ($userProfile->type == "Admin" || $userProfile->type == "Assistant")

                                <!-- <li class="{{ Request::is('fournisseurs*') ? 'active' : '' }}"><a
                                        href="{{ route('fournisseurs.index') }}"><i class="fa fa-ship"></i>
                                        <span>Fournisseurs</span></a></li> -->

                                    <li class="treeview {{ Request::is('fournisseurs*') || Request::is('echange*') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-files-o"></i>
                                        <span>Fournisseurs</span>
                                        <span class="pull-right-container">
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('fournisseurs*') ? 'active' : '' }}"><a href="{{ route('fournisseurs.index') }}"><i class="fa fa-circle-o"></i> Fournisseurs</a></li>
                                        <li class="{{ Request::is('echange*') ? 'active' : '' }}"><a href="{{ url('echange/fournisseur') }}"><i class="fa fa-circle-o"></i>Echange</a></li>
                                    </ul>
                                </li>




                                <li class="treeview {{ Request::is('produits*') || Request::is('categories*') ? 'active' : '' }}">
                                        <a href="#">
                                            <i class="fa fa-barcode"></i>
                                            <span>Produits</span>
                                            <span class="pull-right-container">
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="{{ Request::is('produits*') ? 'active' : '' }}"><a href="{{ route('produits.index') }}"><i class="fa fa-circle-o"></i> Produits</a></li>
                                            <li class="{{ Request::is('categories*') ? 'active' : '' }}"><a href="{{ route('categories.index') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
                                        </ul>
                                </li>

                                <li class="treeview {{ Request::is('stock*') || Request::is('etatstock*') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-files-o"></i>
                                        <span>Stock</span>
                                        <span class="pull-right-container">
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('stock*') ? 'active' : '' }}"><a href="{{ route('stock.index') }}"><i class="fa fa-circle-o"></i> Stock</a></li>
                                        <li class="{{ Request::is('etatstock*') ? 'active' : '' }}"><a href="{{ url('etatstock') }}"><i class="fa fa-circle-o"></i> Etat de stock</a></li>
                                    </ul>
                                </li>



                                <li class="{{ Request::is('depenses*') ? 'active' : '' }}"><a
                                        href="{{ route('depenses.index') }}"><i class="fa  fa-money"></i> <span>Gestion de caisse</span></a></li>

                        @endif <!-- endif assistant & admin-->

                        @if ($userProfile->type == "Admin" )
                        <li class="header">GESTION UTILISATEURS</li>
                        <li class="{{ Request::is('utilisateurs*') ? 'active' : '' }}"><a
                                href="{{ route('utilisateurs.index') }}"><i class="fa  fa-group"></i>
                                <span>Utilisateurs</span></a></li>
                        <li class="{{ Request::is('chiffre-affaires*') ? 'active' : '' }}"><a
                                href="{{ route('chiffre-affaires.index') }}"><i class="fa  fa-calculator"></i>
                                <span>Chiffre d'affaires</span></a></li>
                        @endif

                        @if ($userProfile->type == "Commercial" || $userProfile->type == "Admin")
                        <li class="header">NOTIFICATIONS</li>

                        <li class="{{ Request::is('messages*') ? 'active' : '' }}"><a href="{{ route('messages.index') }}">
                                <i class="fa fa-envelope-o"></i></i> <span>Messages</span>
                                <span class="pull-right-container">
                                    <small class="label pull-right bg-red">{{count($mgs)}}</small>
                                </span>
                            </a></li>
                        <li class="{{ Request::is('notifsrdv*') ? 'active' : '' }}"><a
                                href="{{ route('notifsrdv.notifs') }}"><i class="fa fa-bell"></i> <span>Notifications
                                    RDVs</span>
                                <span class="pull-right-container">
                                    @if( \App\Rdv::rdvNotifsCount() > 0 )<small
                                        class="label pull-right bg-red">{{\App\Rdv::rdvNotifsCount()}}</small> @endif
                                </span>
                            </a></li>

                        @endif

                    @endif

                    @if ($userProfile->type == "Teleoperateur" )
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/"><i class="fa fa-street-view"></i>
                            <span>Accueil</span></a></li>
                    <li class="{{ Request::is('rdvs*') ? 'active' : '' }}"><a href="{{ route('rdvs.index') }}"><i
                                class="fa fa-calendar"></i> <span>RDVs</span></a></li>
                    @endif

                    @if ($userProfile->type == "Admin" )

                    <li class="{{ Request::is('rdvs*') ? 'active' : '' }}"><a href="{{ route('rdvs.index') }}"><i
                                class="fa fa-calendar"></i> <span>RDVs</span></a></li>
                    @endif

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->
        <div class="loader loader-default" id="loader-spinner"></div>
        @yield('content')

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 0.1
            </div>
            <strong>Copyright &copy; 2019 <a href="" target="_blank"></a>.</strong> All rights
            reserved.
        </footer>


    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="{{ URL::asset('assets/admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ URL::asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ URL::asset('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('assets/admin/bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::asset('assets/admin/dist/js/adminlte.min.js')}}"></script>

    <script src="{{ URL::asset('assets/admin/bower_components/datetimepicker/jquery.datetimepicker.full.min.js')}}">
    </script>

    <script>

        function enableSpinner(){
            $('#loader-spinner').addClass('is-active');
        }


        function disableSpinner(){
            $('#loader-spinner').removeClass('is-active');
        }

    </script>

    <!-- AdminLTE for demo purposes
<script src="{{ URL::asset('assets/admin/dist/js/demo.js')}}"></script>-->
    <!--<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>-->
    @yield('js')
</body>

</html>
