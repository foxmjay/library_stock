@extends('layouts.admin')

@section('content')
@extends('layouts.admin')
                <div class="card">
                    <div class="card-header">Vente {{ $vente->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/vente') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/vente/' . $vente->id . '/edit') }}" title="Edit Vente"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('vente' . '/' . $vente->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Vente" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $vente->id }}</td>
                                    </tr>
                                    <tr><th> Client Id </th><td> {{ $vente->client_id }} </td></tr><tr><th> Produit Id </th><td> {{ $vente->produit_id }} </td></tr><tr><th> Quantite </th><td> {{ $vente->quantite }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                </div>

@endsection
