@extends('layouts.admin')

@section('content')
@extends('layouts.admin')
                <div class="card">
                    <div class="card-header">Achat {{ $achat->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/achat') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/achat/' . $achat->id . '/edit') }}" title="Edit Achat"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('achat' . '/' . $achat->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Achat" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $achat->id }}</td>
                                    </tr>
                                    <tr><th> Fournisseur Id </th><td> {{ $achat->fournisseur_id }} </td></tr><tr><th> Produit Id </th><td> {{ $achat->produit_id }} </td></tr><tr><th> Quantite </th><td> {{ $achat->quantite }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                </div>

@endsection
