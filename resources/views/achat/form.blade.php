<div class="form-group {{ $errors->has('fournisseur_id') ? 'has-error' : ''}}">
    <label for="fournisseur_id" class="control-label">{{ 'Fournisseur' }}</label>
    @if ($formMode === 'edit')
    <select class="form-control" name="fournisseur_id" required>
        @foreach ($fournisseur as $object)
        @if ($object->id === $achat->client_id)
        <option value="{{$object->id}}" slected>{{$object->raison_sociale}}</option>
        @else
        <option value="{{$object->id}}">{{$object->raison_sociale}}</option>
        @endif
        @endforeach

    </select>
    @else
    <select class="form-control" name="fournisseur_id" required>
        @foreach ($fournisseur as $object)

        <option value="{{$object->id}}">{{$object->raison_sociale}}</option>

        @endforeach

    </select>
    @endif
    {!! $errors->first('fournisseur_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('produit_id') ? 'has-error' : ''}}">
    <label for="produit_id" class="control-label">{{ 'Produit' }}</label>
    @if ($formMode === 'edit')
    <select class="form-control" name="produit_id" required>
        @foreach ($produit as $object)

        @if ($object->id === $achat->produit_id)
        <option value="{{$object->id}}" slected>{{$object->nom}}</option>
        @else
        <option value="{{$object->id}}">{{$object->nom}}</option>
        @endif


        @endforeach
    </select>

    @else
    <select class="form-control" name="produit_id" required>
        @foreach ($produit as $object)
        <option value="{{$object->id}}">{{$object->nom}}</option>
        @endforeach

    </select>
    @endif
    {!! $errors->first('produit_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('quantite') ? 'has-error' : ''}}">
    <label for="quantite" class="control-label">{{ 'Quantite' }}</label>
    <input class="form-control" name="quantite" type="number" id="quantite" value="{{ $formMode === 'edit' ? $achat->quantite : ''}}" >

    {!! $errors->first('quantite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('prix_unitaire') ? 'has-error' : ''}}">
    <label for="prix_unitaire" class="control-label">{{ 'Prix Unitaire HT' }}</label>
    <input class="form-control" name="prix_unitaire" type="number" id="prix_unitaire" value="{{ $formMode === 'edit' ? $achat->prix_unitaire : ''}}" >

    {!! $errors->first('prix_unitaire', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('prix_total') ? 'has-error' : ''}}">
    <label for="prix_total" class="control-label">{{ 'Prix Total HT' }}</label>
    <input class="form-control" name="prix_total" type="number" id="prix_total" value="{{ $formMode === 'edit' ? $achat->prix_total : ''}}" >

    {!! $errors->first('prix_total', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date_achat') ? 'has-error' : ''}}">
    <label for="date_achat" class="control-label">{{ 'Date Achat' }}</label>
    <input class="form-control" name="date_achat" type="date" id="date_achat" value="{{ $formMode === 'edit' ? $achat->date_achat : ''}}" >

    {!! $errors->first('date_achat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image_facture') ? 'has-error' : ''}}">
    <label for="image_facture" >{{ 'Image Facture' }}</label>
    <input  name="image_facture" type="file" id="image_facture" value="{{ $formMode === 'edit' ? $achat->image_facture : ''}}" >
    @if($formMode === 'edit')
        <a class="navbar-brand brand-logo" href="{{ $formMode === 'edit' ? URL::asset(  'storage/'.$achat->image_facture  ) : ''}}" target="_blank"><img height="50px" src="{{ $formMode === 'edit' ? URL::asset('storage/'.$achat->image_facture  ) : ''}}" alt="image"/></a>
    @endif

    {!! $errors->first('image_facture_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type_paiment') ? 'has-error' : ''}}">
    <label for="type_paiment" class="control-label">{{ 'Type Paiment' }}</label>
    <select class="form-control" name="type_paiment" required>

        <option value="Espece" selected>Espece</option>
        <option value="Cheque" >Cheque</option>


    </select>
    {!! $errors->first('type_paiment', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date_cheque') ? 'has-error' : ''}}">
    <label for="date_cheque" class="control-label">{{ 'Date Cheque' }}</label>
    <input class="form-control" name="date_cheque" type="date" id="date_cheque" value="{{ $formMode === 'edit' ? $achat->date_cheque : ''}}" >

    {!! $errors->first('date_cheque', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image_cheque') ? 'has-error' : ''}}">
    <label for="image_cheque" >{{ 'Image Cheque' }}</label>
    <input name="image_cheque" type="file" id="image_cheque" value="{{ $formMode === 'edit' ? $achat->image_cheque : ''}}" >
    @if($formMode === 'edit')
        <a class="navbar-brand brand-logo" href="{{ $formMode === 'edit' ? URL::asset(  'storage/'.$achat->image_cheque  ) : ''}}" target="_blank"><img height="50px" src="{{ $formMode === 'edit' ? URL::asset('storage/'.$achat->image_cheque  ) : ''}}" alt="image"/></a>    
    @endif

    {!! $errors->first('image_cheque_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('avance') ? 'has-error' : ''}}">
    <label for="avance" class="control-label">{{ 'Avance' }}</label>
    <input class="form-control" name="avance" type="number" id="avance" value="{{ $formMode === 'edit' ? $achat->avance : '0'}}" >

    {!! $errors->first('avance', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('etat') ? 'has-error' : ''}}">
    <label for="etat" class="control-label">{{ 'Etat' }}</label>
    <select class="form-control" name="etat" required>

        <option value="En cours" selected>En cours</option>
        <option value="Termine" >Termine</option>


    </select>
    {!! $errors->first('etat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date_complete') ? 'has-error' : ''}}">
    <label for="date_complete" class="control-label">{{ 'Date Complete' }}</label>
    <input class="form-control" name="date_complete" type="date" id="date_complete" value="{{ $formMode === 'edit' ? $achat->date_complete : ''}}" >

    {!! $errors->first('date_complete', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
