@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->


@endsection

@section('content')

<div class="content-wrapper" style="min-height: 926px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stats
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">


        <!-- Main row -->
        <div class="row">

            <!-- Left col -->
            <div class="col-md-6">


                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cheque Décaissement Alerts</h3>

                        <div class="box-tools pull-right">

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Date Cheque</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($echeances as $echeance)
                                    @if ($echeance->bc_id)
                                    <tr class="bg-danger">
                                        <td>{{$echeance->date_cheque}}</td>
                                        <td>
                                            <a type="button" class="btn btn-warning" title="Ancien devis"
                                                href="echeances/{{$echeance->bc_id}}/indexdevis/bc">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


            <!-- Left col -->
            <div class="col-md-6">


                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cheque Encaissement Alerts</h3>

                        <div class="box-tools pull-right">

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Date Cheque</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($echeances as $echeance)
                                    @if ($echeance->devis_id)
                                    <tr class="bg-success">
                                        <td>{{$echeance->date_cheque}}</td>
                                        <td>

                                            <a type="button" class="btn bg-yellow" title="Ancien devis"
                                                href="echeances/{{$echeance->devis_id}}/indexdevis/devis">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>
                                        </td>

                                    </tr>
                                    @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->



            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bon de Commande Non Livré</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Date Création</th>

                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($DevisBcNonLivre as $DBNL)
                                    <tr>
                                        <td>{{$DBNL->raison_sociale}}</td>
                                        <td>{{$DBNL->date_bc}}</td>


                                        <td>

                                            <a href="{{ url('/clients/' . $DBNL->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>

                                        </td>





                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bon de Commande Non Livré (fournisseur)</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Date Création</th>

                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bcNonLivre as $BNL)
                                    <tr>
                                        <td>{{$BNL->raison_sociale}}</td>
                                        <td>{{$BNL->date_bc}}</td>


                                        <td>

                                            <a href="{{ url('/fournisseurs/' . $BNL->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>

                                        </td>





                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>


            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Encaissement</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr role="row" align="center">

                                        <th>Lundi</th>
                                        <th>Mardi</th>
                                        <th>Mercredi </th>
                                        <th>Jeudi</th>
                                        <th>Vendredi</th>
                                        <th>Samedi</th>
                                        <th>Dimanche</th>
                                        <th><label class="badge badge-danger">Somme</label></th>

                                    </tr>
                                </thead>
                                <tbody>


                                    <tr role="row" class="odd" align="center">

                                        <td>{{ $tab1[0] }}</td>

                                        <td>{{ $tab1[1] }}</td>
                                        <td>{{ $tab1[2] }}</td>
                                        <td>{{ $tab1[3] }}</td>
                                        <td>{{ $tab1[4] }}</td>
                                        <td>{{ $tab1[5] }}</td>
                                        <td>{{ $tab1[6] }}</td>
                                        <td>{{ $tab1[7] }}</td>



                                    </tr>



                                </tbody>

                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Decaissement</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr role="row" align="center">

                                        <th>Lundi</th>
                                        <th>Mardi</th>
                                        <th>Mercredi </th>
                                        <th>Jeudi</th>
                                        <th>Vendredi</th>
                                        <th>Samedi</th>
                                        <th>Dimanche</th>
                                        <th><label class="badge badge-danger">Somme</label></th>

                                    </tr>
                                </thead>
                                <tbody>


                                    <tr role="row" class="odd" align="center">

                                        <td>{{ $tab2[0] }}</td>

                                        <td>{{ $tab2[1] }}</td>
                                        <td>{{ $tab2[2] }}</td>
                                        <td>{{ $tab2[3] }}</td>
                                        <td>{{ $tab2[4] }}</td>
                                        <td>{{ $tab2[5] }}</td>
                                        <td>{{ $tab2[6] }}</td>
                                        <td>{{ $tab2[7] }}</td>



                                    </tr>



                                </tbody>

                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
