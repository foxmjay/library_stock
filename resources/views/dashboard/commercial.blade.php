@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->


@endsection

@section('content')

<div class="content-wrapper" style="min-height: 926px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stats du mois
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">


        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-circle-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Objectif</span>
                        @if ($objectif == null)
                        <span class="info-box-number">0<small>
                                DH</small></span>
                        @else
                        <span class="info-box-number">{{  $objectif[0]->objectif }}<small>
                                DH</small></span>
                        @endif

                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa  fa-check-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Chiffre Etteint - {{$pourcentage}}%</span>
                        <span class="info-box-number">{{$chiffreEtteint[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only
            <div class="clearfix visible-sm-block"></div>-->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa  fa-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Commande Facturé</span>
                        <span class="info-box-number">{{$commandeFacture[0]->count}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

        </div>

        <div class="row">

                    <!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-minus-square-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Nombre Devis/Mois</span>
                                <span class="info-box-number">{{$nbrDevisM[0]->count}} <small> Devis</small> - {{$nbrDevisM[0]->total}}<small>DH</small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-purple"><i class="fa fa-minus-square-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Nombre Devis/Semaine</span>
                                <span class="info-box-number">{{$nbrDevisW[0]->count}} <small> Devis</small> - {{$nbrDevisW[0]->total}}<small>DH</small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

        </div>

        <div class="row">

                <!-- /.col -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-minus-square-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Bon de Commande Livré</span>
                            <span class="info-box-number">{{$DevisBcLivre[0]->count}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="fa fa-minus-square-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Bon de Commande Non Livré</span>
                            <span class="info-box-number">{{$DevisBcNonLivre[0]->count}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

    </div>



        <!-- Main row -->
        <div class="row">


            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rendez-Vous</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Application</th>
                                        <th>Date Heur</th>
                                        <th>Etat</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rdvs as $rdv)
                                    <tr>
                                        <td>{{$rdv->nom." ".$rdv->prenom}}</td>
                                        <td>{{$rdv->application}}</td>
                                        <td>{{$rdv->date." ".$rdv->heur}}</td>
                                        <td>{{$rdv->action}}</td>



                                        <td>

                                            <a href="{{ url('/notifsrdv/' . $rdv->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>

                                        </td>




                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>



            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
