@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->


@endsection

@section('content')

<div class="content-wrapper" style="min-height: 926px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stats

            <button type="button" id="filters" class="btn btn-small bg-green pull-right">Filters</button>

        </h1>
        <div >
                <div class="row">

                    <form  method="POST"  action="{{ url('/dashboard/filter') }}" accept-charset="UTF-8" style="display:inline">
                      {{ csrf_field() }}




                      <div class="col-sm-12">
                        <div class="form-group" style="display: none" id="filterList">

                           <label for="commerciale_id">Employes</label>

                           <select class="form-control" name="employe" id="employe">
                               @foreach ($commerciales as $object)
                               <option value="{{$object->id}}">{{$object->nom}} {{$object->prenom}} </option>
                               @endforeach
                           </select>


                           <label class="form-group">Date debut</label>
                           <input class="form-control"  name="dd" type="date" id="dd" value="" disabled >

                           <label class="form-group">Date Fin</label>
                           <input class="form-control"  name="df" type="date" id="df" value="" disabled >



                                <button type="submit" class="btn btn-primary">Chercher</button>

                        </div>
                      </div>



                    </form>

                  </div>


               </div>

    </section>

    <!-- Main content -->
    <section class="content">

        @if ($usertype != "Teleoperateur")
        <!-- Info boxes -->
        <h3 align="center">
            Encaissement
        </h3>
        <hr>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-circle-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Reglé</span>
                        <span class="info-box-number">{{$encaissement_regeler[0]->total}}<small>
                                DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa  fa-check-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">En Attent</span>
                        <span
                            class="info-box-number">{{$encaissement_enattente[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa  fa-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Estimé</span>
                        <span class="info-box-number">{{$encaissement_estimer[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-minus-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Echue</span>
                        <span class="info-box-number">{{$encaissement_echue[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <h3 align="center">
                Decaissement
            </h3>
            <hr>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-circle-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Reglé</span>
                        <span class="info-box-number">{{$decaissement_regeler[0]->total}}<small>
                                DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa  fa-check-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">En Attent</span>
                        <span
                            class="info-box-number">{{$decaissement_enattente[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa  fa-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Estimé</span>
                        <span class="info-box-number">{{$decaissement_estimer[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-minus-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Echue</span>
                        <span class="info-box-number">{{$decaissement_echue[0]->total}}<small>DH</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>

        <hr>

        <div class="col-md-12 ">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content" align="center">
                    <span class="info-box-text">Totale Gain Estimer</span>
                    <span class="info-box-number">{{ ($encaissement_estimer[0]->total + $encaissementtotaldumois[0]->total) - ($decaissement_estimer[0]->total + $decaissementtotaldumois[0]->total) }}<small>
                            DH</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <hr>
        @endif

        <!-- Main row -->
        <div class="row">
            @if ($usertype != "Teleoperateur")
            <!-- Left col -->
            <div class="col-md-6">


                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cheque Décaissement Alerts</h3>

                        <div class="box-tools pull-right">

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Date Cheque</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($echeances as $echeance)
                                    @if ($echeance->bc_id)
                                    <tr class="bg-danger">
                                        <td>{{$echeance->date_cheque}}</td>
                                        <td>
                                            <a type="button" class="btn btn-warning" title="Ancien devis"
                                                href="echeances/{{$echeance->bc_id}}/indexdevis/bc">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


            <!-- Left col -->
            <div class="col-md-6">


                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cheque Encaissement Alerts</h3>

                        <div class="box-tools pull-right">

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Date Cheque</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($echeances as $echeance)
                                    @if ($echeance->devis_id)
                                    <tr class="bg-success">
                                        <td>{{$echeance->date_cheque}}</td>
                                        <td>

                                            <a type="button" class="btn bg-yellow" title="Ancien devis"
                                                href="echeances/{{$echeance->devis_id}}/indexdevis/devis">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>
                                        </td>

                                    </tr>
                                    @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            @endif

            @if($usertype == "Admin" || $usertype == "Assistant")

            {{-- <div class="col-md-12">

                    <!-- PRODUCT LIST -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Bons de commande non Complet</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                        <tr>
                                            <th>Client</th>
                                            <th>Ref bon de commande</th>
                                            <th>Date</th>
                                            <th></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($devisbcNonComplet as $dbc)
                                        <tr>
                                            <td>{{$dbc->raison_sociale}}</td>
                                            <td>{{$dbc->ref_devisbc}}</td>
                                            <td>{{$dbc->date_bc}}</td>
                                            <td>

                                                <a href="{{ url('/clients/'.$dbc->client_id ) }}" class="btn btn-warning"
                                                    title="Visualiser">
                                                    <span class="fa fa-eye" aria-hidden="true"></span>
                                                </a>

                                            </td>


                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>

            --}}@endif

            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rendez-Vous</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Application</th>
                                        <th>Date Heur</th>
                                        <th>Etat</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rdvs as $rdv)
                                    <tr>
                                        <td>{{$rdv->nom." ".$rdv->prenom}}</td>
                                        <td>{{$rdv->application}}</td>
                                        <td>{{$rdv->date." ".$rdv->heur}}</td>
                                        <td>{{$rdv->action}}</td>
                                        @if ($usertype == "Teleoperateur")
                                        <td>

                                            <a href="{{ url('/rdvs/' . $rdv->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>

                                        </td>
                                        @endif

                                        @if ($usertype == "Commercial")
                                        <td>

                                            <a href="{{ url('/notifsrdv/' . $rdv->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>

                                        </td>
                                        @endif



                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>


            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Encaissement</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr role="row" align="center">

                                        <th>Lundi</th>
                                        <th>Mardi</th>
                                        <th>Mercredi </th>
                                        <th>Jeudi</th>
                                        <th>Vendredi</th>
                                        <th>Samedi</th>
                                        <th>Dimanche</th>
                                        <th><label class="badge badge-danger">Somme</label></th>

                                    </tr>
                                </thead>
                                <tbody>


                                    <tr role="row" class="odd" align="center">

                                        <td>{{ $tab1[0] }}</td>

                                        <td>{{ $tab1[1] }}</td>
                                        <td>{{ $tab1[2] }}</td>
                                        <td>{{ $tab1[3] }}</td>
                                        <td>{{ $tab1[4] }}</td>
                                        <td>{{ $tab1[5] }}</td>
                                        <td>{{ $tab1[6] }}</td>
                                        <td>{{ $tab1[7] }}</td>



                                    </tr>



                                </tbody>

                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Decaissement</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr role="row" align="center">

                                        <th>Lundi</th>
                                        <th>Mardi</th>
                                        <th>Mercredi </th>
                                        <th>Jeudi</th>
                                        <th>Vendredi</th>
                                        <th>Samedi</th>
                                        <th>Dimanche</th>
                                        <th><label class="badge badge-danger">Somme</label></th>

                                    </tr>
                                </thead>
                                <tbody>


                                    <tr role="row" class="odd" align="center">

                                        <td>{{ $tab2[0] }}</td>

                                        <td>{{ $tab2[1] }}</td>
                                        <td>{{ $tab2[2] }}</td>
                                        <td>{{ $tab2[3] }}</td>
                                        <td>{{ $tab2[4] }}</td>
                                        <td>{{ $tab2[5] }}</td>
                                        <td>{{ $tab2[6] }}</td>
                                        <td>{{ $tab2[7] }}</td>



                                    </tr>



                                </tbody>

                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')


  <script>




    $(function() {

      $('#filters').on('click',function(){
         $('#filters').hide();
         $('#employe').prop('disabled',false);
         $('#dd').prop('disabled',false);
         $('#df').prop('disabled',false);
         $('#filterList').show();

      });



    })
  </script>
@endsection
