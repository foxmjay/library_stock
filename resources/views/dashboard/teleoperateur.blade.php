@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->


@endsection

@section('content')

<div class="content-wrapper" style="min-height: 926px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stats
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">




        <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa  fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Nombre Clients</span>
                                <span class="info-box-number">{{$nbrAppel[0]->count + $nbrRDV[0]->count }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa  fa-check-square-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Client Appelé</span>
                        <span class="info-box-number">{{$nbrRDV[0]->count}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">

                    <span class="info-box-icon bg-purple"><i class="fa fa-circle-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Client Non-Appelé</span>
                        <span class="info-box-number">{{$nbrAppel[0]->count}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

        </div>



        <!-- Main row -->
        <div class="row">

            <!-- Left col -->




            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rendez-Vous de la semaine</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Application</th>
                                        <th>Date Heur</th>
                                        <th>Etat</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rdvs as $rdv)
                                    <tr>
                                        <td>{{$rdv->nom." ".$rdv->prenom}}</td>
                                        <td>{{$rdv->application}}</td>
                                        <td>{{$rdv->date." ".$rdv->heur}}</td>
                                        <td>{{$rdv->action}}</td>

                                        <td>

                                            <a href="{{ url('/rdvs/' . $rdv->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>

                                        </td>




                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-12">


                <!-- PRODUCT LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rendez-Vous a Rappeler</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Application</th>
                                        <th>Date Heur</th>
                                        <th>Etat</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rappelrdvs as $rdv)
                                    <tr>
                                        <td>{{$rdv->nom." ".$rdv->prenom}}</td>
                                        <td>{{$rdv->application}}</td>
                                        <td>{{$rdv->date_rappel." ".$rdv->heur_rappel}}</td>
                                        <td>{{$rdv->action}}</td>

                                        <td>

                                            <a href="{{ url('/rdvs/' . $rdv->id) }}" class="btn btn-warning"
                                                title="Visualiser">
                                                <span class="fa fa-eye" aria-hidden="true"></span>
                                            </a>
                                            <a href="{{ url('/rdvs/' . $rdv->id . '/edit') }}" class="btn btn-info"
                                                title="Modifier">
                                                <span class="fa fa-edit" aria-hidden="true"></span>
                                            </a>

                                        </td>




                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>


            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
