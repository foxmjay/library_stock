@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonlivraison-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de retour
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de retour</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Details</h3>
                      </div> 
                      <div  class="box-body">
                          
                            <div class="col-xs-6">
                                <div class="form-group ">
                                    <label for="image_br" >Image bon retour</label>
                                    <input name="image_br" type="file" id="image_br"  ref="image_br"  v-on:change="handleFileUpload()">
                                </div>
                            </div>

                         

                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label for="date_br">Date bon retour</label>
                                  <input type="date" class="form-control" id="date_br" name="date_br" placeholder="Date bon retour" v-model="date_br">
                                </div>
              
                          </div>
    
                 </div>
    
          </div>
              <!-- /.col -->

        </div>
              <!-- /.col -->

          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>Produit</th>
                        <th>Marque</th>
                        <th>Ref interne</th>
                        <th>Numero serie</th>
                        <th>Prix vente HT</th>
                        <th>remise</th>
                        <th>Prix HT</th>
                       
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td><input type="checkbox" name="checked" v-bind:id="stock.id" v-bind:value="stock.id" v-model="checkedStocks"><br></td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td> 
                    <td>@{{stock.prix_vente}}</td>  
                    <td>@{{stock.remise}} %</td>  
                    <td>@{{ stock.prix_vente - (stock.prix_vente*stock.remise/100) }} DH</td>
                       
                    
                </tr>
  
                </table>

                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Confirmer
                </button> 
                <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                    <i class="fa fa-close"></i>Annuler
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>


var bl_ventes = {!! json_encode($ventes) !!};
var bl_id = {!! json_encode($bl->id) !!};
var client_id = {!! json_encode($client_id) !!};
var devis_id = {!! json_encode($devis_id) !!};

var app = new Vue({
  el: '#bonlivraison-vue-wrapper',

  data: {

    image_br : '',
    date_br : '',
    selectedStocks: bl_ventes,
    checkedStocks : [],
   },

 
    
  methods: {

    handleFileUpload: function handleFileUpload(){
      this.image_br = this.$refs.image_br.files[0];
    },


    annuler: function annuler(){
      //history.back();
      window.location.href ="/clients/"+client_id;
    },

    confirmer: function(){

         enableSpinner();

        //console.log(this.checkedStocks);

        var _this = this;
        let formData = new FormData();

        var stocksdata = JSON.stringify( { 'stocks' : _this.checkedStocks});

        formData.append('devis_id',devis_id);
        formData.append('client_id',client_id);
        formData.append('bl_id',bl_id);
        formData.append('image_br',this.image_br);
        formData.append('date_br',this.date_br);
        formData.append('stocks',stocksdata);

        axios.post('/stockretour/confrimerbl',formData,{
          headers: {'Content-Type': 'multipart/form-data' }

        }).then(function (response){
          //console.log(response);
          if(response.data['result'] == "OK"){
            window.location.href ="/clients/"+client_id;
          }

        }).catch(function (error){
          console.log(error)
        });


},

    
  }
});



</script>

@endsection
