@extends('layouts.admin')

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
  .highlight {
    background-color: lightslategrey;
  }

  .highlightGreen {
    background-color :limegreen;
  }

</style>

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="bonretour-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bon de retour
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bon de retour</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">


            <div class="col-xs-12">

                <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Details</h3>
                      </div> 
                      <div  class="box-body">
                          
                            <div class="col-xs-6">
                                <div class="form-group ">
                                    <label for="image_br" >Image bon retour</label>
                                    <input name="image_br" type="file" id="image_br"  ref="image_br"  v-on:change="handleFileUpload()">
                                </div>
                            </div>

                         

                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label for="date_br">Date bon retour</label>
                                  <input type="date" class="form-control" id="date_br" name="date_br" placeholder="Date bon retour" v-model="date_br">
                                </div>
              
                          </div>
    
                 </div>
    
          </div>
              <!-- /.col -->

        </div>

        <div class="col-md-12">
            <div class="box">
            <!-- /.box-header -->
            <div class="box-body ">
                <div class="col-md-4">&nbsp</div>
                <div class="input-group margin col-md-4 " align="center" >
                        <input type="text" class="form-control" id="nomproduit" name="nomproduit" placeholder="numero serie" v-model="nomproduit" >
                            <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" v-on:click="searchStock()">
                                            Chercher
                                          </button>
                            </span>
                      </div>
    
            </div>
            <!-- /.box-body -->
    
          </div>
    
    
        </div>
          <!-- /.col -->

          <!-- /.content -->
     <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Stock</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Produit</th>
                                <th>Ref Interne</th>
                                <th>Numero serie</th>
                                <th>Marque</th>
                                <th>Fournisseur</th>
                                <th>Etat</th>
                                <th></th>
                            </tr>
                            <tr v-for="(stock,index) in stocks">
                                <td>@{{stock.id}}</td>
                                <td>@{{stock.nom}}</td>
                                <td>@{{stock.ref_interne}}</td>
                                <td>@{{stock.num_serie}}</td>
                                <td>@{{stock.marque}}</td>
                                <td>@{{stock.raison_sociale}}</td>
                                <td>@{{stock.etat}}</td>
                                
                                <td>
                                        <button type="button" class="btn btn-success" title="sélectionner" data-dismiss="modal" v-on:click="getstockitem(index)">
                                            <span class="fa fa-arrow-circle-down" aria-hidden="true"></span>
                                        </button>
                                </td>
                            </tr>

                            </table>
                        </div>
            </div>

          </div>
        </div>
      </div>
<!-- Main content -->

          
          <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Produits selectiones</h3>
                </div>  
  
              <div  class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Produit</th>
                        <th>Ref Interne</th>
                        <th>Numero serie</th>
                        <th>Marque</th>
                        <th>Fournisseur</th>
                        <th>Etat</th>
                        
                        <th></th>
                    </tr>
                <tr v-for="(stock,index) in selectedStocks" >
                    <td>@{{stock.id}}</td>
                    <td>@{{stock.nom}}</td>
                    <td>@{{stock.ref_interne}}</td>
                    <td>@{{stock.num_serie}}</td>
                    <td>@{{stock.marque}}</td>
                    <td>@{{stock.raison_sociale}}</td>
                    <td>@{{stock.etat}}</td>
   
                    <td>
                            <button type="button" class="btn btn-danger" title="sélectionner" data-dismiss="modal" v-on:click="removeStock(index , stock.produit_id)">
                                <span class="fa fa-times-circle" aria-hidden="true"></span>
                            </button>
                    </td>
                </tr>
  
                </table>
                
            </div>
                
            
          </div>
  
        </div>
        <!-- /.col -->

        <div class="col-xs-12">

            <div class="box">
             
            <div  class="box-body table-responsive">
                <button type="button" class="btn btn-app pull-right bg-green" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Confirmer
                </button> 
                <button type="button" class="btn btn-app pull-left bg-red" v-on:click="annuler()">
                    <i class="fa fa-close"></i>Annuler
                </button>     
          </div>

        </div>

      </div>

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
<script>


var client_id = {!! json_encode($client_id) !!};


var app = new Vue({
  el: '#bonretour-vue-wrapper',

  data: {
    image_br : '',
    date_br : '',
    nomproduit: '',
    stocks: [],
    selectedStocks: [],
   },

  mounted: function mounted() {
    
  },

  methods:{

    handleFileUpload: function handleFileUpload(){
      this.image_br = this.$refs.image_br.files[0];
    },

    searchStock: function searchStock() {
      var _this = this;

      var data = JSON.stringify( { 'nomproduit' : this.nomproduit ,'client_id': client_id});

      axios.post('/getstock/retour',{data: data}).then(function (response){
        if(response.data['result'] == "OK"){
          //console.log(response.data);
          _this.stocks = response.data['stocks'];
        }

        if(response.data['result'] == "KO"){
          console.log("KO");
        }
  
      }).catch(function (error){
          console.log(error)
      })
    },

    getstockitem: function getstockitem(index) {
        var _this = this;
        var exists = 0;

        if(this.stocks[index].etat == "Disponible")
          alert('Produit deja existant dans le stock');

        this.selectedStocks.forEach(function(value,key){
           if(_this.stocks[index].id == value.id)
              exists =1;
           //console.log(value);
        });
        if(exists == 0)
          this.selectedStocks.push(this.stocks[index]);
        else
          alert('Produit deja selectione');
    },


    confirmer: function(){

        enableSpinner();

        var _this = this;
        let formData = new FormData();

        var stocksdata = JSON.stringify( { 'stocks' : _this.selectedStocks});
      
        formData.append('client_id',client_id);
        formData.append('image_br',this.image_br);
        formData.append('date_br',this.date_br);
        formData.append('stocks',stocksdata);

        axios.post('/stockretour/confrimer',formData,{
          headers: {'Content-Type': 'multipart/form-data' }

        }).then(function (response){
          //console.log(response);
          if(response.data['result'] == "OK"){
            window.location.href ="/clients/"+client_id;
          }

        }).catch(function (error){
          console.log(error)
      });


    },
    annuler: function annuler(){
      //history.back();
      window.location.href ="/clients/"+client_id;
    },

  }

});

</script>

@endsection
