@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="echeance-vue-wrapper">
    <section class="content-header">
        <div class="alert alert-danger alert-dismissible" v-show="error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            les pourcentages ne peuvent pas dépasser les 100%.
        </div>
    </section>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Echeances paiements
            <small>Gestion echeance paiements</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Echeances paiements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Echeances paiements</h3>
                    </div>

                    @include ('echeances.form')

                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->


@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>
    var devis_id = {!! json_encode($devis_id) !!};
    var montant = {!! json_encode($devis->prix_total) !!};


var app = new Vue({
  el: '#echeance-vue-wrapper',

  data: {
    items: [],
    echeanceModel : {'devis_id':devis_id,'id':'','echeance':'','pourcentage': 0,'date': '' ,'type_paiement':'','image_cheque': '','date_cheque':'','prix': 0},
    image_cheque : '',
    loading : false,
    somme : 0,
    error : false,
   },

  mounted: function mounted() {
    this.getVueItems();
  },

  methods: {

    handleFileUpload: function handleFileUpload(){
      this.image_cheque = this.$refs.image_cheque.files[0];
    },

    getVueItems: function getVueItems() {
      var _this = this;

      axios.get('/echeances/'+devis_id+'/perdevis').then(function (response) {
        _this.items = response.data;
      });
    },

    calcP: function calcP() {
      var _this = this;

      this.echeanceModel.prix =  (montant * this.echeanceModel.pourcentage)/100
      this.echeanceModel.prix +=  this.echeanceModel.prix * 20 /100;

    },

    addItem: function(){

      var _this = this;
      var data = this.echeanceModel;
      let formData = new FormData();
      this.items.forEach(element => {
        this.somme += element.pourcentage;
     });
     if (parseInt(this.somme) + parseInt(data.pourcentage) > 100) {
        this.error = true;
     }else{

        this.error = false;
        formData.append('devis_id',devis_id);
      formData.append('jours',data.jours);
      formData.append('pourcentage',data.pourcentage);
      formData.append('date',data.date);
      formData.append('type_paiement',data.type_paiement);
      formData.append('date_cheque',data.date_cheque);
      formData.append('image_cheque',this.image_cheque);
      formData.append('prix',data.prix);

      //console.log(data.image_cheque);
      //console.log(data);
      axios.post('/echeances',formData,{
        headers: {'Content-Type': 'multipart/form-data' }

      }).then(function (response){
        //console.log(response);
        _this.getVueItems();
      });
      this.somme = 0;
     }




    },
    ValidationEcheance: function ValidationEcheance(item){
      var _this = this;
      this.loading = true;
      axios.post('/validation/'+item.id).then(function(response){
        _this.getVueItems();
      });
      this.loading = false;
    },
    deleteItem: function deleteItem(item){
      var _this = this;
      axios.post('/echeances/'+item.id+'/delete').then(function(response){
        _this.getVueItems();
      });
    }
  }
});

</script>

@endsection
