<div class="box-body">


    <div class="col-md-12">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <td> <input type="text" class="form-control" id="pourcentage" name="pourcentage"
                            placeholder="Pourcentage" v-on:keyup="calcP" v-model="echeanceModel.pourcentage"></td>
                    <td> <input type="text" class="form-control" id="jours" name="jours" placeholder="Jours"
                            v-model="echeanceModel.jours"></td>
                    <td> <input type="date" class="form-control" id="date" name="date" placeholder="date"
                            v-model="echeanceModel.date"></td>
                    <td>
                        <select class="form-control" name="type_paiement" id="type_paiement"
                            v-model="echeanceModel.type_paiement">
                            @foreach (['Espece','Cheque'] as $object)
                            <option value="{{$object}}">{{$object}} </option>
                            @endforeach
                        </select>
                    </td>
                    <td v-show="echeanceModel.type_paiement == 'Cheque'"> <input type="file" class="form-control"
                            ref="image_cheque" id="image_cheque" name="image_cheque" placeholder="image_cheque"
                            v-on:change="handleFileUpload()"></td>
                    <td v-show="echeanceModel.type_paiement == 'Cheque'"> <input type="date" class="form-control"
                            id="date_cheque" name="date_cheque" placeholder="date_cheque"
                            v-model="echeanceModel.date_cheque"></td>
                    <td> <input type="number" class="form-control" id="prix" name="prix" placeholder="Montant"
                            v-model="echeanceModel.prix" readonly></td>
                    <td>
                        <button type="button" class="btn btn-success" title="Ajouter" @click.prevent="addItem()">
                            <span class="fa fa-plus" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-body -->



    </div>

    <div v-show="loading">
        <h1>loading</h1>
    </div>

    <div class="col-md-12">

        <hr>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>ID</th>
                    <th>Pourcentage (%)</th>
                    <th>Jours</th>
                    <th>Date</th>
                    <th>Type_paiement</th>
                    <th>Image cheque</th>
                    <th>Date cheque</th>
                    <th>Reçu</th>
                    <th>Montant TTC</th>
                    <th></th>
                </tr>
                <tr v-for="item in items">
                    <td>@{{item.id}}</td>
                    <td>@{{item.pourcentage}}</td>
                    <td>@{{item.jours}}</td>
                    <td>@{{item.date}}</td>
                    <td>@{{item.type_paiement}}</td>
                    <td><a v-if="item.image_cheque != null" class="navbar-brand brand-logo"
                            v-bind:href="'/storage/'+item.image_cheque" target="_blank"><img height="50px"
                                v-bind:src="'/storage/'+item.image_cheque" alt="image" /></a>
                    </td>
                    <td>@{{item.date_cheque}}</td>
                    <td v-if="item.recu == true"><i class="fa fa-fw  fa-check "></i></td>
                    <td v-else><i class="fa fa-fw fa-close "></i></td>
                    <td>@{{item.prix}}</td>
                    <td v-if="item.recu == false">
                        <button type="button" class="btn btn-danger" title="Supprimer"
                            @click.prevent="deleteItem(item)">
                            <span class="fa fa-trash" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-success" title="Valider reception"
                            @click.prevent="ValidationEcheance(item)">
                            <span class="fa fa-check" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>

            </table>
        </div>
        <!-- /.box-body -->



    </div>

</div>
