
@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Boite de Messages

      </h1>
      <ol class="breadcrumb">

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="{{ url('/messages/create') }}" class="btn btn-success btn-block margin-bottom"> Nouveau Message</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">-</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><i class="fa fa-inbox"></i> Reçus</a></li>
                  {{-- <span class="label label-primary pull-right">12</span></a></li> --}}
                <li><a href="{{ url('/messages/sent') }}"><i class="fa fa-envelope-o"></i> Envoyés</a></li>

              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->

          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Messages</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">

                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">

              </div>
              <div class="table-responsive mailbox-messages">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                              <tr role="row">
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1">#</th><th>objet</th><th>Message</th><th  class="col" >Actions</th>


                              </tr>
                            </thead>
                            <tbody>

                            @foreach($messages as $item)
                                <tr role="row" class="odd">
                                    <td class="sorting_1">{{ $loop->iteration }}</td>
                                    <td class="mailbox-name"><a href="{{ url('/messages/' . $item->id) }}">{{ str_limit($item->objet,20) }}</a></td><td>{{ str_limit(strip_tags($item->message),100) }}</td>
                                    <td>
                                        <!--<a href="{{ url('/messages/' . $item->id) }}" title="View Message"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> -->
                                        {{-- <a href="{{ url('/messages/' . $item->id . '/edit') }}" title="Modifier Message"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a> --}}

                                        {{-- <form method="POST" action="{{ url('/messages' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Supprimer Message" onclick="return confirm(&quot;êtes-vous sûr de vouloir supprimer?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                        </form> --}}
                                    </td>
                                </tr>
                            @endforeach




                            </tbody>

                          </table>

                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->

                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable()

    })
  </script>
@endsection
