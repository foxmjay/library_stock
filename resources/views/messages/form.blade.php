
<div class="form-group">
    <label for="id_distinataire">destinataire</label>
    @if (!empty($message) == 1)
      <select class="form-control" name="id_distinataire" disabled>
      @foreach ($destinataires as $object)
        @if ($object->id === $message->id_distinataire)
        <option value="{{$object->id}}" selected="selected">{{$object->nom}}</option>
        @else
        <option value="{{$object->id}}">{{$object->nom}} </option>
        @endif
      @endforeach
      </select>
    @else
      <select class="form-control" name="id_distinataire" id="id_distinataire">
      @foreach ($destinataires as $object)
      @if ($object->id === Auth::id())

      @else
        <option value="{{$object->id}}">{{$object->name}}</option>
      @endif

      @endforeach
      </select>
    @endif
    {!! $errors->first('id_distinataire', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('objet') ? 'has-error' : ''}}">
    <label for="objet" class="control-label">{{ 'Objet' }}</label>
    <input class="form-control" name="objet" type="text" id="objet" value="{{ $formMode === 'edit' ? $message->objet : ''}}" required>

    {!! $errors->first('objet', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}">
    <label for="message" class="control-label">{{ 'Message' }}</label>
    <textarea id="compose-textarea" class="form-control" rows="5" name="message" type="textarea" id="message" required>{{ $formMode === 'edit' ? $message->message : ''}}</textarea>

    {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
</div>





<div class="form-group">
    <input class="btn btn-primary" type="submit" onSubmit="enableSpinner();" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
