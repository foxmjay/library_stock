@extends('layouts.admin')

@section('content')

<div class="content-wrapper" style="min-height: 956px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">

          <ol class="breadcrumb">

          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
                    <a href="{{ url('/messages/create') }}" class="btn btn-success btn-block margin-bottom"> Nouveau Message</a>

              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">-</h3>

                  <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                        <li ><a href="{{ url('/messages') }}"><i class="fa fa-inbox"></i> Reçus</a></li>
                      <li><a href="{{ url('/messages/sent') }}"><i class="fa fa-envelope-o"></i> Envoyés</a></li>
                  </ul>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /. box -->

              <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{{ $message->objet }}</h3>

                  <div class="box-tools pull-right">

                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">

                    <h5>From: {{ $message->name }}
                      <span class="mailbox-read-time pull-right"> {{ $message->created_at }}</span></h5>
                  </div>
                  <!-- /.mailbox-read-info -->

                  <!-- /.mailbox-controls -->
                  <div class="mailbox-read-message">

                     {!! html_entity_decode($message->message) !!}
                  </div>
                  <!-- /.mailbox-read-message -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

              </div>
              <!-- /. box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>



@endsection
