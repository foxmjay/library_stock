<div class="box-body">



<div class="col-md-6">

<div class="form-group">
      <label for="produit_id">Produit</label>
      @if (!empty($stock) == 1)
        <select class="form-control" name="produit_id" disabled>
        @foreach ($produits as $object)
          @if ($object->id === $stock->produit_id)
          <option value="{{$object->id}}" selected="selected">{{$object->nom}}</option>
          @else
          <option value="{{$object->id}}">{{$object->nom}} </option>
          @endif
        @endforeach
        </select>
      @else
        <select class="form-control" name="produit_id" id="produit_id">
        @foreach ($produits as $object)
          <option value="{{$object->id}}">{{$object->nom}}</option>
        @endforeach
        </select>
      @endif
</div>

  <div class="form-group">
    <label for="num_serie">Numero serie</label>
    <input type="text"   class="form-control" id="num_serie" name="num_serie" placeholder="numero serie" value="{{ old('num_serie', optional($stock)->num_serie) }}">
    {!! $errors->first('num_serie', '<p class="text-danger ">:message</p>') !!}
  </div>


  <div class="form-group">
    <label for="prix_unitaire">Prix unitaire</label>
    <input type="number"  step=any  class="form-control" id="prix_unitaire" name="prix_unitaire" placeholder="Prix unitaire" value="{{ old('prix_unitaire', optional($stock)->prix_unitaire) }}">
    {!! $errors->first('prix_unitaire', '<p class="text-danger ">:message</p>') !!}
  </div>
       
</div>
<div class="col-md-6">

<div class="form-group">
      <label for="fournisseur_id">Fournisseur</label>
      @if (!empty($stock) == 1)
        <select class="form-control" name="fournisseur_id" id="fournisseur_id" disabled>
        @foreach ($fournisseurs as $object)
          @if ($object->id === $stock->fournisseur_id)
          <option value="{{$object->id}}" selected="selected">{{$object->raison_sociale}}</option>
          @else
          <option value="{{$object->id}}">{{$object->raison_sociale}} </option>
          @endif
        @endforeach
        </select>
      @else
        <select class="form-control" name="fournisseur_id" id="fournisseur_id">
        @foreach ($fournisseurs as $object)
          <option value="{{$object->id}}">{{$object->raison_sociale}}</option>
        @endforeach
        </select>
      @endif
</div>


  <div class="form-group">
    <label for="ref_fournisseur">Reference Fournisseur</label>
    <input type="text" class="form-control" id="ref_fournisseur" name="ref_fournisseur" placeholder="reference fournisseur" value="{{ old('ref_fournisseur', optional($stock)->ref_fournisseur) }}">
  </div>

  <div class="form-group">
    <label for="etat">Etat</label>
    <input type="text" class="form-control" id="etat" name="etat" placeholder="etat" value="{{ old('etat', optional($stock)->etat) }}" readonly>
  </div>

</div>


</div>

