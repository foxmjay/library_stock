
@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

@php 
    $userProfile =  \App\UserProfile::where('user_id',Auth::user()->id)->first();
 @endphp

<div class="content-wrapper">
<section class="content-header">
    <h1>
      Stock

    </h1>

</section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

         
          <div class="box">
            <div class="box-header with-border">
            @if ( $userProfile->type == "Admin" )
              <a href="{{ url('/stock/create') }}" class="btn btn-success btn-sm" title="Add New Ville">
                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Stock
                </a>

                <a href="{{ url('/stocks/importcsv') }}" class="btn bg-red btn-sm pull-right" title="Add New Ville">
                  <i class="fa fa-plus" aria-hidden="true"></i> Importer depuis CSV
              </a>
            @endif

              <hr>
              <div class="box-body">
                  <div >
                    <div class="row">
                       
                        <form  method="POST"  action="{{ url('/etatstock/search') }}" accept-charset="UTF-8" style="display:inline">
                          {{ csrf_field() }}
                          <div class="col-sm-10">
                          <div class="form-group">
                            <input class="form-control"  name="search" type="text" id="search" value="" >
                         </div>
                          </div>
                          <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary">Chercher</button>
                          </div>
      
                        </form>

                      </div>
                   </div>
              </div>
              @if(count($stock) >0 )
              <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  
                  <div class="row">
                    <div class="col-sm-12">
                   
                      <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <!--<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1">#</th>-->
                            <th>ID </th>
                            <th>Ref Interne </th>
                            <th>Produit </th>
                            <th>Marque</th>
                            <th>Quantite </th>
                            <th>Quantite reserve</th>

                            
                            <th  class="col" >Actions</th>


                          </tr>
                        </thead>
                        <tbody>
                        
                          @foreach($stock as $item)
                              <tr role="row" class="odd">
                                  <!--<td class="sorting_1">{{ $loop->iteration }}</td> -->
                                  <td>{{ $item->id }}</td>
                                  <td>{{ $item->ref_interne }}</td>
                                  <td>{{ $item->nom }}</td>
                                  <td>{{ $item->marque }}</td> 
                                  <td>{{ $item->quantite }}</td>
                                  <td>{{ $item->reserve }}</td>


                                  <td>
                                  @if ( $userProfile->type == "Admin" )
                                      <!--<a href="{{ url('/stock/' . $item->id) }}" title="View Stock"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> -->
                                      <a href="{{ url('/produits/' . $item->id . '/edit') }}" title="Modifier Stock"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                                      <!--<form method="POST" action="{{ url('/stock' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                          {{ method_field('DELETE') }}
                                          {{ csrf_field() }}
                                          <button type="submit" class="btn btn-danger btn-sm" title="Supprimer Stock" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                      </form> -->
                                    @endif
                                  </td>
                              </tr>
                          @endforeach

                        </tbody>

                      </table>

                    </div>
                  </div>
                
                </div>

              </div>

            @endif


              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

            </div>
          </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        
    </div>
    <!-- /.row -->
</section>
</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable({
        searching:false
      })

    })
  </script>
@endsection
