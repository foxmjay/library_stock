@extends('layouts.admin')

@section('css')
<!-- <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/select2.min.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="stock-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Stocks
        <small>gestion des stocks</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Stocks</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Creation nouveau stock</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->


                @include ('stocks.create_form', ['stock' => null,])


          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <section class="content">
        <div class="row">
          <div class="col-xs-12">

              <div class="box">
                  <div class="box-header">
                      <h3 class="box-title"></h3>
                    </div>


                <div  class="box-body">


                    <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="type">Type: </label>
                            <select class="form-control" id="type" v-model="type" name="type">
                                <option v-for="tp in typeList" v-bind:value="tp.type">@{{tp.name}}</option>
                            </select>
                          </div>
                        </div>
                    </div>

                    <div v-if="type === 'nonserialisable'" id="NonSerialisable">
                        <label for="type">Quantite: </label>
                        <input type="text"   class="form-control" id="quantite" name="quantite" placeholder="quantite" v-model="quantite">
                    </div>

                    <div v-if="type === 'serialisable'" id="Serialisable">
                        <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">

                                <div class="input-group input-group-sm">
                                    <input type="text"   class="form-control" id="num_serie" name="num_serie" placeholder="numero serie" v-model="num_serie"  v-on:keyup.enter="addItem()">
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-success btn-flat"    @click.prevent="addItem()"> <span class="fa fa-plus" aria-hidden="true"></span></button>
                                        </span>
                                  </div>
                              </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Numero de serie</th>
                                        <th></th>
                                    </tr>
                                    <tr v-for="(stock,index) in stocks" >
                                        <td>@{{stock}}</td>
                                        <td >
                                            <button type="button" class="btn btn-danger" title="Supprimer" @click.prevent="deleteItem(index)">
                                                <span class="fa fa-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                          </div>


                        </div>
                  </div>

                  </div>

                </div>

                <button type="button" class="btn btn-app pull-right bg-green" id="confirmer" v-on:click="confirmer()">
                    <i class="fa fa-save"></i>Ajouter
                </button>

            </div>
            <!-- /.box -->
          </div>

        <!-- /.row -->
      </section>


  </div>
  <!-- /.content-wrapper -->


@endsection


@section('js')
<!--
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

<script src="{{ URL::asset('assets/admin/dist/js/vue_.js')}}"></script>
<script src="{{ URL::asset('assets/admin/dist/js/axios_.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>

var fournisseurs = {!! json_encode($fournisseurs) !!};


var app = new Vue({
  el: '#stock-vue-wrapper',

  data: {
    type : 'serialisable',
    fournisseurs : fournisseurs,
    stocks: [],
    fournisseur_id : fournisseurs[0].id,
    num_serie : '',
    prix_unitaire : 0,
    ref_fournisseur : '',
    typeList : [{'type':'serialisable', 'name':'Serialisable'},{'type':'nonserialisable', 'name':'Non Serialisable'}],
    quantite : 1

   },


  methods: {

    addItem: function(){
      var _this = this;

      exists=0;
      this.stocks.forEach(function(element){
        console.log(element);
        console.log(num_serie);
          if(_this.num_serie == element)
               exists=1;
      });

      if(exists == 1){
        alert('Numero de serie existe deja dans la liste !');
        return;
      }


      if(this.num_serie == ""){
        alert('Entrer un numero de serie!');
        return;
      }
      this.stocks.push(this.num_serie)
      this.num_serie = '';
    },

    confirmer: function confirmer(){

           enableSpinner();

          var _this = this;
          //var data = this.echeanceModel;

          produit_id=$('#produit_id').select2('val');

          if(this.stocks.length <= 0 && this.type === 'serialisable' ){
            alert("Ajouter au minimum un numero de serie");
            disableSpinner();
            return;
          }


          if(this.type === 'serialisable' )
             var data = JSON.stringify( { 'type':this.type ,'stocks' : this.stocks ,'produit_id': produit_id, 'fournisseur_id':this.fournisseur_id, 'prix_unitaire':this.prix_unitaire, 'ref_fournisseur': this.ref_fournisseur });
          else
             var data = JSON.stringify( { 'type':this.type , 'quantite':this.quantite,'produit_id': produit_id, 'fournisseur_id':this.fournisseur_id, 'prix_unitaire':this.prix_unitaire, 'ref_fournisseur': this.ref_fournisseur });

          axios.post('/stock/storeItems',{data: data}).then(function (response){
            //console.log(response.data);
            if(response.data['result'] == "OK"){
              //console.log(response.data['result']);
              window.location.href = "/stock/";
            }

            if(response.data['result'] == "KO"){
                disableSpinner();
              //console.log(response.data['result']);
                var stock=response.data['stock']
                alert("Le produit avec numero  de serie : "+stock+" exist deja dans le stock");
            }

          }).catch(function (error){
            console.log(error)
          })

    },

    deleteItem: function deleteItem(index){
      var _this = this;
      this.stocks.splice(index,1);

    }
  }
});


</script>

<script>
    $(document).ready(function () {

        // $('#produit_id').select2();
        // $('#produit_id').on('change', function (e) {
        //     var data = $('#produit_id').select2("val");
        // });

            $('#produit_id').select2({
            minimumInputLength: 2,
            ajax: {
                url: '/product_select2',
                dataType: 'json',
                data: function (param) {
                    return {
                        term: param.term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
            });
    });



</script>

@endsection
