<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Advanced</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/AdminLTE.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style>
          @page {
              margin-top:0;
              margin-bottom: 0;
              margin-left:10%;
              margin-right:10%;
              -webkit-print-color-adjust: exact;
              }

    .headerFont {
        font-family: Georgia, serif;
        font-size: 45px;
        letter-spacing: 0px;
        word-spacing: -2.6px;
        color: #DD4343 !important;
        font-weight: 700;
        text-decoration: none;
        font-style: normal;
        font-variant: normal;
        text-transform: none;
        }

        .headtable {
        background-color: #DD4343 !important;

        }

        table,th,td,tr,th, thead{
        border: 1px solid rgb(61, 59, 59)!important;
        border-bottom: 1px solid rgb(61, 59, 59) !important;

  </style>

    <style type="text/css" media="print">
      @media print {
        div.divfooter{
          position: fixed;
          bottom: 0;
        }
        body {-webkit-print-color-adjust: exact;}
      }


      </style>


</head>
<body onload="window.print(); > <!-- onload="window.print();" -->

<div class="wrapper" >
  <!-- Main content -->
  <section class="invoice"  >
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header" style="padding-bottom: 2%;padding-top:5%;padding-left:15%  ";>
           <img src="{{ URL::asset(  'storage/'.\App\Parameter::first()->logo)}}"  height="90"/> 
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <div class="row">

      <div class="col-xs-12">
        <h2 >
          <h1 class="headerFont" style="text-align: center"> Facture</h1>
          <h3 style="text-align: center"><b>N° :</b> {{$facture->ref_facture}} </h3>
        </h2>
      </div>

      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-6 col-xs-12">
          <h4 ><b>Date :</b>{{ \Carbon\Carbon::parse($devisbc->date_bc)->format('d / m / Y') }}</h4>
          <h4 ><b>N° Commande client :</b>{{$devisbc->num_bc}}</h4>
          <h4 ><b>N° ICE client : </b>{{$devis->client->ice}}</h4>


      </div>
      <!-- /.col -->
      <div class="col-sm-6 col-xs-12" style="padding-bottom: 6%;">
        <div style="padding-left: 25%">

          <h4><b>client :</b>  {{$devis->client->raison_sociale}} </h4>
          <h4 ><b>Adresse : </b>  {{$devis->client->adresse_facturation}} </h4>
        </div>
      </div>

    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row"  >
      <div class="col-xs-12 table-responsive">
        <table class="table ">
          <thead >
          <tr >
            <th>Article</th>
            <th>Ref interne</th>
            <th>Désignation</th>
            <th>Quantite</th>
            <th>Prix Unitaire HT</th>
            <th>Remise</th>
            <th>Prix Total HT</th>
          </tr>
          </thead>
          <tbody>

          @php
          $cc=0;
          @endphp

          @foreach($ventes as $vente)
           @php$cc+=1 @endphp
          <tr >
            <td>{{ $loop->iteration }}</td>
            <td>{{$vente->ref_interne}}</td>
            <td>{{$vente->description}}</td>
            <td>{{$vente->quantite}}</td>
            <td>{{$vente->prix_unitaire}}</td>
            <td>{{$vente->remise}}%</td>
            <td>{{$vente->prix_total}}</td>
          </tr>
          @endforeach

          @for($i = $cc ; $i < 10 ; $i++)
          <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>

            </tr>
          @endfor

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">
        <p class="lead" style="margin-bottom: 0% !important;">Modalité de paiement :</p>
        <b>Mode de paiement :</b> {{ count($echeances) <= 0 ? 'Cheque' : $echeances[0]->type_paiement }}<br>
        <b>Echéance de paiement :</b> {{count($echeances)}}<br>
        @foreach( $echeances as $echeance)
          <small style="padding-left: 3%">{{$echeance->pourcentage}}% {{$echeance->jours}} jours date de facturation</small> <br>
         @endforeach    
         
         <!-- <b>Date de livraison :</b> {{$devis->delai_livraison}}<br> -->

        
      </div>


      <!-- /.col -->
      <div class="col-xs-6">
        <p class="lead" style="margin-bottom: 0% !important;">Total </p>

        <div class="table-responsive">
        @php

          $tva = $devis->prix_total * 20/100;

        @endphp
          <table class="table">
            <tr>
              <th style="width:50%">Total HT:</th>
              <td>{{$devis->prix_total }} DH</td>
            </tr>
            <tr>
              <th>TVA (20%)</th>
              <td> {{ $tva}} DH</td>
            </tr>
            <tr>
              <th>Total TTC:</th>
              <td>{{$devis->prix_total  + $tva}} DH</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


    <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">Service Financier {{\App\Parameter::first()->nom}} :</p>
              <br>
              <br>
              <br>


            </div>
            <!-- /.col -->
            <div class="col-xs-6">
              <p class="lead">Service Financier {{$devis->client->raison_sociale}} </p>
              <br>
              <br>
              <br>

            </div>
            <!-- /.col -->
          </div>

     <div class="divfooter">
        <div class="col-xs-12">
            <img src="{{ URL::asset('assets/images/stepone_footer.png')}}" style="width:100%"/>
        </div>

    </div>


  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
