@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="echeance-vue-wrapper">

      
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Utilisateurs
        <small>Utilisateurs</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Utilisateurs</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Utilisateurs</h3>
              </div>

              <div class="box-body" >


                <div class="col-md-12">

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                        <tr>
                            <td> 
                            <div class="form-group">
                                  <select class="form-control" name="bl_type" id="bl_type" v-model="selectedUser">
                                      <option v-for="user in users" v-bind:value="user.id">@{{user.nom}}</option>
                                  </select>
                                </div>
                            </td>
                          
                            <td>
                                <select class="form-control" name="type_paiement" id="type_paiement" v-model="selectedType">
                                  <option v-for="type in types" v-bind:value="type.type">@{{type.desc}} </option>
                                </select>
                            </td>
                            
                            <td>
                                <button type="button" class="btn btn-success" title="Ajouter" @click.prevent="addItem()">
                                    <span class="fa fa-plus" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->



                </div>

            
                <div class="col-md-12">

                        <hr>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                        <tr>
                            <th>Utilisateur</th>
                            <th>Droits</th>
                            <th>Profile</th>
                            <th></th>
                        </tr>
                        <tr v-for="item in items">
                            <td>@{{item.nom}}</td>
                            <td>@{{item.type}}</td>
                            <td>@{{item.profileType}}</td>
                            <td v-if="item.profileType != 'Admin' && item.profileType != 'assiatant' && currentUser != item.userprofile_id" >
                                <button type="button" class="btn btn-danger" title="Supprimer"  @click.prevent="deleteItem(item)">
                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>

                        </table>
                    </div>
                    <!-- /.box-body -->



                </div>

                </div>

            
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>

var bc_id = "";
var montant = "";
var userProfiles = {!! json_encode($userProfiles) !!};
var devis_id = {!! json_encode($devis->id) !!};
var currentUserProfileID = {!! json_encode( \App\UserProfile::where('user_id','=',$devis->user_id)->first()->id) !!};



var app = new Vue({
  el: '#echeance-vue-wrapper',

  data: {
    currentUser : currentUserProfileID,
    users : userProfiles,
    selectedUser : '',
    selectedType : 'edit',
    types : [{'type':'viewonly','desc':'Visualiser uniquement'},{'type':'edit','desc':'modification'}],
    items: [],
   },

  mounted: function mounted() {
    this.getVueItems();
  },

  methods: {


    getVueItems: function getVueItems() {
      var _this = this;

      axios.get('/usersallow/'+devis_id+'/devis').then(function (response) {
        _this.items = response.data;
      });
    },

    addItem: function(){

      _this = this;

      var data = JSON.stringify( { 'userprofile_id' : this.selectedUser ,'type': this.selectedType ,'devis_id': devis_id});

      axios.post('/usersallow/devis',{data: data}).then(function (response){
        if(response.data['result'] == "OK"){
          console.log(response.data['data']);
          //_this.items = response.data['data'];
                  
        }
        if(response.data['result'] == "KO"){
          alert(response.data['message']);
          console.log(response.data['message']);
        }

        _this.getVueItems();
        
      }).catch(function (error){
        console.log(error)
      })
     

    },
  
    deleteItem: function deleteItem(item){
      
      var _this = this;
      axios.post('/usersallow/'+item.id+'/delete').then(function(response){
        _this.getVueItems();
      });
    }
  }
});

</script>

@endsection
