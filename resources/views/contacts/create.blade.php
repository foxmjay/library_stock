@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
<section class="content-header">


</section>



<section class="content">
    <div class="row">
        <div class="col-xs-12">



          <div class="box">
            <div class="box-header with-border">

              @if ($errors->any())
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              @endif


              <hr>

              <div class="box-body">
               @isset($client)
                <form method="POST" action="{{ url('contacts/store/' . $client->id.'/client') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
               @endisset
               @isset($fournisseur)
                <form method="POST" action="{{ url('contacts/store/' . $fournisseur->id.'/fournisseur') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
               @endisset
                {{ csrf_field() }}

                @include ('contacts.formfix', ['formMode' => 'create'])
              </div>



            </div>

          </div>


            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

  </div>

@endsection
