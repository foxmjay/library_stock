@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
<section class="content-header">
    <h1>
        Contact

    </h1>

</section>



<section class="content">
    <div class="row">
        <div class="col-xs-12">



          <div class="box">
            <div class="box-header with-border">
              <a href="{{ url('/contacts') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
              <br />
              <br />

              @if ($errors->any())
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              @endif


              <hr>

              <div class="box-body">
                <form method="POST" action="{{ url('/contacts') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    @include ('contacts.form', ['formMode' => 'create'])

                </form>
              </div>



            </div>

          </div>


            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
  </div>


@endsection
