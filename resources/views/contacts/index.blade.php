
@extends('layouts.admin')


@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

<div class="content-wrapper">
<section class="content-header">
    <h1>
      Contacts

    </h1>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">



          <div class="box">
            <div class="box-header with-border">
              @isset($clientid)
              <a href="{{ url('/contacts/create/'.$clientid).'/client' }}" class="btn btn-success btn-sm" title="Add New Ville">
                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Contact
                </a>
              @endisset

              @isset($fournisseurid)
              <a href="{{ url('/contacts/create/'.$fournisseurid .'/fournisseur') }}" class="btn btn-success btn-sm" title="Add New Ville">
                    <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Contact
                </a>
              @endisset

              <hr>

              <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                  <div class="row">
                    <div class="col-sm-12">
                      <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1">#</th>
                            <th>Nom</th>
                            <th>Poste</th>
                            <th>Telephone</th>
                            <th  class="col" >Actions</th>


                          </tr>
                        </thead>
                        <tbody>

                        @foreach($contacts as $item)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{ $loop->iteration }}</td>
                                <td>{{ $item->nom }}</td>
                                <td>{{ $item->poste }}</td>
                                <td>{{ $item->telephone }}</td>
                                <td>
                                    <!--<a href="{{ url('/contacts/' . $item->id) }}" title="View Contact"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> -->
                                    @isset($clientid)
                                    <a href="{{ url('/contacts/edit/' . $item->id . '/client') }}" title="Modifier Contact"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                                    @endisset()
                                    @isset($fournisseurid)
                                    <a href="{{ url('/contacts/edit/' . $item->id . '/fournisseur') }}" title="Modifier Contact"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                                    @endisset()
                                    <form method="POST" action="{{ url('/contacts' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger btn-sm" title="Supprimer Contact" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach




                        </tbody>

                      </table>
                    </div>
                  </div>

                </div>
              </div>
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif


            </div>

          </div>


            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
</div>

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable()

    })
  </script>
@endsection
