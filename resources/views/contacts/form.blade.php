<div class="form-group {{ $errors->has('client_id') ? 'has-error' : ''}}">
    <label for="client_id" class="control-label">{{ 'Client' }}</label>
    @if ($formMode === 'edit')
    <select class="form-control" name="client_id" required>
        @foreach ($client as $object)
        @if ($object->id === $contact->client_id)
        <option value="{{$object->id}}" slected>{{$object->raison_sociale}}</option>
        @else
        <option value="{{$object->id}}">{{$object->raison_sociale}}</option>
        @endif
        @endforeach

    </select>
    @else
    <select class="form-control" name="client_id" required>
        @foreach ($client as $object)

        <option value="{{$object->id}}">{{$object->raison_sociale}}</option>

        @endforeach

    </select>
    @endif
    {!! $errors->first('client_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fournisseur_id') ? 'has-error' : ''}}">
    <label for="fournisseur_id" class="control-label">{{ 'Fournisseur' }}</label>
    @if ($formMode === 'edit')
    <select class="form-control" name="fournisseur_id" required>
        @foreach ($fournisseur as $object)
        @if ($object->id === $contact->client_id)
        <option value="{{$object->id}}" slected>{{$object->raison_sociale}}</option>
        @else
        <option value="{{$object->id}}">{{$object->raison_sociale}}</option>
        @endif
        @endforeach

    </select>
    @else
    <select class="form-control" name="fournisseur_id" required>
        @foreach ($fournisseur as $object)

        <option value="{{$object->id}}">{{$object->raison_sociale}}</option>

        @endforeach

    </select>
    @endif
    {!! $errors->first('fournisseur_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    <label for="nom" class="control-label">{{ 'Nom' }}</label>
    <input class="form-control" name="nom" type="text" id="nom" value="{{ $formMode === 'edit' ? $contact->nom : ''}}" required>

    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('poste') ? 'has-error' : ''}}">
    <label for="poste" class="control-label">{{ 'Poste' }}</label>
    <input class="form-control" name="poste" type="text" id="poste" value="{{ $formMode === 'edit' ? $contact->poste : ''}}" >

    {!! $errors->first('poste', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
    <label for="telephone" class="control-label">{{ 'Telephone' }}</label>
    <input class="form-control" name="telephone" type="text" id="telephone" value="{{ $formMode === 'edit' ? $contact->telephone : ''}}" >

    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('gsm') ? 'has-error' : ''}}">
    <label for="gsm" class="control-label">{{ 'Gsm' }}</label>
    <input class="form-control" name="gsm" type="text" id="gsm" value="{{ $formMode === 'edit' ? $contact->gsm : ''}}" >

    {!! $errors->first('gsm', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ $formMode === 'edit' ? $contact->email : ''}}" required>

    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" onSubmit="enableSpinner();" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
