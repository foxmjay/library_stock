@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
<section class="content-header">
    <h1>
        Contact #{{ $contact->id }}

    </h1>

</section>



<section class="content">
    <div class="row">
        <div class="col-xs-12">



          <div class="box">
            <div class="box-header with-border">

              @if ($errors->any())
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              @endif


              <hr>

              <div class="box-body">
                <form method="POST" action="{{ url('/contacts/' . $contact->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                @include ('contacts.formfix', ['formMode' => 'edit'])
              </div>



            </div>

          </div>


            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

  </div>

@endsection
