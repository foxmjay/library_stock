@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clients
        <small>gestion des clients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Clients</li>
      </ol>
    </section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
             
              <a href="{{ url('/clients/create') }}" class="btn btn-success ">
                <i class="fa fa-plus"></i> Ajouter
              </a>

            </div>
            <div class="box-body">
                <div >
                  <div class="row">
                     
                      <form  method="POST"  action="{{ url('/clients/search') }}" accept-charset="UTF-8" style="display:inline">
                        {{ csrf_field() }}
                        <div class="col-sm-10">
                        <div class="form-group">
                          <input class="form-control"  name="search" type="text" id="search" value="" >
                       </div>
                        </div>
                        <div class="col-sm-2">
                          <button type="submit" class="btn btn-primary">Chercher</button>
                        </div>
    
                      </form>

                    </div>
                 </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($clients) > 0)
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Raison sociale</th>
                      <th>Identifient fiscal</th>
                      <th>N patente</th>
                      <th>N registre commerce</th>
                      <th>Telephone</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($clients as $client)
                        <tr>
                          <td>{{ $client->raison_sociale }}</td>
                          <td>{{ $client->identifient_fiscal }}</td>
                          <td>{{ $client->n_patente }}</td>
                          <td>{{ $client->n_reg_commerce }}</td>
                          <td>{{ $client->telephone }}</td>
                          <td>

                              <form method="POST" action="{{ url('/clients' . '/' . $client->id) }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/clients/' . $client->id . '/edit') }}" class="btn btn-info" title="Modifier">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                                    <!--<a href="" class="btn btn-primary" title="Edit Client">
                                        <span class="icon-pencil" aria-hidden="true"></span>
                                    </a> -->

                                    <!--<button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Supprimer Client?&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button>-->
                                    <!--<a href="{{ url('/contacts/' . $client->id) .'/client' }}" class="btn btn-warning" title="Contacts">
                                        <span class="fa fa-user" aria-hidden="true"></span>
                                    </a> -->

                                    <a href="{{ url('/clients/' . $client->id)}}" title="Info's" type="button" class="btn btn-success">
                                        <span class="fa fa-align-center" aria-hidden="true"></span>
                                    </a>


                                </div>
                              </form>

                          </td>
                        </tr>
                        @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Raison sociale</th>
                        <th>Identifient fiscal</th>
                        <th>N patente</th>
                        <th>N registre commerce</th>
                        <th>Telephone</th>
                        <th></th>
                    </tr>
                    </tfoot>
                  </table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable({
        searching: false
      })

    })
  </script>
@endsection
