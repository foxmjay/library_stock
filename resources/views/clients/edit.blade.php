@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="contact-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clients
        <small>gestion des clients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Clients</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification client</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/clients/' . $client->id) }}" accept-charset="UTF-8" id="create_client_form" name="create_client_form" class="form-sample">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('clients.form', ['client' => $client,])
            
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Sauvgarder</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Contacts</h3>
              </div>

              @include ('clients.form_contact')
              
              
            </div>
            <!-- /.box -->
              
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>


var client_id = {!! json_encode($client->id) !!};

var app = new Vue({
  el: '#contact-vue-wrapper',

  data: {
    items: [],
    contactModel : {'client_id':client_id,'id':'','nom':'','poste': '' ,'telephone':'','gsm': '','email':''},
   },

  mounted: function mounted() {
    this.getVueItems();
  },
 
  methods: {

    getVueItems: function getVueItems() {
      var _this = this;
     
      axios.get('/contacts/'+client_id+'/perclient').then(function (response) {
        _this.items = response.data;
      });
    },
    
    addItem: function(){
     
      var _this = this;
      var data = this.contactModel;
      //console.log(data);
      axios.post('/contacts',data).then(function (response){
        //console.log(response);
        _this.getVueItems();
      });

    },
    deleteItem: function deleteItem(item){
      var _this = this;
      axios.post('/contacts/'+item.id+'/delete').then(function(response){
        _this.getVueItems();
      });
    }
  }
});

</script>

@endsection
