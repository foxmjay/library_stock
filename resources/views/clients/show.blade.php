@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"> -->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')

@php
    $userProfile =  \App\UserProfile::where('user_id',Auth::user()->id)->first();
 @endphp


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="devisref">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$client->raison_sociale}}
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h1 class="text-center"><b>{{$client->raison_sociale}}</b></h1>
              <hr>
              <p class="text-muted text-center">{{$client->forme_juridique}} , {{$client->categorie}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Nombre Devis:</b> <a class="pull-right">{{count($devis)}}</a>

                </li>
                <li class="list-group-item">
                  <b>Nombre facture : </b> <a class="pull-right">{{$nbfactures}}</a>
                </li>
                <li class="list-group-item">
                  <b>Chiffre d'affaire HT:</b> <a class="pull-right">{{ $total }} DH</a>
                </li>
                <li class="list-group-item">
                  <b>Total encaissé HT :</b> <a class="pull-right">{{ $total_encaisse }} DH</a>
                </li>

              </ul>

              <a href="{{ url('/devis/'.$client->id.'/create') }}" class="btn btn-success btn-block"><b>Ajouter un Devis</b></a>


              <!--<a href="{{ url('/bonretour/' . $client->id . '/create') }}" class="btn btn-block bg-yellow"><b>Creer Bon retour</b></a>-->



            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Plues d'info</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-fw fa-envelope margin-r-5"></i> Email : {{$client->email}}</strong>
              <hr>
              <strong><i class="fa fa-fw fa-mobile margin-r-5"></i> GSM : {{$client->gsm}}</strong>
              <hr>
              <strong><i class="fa fa-fw fa-phone margin-r-5"></i> Fixe :  {{$client->telephone}}</strong>
              <hr>
              <strong><i class="fa fa-fw fa-map-pin margin-r-5"></i> Ville : {{$client->telephone}}</strong>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div  class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li id="devistab" class="active"><a href="#Devis" data-toggle="tab">Devis</a></li>
              @if ( $userProfile->type == "Admin" || $userProfile->type == "Assistant"  )
              <li id="bctab" ><a href="#Devisbc" data-toggle="tab">Bons de commande</a></li>
              <li id="facturetab" ><a href="#Facture" data-toggle="tab">Factures</a></li>
              <li id="bonretourtab"><a href="#Bonretour" data-toggle="tab">Bons de retour</a></li>
              <li id="echangetab"><a href="#Echange" data-toggle="tab">Echanges</a></li>
              @endif


            </ul>
            <div class="tab-content">

              <div class="active tab-pane" id="Devis">

                <div class="box-body table-responsive">

                    @if(count($devis) == 0)
                    @else
                        <table id="devisTable" class="table table-bordered">
                          <thead>
                          <tr>
                            <th>ID</th>
                            <th>Reference Devis</th>
                            <th>Objet</th>
                            <th>Date</th>
                            <th>Montant HT</th>
                            <th>Cree par</th>
                            <th></th>
                          </tr>
                          </thead>
                          <tbody>
                              @foreach($devis as $devis)
                                  @php
                                    $ff = $userallows->where('devis_id','=',$devis->id)->where('userprofile_id',$userProfile->id);
                                  @endphp

                                  @if(count($ff) > 0)
                                     @if($devis->locked == false)
                                        <tr>
                                          <td >{{ $devis->id }}</td>
                                          <td>{{ $devis->ref_devis }}-{{ $devis->version }}</td>
                                          <td >{{ $devis->objet }}</td>
                                          <td >{{ $devis->created_at->format('d/m/Y') }}</td>
                                          <td >{{ $devis->prix_total }}</td>
                                          <td >{{ $devis->nom  }} {{ $devis->prenom  }} </td>
                                          <td style="min-width: 80px" >
                                                  <form method="POST" action="{{ url('/devis' . '/' . $devis->id) }}" accept-charset="UTF-8">
                                                  <input name="_method" value="DELETE" type="hidden">
                                                  {{ csrf_field() }}
                                                    <div class="btn-group btn-group-sm" role="group">
                                                      <a href="{{ url('/devis/' . $devis->id . '/edit') }}" class="btn btn-info" title="Modifier"> <span class="fa fa-edit" aria-hidden="true"></span></a>
                                                      @if($devis->valider == true)
                                                      <a href="{{ url('/devis/' . $devis->id . '/printdevis') }}" target="_blank" class="btn btn-warning" title="Imprimer Devis"><span class="fa fa-print" aria-hidden="true"></span></a>
                                                      @endif
                                                      <button type="button" class="btn btn-active" title="Ancien devis" v-on:click="getOldDevis({{$devis->parentdevis_id}})" ><span class="fa fa-eye" aria-hidden="true"></span></button>
                                                      <a href="{{ url('/echeances/' . $devis->id . '/indexdevis/devis') }}"  class="btn btn-success" title="Echeance Paiement"><span class="fa fa-tags" aria-hidden="true"></span></a>
                                                      <a href="{{ url('/usersallow/'. $devis->id) }}"  class="btn bg-blue" title="Utilisateurs autorisés"><span class="fa fa-users" aria-hidden="true"></span></a>
                                                    </div>
                                                  </form>

                                          </td>
                                        </tr>
                                      @endif
                                  @endif
                              @endforeach

                          </tbody>

                        </table>
                    @endif
                  </div>

              </div>
              <!-- /.tab-pane -->
              @if ( $userProfile->type == "Admin" || $userProfile->type == "Assistant"  )
              <div class="tab-pane" id="Devisbc">

                <div class="box-body table-responsive">

                    @if(count($devisbcs) == 0)
                    @else
                        <table id="devisbcTable" class="table table-bordered">
                          <thead>
                          <tr>
                            <th>ID</th>
                            <th>Reference bon commande</th>
                            <th>Date</th>
                            <th>Montant HT</th>
                            <th>Cree par</th>
                            <th></th>
                          </tr>
                          </thead>
                          <tbody>
                              @foreach($devisbcs as $devisbc)

                                  @php
                                    $ff = $userallows->where('devis_id','=',$devisbc->devisid)->where('userprofile_id',$userProfile->id);
                                  @endphp

                                  @if(count($ff) > 0)

                                    @if($devisbc->etat == "NonComplet")
                                    <tr>
                                      <td >{{ $devisbc->id }}</td><td>{{ $devisbc->ref_devis }}</td><td >{{ $devisbc->date_bc }}</td><td>{{ $devisbc->prix_total }}DH</td>
                                      <td >{{ $devisbc->nom }} {{ $devisbc->prenom }}</td>
                                      <td style="min-width: 80px" >
                                            <div class="btn-group btn-group-sm" role="group">
                                            @if ( $userProfile->type == "Admin" || $userProfile->type == "Assistant"   )
                                              <a href="{{ url('/bonlivraison/' . $devisbc->devisid . '/create') }}"  class="btn btn-success" title="Creer Bon de livraison"><span class="fa fa-clone" aria-hidden="true"></span></a>
                                            @endif
                                              <a href="{{ url('/devis/' . $devisbc->devisid . '/edit2') }}" class="btn btn-info" title="Modifier"> <span class="fa fa-edit" aria-hidden="true"></span></a>
                                              <button type="button" class="btn btn-active" title="Liste Bons livraison" v-on:click="getBls({{$devisbc->devisid}})" ><span class="fa fa-eye" aria-hidden="true"></span></button>
                                              <a href="{{ url('/echeances/' . $devisbc->devisid . '/indexdevis/devis') }}"  class="btn btn-success" title="Echeance Paiement"><span class="fa fa-tags" aria-hidden="true"></span></a>
                                              <a href="{{ url('/usersallow/'. $devisbc->devisid) }}"  class="btn bg-blue" title="Utilisateurs autorisés"><span class="fa fa-users" aria-hidden="true"></span></a>
                                              <a  href="{{ URL::asset(  'storage/'.$devisbc->image_bc  )}}" target="_blank" class="btn bg-yellow"> <span class="fa fa-file-image-o" aria-hidden="true"></span> </a>
                                            </div>
                                      </td>
                                    </tr>
                                    @endif

                                  @endif
                              @endforeach

                          </tbody>

                        </table>
                    @endif
                  </div>

              </div>
              <!-- /.tab-pane -->


              <div class="tab-pane" id="Facture">

                  <div class="box-body table-responsive">

                      @if(count($devisbcs) == 0)
                      @else


                          <table id="factureTable" class="table table-bordered">
                            <thead>
                            <tr>
                              <th>Reference bon de commande</th>
                              <th>Reference de facture</th>
                              <th>Type</th>
                              <th>Date</th>
                              <th>Montant HT</th>
                              <th>Cree par</th>
                              <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($facturesPartiel as $facture)

                                  @php
                                    $ff = $userallows->where('devis_id','=',$facture->devis_id)->where('userprofile_id',$userProfile->id);
                                  @endphp

                                  @if(count($ff) > 0)

                                        <tr style="{{ ($facture->etat == 'NonComplet') ?  'background-color:rgb(255, 160, 160) ' : ''}}" >
                                          <td>{{ $facture->ref_devis }}</td><td>{{ $facture->ref_facture }}</td><td>{{ $facture->type_facture }}</td><td >{{ $facture->created_at }}</td> <td >{{ $facture->prix_total }}DH</td>
                                          <td >{{ $facture->nom }} {{ $facture->prenom }}</td>
                                          <td style="min-width: 60px" >
                                            <div class="btn-group btn-group-sm" role="group">

                                            <a href="{{ url('/devis/' . $facture->devis_id . '/edit2') }}" class="btn btn-info" title="Modifier"> <span class="fa fa-edit" aria-hidden="true"></span></a>
                                            <a href="/bonlivraison/{{ $facture->bl_id}} /show"  class="btn btn-success" title="Affichier Bon de livraison"><span class="fa fa-tv" aria-hidden="true"></span> </a>
                                             <!-- <button type="button" class="btn btn-active" title="Liste Bons livraison" v-on:click="getBls({{$facture->devis_id}})" ><span class="fa fa-eye" aria-hidden="true"></span></button> -->
                                             @if ( $userProfile->type == "Admin" || $userProfile->type == "Assistant"   )

                                               <a href="/bonlivraison/{{ $facture->bl_id}} /printbl" target="_blank"  class="btn bg-purple" title="Imprimer bon livraison"> <span class="fa fa-print" aria-hidden="true"></span></a>
                                              <a href="/bonlivraison/{{ $facture->bl_id}} /printfacture" target="_blank" class="btn bg-maroon" title="Imprimer Facture"> <span class="fa fa-print" aria-hidden="true"></span></a>
                                            @endif
                                             <a href="{{ url('/echeances/' . $facture->devis_id . '/indexdevis/devis') }}"  class="btn btn-success" title="Echeance Paiement"><span class="fa fa-tags" aria-hidden="true"></span></a>
                                             <a href="{{ url('/usersallow/'. $facture->devis_id) }}"  class="btn bg-blue" title="Utilisateurs autorisés"><span class="fa fa-users" aria-hidden="true"></span></a>
                                             @if ( $userProfile->type == "Admin" || $userProfile->type == "Assistant"   )

                                             <a href="/bonretour/{{ $facture->bl_id}} /frombl"  class="btn bg-aqua" title="Bon de retour"><span class="fa fa-reply" aria-hidden="true"></span></a>
                                             <a href="/echange/{{ $facture->bl_id}}/create"  class="btn bg-fuchsia" title="Echange"><span class="fa fa-exchange" aria-hidden="true"></span></a>
                                             @endif
                                            </div>
                                          </td>
                                        </tr>
                                  @endif
                                @endforeach



                                @foreach($facturesComplet as $facture)

                                @php
                                  $ff = $userallows->where('devis_id','=',$facture->devis_id)->where('userprofile_id',$userProfile->id);
                                @endphp

                                @if(count($ff) > 0)
                                      <tr>
                                      <td>{{ $facture->ref_devis }}</td> <td>{{ $facture->ref_facture }}</td><td>{{ $facture->type_facture }}</td><td >{{ $facture->created_at }}</td> <td >{{ $facture->prix_total }}DH</td>
                                      <td >{{ $facture->nom }} {{ $facture->prenom }}</td>

                                        <td style="min-width: 60px" >
                                          <div class="btn-group btn-group-sm" role="group">
                                          <a href="{{ url('/devis/' . $facture->devis_id . '/edit2') }}" class="btn btn-info" title="Modifier"> <span class="fa fa-edit" aria-hidden="true"></span></a>
                                          <a  href="/bonlivraison/{{$facture->devis_id}}/printfacturetotal" target="_blank" class="btn bg-maroon" title="Imprimer Facture"><span class="fa fa-print" aria-hidden="true"></span></a>
                                          <button type="button" class="btn btn-active" title="Liste Bons livraison" v-on:click="getBls({{$facture->devis_id}})" ><span class="fa fa-eye" aria-hidden="true"></span></button>
                                           <a href="{{ url('/echeances/' . $facture->devis_id . '/indexdevis/devis') }}"  class="btn btn-success" title="Echeance Paiement"><span class="fa fa-tags" aria-hidden="true"></span></a>
                                           <a href="{{ url('/usersallow/'. $facture->devis_id) }}"  class="btn bg-blue" title="Utilisateurs autorisés"><span class="fa fa-users" aria-hidden="true"></span></a>
                                          </div>
                                        </td>
                                      </tr>
                                @endif
                                @endforeach

                            </tbody>

                          </table>
                      @endif
                    </div>

                  </div>
                  <!-- /.tab-pane -->


              <div class="tab-pane" id="Bonretour">

                    <div class="box-body table-responsive">
                        <div i class="col-md-9">
                          </div>

                    <div class="col-md-12">
                        @if(count($bonretours) == 0)
                        @else
                            <table id="bonretourTable" class="table table-bordered">
                              <thead>
                              <tr>
                                <th>ID</th>
                                <th>Reference bon de retour</th>
                                <th>Date</th>
                                <th>Montant HT</th>
                                <th>Cree par</th>
                                <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                  @foreach($bonretours as $bonretour)
                                    @php
                                      $ff = $userallows->where('devis_id','=',$bonretour->devis_id)->where('userprofile_id',$userProfile->id);
                                    @endphp
                                    @if(count($ff) > 0)
                                      <tr>
                                        <td >{{ $bonretour->id }}</td>
                                        <td>{{ $bonretour->ref_br }}</td>
                                        <td >{{ $bonretour->date_br }}</td>
                                        <td >{{ $bonretour->prix_total }} DH</td>
                                        <td >{{ $bonretour->nom }} {{ $bonretour->prenom }}</td>


                                        <td style="min-width: 80px" >

                                          <a href="/bonretour/{{$bonretour->id}}/printbr" target="_blank"  class="btn bg-purple" title="Imprimer bon de retour">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                          </a>
                                          <a href="/bonretour/{{$bonretour->id}}/printavoir" target="_blank"  class="btn bg-red" title="Imprimer avoir">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                          </a>
                                          <a  href="{{ URL::asset(  'storage/'.$bonretour->image_br  )}}" target="_blank" class="btn bg-yellow" ><span class="fa fa-file-image-o" aria-hidden="true"></span></a>


                                        </td>
                                      </tr>
                                    @endif
                                  @endforeach

                              </tbody>

                            </table>
                        @endif
                      </div>
                    </div>

                  </div>
              <!-- /.tab-pane -->


              <div class="tab-pane" id="Echange">

                  <div class="box-body table-responsive">
                      <div i class="col-md-9">
                        </div>

                  <div class="col-md-12">
                      @if(count($echanges) == 0)
                      @else
                          <table id="echangeTable" class="table table-bordered">
                            <thead>
                            <tr>
                              <th>ID</th>
                              <th>Reference echange</th>
                              <th>Date</th>
                              <th>Cree par</th>
                              <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($echanges as $echange)
                                  @php
                                    $ff = $userallows->where('devis_id','=',$echange->devis_id)->where('userprofile_id',$userProfile->id);
                                  @endphp
                                  @if(count($ff) > 0)
                                    <tr>
                                      <td >{{ $echange->id }}</td>
                                      <td>{{ $echange->ref_echange }}</td>
                                      <td >{{ $echange->created_at }}</td>
                                      <td >{{ $echange->nom }} {{ $echange->prenom }}</td>
                                      <td style="min-width: 80px" >

                                        <a href="/echange/{{$echange->id}}/printbl" target="_blank"  class="btn bg-purple" title="Imprimer bon de retour">
                                          <span class="fa fa-print" aria-hidden="true"></span>
                                        </a>

                                      </td>
                                    </tr>
                                  @endif
                                @endforeach

                            </tbody>

                          </table>
                      @endif
                    </div>
                  </div>

                </div>
            <!-- /.tab-pane -->
                @endif

              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <div id="devislie" class="col-md-12">
          <div class="box box-primary" >

              <div class="box-header">
                <h3 class="box-title">Devis lies</h3>
              </div>

                <div class="box-body box-profile">

                        <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Reference devis</th>
                                    <th>Date</th>
                                    <th>Cree par</th>
                                    <th></th>
                                </tr>
                                <tr v-for="item in items">
                                    <td>@{{item.id}}</td>
                                    <td>@{{item.ref_devis}}-@{{item.version}}</td>
                                    <td>@{{item.created_at}}</td>
                                    <td>@{{item.nom}} @{{item.prenom}}</td>

                                    <td>
                                        <!--<a v-bind:href="'/devis/'+item.id+'/edit2'" class="btn btn-info" title="Modifier"> <span class="fa fa-edit" aria-hidden="true"></span></a>-->

                                        <a v-if="item.valider == true" v-bind:href="'/devis/'+item.id+'/printdevis'" target="_blank" class="btn btn-warning" title="Imprimer Devis"><span class="fa fa-print" aria-hidden="true"></span></a>

                                            <!--<button type="button" class="btn btn-danger" title="Supprimer" >
                                                <span class="fa fa-trash" aria-hidden="true"></span>
                                            </button>-->
                                    </td>
                                </tr>

                                </table>
                            </div>

                </div>
                <!-- /.box-body -->
          </div>

        </div>
        <div id="bllie" class="col-md-12">
          <div class="box box-primary" >

          <div class="box-header">
              <h3 class="box-title">Bons de livraison</h3>
          </div>

            <div class="box-body box-profile">

                    <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                            <tr>
                                    <th>Reference bon livraison</th>
                                    <th>Type facture</th>
                                    <th>Date</th>
                                    <th>Montant HT</th>
                                    <th></th>
                            </tr>
                            <tr v-for="bl in bls">
                                <td>@{{bl.ref_bl}}</td>
                                <td>@{{bl.type_facture}}</td>
                                <td>@{{bl.created_at}}</td>
                                <td>@{{bl.prix_total}} DH</td>

                                <td>
                                    <a v-bind:href="'/bonlivraison/'+bl.id+'/show'"  class="btn btn-success" title="Affichier Bon de livraison">
                                              <span class="fa fa-tv" aria-hidden="true"></span>
                                    </a>
                                    <a v-bind:href="'/bonlivraison/'+bl.id+'/printbl'" target="_blank"  class="btn bg-purple" title="Imprimer bon livraison">
                                      <span class="fa fa-print" aria-hidden="true"></span>
                                    </a>

                                    <a v-if="bl.type_facture == 'Partiel'" v-bind:href="'/bonlivraison/'+bl.id+'/printfacture'" target="_blank" class="btn bg-maroon" title="Imprimer Facture">
                                      <span class="fa fa-print" aria-hidden="true"></span>
                                    </a>
                                    <a v-if="bl.etat == 'Complet'" v-bind:href="'/bonretour/'+bl.id+'/frombl'"  class="btn bg-aqua" title="Bon de retour">
                                              <span class="fa fa-reply" aria-hidden="true"></span>
                                    </a>

                                    <a v-if="bl.etat == 'Complet'" v-bind:href="'/echange/'+bl.id+'/create'"  class="btn bg-fuchsia" title="Echange"><span class="fa fa-exchange" aria-hidden="true"></span></a>


                                </td>

                            </tr>

                            </table>
                        </div>

            </div>
            <!-- /.box-body -->
      </div>

    </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>


@endsection

@section('js')


 <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
  <script>
    $(function() {
      $('#devisTable').DataTable()
      $('#devisbcTable').DataTable()
      $('#factureTable').DataTable()
      $('#bonretourTable').DataTable()
      $('#echangeTable').DataTable()

    })
  </script>

<script>

    $('#bllie').hide();
    $('#devislie').show();


    var app = new Vue({
      el: '#devisref',

      data: {

        items: [],
        bls: [],
        //showed:false,

       },

      mounted: function mounted() {
        //this.getVueItems();
        //this.prix_total = this.calcPrixTotal();
      },
      methods: {
        getOldDevis: function (devis_id) {
                //this.showed = true;
                var _this = this;

                axios.get('/devis/'+devis_id+'/getolddevis').then(function (response) {
                    _this.items = response.data;
                });
        },

        getBls:  function getBls(devis_id){
          var _this = this;
          axios.get('/bonlivraison/'+devis_id+'/perbl').then(function (response) {
                _this.bls = response.data;
          });

        },

      }

    });



    $('document').ready(function() {

          $('#bctab').on('click', function(event){
            $('#bllie').show();
            $('#devislie').hide();
             app.bls = [];
          });

          $('#facturetab').on('click', function(event){
            $('#bllie').show();
            $('#devislie').hide();
             app.bls = [];
          });

          $('#devistab').on('click', function(event){
            $('#bllie').hide();
            $('#devislie').show();
          });

          $('#bonretourtab').on('click', function(event){
            $('#bllie').hide();
            $('#devislie').hide();
            app.bls = [];
          });

          $('#echangetab').on('click', function(event){
            $('#bllie').hide();
            $('#devislie').hide();
            app.bls = [];
          });

    });


    </script>


@endsection
