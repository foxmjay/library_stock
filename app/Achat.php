<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Achat extends Model
{

    //use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'achats';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['bc_id', 'produit_id', 'prix_unitaire','quantite','prix_total', 'created_at', 'updated_at'];


    public function produit()
    {
        return $this->belongsTo('App\Produit');
    }

}
