<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contact extends Model
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id', 'fournisseur_id', 'nom', 'poste', 'telephone', 'gsm', 'email'];


        public function client()
        {
            return $this->belongsTo('App\Client');
        }
        public function fournisseur()
        {
            return $this->belongsTo('App\Fournisseur');
        }



}
