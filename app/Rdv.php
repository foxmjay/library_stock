<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\UserProfile;
use Auth;

class Rdv extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rdvs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'nom',
                'societe',
                'prenom',
                'profession',
                'tel',
                'email',
                'adresse',
                'application',
                'etat',
                'date',
                'date_rappel',
                'heur',
                'heur_rappel',
                'commercial',
                'created_by',
                'action',
                'note',
                'feedback',
                'etat_retour',
                'etat_appel',
                'created_at',
                'updated_at',
                'deleted_at'
              ];


    public function commercial(){

        return $this->belongsTo('App\UserProfile');
    }                  


    public static function rdvNotifsCount(){
        $user_id = Auth::id();
        $userprofile = UserProfile::where('user_id',$user_id)->first();
        $rdvs = Rdv::where('commercial',$userprofile->id)->where('action','Aucune')->get();
        return count($rdvs);
    }

}
