<?php

namespace App;

use App\Usersallow;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;


use Illuminate\Database\Eloquent\Model;

class Usersallow extends Model{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usersallows';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'devis_id',
                'bc_id',
                'userprofile_id',
                'type',
                'created_at',
                'updated_at',
              ];


    /*public static function haspermission(){
        
    }*/

    public static function copyPermissions($src_id,$dst_id,$type){

      
      if($type=="devis"){
        $userallowSrc = Usersallow::where('devis_id',$src_id)->get();
        foreach($userallowSrc as $u){
          $usersallow = Usersallow::create(['devis_id'=>$dst_id,'userprofile_id'=>$u->userprofile_id]);
        }

      }

    }

    public static function addPermissions($id,$type,$user_id){

      $userProfile = UserProfile::where('user_id',$user_id)->first();
      if($type=="devis")$usersallow = Usersallow::create(['devis_id'=>$id,'userprofile_id'=>$userProfile->id]);
      if($type=="boncommand")$usersallow = Usersallow::create(['bc_id'=>$id,'userprofile_id'=>$userProfile->id]);
    }

    public static function setPermissions($id,$type){

        $admins = UserProfile::where('type','=','Admin')->get();

        $assistants = UserProfile::where('type','=','Assistant')->get();

        foreach($admins as $admin){
          if($type=="devis")$usersallow = Usersallow::create(['devis_id'=>$id,'userprofile_id'=>$admin->id]);
          if($type=="boncommand")$usersallow = Usersallow::create(['bc_id'=>$id,'userprofile_id'=>$admin->id]);
        }

        foreach($assistants as $assistant){
          if($type=="devis")$usersallow = Usersallow::create(['devis_id'=>$id,'userprofile_id'=>$assistant->id]);
          if($type=="boncommand")$usersallow = Usersallow::create(['bc_id'=>$id,'userprofile_id'=>$assistant->id]);
        }

        $userProfile = UserProfile::where('user_id',Auth::id())->first();
        if($type=="devis")$usersallow = Usersallow::create(['devis_id'=>$id,'userprofile_id'=>$userProfile->id]);
        if($type=="boncommand")$usersallow = Usersallow::create(['bc_id'=>$id,'userprofile_id'=>$userProfile->id]);
    }

}
