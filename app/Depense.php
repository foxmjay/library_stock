<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depense extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'depenses';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'nom',
                'prenom',
                'montant',
                'date',
                'motif',
                'autremotif',
                'justifie',
                'factureref',
                'imagesfacture',
                'type',
                'created_at',
                'updated_at',
                'deleted_at'
              ];

}
