<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenteDirect extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vente_directs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'nom',
                'adresse',
                'ref',
                'prix_total',
                'objet',
                'garantie',
                'etat',
                'user_id',
                'created_at',
                'updated_at',
              ];
        
}
