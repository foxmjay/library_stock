<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointVente extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'point_ventes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'client_id',
                'ref',
                'prix_total',
                'objet',
                'garantie',
                'etat',
                'user_id',
                'recu',
                'remise',
                'rendu',
                'type_paiement',
                'image_cheque_id',
                'created_at',
                'updated_at',
              ];
}
