<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{

   //use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stocks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['produit_id','fournisseur_id', 'num_serie','ref_fournisseur','prix_unitaire','etat',];

    public function produit(){
        return $this->belongsTo('App\Produit');
    }

    public function fournisseur(){

        return $this->belongsTo('App\Fournisseur');
    }


}
