<?php

namespace App\Http\Controllers;

use App\BonretourStock;
use App\Bonretour;
use App\Vente;
use App\Devis;
use App\Devisbc;
use App\Stock;
use App\Bonlivraison;
use App\Bl_vente;
use App\Client;
use App\Echange;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class EchangeController extends Controller{


    public function create($bl_id){

        $bl = Bonlivraison::findOrFail($bl_id);
        $devis_id = $bl->devis_id;
        $devis = Devis::findOrFail($devis_id);
        $client_id = $devis->client_id;
        $num_bc = Devisbc::where('devis_id',$devis->id)->first()->num_bc;



        $ventes = DB::select("SELECT bl_ventes.stock_id as id, produits.nom, produits.id as produit_id, produits.marque, stocks.num_serie, produits.ref_interne,
            produits.description,produits.garantie, fournisseurs.raison_sociale, bl_ventes.vente_id, bl_ventes.created_at as date_vente
            FROM bl_ventes
            join ventes on ventes.id = bl_ventes.vente_id
            join produits on produits.id = ventes.produit_id
            join stocks on stocks.id = bl_ventes.stock_id
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            where bl_ventes.bl_id = ".$bl_id."
            and not exists( select echanges.stock_src from echanges 
            where echanges.stock_src = bl_ventes.stock_id and
            echanges.bl_id = ".$bl_id." );");


        return view('echanges.create',compact('bl','devis_id','ventes','client_id','num_bc'));
    }


    public function indexFournisseur(Request $request){

        $keyword = $request->get('search');  
      
        $echanges = Collection::make(new Echange);

        if (!empty($keyword)) {
            $echanges = Echange::join('stocks','stocks.id','=','echanges.stock_src')
                            ->join('produits','produits.id','=','stocks.produit_id')
                            ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')
                            ->where('stocks.ref_fournisseur','like', '%'.$keyword.'%')
                            ->orwhere('produits.nom','like','%'.$keyword.'%')
                            ->orwhere('produits.marque','like','%'.$keyword.'%')
                            ->orwhere('produits.description','like','%'.$keyword.'%')
                            ->orwhere('stocks.num_serie','like','%'.$keyword.'%')
                            ->orwhere('fournisseurs.raison_sociale','like','%'.$keyword.'%')
                            ->get(['echanges.id','produits.nom','fournisseurs.raison_sociale','produits.marque','produits.ref_interne','stocks.num_serie', 'stocks.etat']);
           
        }else{
            
            $echanges = Echange::join('stocks','stocks.id','=','echanges.stock_src')
            ->join('produits','produits.id','=','stocks.produit_id')
            ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')->take(20)
            ->get(['echanges.id','produits.nom','fournisseurs.raison_sociale','produits.marque','produits.ref_interne','stocks.num_serie','stocks.etat']);

        }

        //$echanges  = $echanges->where('etat','=','Echange');

        return view('echanges.index_fournisseur', compact('echanges'));
    }



    public function store(Request $request){

        
        //$data = json_decode($request->data,true);
        //info($request->stockOrig);
        $date=date("Y");
        $echange = Echange::latest()->first(['id','ref_echange']);
        $ref_echange="";
        if(empty($echange)){
            $ref_echange = $date.sprintf('-%05d',1);
        }else{
            $ref_echange = $date.sprintf('-%05d',$echange->id +1);   
        }

        if($request->action == "Echange"){

                $echange = Echange::create(['client_id'=>$request->client_id,
                                            'devis_id'=>$request->devis_id,
                                            'bl_id'=>$request->bl_id,
                                            'vente_id'=>$request->vente_id,
                                            'stock_src'=>$request->stockOrig,
                                            'stock_dst'=>$request->stockEchange,
                                            'ref_echange'=>$ref_echange,
                                            ]);

                $stock = Stock::find($request->stockOrig);
                $stock->etat = 'Echange';
                $stock->save();

                $stock = Stock::find($request->stockEchange);
                $stock->etat = 'Vendu';
                $stock->save();
        }else{

                $echange = Echange::create(['client_id'=>$request->client_id,
                                            'devis_id'=>$request->devis_id,
                                            'bl_id'=>$request->bl_id,
                                            'vente_id'=>$request->vente_id,
                                            'stock_src'=>$request->stockOrig,
                                            'ref_echange'=>$ref_echange,
                                            ]);

                $stock = Stock::find($request->stockOrig);
                $stock->etat = 'Echange';
                $stock->save();

        }

        return response()->json(['result' => "OK"]);

    }



    public function echange_fournisseur($echange_id){


        $echange = DB::select("SELECT echanges.id, echanges.stock_src, produits.nom, produits.marque, produits.description, produits.ref_interne,
        stocks.num_serie , produits.garantie , stocks.created_at as date_achat
         from echanges 
         join stocks on stocks.id  = echanges.stock_src
         join produits on produits.id = stocks.produit_id
         where echanges.id = ".$echange_id.";");


        return view('echanges.echange_fournisseur',compact('echange'));
    }

    public function store_fournisseur(Request $request){



        //------------------- if the num serie exists already -------------//

        if(DB::table('stocks')->where('num_serie', $request->num_serie)->exists()){
            return response()->json(['result' => "KO",'stock' => $request->num_serie]);
        }
    
      
        //------------------------------------------------------------------------//
    

        $date=date("Y");
        $echange = Echange::latest()->first(['id','ref_echange']);
        $ref_echange="";
        if(empty($echange)){
            $ref_echange = $date.sprintf('-%05d',1);
        }else{
            $ref_echange = $date.sprintf('-%05d',$echange->id +1);   
        }

        $echange = Echange::find($request->id);
        

        if($request->action == "Echange"){

            $stock = Stock::find($echange->stock_dst);
            $stock->etat="Gone";
            $stock->save();

            $newStock = Stock::create(['produit_id'=>$stock->produit_id,
                                       'fournisseur_id'=>$stock->fournisseur_id,
                                       'num_serie'=>$request->num_serie,
                                       'ref_fournisseur'=>$request->ref_fournisseur,
                                       'prix_unitaire'=>$stock->prix_unitaire,
            ]);

            $echange = Echange::create(['fournisseur_id'=>$stock->fournisseur_id,
                                        'stock_src'=>$stock->id,
                                        'stock_dst'=>$newStock->id,
                                        'ref_echange'=>$ref_echange,
                                        ]);

        }else{


        }

        return response()->json(['result' => "OK"]);

    }


    public function printbl($echange_id){

        $echange = Echange::findOrFail($echange_id);
        $devis = Devis::find($echange->devis_id);

        $client = Client::findOrFail($echange->client_id);
        
        $stock = DB::select("SELECT produits.id, produits.nom, produits.description, count(*) as quantite
        FROM echanges
        join stocks on stocks.id = echanges.stock_dst
        join produits on produits.id = stocks.produit_id
        where echanges.id = ".$echange_id."
        Group by produits.id;");

        return view('prints.printbl_echange',compact('echange','stock','client'));
    }

}
