<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Message;
use App\user;
use DB;
use Illuminate\Http\Request;





class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {



            $messages = DB::select("select  users.name,messages.* FROM messages,users WHERE WEEK(SYSDATE()) = WEEK(messages.created_at) and id_distinataire = ".Auth::id()." and users.id = messages.id_createur ORDER BY messages.created_at desc");


        return view('messages.index', compact('messages'));
    }



    public function envoye()
    {
            $messages = DB::select("select  users.name,messages.* FROM messages,users WHERE WEEK(SYSDATE()) = WEEK(messages.created_at) and id_createur = ".Auth::id()." and users.id = messages.id_distinataire ORDER BY messages.created_at desc");

        return view('messages.envoye', compact('messages'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $destinataires = User::all();
        // dd($destinataires);
        return view('messages.create', compact('destinataires'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'message' => 'required',
			'objet' => 'required'
		]);

        $request['type'] =  'local';
        $request['id_createur'] = Auth::id();
        $requestData = $request->all();
        // dd($requestData);


        Message::create($requestData);

        return redirect('messages')->with('flash_message', 'Message added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);

        return view('messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $message = Message::findOrFail($id);
        $destinataires = User::cursor();
        return view('messages.edit', compact('message','destinataires'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'message' => 'required',
			'objet' => 'required'
		]);
        $requestData = $request->all();

        $message = Message::findOrFail($id);
        $message->update($requestData);

        return redirect('messages')->with('flash_message', 'Message updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Message::destroy($id);

        return redirect('messages')->with('flash_message', 'Message deleted!');
    }
}
