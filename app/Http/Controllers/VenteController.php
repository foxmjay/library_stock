<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vente;
use App\Produit;
use App\Client;
use App\Image;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $vente = Vente::where('client_id', 'LIKE', "%$keyword%")
                ->orWhere('produit_id', 'LIKE', "%$keyword%")
                ->orWhere('quantite', 'LIKE', "%$keyword%")
                ->orWhere('prix_unitaire', 'LIKE', "%$keyword%")
                ->orWhere('prix_total', 'LIKE', "%$keyword%")
                ->orWhere('image_facture_id', 'LIKE', "%$keyword%")
                ->orWhere('type_paiment', 'LIKE', "%$keyword%")
                ->orWhere('date_cheque', 'LIKE', "%$keyword%")
                ->orWhere('image_cheque_id', 'LIKE', "%$keyword%")
                ->orWhere('avance', 'LIKE', "%$keyword%")
                ->orWhere('etat', 'LIKE', "%$keyword%")
                ->orWhere('date_complete', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $vente = Vente::with(['Produit','client'])->latest()->paginate($perPage);
        }


        return view('vente.index', compact('vente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      $produit = produit::latest()->cursor();
      $client = Client::latest()->cursor();
        return view('vente.create', compact('client','produit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $this->validate($request, [
			'client_id' => 'required',
			'stock_id' => 'required',
			'type_paiment' => 'required',
			'etat' => 'required'
        ]);

        $image_facture=null;
        $image_cheque=null;

        $requestData = $request->all();

        if($request->hasFile('image_facture')){
            $path = $request->image_facture->store('uploads','public');
            $requestData['image_facture'] = $path;
        }
        if($request->hasFile('image_cheque')){
            $path = $request->image_cheque->store('uploads','public');
            $requestData['image_cheque'] = $path;
        }


        Vente::create($requestData);

        return redirect('vente')->with('flash_message', 'Vente added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $vente = Vente::findOrFail($id);

        return view('vente.show', compact('vente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $vente = Vente::findOrFail($id);

        $produit = Produit::latest()->cursor();
        $client = Client::latest()->cursor();
        return view('vente.edit', compact('vente','client','produit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $image_facture=null;
        $image_cheque=null;

        $requestData = $request->all();

        if($request->hasFile('image_facture')){
            $path = $request->image_facture->store('uploads','public');
            $requestData['image_facture'] = $path;
        }
        if($request->hasFile('image_cheque')){
            $path = $request->image_cheque->store('uploads','public');
            $requestData['image_cheque'] = $path;
        }

        $this->validate($request, [
			'client_id' => 'required',
			'stock_id' => 'required',
			'type_paiment' => 'required',
			'etat' => 'required'
		]);

        $vente = Vente::findOrFail($id);
        $vente->update($requestData);

        return redirect('vente')->with('flash_message', 'Vente updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Vente::destroy($id);

        return redirect('vente')->with('flash_message', 'Vente deleted!');
    }


      /*--------------------------- API  for VueJS ------------------------------*/

      public function getItems($devis_id){

        //$vente = Vente::join('produits','produits.id','=','ventes.produit_id')->where('devis_id',$devis_id)->get(['ventes.id','nom','quantite','prix_unitaire','prix_total']);
        /*$vente = Vente::join('produits','produits.id','=','ventes.produit_id')
                        ->where('ventes.devis_id',$devis_id)
                        ->get(['ventes.id','produits.id as produit_id','produits.nom','produits.ref_interne' ,'ventes.quantite','ventes.prix_unitaire','ventes.remise','ventes.prix_total']);
        */

        $vente = DB::select("SELECT ventes.id, produits.id as produit_id, produits.nom,produits.ref_interne ,ventes.quantite,ventes.prix_unitaire,ventes.remise,ventes.prix_total,
            IFNULL((select max(stocks.prix_unitaire) from stocks where stocks.produit_id = produits.id and stocks.etat = 'Disponible' ),0) as prix_max
            FROM  ventes
            join  produits on produits.id = ventes.produit_id
            WHERE ventes.devis_id = ".$devis_id.";");

        return $vente;
    }
    
    public function getQuantite(Request $request){

        $data = json_decode($request->data,true);

        $quantite = DB::select("SELECT bl_ventes.vente_id , count(*) as quantite
        FROM bl_ventes
        WHERE bl_ventes.vente_id =".$data['vente_id'].";");


        return response()->json(['result' => "OK",'quantite' => $quantite[0]->quantite]);

    }

    public function  getVentesNotSelected($devis_id){
        
        /*$vente = Vente::join('stocks','stocks.id','=','ventes.stock_id')
        ->join('produits','produits.id','=','stocks.produit_id')
        ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')
        ->join('bl_ventes','bl_ventes.vente_id','=','ventes.id')
        ->join('bonlivraisons','bonlivraisons.id','=','bl_ventes.bl_id')
        ->where('bonlivraisons.devis_id',$devis_id)->get(['ventes.id','produits.nom','fournisseurs.raison_sociale','ventes.quantite','ventes.prix_unitaire','ventes.prix_total','selected']);*/

        $vente = DB::select("SELECT ventes.id, produits.nom, produits.ref_interne, stocks.num_serie ,fournisseurs.raison_sociale, ventes.prix_vente, ventes.remise, ventes.prix_final FROM ventes 
                inner join stocks on ventes.stock_id = stocks.id
                inner join produits on produits.id = stocks.produit_id
                inner join fournisseurs on fournisseurs.id = stocks.fournisseur_id
                where ventes.devis_id = ".$devis_id." and  not exists (
                Select bl_ventes.id from bl_ventes 
                where ventes.id = bl_ventes.vente_id);");

        return $vente;
    }

    public function storeItem(Request $request){

        $vente = new Vente();
        $vente->devis_id = $request->devis_id;
        $vente->produit_id = $request->produit_id;
        $vente->quantite = $request->quantite;
        $vente->prix_unitaire = $request->prix_vente;
        $vente->remise = $request->remise;
        $vente->prix_total = $request->prix_total;
        $vente->save();
        return $vente;
    }

    public function deleteItem($id){
        $vente = Vente::findOrFail($id);
        $vente->delete();
    }

    public function cid(Request $request,$IdClient)
    {

        $vente = Vente::findOrFail($IdClient);

        $produit = Produit::latest()->cursor();
        $client = Client::latest()->cursor();
        return view('vente.edit', compact('vente','client','produit'));

    }
}
