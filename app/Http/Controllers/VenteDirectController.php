<?php

namespace App\Http\Controllers;

use App\VenteDirect;
use App\VDStock;
use App\Stock;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class VenteDirectController extends Controller
{
    public function index(Request $request){

        $keyword = $request->get('search');  
      
        $vds = Collection::make(new VenteDirect);

        if (!empty($keyword)) {
            $vds = VenteDirect::where('nom','like','%'.$keyword.'%')
            ->orwhere('adresse','like','%'.$keyword.'%')
            ->orwhere('ref','like','%'.$keyword.'%')
            ->orwhere('objet','like','%'.$keyword.'%')
            ->get(['id','nom','adresse','ref','objet','prix_total']);

           
        }else{
            
            $vds = VenteDirect::take(10)
            ->get(['id','nom','adresse','ref','objet','prix_total']);

        }

        return view('ventedirects.index', compact('vds'));
    }
     

    public function create(){

        $max = \App\UserProfile::where('user_id',Auth::id())->first()->remise_max; 
        $min = \App\UserProfile::where('user_id',Auth::id())->first()->remise_min; 

        $date=date("Y");
        $vd = VenteDirect::latest()->first(['id','ref']);
        $ref="";
        if(empty($vd)){
            $ref = $date.sprintf('-%05d',1);
        }else{
            $ref = $date.sprintf('-%05d',$vd->id +1);   
        }
     
        return view('ventedirects.create',compact('min','max','ref'));
    }



    public function getStock(Request $request){

        $data = json_decode($request->data,true);

        if($data['type_search'] == "Serialisable"){
        
            $stocks = DB::select("SELECT stocks.id as id, produits.nom,  produits.id as produit_id,
            produits.marque, stocks.num_serie, produits.ref_interne,produits.barcode, produits.description, fournisseurs.raison_sociale, produits.prix_vente
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            WHERE stocks.etat = 'Disponible'
            and stocks.num_serie like '%".$data['keyword']."%'
            or produits.barcode like '%".$data['keyword']."%'
            ;");
    
            return $stocks;

        }else{

            $stocks = DB::select("SELECT stocks.id as id, produits.nom,  produits.id as produit_id,
            produits.marque, stocks.num_serie, produits.ref_interne,produits.barcode ,produits.description, fournisseurs.raison_sociale, produits.prix_vente
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            WHERE stocks.etat = 'Disponible'
            and (
             produits.nom like '%".$data['keyword']."%'
             or produits.ref_interne like '%".$data['keyword']."%')
             or produits.barcode like '%".$data['keyword']."%')
             ;");
    
            return $stocks;

        }
    }



    public function store(Request $request){

       
        $data = json_decode($request->data,true);
        $date=date("Y");


        //------------------- if the selected Stock is not avaialble -------------//
        foreach($data['stocks'] as $v){
            $stock = Stock::findOrFail($v['id']);
            if($stock->etat != "Disponible"){
                return response()->json(['result' => "KO",'stock' => $v]);
            }

        }

        //------------------------------------------------------------------------//

        //------------------- Create VD ----------------------------//

        $vd = VenteDirect::create(['nom'=>$data['nom'],
                                    'adresse'=>$data['adresse'],
                                    'ref'=>$data['ref'],
                                    'prix_total'=>$data['prix_total'],
                                    'objet'=>$data['objet'],
                                    'garantie'=>$data['garantie'],
                                    'user_id'=>Auth::id()
                                    ]);

        //-----------------------------------------------------------//

      
        //------------  update etat stock & create vd_stock  -------------------------//
        foreach($data['stocks'] as $v){
            $stock = Stock::findOrFail($v['id']);
            $stock->etat = "Vendu";
            $stock->save();
            $vds = VDStock::create(['vd_id'=>$vd->id,'stock_id' => $v['id'],'remise' => $v['remise'],'prix_unitaire' => $v['prix_vente'],'prix_total' => $v['prix']]);
        }

        return response()->json(['result' => "OK"]);

    }

    public function show($id){


        $vd = VenteDirect::findOrFail($id);
        $vds = VDStock::join('stocks','stocks.id','=','vd_stocks.stock_id')
                       ->join('produits','produits.id','stocks.produit_id')
                       ->where('vd_stocks.vd_id',$id)
                       ->get(['produits.nom','stocks.num_serie','produits.ref_interne','produits.marque','produits.description','vd_stocks.prix_unitaire as prix_vente', 'vd_stocks.remise','vd_stocks.prix_total as prix']);

       
        return view('ventedirects.show',compact('vds','vd'));
    }



    public function printbl($id){

        $vd = VenteDirect::findOrFail($id);

        $vds = DB::select("SELECT produits.id, produits.nom, produits.description, count(*) as quantite
        FROM vd_stocks
        join stocks on stocks.id = vd_stocks.stock_id
        join produits on produits.id = stocks.produit_id
        where vd_stocks.vd_id = ".$id."
        Group by produits.id;");

        //return $vds;

        return view('prints.print_vd_bl',compact('vds','vd'));
    }


}
