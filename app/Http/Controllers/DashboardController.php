<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect,Response,Config;

use Mail;

use App\UserProfile;
use App\Rdv;
use App\Devis;
use App\Boncommand;
use App\Devisbc;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Collection;


class DashboardController extends Controller{


    public function home(Request $request){



        $user_id = Auth::id();
        $userProfile = UserProfile::where('user_id',$user_id)->first();
        $usertype= $userProfile->type;

        $rdvs = Collection::make(new Rdv);
        $rdvs = DB::select("SELECT * FROM rdvs where commercial = ".$userProfile->id);

        if($usertype == "Admin" ){

            $commerciales = DB::select("select  users.id,userprofiles.nom,userprofiles.prenom FROM userprofiles,users
            WHERE userprofiles.user_id = users.id and userprofiles.type = 'Commercial' ");

            if($request->has('dd') && $request->has('df')){


                if ($request->employe != null) {

                    $objectif = DB::select("select  sum(objectif) as objectif from chiffreaffaires where commerciale_id = ".$request->employe." and created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");

                    $chiffreEtteint = DB::select("select SUM(echeances.prix) as total from devis,echeances where etat = 'Termine' and user_id = ".$request->employe." and devis.id = echeances.devis_id and devis.created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");
                    $commandeFacture = DB::select("select COUNT(devis.id) as count from factures,devis WHERE factures.devis_id = devis.id and devis.user_id = ".$request->employe." and devis.created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");
                   // $bonCommande = DB::select("select COUNT(boncommands.id) as count from boncommands WHERE boncommands.user_id = ".$user_id." and MONTH(boncommands.created_at) = MONTH(SYSDATE())");
                    $nbrDevisM = DB::select("select COUNT(devis.id) as count ,sum(prix_total) as total from devis WHERE devis.user_id = ".$request->employe." and created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");
                    $nbrDevisW = DB::select("select COUNT(devis.id) as count ,sum(prix_total) as total from devis WHERE devis.user_id = ".$request->employe." and created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");
                    if ($objectif == null || $chiffreEtteint[0]->total == 0) {
                        $pourcentage = 0 ;
                    }else {
                        $pourcentage = ($chiffreEtteint[0]->total * 100)/$objectif[0]->objectif;

                    }


                    $DevisBcLivre = DB::select("select count(devisbcs.id) as count from devisbcs,devis where devisbcs.etat = 'Complet' and devis.user_id = ".$request->employe." and devis.id = devisbcs.devis_id");
                    $DevisBcNonLivre = DB::select("select count(devisbcs.id) as count from devisbcs,devis where devisbcs.etat = 'NonComplet' and devis.user_id = ".$request->employe." and devis.id = devisbcs.devis_id");
                    
                    $CommProfile = UserProfile::where('user_id',$request->employe)->first();
                    $rdvs = DB::select("SELECT * FROM rdvs where commercial = ".$CommProfile->id);


                  return view('dashboard.commercial', compact('rdvs','objectif','chiffreEtteint','commandeFacture','nbrDevisM','nbrDevisW','pourcentage','DevisBcNonLivre','DevisBcLivre'));

                }else {

                    $encaissement_regeler = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and devis_id is not null AND recu = 1");

                    $encaissement_enattente = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and '".$request->df."' < date and devis_id is not null AND recu = 0");
                    $encaissement_estimer = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and devis_id is not null ");
                    $encaissement_echue = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and '".$request->df."' > date and devis_id is not null AND recu = 0");


                    $decaissement_regeler = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and devis_id is  null AND recu = 1");
                    $decaissement_enattente = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and '".$request->df."' < date and devis_id is  null AND recu = 0");
                    $decaissement_estimer = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and devis_id is  null ");
                    $decaissement_echue = DB::select("select sum(prix) as total from echeances where created_at BETWEEN  '".$request->dd."' AND '".$request->df."' and '".$request->df."' > date and devis_id is  null AND recu = 0");

                    $echeances = DB::select("select * from echeances_alert");



                    $encaissementtotaldumois = DB::select("SELECT SUM(montant) as total from depenses where depenses.type = 'Gain'  and created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");
                    $decaissementtotaldumois = DB::select("SELECT SUM(montant) as total from depenses where depenses.type = 'Depense' and created_at BETWEEN  '".$request->dd."' AND '".$request->df."'");

                    $encaisementjourdesemaine = DB::select('call echeance_week_encaissement');
                    $decaisementjourdesemaine = DB::select('call echeance_week_decaissement');

                    $tab1 = array('-', '-', '-', '-', '-', '-', '-', 0);

                    $tab2 = array('-', '-', '-', '-', '-', '-', '-', 0);

                    foreach ($encaisementjourdesemaine as $day) {

                        $tab1[$day->jour] = ($day->prix == null)? 0 : $day->prix;
                        $tab1[7] += ($day->prix == null)? 0 : $day->prix;
                    }

                    foreach ($decaisementjourdesemaine as $day) {

                        $tab2[$day->jour] = ($day->prix == null)? 0 : $day->prix;
                        $tab2[7] += ($day->prix == null)? 0 : $day->prix;
                    }
                    return view('dashboard.home', compact('echeances','decaissementtotaldumois','encaissementtotaldumois','usertype','rdvs','tab1','tab2','encaissement_regeler','encaissement_enattente','encaissement_estimer','encaissement_echue','decaissement_regeler','decaissement_enattente','decaissement_estimer','decaissement_echue'));
                }


        }else {
            $encaissement_regeler = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and devis_id is not null AND recu = 1");
            $encaissement_enattente = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and SYSDATE() < date and devis_id is not null AND recu = 0");
            $encaissement_estimer = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and devis_id is not null ");
            $encaissement_echue = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and SYSDATE() > date and devis_id is not null AND recu = 0");


            $decaissement_regeler = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and devis_id is  null AND recu = 1");
            $decaissement_enattente = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and SYSDATE() < date and devis_id is  null AND recu = 0");
            $decaissement_estimer = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and devis_id is  null ");
            $decaissement_echue = DB::select("select sum(prix) as total from echeances where MONTH(SYSDATE()) = MONTH(created_at) and SYSDATE() > date and devis_id is  null AND recu = 0");

            $echeances = DB::select("select * from echeances_alert");



            $encaissementtotaldumois = DB::select("SELECT SUM(montant) as total from depenses where depenses.type = 'Gain'  and MONTH(date) = MONTH(SYSDATE())");
            $decaissementtotaldumois = DB::select("SELECT SUM(montant) as total from depenses where depenses.type = 'Depense' and MONTH(date) = MONTH(SYSDATE())");

            $encaisementjourdesemaine = DB::select('call echeance_week_encaissement');
            $decaisementjourdesemaine = DB::select('call echeance_week_decaissement');

            $tab1 = array('-', '-', '-', '-', '-', '-', '-', 0);

            $tab2 = array('-', '-', '-', '-', '-', '-', '-', 0);

            foreach ($encaisementjourdesemaine as $day) {

                $tab1[$day->jour] = ($day->prix == null)? 0 : $day->prix;
                $tab1[7] += ($day->prix == null)? 0 : $day->prix;
            }

            foreach ($decaisementjourdesemaine as $day) {

                $tab2[$day->jour] = ($day->prix == null)? 0 : $day->prix;
                $tab2[7] += ($day->prix == null)? 0 : $day->prix;
            }
            return view('dashboard.home', compact('commerciales','echeances','decaissementtotaldumois','encaissementtotaldumois','usertype','rdvs','tab1','tab2','encaissement_regeler','encaissement_enattente','encaissement_estimer','encaissement_echue','decaissement_regeler','decaissement_enattente','decaissement_estimer','decaissement_echue'));
        }


        }


        if($usertype == "Assistant"){

            $bcNonLivre = DB::select("select fournisseurs.raison_sociale,boncommands.date_bc,fournisseurs.id from boncommands,fournisseurs where boncommands.etat = 'En cours'  and fournisseurs.id = boncommands.fournisseur_id");
            $DevisBcNonLivre = DB::select("select clients.raison_sociale,devisbcs.date_bc,clients.id from devisbcs,devis,clients where devisbcs.etat = 'NonComplet' and devis.id = devisbcs.devis_id and clients.id = devis.client_id");



            $echeances = DB::select("select * from echeances_alert");




            $encaisementjourdesemaine = DB::select('call echeance_week_encaissement');
            $decaisementjourdesemaine = DB::select('call echeance_week_decaissement');

            $tab1 = array('-', '-', '-', '-', '-', '-', '-', 0);

            $tab2 = array('-', '-', '-', '-', '-', '-', '-', 0);

            foreach ($encaisementjourdesemaine as $day) {

                $tab1[$day->jour] = ($day->prix == null)? 0 : $day->prix;
                $tab1[7] += ($day->prix == null)? 0 : $day->prix;
            }

            foreach ($decaisementjourdesemaine as $day) {

                $tab2[$day->jour] = ($day->prix == null)? 0 : $day->prix;
                $tab2[7] += ($day->prix == null)? 0 : $day->prix;
            }
            return view('dashboard.assistante', compact('echeances','DevisBcNonLivre','bcNonLivre','usertype','rdvs','tab1','tab2'));

        }

        if($usertype == "Teleoperateur"){
        //   $rdvs = Rdv::where('created_by',$user_id)->take(10)->latest()->get();

            $rappelrdvs = DB::select("select * from rdvs WHERE action = 'Aucune' AND date_rappel is not null and  created_by = ".$user_id);
            $nbrAppel = DB::select("select COUNT(id) as count from rdvs WHERE etat_appel = 'Non Appele' and WEEK(SYSDATE()) = WEEK(created_at) and  created_by = ".$user_id);
            // dd($nbrAppel);
            $nbrRDV = DB::select("select COUNT(id) as count from rdvs WHERE etat_appel <> 'Non Appele' and WEEK(SYSDATE()) = WEEK(created_at) and  created_by = ".$user_id);
          return view('dashboard.teleoperateur', compact('rdvs','rappelrdvs','nbrAppel','nbrRDV'));
        }

        if($usertype == "Commercial"){

           $objectif = DB::select("select  * from chiffreaffaires where commerciale_id = ".$user_id." and MONTH(SYSDATE()) = mois");

            $chiffreEtteint = DB::select("select SUM(echeances.prix) as total from devis,echeances where etat = 'Termine' and user_id = ".$user_id." and devis.id = echeances.devis_id and MONTH(devis.created_at) = MONTH(SYSDATE())");
            $commandeFacture = DB::select("select COUNT(devis.id) as count from factures,devis WHERE factures.devis_id = devis.id and devis.user_id = ".$user_id." and MONTH(devis.created_at) = MONTH(SYSDATE())");
           // $bonCommande = DB::select("select COUNT(boncommands.id) as count from boncommands WHERE boncommands.user_id = ".$user_id." and MONTH(boncommands.created_at) = MONTH(SYSDATE())");
            $nbrDevisM = DB::select("select COUNT(devis.id) as count ,sum(prix_total) as total from devis WHERE devis.user_id = ".$user_id." and MONTH(devis.created_at) = MONTH(SYSDATE())");
            $nbrDevisW = DB::select("select COUNT(devis.id) as count ,sum(prix_total) as total from devis WHERE devis.user_id = ".$user_id." and WEEK(devis.created_at) = WEEK(SYSDATE())");
            if ($objectif == null) {
                $pourcentage = 0 ;
            }else {
                $pourcentage = ($chiffreEtteint[0]->total * 100)/$objectif->objectif;
            }


            $DevisBcLivre = DB::select("select count(devisbcs.id) as count from devisbcs,devis where devisbcs.etat = 'Complet' and devis.user_id = ".$user_id." and devis.id = devisbcs.devis_id");
            $DevisBcNonLivre = DB::select("select count(devisbcs.id) as count from devisbcs,devis where devisbcs.etat = 'NonComplet' and devis.user_id = ".$user_id." and devis.id = devisbcs.devis_id");


          return view('dashboard.commercial', compact('rdvs','objectif','chiffreEtteint','commandeFacture','nbrDevisM','nbrDevisW','pourcentage','DevisBcNonLivre','DevisBcLivre'));
        }


      }

      public function sendEmail()
      {

        $echeances = DB::select("select * from echeances_alert");

            $data['echeances']= $echeances;



        // dd($data);

          Mail::send('emails.myTestMail', $data, function($message) {

              $message->to('rgaguena.elmehdi@gmail.com', 'Advanced')

              ->subject('Cheque Alert - '.date("d/m/Y"));
          });

          if (Mail::failures()) {
             dd('fail');
           }else{
            dd('done');
           }
      }
}
