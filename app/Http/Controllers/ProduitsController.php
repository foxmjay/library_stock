<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Produit;
use App\Categorie;
use App\Stock;
use App\Vente;
use App\UserProfile;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Auth;


class ProduitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request){

        $user_id = Auth::id();
        $userProfile = UserProfile::where('user_id',$user_id)->first();


        $keyword = $request->get('search');
        $produits = Collection::make(new Produit);
        info($request->has('prix_vente'));
        if($request->has('prix_vente')){
                if (!empty($keyword)) {
                    //$produits = Produit::where('ref_constructeur','like', '%'.$keyword.'%')->get();

                    $produits = Produit::join('categories','categories.id','=','produits.categorie_id')->where('categories.categorie','like', '%'.$keyword.'%')
                    ->orwhere('produits.nom','like','%'.$keyword.'%')
                    ->orwhere('produits.marque','like','%'.$keyword.'%')
                    ->orwhere('produits.description','like','%'.$keyword.'%')
                    ->orwhere('produits.barcode','like','%'.$keyword.'%')
                    ->get(['produits.id','produits.nom','produits.marque','categories.categorie','produits.prix_vente']);
                    $produits = $produits->where('prix_vente','=',$request->prix_vente);
                }
                else{
                    $produits = Produit::join('categories','categories.id','=','produits.categorie_id')
                    ->where('prix_vente','=',$request->prix_vente)->take(10)
                    ->get(['produits.id','produits.nom','produits.marque','categories.categorie','produits.prix_vente']);
                }

        }else{

            if (!empty($keyword)) {
                //$produits = Produit::where('ref_constructeur','like', '%'.$keyword.'%')->get();

                $produits = Produit::join('categories','categories.id','=','produits.categorie_id')->where('categories.categorie','like', '%'.$keyword.'%')
                ->orwhere('produits.nom','like','%'.$keyword.'%')
                ->orwhere('produits.marque','like','%'.$keyword.'%')
                ->orwhere('produits.description','like','%'.$keyword.'%')
                ->orwhere('produits.barcode','like','%'.$keyword.'%')
                ->get(['produits.id','produits.nom','produits.marque','categories.categorie','produits.prix_vente']);
            }
            else{
                $produits = Produit::join('categories','categories.id','=','produits.categorie_id')->take(10)
                ->get(['produits.id','produits.nom','produits.marque','categories.categorie','produits.prix_vente']);
            }


        }

        return view('produits.index', compact('produits','userProfile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(){
        $categories = Categorie::latest()->cursor();
        return view('produits.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'prix_vente' => 'required|numeric',

		]);
        $requestData = $request->all();

        Produit::create($requestData);

        return redirect('produits')->with('success_message', 'Produit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $produit = Produit::findOrFail($id);

        return view('produits.show', compact('produit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $produit = Produit::findOrFail($id);
        $categories = Categorie::latest()->cursor();

        return view('produits.edit', compact('produit','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required',
            'prix_vente' => 'required|numeric',
		]);
        $requestData = $request->all();

        $produit = Produit::findOrFail($id);
        $produit->update($requestData);

        return redirect('produits')->with('success_message', 'Produit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Produit::destroy($id);

        return redirect('produits')->with('success_message', 'Produit deleted!');
    }



    public function importcsv(){


        $produits = Collection::make(new Produit);

        return view('produits.importcsv', compact('produits'));
    }


    public function storecsv(Request $request){


      $path = $request->file('csv_file')->getRealPath();
      $rows = array_map('str_getcsv',file($path));
      $cols=[] ;
      foreach($rows[0] as $index =>$col){
        if(strpos($col,"#") !== false)
            $cols += array($col => $index);
      }

      return $rows;

      if(count($cols) < 8){
        return redirect('produits/importcsv')->with('error_message', 'La structure du fichier est invalide !');
      }else{

        foreach($rows as $row){

            if(strpos($row[0],"#") !==false){
                // do nothing
                info($row[0]);
            }
            else{
                $produit = Produit::create(['nom'=>$row[$cols['#nom']],
                                            'marque'=>$row[$cols['#marque']],
                                            'description'=>$row[$cols['#description']],
                                            'categorie_id'=>$row[$cols['#categorie_id']],
                                            'ref_interne'=>$row[$cols['#ref_interne']],
                                            'prix_vente'=>$row[$cols['#prix_vente']],
                                            'code_bar'=>$row[$cols['#code_bar']],
                                            'garantie'=>$row[$cols['#garantie']]
                ]);
            }
        }
      }


      //return count($cols);
      return redirect('produits/importcsv')->with('success_message', 'Produits importes avec succes');

    }

    /* -------------------  API --------------------------*/

    public function SearchProduct($keyword){

        //$product = DB::select("SELECT  * FROM produits WHERE produits.nom LIKE '%".$nomProduit."%'");

        $produits = Produit::where('produits.nom','like','%'.$keyword.'%')
        ->orwhere('produits.marque','like','%'.$keyword.'%')
        ->orwhere('produits.description','like','%'.$keyword.'%')
        ->orwhere('produits.ref_interne','like','%'.$keyword.'%')
        ->orwhere('produits.barcode','like','%'.$keyword.'%')
        ->get();

        return $produits;
    }

    public function getAllProducts(){
        $produits = Produit::all();
        return $produits;
    }

    public function productSelect2(Request $request){

        $products = Produit::where('nom','like','%'.$request->term.'%')->get(['id','nom as text']);
        info($products[0]);
        return $products;
    }

    public function addProduit(Request $request){


        $produit = new Produit();
        $produit->nom = $request->nom;
        $produit->marque = $request->marque;
        $produit->description = $request->description;
        $produit->categorie_id = $request->categorie_id;
        $produit->ref_interne = $request->ref_interne;
        $produit->garantie = $request->garantie;
        $produit->ref_constructeur = $request->ref_constructeur;
        $produit->barcode = $request->barcode;
        $produit->save();

        return $produit;

    }



    public function getStock(Request $request){

        $data = json_decode($request->data,true);

        $vente = Vente::find($data['vente_id'],['produit_id']);

        /*$stocksGroups = DB::select("SELECT stocks.produit_id as id, stocks.fournisseur_id as fournisseur_id, fournisseurs.raison_sociale, count(*) as quantite
        FROM stocks
        join fournisseurs on fournisseurs.id = stocks.fournisseur_id
        join produits on produits.id = stocks.produit_id
        WHERE produits.id = ".$vente->produit_id."
        group by stocks.fournisseur_id, stocks.produit_id;");*/


        //$data = json(['fournisseur1' => ['P1','P2'] ]);
        //return $data;

       /* $data = array();

        foreach($stocksGroups as $sg){
            array_push($data, ['fournisseur_id' => $sg->fournisseur_id,'raison_sociale' => $sg->raison_sociale , 'produits' => 0 ]);
            //$data +=  [ 'fournisseur_id' => $sg->fournisseur_id ,'raison_sociale' => $sg->raison_sociale , 'produits' => 0 ];
        }

     */


        /*foreach($data as $index=>$value){

            //$data[$index]['produits']


            $stocks = DB::select("SELECT stocks.id as id, produits.nom,
            produits.marque, stocks.num_serie, produits.ref_interne, produits.description, produits.prix_vente
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            WHERE stocks.produit_id = ".$vente->produit_id." and
            stocks.fournisseur_id = ".$value['fournisseur_id'].";");


            $data[$index]['produits'] = $stocks;

            //array_push($data[$index]['produits'], $stocks);

        }*/



        /*$stocks = DB::select("SELECT stocks.produit_id as id, stocks.fournisseur_id, fournisseurs.raison_sociale, produits.nom,
            produits.marque, stocks.num_serie, produits.ref_interne, produits.description, produits.prix_vente
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            WHERE stocks.produit_id = ".$vente->produit_id.";");
        */

        /*$vente = DB::select("SELECT ventes.id, produits.nom, produits.ref_interne, stocks.num_serie ,fournisseurs.raison_sociale, ventes.prix_vente, ventes.remise, ventes.prix_final FROM ventes
                inner join stocks on ventes.stock_id = stocks.id
                inner join produits on produits.id = stocks.produit_id
                inner join fournisseurs on fournisseurs.id = stocks.fournisseur_id
                where ventes.devis_id = ".$devis_id." and  not exists (
                Select bl_ventes.id from bl_ventes
                where ventes.id = bl_ventes.vente_id);");*/

        if($data['type_search'] == "Serialisable"){


            $stocks = DB::select("SELECT stocks.id as id, produits.nom,  produits.id as produit_id,
            produits.marque, stocks.num_serie, produits.ref_interne, produits.barcode,produits.description, fournisseurs.raison_sociale, produits.prix_vente
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            WHERE stocks.produit_id = ".$vente->produit_id." and  stocks.etat = 'Disponible'
            and stocks.num_serie like '%".$data['keyword']."%';");
            //and not exists( select bl_ventes.stock_id from bl_ventes
            //where stocks.id = bl_ventes.stock_id);");

            return $stocks;

        }else{
            info($data['vente_id']);

            $stocks = DB::select("SELECT stocks.id as id, produits.nom,  produits.id as produit_id,
            produits.marque, stocks.num_serie, produits.ref_interne,produits.barcode, produits.description, fournisseurs.raison_sociale, produits.prix_vente
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            WHERE stocks.produit_id = ".$vente->produit_id." and  stocks.etat = 'Disponible'
            and (
             produits.nom like '%".$data['keyword']."%'
             or produits.ref_interne like '%".$data['keyword']."%');");
            //and not exists( select bl_ventes.stock_id from bl_ventes
            //where stocks.id = bl_ventes.stock_id);");

            return $stocks;

        }




    }

}
