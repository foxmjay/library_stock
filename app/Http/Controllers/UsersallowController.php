<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Devis;
use App\Boncommand;
use App\Usersallow;
use App\UserProfile;

use Illuminate\Support\Collection;


class UsersallowController extends Controller{

    public function index($devis_id){

        $userProfiles = UserProfile::where('type','Commercial')->get();
        $devis = Devis::find($devis_id);
        return view('usersallows.index',compact('userProfiles','devis'));
    }



     /*--------------------------- API  for VueJS ------------------------------*/


    public function getItems($id,$type){

        if($type == "devis"){
            $usersallows = Usersallow::join('userprofiles','userprofiles.id','=','usersallows.userprofile_id')
                        ->where('usersallows.devis_id',$id)
                        ->get(['usersallows.id as id',
                                'usersallows.type',
                                'usersallows.userprofile_id',
                                'userprofiles.nom',
                                'userprofiles.type as profileType',
                               ]);
            return $usersallows;
        }

    }

    public function storeItem(Request $request){

        $data = json_decode($request->data,true);
        //info($data['devis_id']);

        $userallow = Usersallow::where('devis_id',$data['devis_id'])->where('userprofile_id',$data['userprofile_id'])->get();
        if(count($userallow) <=0){
            $userallow = Usersallow::create(['devis_id'=>$data['devis_id'],'userprofile_id'=>$data['userprofile_id'],'type'=>$data['type']]);
            return response()->json(['result' => "OK",'data' => $userallow]);

        }
        else{
            return response()->json(['result' => "KO",'message' => "Utilisateur exist deja dans la liste"]);
        }

    }

    public function deleteItem(Request $request , $id){
        info($id);

        Usersallow::findOrFail($id)->delete();
    }


}

