<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Echeance;
use App\Devis;

use DateTime;
use DateInterval;
use App\Boncommand;

use function Psy\info;

class EcheanceController extends Controller{

    public function index($bc_id){
        $bc = Boncommand::where('id',$bc_id)->first();

        return view('echeances.index',compact('bc_id','bc'));
    }


     /*--------------------------- API  for VueJS ------------------------------*/

     public function getItemsBc($bc_id){
        $echeances = Echeance::join('boncommands','boncommands.id','=','echeances.bc_id')
        ->where('bc_id',$bc_id)->get(['echeances.id as id','echeances.jours','echeances.pourcentage','echeances.date',
                                     'echeances.type_paiement','echeances.image_cheque','echeances.date_cheque','echeances.recu','echeances.prix']);
        return $echeances;
    }

    public function indexdevis($id,$type){

        if($type == "devis"){
            $devis = Devis::findOrFail($id);
            $devis_id =$id;
            return view('echeances.indexdevis',compact('devis_id','devis'));
        }

        if($type == "bc"){
            $bc = Boncommand::findOrFail($id);
            $bc_id =$id;
            return view('echeances.index',compact('bc_id','bc'));
        }



    }

    public function ValidationEcheance($id){

        $echeance = Echeance::findOrFail($id);
        $echeance->recu = true;
        $echeance->update();

        return $echeance;

    }




     /*--------------------------- API  for VueJS ------------------------------*/

     public function getItemsDevis($devis_id){
        $echeances = Echeance::join('devis','devis.id','=','echeances.devis_id')->where('devis_id',$devis_id)->orderby("echeances.recu")->get(['echeances.id as id',
                                                                                                                        'echeances.jours',
                                                                                                                        'echeances.pourcentage',
                                                                                                                        'echeances.date',
                                                                                                                        'echeances.type_paiement',
                                                                                                                        'echeances.image_cheque',
                                                                                                                        'echeances.date_cheque',
                                                                                                                        'echeances.recu',
                                                                                                                        'echeances.prix']);
        return $echeances;
    }

    /*public function getItemsFournisseur($fournisseur_id){
        $contacts = Contact::join('fournisseurs','fournisseurs.id','=','contacts.fournisseur_id')->where('fournisseur_id',$fournisseur_id)->get(['fournisseurs.id as fournisseur_id','contacts.id','contacts.nom','contacts.poste','contacts.telephone','contacts.gsm','contacts.email']);
        return $contacts;
    }*/

    public function storeItem(Request $request){

        $echeance = new Echeance();

        if($request->hasFile('image_cheque')){
            $path = $request->image_cheque->store('uploads','public');
            $echeance->image_cheque = $path;
        }

        if($request->has('bc_id')){
            
            /*$bc = Boncommand::find($request->bc_id);
            $bc->echeance_paiement+=1;
            $bc->save();*/
            $echeance->bc_id = $request->bc_id;

        }
        else
            $echeance->devis_id = $request->devis_id;


        $echeance->jours = $request->jours;
        $echeance->pourcentage = $request->pourcentage;


        //  $date = new DateTime();
        // $interval = new DateInterval('P'.$request->jours.'D');

        // $date->add($interval);


        // $echeance->date = $date->format('Y-m-d') ;
        $echeance->date = $request->date;
        $echeance->type_paiement = $request->type_paiement;
        $echeance->date_cheque = $request->date_cheque;
        $echeance->prix = $request->prix;

        $echeance->save();
        return $echeance;
    }

    public function deleteItem( $id){

        Echeance::find($id)->delete();
    }


}

