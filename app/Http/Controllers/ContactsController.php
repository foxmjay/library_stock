<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contact;
use App\Fournisseur;
use App\Client;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contacts = Contact::where('id_client', 'LIKE', "%$keyword%")
                ->orWhere('id_fournisseur', 'LIKE', "%$keyword%")
                ->orWhere('nom', 'LIKE', "%$keyword%")
                ->orWhere('poste', 'LIKE', "%$keyword%")
                ->orWhere('telephone', 'LIKE', "%$keyword%")
                ->orWhere('gsm', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $contacts = Contact::with(['client','fournisseur'])->latest()->paginate($perPage);

        }

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $client = Client::latest()->cursor();
        $fournisseur = Fournisseur::latest()->cursor();
        return view('contacts.create', compact('fournisseur','client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required',
			'email' => 'required'
		]);

        $requestData = $request->all();

        Contact::create($requestData);

        return redirect('contacts')->with('flash_message', 'Contact added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contact = Contact::findOrFail($id);

        return view('contacts.show', compact('contact'));
    }

    public function edit($id){
        $contact = Contact::findOrFail($id);
        $client = Client::latest()->cursor();
        $fournisseur = Fournisseur::latest()->cursor();

        return view('contacts.edit', compact('contact','fournisseur','client'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom' => 'required',
			'email' => 'required'
		]);
        $requestData = $request->all();

        $contact = Contact::findOrFail($id);
        $contact->update($requestData);

        if($request->has('client_id'))
            return redirect('contacts/'.$request->client_id.'/client')->with('flash_message', 'Contact updated!');

        if($request->has('fournisseur_id'))
            return redirect('contacts/'.$request->fournisseur_id.'/fournisseur')->with('flash_message', 'Contact updated!');
    }
    public function destroy($id){
        Contact::destroy($id);
        return back();
        //return redirect('contacts')->with('flash_message', 'Contact deleted!');
        
    }


    public function createWithClient($id){
        $client = Client::findOrFail($id);      // LIMIT FIELDS !!!!!!!!!!
        return view('contacts.create', compact('client'));
    }

    public function indexClient($id){

        $contacts = Contact::where('client_id',$id)->get();
        $clientid = $id;
        return view('contacts.index', compact('contacts','clientid'));
    }
    
    public function storeWithClient(Request $request) {

        $this->validate($request, [
            'client_id' => 'required',
			'nom' => 'required',
			'email' => 'required'
		]);
        $requestData = $request->all();
        $contact = Contact::create($requestData);
        return redirect('contacts/'.$contact->client_id.'/client')->with('flash_message', 'Contact added!');
    }

    public function editWithClient($id){
        $contact = Contact::findOrFail($id);
        $client = Client::findOrFail($contact->client_id); // LIMIT FIELDS !!!!!!!!!!
        return view('contacts.edit', compact('contact','client'));
    }


    //----------------------------------------------------------------------------------------

    public function createWithFournisseur($id){
        $fournisseur = Fournisseur::findOrFail($id);      // LIMIT FIELDS !!!!!!!!!!
        return view('contacts.create', compact('fournisseur'));
    }

    public function indexFournisseur($id){

        $contacts = Contact::where('fournisseur_id',$id)->get();
        $fournisseurid = $id;
        return view('contacts.index', compact('contacts','fournisseurid'));
    }
    
    public function storeWithFournisseur(Request $request) {

        $this->validate($request, [
            'fournisseur_id' => 'required',
			'nom' => 'required',
			'email' => 'required'
		]);
        $requestData = $request->all();
        $contact = Contact::create($requestData);
        return redirect('contacts/'.$contact->fournisseur_id.'/fournisseur')->with('flash_message', 'Contact added!');
    }

    public function editWithFournisseur($id){
        $contact = Contact::findOrFail($id);
        $fournisseur = Fournisseur::findOrFail($contact->fournisseur_id); // LIMIT FIELDS !!!!!!!!!!
        return view('contacts.edit', compact('contact','fournisseur'));
    }


    /*--------------------------- API  for VueJS ------------------------------*/

    public function getItemsClient($client_id){
        $contacts = Contact::join('clients','clients.id','=','contacts.client_id')->where('client_id',$client_id)->get(['clients.id as client_id','contacts.id','contacts.nom','contacts.poste','contacts.telephone','contacts.gsm','contacts.email']);
        return $contacts;
    }

    public function getItemsFournisseur($fournisseur_id){
        $contacts = Contact::join('fournisseurs','fournisseurs.id','=','contacts.fournisseur_id')->where('fournisseur_id',$fournisseur_id)->get(['fournisseurs.id as fournisseur_id','contacts.id','contacts.nom','contacts.poste','contacts.telephone','contacts.gsm','contacts.email']);
        return $contacts;
    }

    public function storeItem(Request $request){

        $contact = new Contact();
        if($request->has('client_id'))
            $contact->client_id = $request->client_id;
        else
            $contact->fournisseur_id = $request->fournisseur_id;

        $contact->nom = $request->nom;
        $contact->poste = $request->poste;
        $contact->telephone = $request->telephone;
        $contact->gsm = $request->gsm;
        $contact->email = $request->email;
        $contact->save();
        return $contact;
    }

    public function deleteItem(Request $request, $id){
        Contact::find($id)->delete();
    }


}
