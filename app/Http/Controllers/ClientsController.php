<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Client;
use App\Ville;
use App\Pays;
use App\Region;
use App\Langue;
use App\Devise;
use App\Devis;
use App\Devisbc;
use App\UserProfile;
use App\Bonretour;
use App\Usersallow;
use App\Facture;
use App\Bonlivraison;
use App\Echange;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class ClientsController extends Controller
{

    /**
     * Display a listing of the clients.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request){

        $keyword = $request->get('search');

        $clients = Collection::make(new Client);

        if (!empty($keyword)) {
            //$produits = Produit::where('ref_constructeur','like', '%'.$keyword.'%')->get();

            $clients = Client::where('raison_sociale','like', '%'.$keyword.'%')
            ->orwhere('secteur_activite','like','%'.$keyword.'%')
            ->orwhere('forme_juridique','like','%'.$keyword.'%')
            ->orwhere('identifient_fiscal','like','%'.$keyword.'%')
            ->orwhere('n_patente','like','%'.$keyword.'%')
            ->get();
        }else{
            $clients = Client::take(10)->get();

        }
        return view('clients.index',compact('clients'));

    }

    /**
     * Show the form for creating a new client.
     *
     * @return Illuminate\View\View
     */
    public function create(){
        $villes = Ville::latest()->cursor();
        $pays = Pays::latest()->cursor();
        $regions = Region::latest()->cursor();
        $langues = Langue::latest()->cursor();
        $devises = Devise::latest()->cursor();
        return view('clients.create',compact('villes','pays','regions','langues','devises'));
    }

    /**
     * Store a new client in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request){

            $data = $this->getData($request);
            Client::create($data);
            return redirect('clients/')
                             ->with('success_message', 'Client ajoute avec succes');

    }

    /**
     * Display the specified client.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $userProfile = UserProfile::where('user_id',Auth::user()->id)->first();

        $client = Client::findOrFail($id);

        /*$villes = Ville::findOrFail($client->ville_id);
        $pays = Pays::findOrFail($client->pays_id);
        $regions = Region::findOrFail($client->region_id);
        $langues = Langue::findOrFail($client->langue_id);*/



        //$facturesPartiel = Collection::make(new Bonlivraison);
        //$facturesComplet = Collection::make(new Facture);
        //$devis = Collection::make(new Devis);
        $total_encaisse=0;
        $total=0;

        if ($userProfile->type  == "Admin") {
            //$total = DB::select("select SUM(prix_total) as som from devis");
            $total = DB::select("select SUM(bonlivraisons.prix_total) as som from bonlivraisons
                                join devis on devis.id = bonlivraisons.devis_id
                                where devis.client_id = ".$client->id);

            $avoir = DB::select("SELECT sum(bonretour_stocks.prix) as som FROM bonretour_stocks
                                join bonretours on bonretours.id = bonretour_stocks.br_id
                                join devis on devis.id = bonretours.devis_id
                                where devis.client_id = ".$client->id);

            $total = $total[0]->som - $avoir[0]->som;

            $total_encaisse = DB::select("SELECT sum(echeances.prix) as som FROM echeances
                                    join devis on devis.id = echeances.devis_id
                                    join clients on clients.id = devis.client_id
                                    where echeances.recu = 1 and devis.client_id=".$client->id);

            /*$factures = Facture::join('devis', 'devis.id','=', 'factures.devis_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->where('devis.client_id','=',$client->id)->get(['factures.id as id','factures.devis_id','factures.bl_id','devisbcs.ref_devisbc','devisbcs.type_facture','factures.ref_facture','factures.created_at']);*/


            /*$facturesPartiel = Bonlivraison::join('devis', 'devis.id','=', 'bonlivraisons.devis_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->join('factures', 'factures.bl_id','=', 'bonlivraisons.id')
                            ->where('devis.client_id','=',$client->id)
                            ->where('devisbcs.type_facture','=','Partiel')
                            ->get(['devisbcs.devis_id','bonlivraisons.id as bl_id','devis.ref_devis','devisbcs.ref_devisbc','factures.ref_facture','devisbcs.etat','devisbcs.type_facture','bonlivraisons.created_at']);*/

            /*$facturesComplet = Facture::join('devis', 'devis.id','=', 'factures.devis_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->where('devisbcs.type_facture','=','Complet')
                            ->where('devis.client_id','=',$client->id)->get(['factures.id as id','factures.devis_id','factures.ref_facture','devis.ref_devis','devisbcs.ref_devisbc','devisbcs.etat','devisbcs.type_facture','factures.created_at']);*/



            //$devis = Devis::where('client_id',$client->id)->where('etat',"En cours")->where('locked',0)->latest()->get();


            //$factures = Devis::where('client_id',$client->id)->where('etat',"Termine")->latest()->get();





        }else {
            //$total = DB::select("select SUM(prix_total) as som from devis where user_id = ".Auth::id());
            $total = DB::select("select SUM(bonlivraisons.prix_total) as som from bonlivraisons
                                 join devis on devis.id = bonlivraisons.devis_id where devis.user_id = ".Auth::id()." and devis.client_id =".$client->id);

            $avoir = DB::select("SELECT sum(bonretour_stocks.prix) as som FROM bonretour_stocks
                                 join bonretours on bonretours.id = bonretour_stocks.br_id
                                 join devis on devis.id = bonretours.devis_id
                                 where devis.client_id = ".$client->id." and devis.user_id = ".Auth::id());

             $total = $total[0]->som - $avoir[0]->som;



            $total_encaisse = DB::select("SELECT sum(echeances.prix) as som FROM echeances
                                 join devis on devis.id = echeances.devis_id
                                 join clients on clients.id = devis.client_id
                                 where echeances.recu = 1 and devis.client_id=".$client->id."
                                 and devis.user_id = ".Auth::id());

            /*$factures = Facture::join('devis', 'devis.id','=', 'factures.devis_id')
                            ->where('devis.user_id', '=',Auth::id())
                            ->where('devis.client_id','=',$client->id)->get(['factures.id as id','factures.devis_id','factures.ref_facture','factures.created_at']);*/

           /* $facturesPartiel = Bonlivraison::join('devis', 'devis.id','=', 'bonlivraisons.devis_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->join('factures', 'factures.bl_id','=', 'bonlivraisons.id')
                            ->where('devis.user_id', '=',Auth::id())
                            ->where('devis.client_id','=',$client->id)
                            ->where('devisbcs.type_facture','=','Partiel')
                            ->get(['devisbcs.devis_id','bonlivraisons.id as bl_id','devis.ref_devis','devisbcs.ref_devisbc','factures.ref_facture','devisbcs.etat','devisbcs.type_facture','bonlivraisons.created_at']);

            $facturesComplet = Facture::join('devis', 'devis.id','=', 'factures.devis_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->where('devis.user_id', '=',Auth::id())
                            ->where('devisbcs.type_facture','=','Complet')
                            ->where('devis.client_id','=',$client->id)->get(['factures.id as id','factures.devis_id','factures.ref_facture','devis.ref_devis','devisbcs.ref_devisbc','devisbcs.etat','devisbcs.type_facture','factures.created_at']);
            */
            //$devis = Devis::where('client_id',$client->id)->where('etat',"En cours")->where('user_id',Auth::id())->where('locked',0)->latest()->get();


           /* $devisbcs = Devisbc::join('devis','devis.id','=','devisbcs.devis_id')
                               ->join('clients','clients.id','=','devis.client_id')
                               ->where('devis.client_id','=',$id)
                               ->where('devis.etat','=','Termine')
                               ->where('devis.user_id','=',Auth::id())
                               ->get(['devisbcs.id','devis.id as devisid','devisbcs.ref_devisbc','devisbcs.date_bc','devisbcs.image_bc','devisbcs.etat']);*/



        }

        $total_encaisse = $total_encaisse[0]->som;

        $devis = Devis::join('userprofiles','userprofiles.user_id','devis.user_id')
        ->where('client_id',$client->id)->where('etat',"En cours")->where('locked',0)->latest()->get(['devis.id as id','devis.ref_devis','devis.objet','devis.created_at','devis.prix_total','devis.version','devis.locked','devis.parentdevis_id','devis.valider','userprofiles.nom','userprofiles.prenom']);


        $devisbcs = Devisbc::join('devis','devis.id','=','devisbcs.devis_id')
            ->join('userprofiles','userprofiles.user_id','devis.user_id')
            ->join('clients','clients.id','=','devis.client_id')
            ->where('devis.client_id','=',$id)
            ->where('devis.etat','=','Termine')
            ->get(['devisbcs.id','devis.id as devisid','devisbcs.ref_devisbc','devis.prix_total', 'devis.ref_devis' ,'devisbcs.date_bc','devisbcs.image_bc','devisbcs.etat','devisbcs.type_facture','userprofiles.nom','userprofiles.prenom']);


        $facturesPartiel = Bonlivraison::join('devis', 'devis.id','=', 'bonlivraisons.devis_id')
                            ->join('userprofiles','userprofiles.user_id','devis.user_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->join('factures', 'factures.bl_id','=', 'bonlivraisons.id')
                            ->where('devis.client_id','=',$client->id)
                            ->where('devisbcs.type_facture','=','Partiel')
                            ->get(['devisbcs.devis_id','bonlivraisons.id as bl_id','devis.ref_devis','factures.prix_total','devisbcs.ref_devisbc','factures.ref_facture','devisbcs.etat','devisbcs.type_facture','bonlivraisons.created_at','userprofiles.nom','userprofiles.prenom']);

        $facturesComplet = Facture::join('devis', 'devis.id','=', 'factures.devis_id')
                            ->join('userprofiles','userprofiles.user_id','devis.user_id')
                            ->join('devisbcs', 'devisbcs.devis_id','=', 'devis.id')
                            ->where('devisbcs.type_facture','=','Complet')
                            ->where('devis.client_id','=',$client->id)->get(['factures.id as id','factures.devis_id','factures.prix_total','factures.ref_facture','devis.ref_devis','devisbcs.ref_devisbc','devisbcs.etat','devisbcs.type_facture','factures.created_at','userprofiles.nom','userprofiles.prenom']);



        $nbfactures = count($devisbcs->where('etat','=','Complet'));



        //-------- acess permission to be applied -----------//
        $bonretours = Bonretour::where('bonretours.client_id',$client->id)
                    ->join('devis','devis.id','bonretours.devis_id')
                    ->join('userprofiles','userprofiles.user_id','devis.user_id')
                    ->get(['bonretours.id','bonretours.ref_br','bonretours.date_br','bonretours.prix_total','userprofiles.nom','userprofiles.prenom','bonretours.devis_id']);

        $echanges = Echange::where('echanges.client_id',$client->id)
                    ->join('devis','devis.id','echanges.devis_id')
                    ->join('userprofiles','userprofiles.user_id','devis.user_id')
                    ->get(['echanges.id','echanges.ref_echange','echanges.created_at','userprofiles.nom','userprofiles.prenom','echanges.devis_id']);


        $userallows = Usersallow::join('devis','devis.id','=','usersallows.devis_id')
                                 ->where('devis.client_id','=',$client->id)//->get();
                                 ->get(['usersallows.devis_id','usersallows.userprofile_id','usersallows.type']);

        //return $userallows->where('devis_id','=','48')->where('userprofile_id','8');

        /*$userallows->filter(function ($item){
            info($item);
        });*/

        return view('clients.show', compact('client','devis','facturesPartiel','facturesComplet' ,'total','total_encaisse','devisbcs','bonretours','userallows','userProfile','nbfactures','echanges'));
    }

    /**
     * Show the form for editing the specified client.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id){

        $client = Client::findOrFail($id);
        $villes = Ville::latest()->cursor();
        $pays = Pays::latest()->cursor();
        $regions = Region::latest()->cursor();
        $langues = Langue::latest()->cursor();
        $devises = Devise::latest()->cursor();
        return view('clients.edit', compact('client','villes','pays','regions','langues','devises'));
    }

    /**
     * Update the specified client in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request){


        $data = $this->getData($request);
        $client = Client::findOrFail($id);
        $client->update($data);

        return redirect('clients/')
                            ->with('success_message', 'Sauvgarde');


    }

    /**
     * Remove the specified client from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
            $client = Client::findOrFail($id);
            $client->delete();

            return redirect('clients/')
                             ->with('success_message', 'Client supprime');


    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request){
        $rules = [
            'raison_sociale' => 'required|string|min:0|max:191' ,
            'categorie' => 'nullable|string|min:0|max:191',
            'secteur_activite' => 'nullable|string|min:0|max:191',
            'forme_juridique'=> 'nullable|string|min:0|max:191',
            'identifient_fiscal'=> 'nullable|string|min:0|max:191',
            'n_patente'=> 'nullable|string|min:0|max:191',
            'n_reg_commerce'=> 'nullable|string|min:0|max:191',
            'ville_reg_commerce'=> 'nullable|string|min:0|max:191',
            'ice'=> 'nullable|string|min:0|max:191',
            'adresse'=> 'nullable|string|min:0|max:191',
            'adresse_facturation'=> 'nullable|string|min:0|max:191',
            'telephone'=> 'nullable|string|min:0|max:191',
            'gsm'=> 'nullable|string|min:0|max:191',
            'email'=> 'nullable|string|min:0|max:191',
            'ville_id'=> 'nullable',
            'pays_id'=> 'nullable',
            'region_id'=> 'nullable',
            'langue_id'=> 'nullable',
            'devise_id'=> 'nullable',
            'note' =>'nullable',
            //'mode_paiement'=> 'nullable|string|min:0|max:191',
            //'echeance_paiement' => 'nullable',

        ];

        $data = $request->validate($rules);


        return $data;
    }


    /************************* API *****************************/


    public function getClients(Request $request){

        $data = json_decode($request->data,true);

        $clients = DB::select("SELECT  *  FROM clients
        WHERE clients.raison_sociale like '%".$data['keyword']."%';");

        if ( $clients )
            return json_encode($clients);
        else
            return response()->json(['Error' => 'Not Found']);

    }

}
