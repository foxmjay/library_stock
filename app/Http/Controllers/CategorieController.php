<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Categorie;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class CategorieController extends Controller{

    public function index(){
        
        $categories = Categorie::all();
        return view('categories.index', compact('categories'));
    }

    public function create(){
        
        return view('categories.create');
    }

    public function store(Request $request){
        
        $this->validate($request, [
            'categorie' => 'required',
            's_categorie' => 'nullable',

		]);
        $requestData = $request->all();

        Categorie::create($requestData);

        return redirect('categories')->with('success_message', 'Categorie added!');


    }
    public function edit($id){
        $categorie = Categorie::findOrFail($id);
        return view('categories.edit', compact('categorie'));
    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'categorie' => 'required',
            's_categorie' => 'nullable',

        ]);
        
        $requestData = $request->all();

        $categorie = Categorie::findOrFail($id);
        $categorie->update($requestData);

        return redirect('categories')->with('success_message', 'Categorie updated!');
    }

    public function destroy($id)    {
        Categorie::destroy($id);

        return redirect('categories')->with('success_message', 'Categorie deleted!');
    }



}


