<?php

namespace App\Http\Controllers;

use App\BonretourStock;
use App\Bonretour;
use App\Vente;
use App\Devis;
use App\Devisbc;
use App\Stock;
use App\Bonlivraison;
use App\Bl_vente;
use App\Client;
use App\Facture;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;


class BonretoursController extends Controller{
    

    public function create($client_id){

        //$devis = Devis::find($devis_id);

        return view('bonretours.create',compact('client_id'));
    }



    public function confirmer(Request $request){


        $bonretour = new Bonretour();


        if($request->hasFile('image_br')){
            $path = $request->image_br->store('uploads','public');
            $bonretour->image_br = $path;
        }
        $bonretour->date_br = $request->date_br;
        $bonretour->client_id = $request->client_id;

        
        $date=date("Y");
        $br = Bonretour::latest()->first(['id']);

        if(empty($br)){
            $bonretour->ref_br = $date.sprintf('-%05d',1);
        }else{
            $bonretour->ref_br = $date.sprintf('-%05d',$br->id + 1);   
        }

        $bonretour->save();

        $stocks = json_decode($request->stocks,true);
        $stocks = $stocks['stocks'];

        foreach($stocks as $st){

            $bl_vente = Bl_vente::where('bl_id',$st['bl_id'])->where('stock_id',$st['id'])->first();
            $vente = Vente::find($bl_vente->vente_id,['remise','prix_unitaire']);
            $prix = $vente->prix_unitaire - ($vente->prix_unitaire * $vente->remise / 100 );


            $br_stock = BonretourStock::create(['br_id'=>$bonretour->id,
                                                'stock_id'=>$st['id'],
                                                'bl_id'=>$st['bl_id'],
                                                'vente_id'=>$bl_vente->vente_id,
                                                'prix'=>$prix]);

            //$bl_vente->delete();
            $stock = Stock::find($st['id']);
            $stock->etat = "Disponible";
            $stock->save();

            
            //$bl = Bonlivraison::find($st['bl_id']);
            //$bl->prix_total = $bl->prix_total - ( $vente->prix_unitaire - ($vente->prix_unitaire * $vente->remise / 100 ));
            //$bl->save();

            //$devisbc = Devisbc::where('devis_id',$bl->devis_id)->first();
            //$devisbc->etat ="NonComplet";
            //$devisbc->save();
        }


        
        return response()->json(['result' => "OK"]);

    }


    public function confirmerBL(Request $request){



        $bonretour = new Bonretour();


        if($request->hasFile('image_br')){
            $path = $request->image_br->store('uploads','public');
            $bonretour->image_br = $path;
        }
        $bonretour->date_br = $request->date_br;
        $bonretour->client_id = $request->client_id;

        
        $date=date("Y");
        $br = Bonretour::latest()->first(['id']);

        if(empty($br)){
            $bonretour->ref_br = $date.sprintf('-%05d',1);
        }else{
            $bonretour->ref_br = $date.sprintf('-%05d',$br->id + 1);   
        }

        $bonretour->devis_id = $request->devis_id;
        $bonretour->save();

        $stocks = json_decode($request->stocks,true);
        $stocks = $stocks['stocks'];
        $prix_total = 0;
        foreach($stocks as $st){
            $bl_vente = Bl_vente::where('bl_id',$request->bl_id)->where('stock_id',$st)->first();
            $vente = Vente::find($bl_vente->vente_id,['remise','prix_unitaire']);
            $prix = $vente->prix_unitaire - ($vente->prix_unitaire * $vente->remise / 100 );
            $prix_total += $prix;

            $br_stock = BonretourStock::create(['br_id'=>$bonretour->id,
                                                'stock_id'=>$st,
                                                'bl_id'=>$request->bl_id,
                                                'vente_id'=>$bl_vente->vente_id,
                                                'prix'=>$prix]);

            //$bl_vente->delete();
            $stock = Stock::find($st);
            $stock->etat = "Disponible";
            $stock->save();

            
            //$bl = Bonlivraison::find($request->bl_id);
            //$bl->prix_total = $bl->prix_total - ( $vente->prix_unitaire - ($vente->prix_unitaire * $vente->remise / 100 ));
            //$bl->save();

            //$devisbc = Devisbc::where('devis_id',$bl->devis_id)->first();
            //$devisbc->etat ="NonComplet";
            //$devisbc->save();
        }

        $bonretour->prix_total = $prix_total;
        $bonretour->bl_id = $request->bl_id;
        $bonretour->save();

        
        return response()->json(['result' => "OK"]);

    }

    public function frombl($bl_id){
        //$devis_id = Devisbc::findOrFail($devisbc_id,['devis_id'])->devis_id;
        //$devis_id = Devis::where()->first();
        //return view('bonlivraisons.index',compact('devisbc_id','devisbc'));

        //$ventes = Ventes::where('bl_id',$bl_id)->get();

        $bl = Bonlivraison::findOrFail($bl_id);
        $devis_id = $bl->devis_id;
        $devis = Devis::findOrFail($devis_id);
        $client_id = $devis->client_id;
        $num_bc = Devisbc::where('devis_id',$devis->id)->first()->num_bc;



        $ventes = DB::select("SELECT bl_ventes.stock_id as id, produits.nom, produits.id as produit_id, produits.marque, stocks.num_serie, produits.ref_interne,
            produits.description, fournisseurs.raison_sociale, produits.prix_vente, bl_ventes.vente_id, ventes.remise
            FROM bl_ventes
            join ventes on ventes.id = bl_ventes.vente_id
            join produits on produits.id = ventes.produit_id
            join stocks on stocks.id = bl_ventes.stock_id
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            where bl_ventes.bl_id = ".$bl_id."
            and not exists( select bonretour_stocks.stock_id from bonretour_stocks 
            where bonretour_stocks.stock_id = bl_ventes.stock_id and
            bonretour_stocks.bl_id = ".$bl_id." );");


        return view('bonretours.bl',compact('bl','devis_id','ventes','client_id','num_bc'));
    }


    public function printbr($br_id){


        $br = Bonretour::findOrFail($br_id);
        $client = Client::findOrFail($br->client_id);

        $facture = Facture::where('bl_id',$br->bl_id)->first();
        if(empty($facture)) {
            $facture = Facture::where('devis_id',$br->devis_id)->first();
        }

        
        $br_stocks = DB::select("SELECT produits.id, produits.nom,  produits.ref_interne , produits.description, count(*) as quantite
        FROM bonretour_stocks
        join stocks on stocks.id = bonretour_stocks.stock_id
        join produits on produits.id = stocks.produit_id
        where bonretour_stocks.br_id = ".$br_id."
        Group by produits.id;");

        return view('prints.printbr',compact('br','br_stocks','client','facture'));
    }


    public function printavoir($br_id){

        $br = Bonretour::findOrFail($br_id);
        $client = Client::findOrFail($br->client_id);

        $facture = Facture::where('bl_id',$br->bl_id)->first();
        if(empty($facture)) {
            $facture = Facture::where('devis_id',$br->devis_id)->first();
        }


        $ventes = DB::select("SELECT produits.id, produits.nom, produits.description,produits.ref_interne, produits.id ,produits.nom, produits.description, count(*) as quantite , sum(bonretour_stocks.prix) as prix_total
        FROM bonretour_stocks
        join ventes on ventes.id = bonretour_stocks.vente_id
        join stocks on stocks.id = bonretour_stocks.stock_id
        join produits on produits.id = stocks.produit_id
        where bonretour_stocks.br_id = ".$br_id."
        Group by produits.id;");
        
        return view('prints.printavoir',compact('br','ventes','client','facture'));
    }

}
