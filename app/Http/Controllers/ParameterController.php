<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ParameterController extends Controller{
    
   

    public function edit(){

        $parameter = new Parameter();
        

        try {

            $parameter = Parameter::findOrFail(0);
        
        } catch (ModelNotFoundException $ex) {

            $parameter = Parameter::create(['id'=>0]);
            $parameter->id=0;
            $parameter->save();

        }

        return view('parameters.edit', compact('parameter'));
    }


    public function update(Request $request){
            
        
        $logo=null;
        $data = $this->getData($request);

        if($request->hasFile('logo')){
            $path = $request->logo->store('uploads','public');
            $data['logo'] = $path;
        }

        $parameter = Parameter::find(0);
        $parameter->update($data);

        return redirect('/')->with('success_message', 'Sauvgarde');

    }



    protected function getData(Request $request){
        $rules = [
            'nom' => 'nullable|string|min:0|max:191' ,
            'telephone' => 'nullable|string|min:0|max:191',
            'email' => 'nullable|string|min:0|max:191',
            'adresse' => 'nullable|string|min:0|max:191',
        ];
        
        $data = $request->validate($rules);


        return $data;
    }
}
