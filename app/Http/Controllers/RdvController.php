<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rdv;
use App\User;
use App\UserProfile;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class RdvController extends Controller{

    public function index(){

        $createdby_id = Auth::id();
        $rdvs = Rdv::where('created_by',$createdby_id)->latest()->get();

        return view('rdvs.index',compact('rdvs'));
    }

    public function create(){

        $commercials = UserProfile::where('type','Commercial')->get();
        return view('rdvs.create',compact('commercials'));
    }

    public function store(Request $request){

        $user_id = Auth::id();
        $data = $this->getData($request);
        $data['created_by'] = $user_id;
        Rdv::create($data);
        return redirect('rdvs/')->with('success_message', 'Client ajoute avec succes');

    }


    public function edit($id){

        $commercials = UserProfile::where('type','Commercial')->get();
        $rdv = Rdv::findOrFail($id);
        return view('rdvs.edit', compact('rdv','commercials'));

    }

    public function update($id, Request $request){

        $data = $this->getData($request);
        $rdv = Rdv::findOrFail($id);

        if(isset($data['commercial'])){
            if($rdv->commercial != $data['commercial'])
                $data['action'] = "Aucune";
        }

        $rdv->update($data);


        if( Str::contains(url()->previous(),'notifsrdv') ){
            return redirect('notifsrdv')->with('success_message', 'Sauvgarde');
        }

        if( Str::contains(url()->previous(),'rdvs') ){
            return redirect('rdvs/')->with('success_message', 'Sauvgarde');
        }

    }

    public function destroy($id)
    {

        $rdv = Rdv::findOrFail($id);
        $rdv->delete();

        return redirect('rdvs/')->with('success_message', 'Client supprime');


    }

    public function show($id){
        $rdv = Rdv::findOrFail($id);
        $commercials = UserProfile::where('type','Commercial')->get();

        return view('rdvs.show', compact('rdv','commercials'));
    }

    public function notifsRdv(){

        $user_id = Auth::id();
        $userprofile = UserProfile::where('user_id',$user_id)->first();

        $rdvs = Rdv::where('commercial',$userprofile->id)->get();
        return view('rdvs.notifs',compact('rdvs'));
    }

    public function notifsRdvShow($id){

        $rdv = Rdv::findOrFail($id);
        $commercials = UserProfile::where('type','Commercial')->get();
        return view('rdvs.show_commercial', compact('rdv','commercials'));

    }

    protected function getData(Request $request){
        $rules = [
            'nom' => 'required|string|min:0|max:191' ,
            'societe' => 'required|string|min:0|max:191' ,
            'prenom' => 'nullable|string|min:0|max:191',
            'tel' => 'nullable|string|min:0|max:191',
            'email'=> 'nullable|string|min:0|max:191',
            'adresse'=> 'nullable|string|min:0|max:191',
            'etat'=> 'nullable|string|min:0|max:191',
            'profession'=> 'nullable|string|min:0|max:191',
            'etat'=> 'nullable|string|min:0|max:191',
            'action'=> 'nullable|string|min:0|max:191',
            'application'=> 'nullable|string|min:0|max:191',
            'commercial'=> 'nullable|integer|min:0|max:191',
            'feedback'=> 'nullable|string|min:0|max:191',
            'etat_retour'=> 'nullable|string|min:0|max:191',
            'date'=> 'nullable',
            'heur'=> 'nullable',
            'date_rappel'=> 'nullable',
            'heur_rappel'=> 'nullable',
            'note'=> 'nullable|string|min:0|max:191',
            'etat_appel' => 'nullable',

        ];

        $data = $request->validate($rules);


        return $data;
    }

}
