<?php

namespace App\Http\Controllers;



use App\Devis;
use App\Client;
use App\Produit;
use App\Stock;
use App\Devisbc;
use App\Vente;
use App\Bonlivraison;
use App\Bl_vente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class DevisbcsController extends Controller
{
    

    public function confirm($id ,Request $request) {

       
        $devis = Devis::findOrFail($id);
        $devis->prix_total = $request->prix_total;
        $devis->objet = $request->objet;
        $devis->delai_livraison = $request->delai_livraison;
        $devis->garantie = $request->garantie;
        $devis->etat = 'Termine';
        $devis->save();

        $rules=['image_bc' => 'nullable','date_bc' => 'nullable' ,'num_bc'=> 'nullable' ];
        $bcdata = $request->validate($rules);

        if ($request->hasFile('image_bc')) {
            $path = $request->image_bc->store('uploads', 'public');
            $bcdata['image_bc'] = $path;
        }


        $date=date("Y");
        $bcdata['devis_id'] = $id;

        $devisbc = Devisbc::latest()->first(['id','ref_devisbc']);
        
        if(empty($devisbc)){
            $bcdata['ref_devisbc'] = $date.sprintf('-%05d',1);
        }else{
            $bcdata['ref_devisbc'] = $date.sprintf('-%05d',$devisbc->id +1);
        }

        Devisbc::create($bcdata);

        return redirect('clients/' . $devis['client_id'])->with('flash_message', 'devis confirmer!');

    }


    


}
