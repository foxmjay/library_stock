<?php

namespace App\Http\Controllers;

use App\Devis;
use App\Client;
use App\Produit;
use App\Stock;
use App\Devisbc;
use App\Usersallow;
use App\UserProfile;
use App\Echeance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vente;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DevisController extends Controller
{

    public function index()
    {
        $devis = Devis::all();

        return view('devis.index', compact('devis'));
    }

    public function create($c_id)
    {
        //$clients = Client::latest()->cursor();
        $client = Client::find($c_id,['id','raison_sociale']);
        $devis = Devis::latest()->first(['id','ref_devis']);
        $ref_devis="";
        $date=date("Y");
        
        if(empty($devis)){
            $ref_devis = $date.sprintf('-%05d',1);
        }else{
            $ref_devis = $date.sprintf('-%05d',$devis->id + 1);   
        }


        return view('devis.create', compact('client','ref_devis'));
    }


    public function store(Request $request)
    {
        // try {

        $data = $this->getData($request);
        $data['user_id'] = Auth::id();
        $devis = Devis::create($data);

        Usersallow::setPermissions($devis->id,'devis');


        return redirect('devis/'.$devis->id.'/edit')->with('success_message', 'Devis ajoute avec succes');

        /*  } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }*/
    }

    public function edit($id)
    {

        $devis = Devis::findOrFail($id);
        $client = Client::find($devis->client_id,['id','raison_sociale']);
        //$stocks = Stock::latest()->cursor();

        $devisbc = Devisbc::where('devis_id',$id)->first();

        $max = \App\UserProfile::where('user_id',Auth::user()->id)->first()->remise_max; 
        $min = \App\UserProfile::where('user_id',Auth::user()->id)->first()->remise_min; 


        return view('devis.edit', compact('devis', 'client','min','max','devisbc'));
    }

    public function edit2($id)
    {

        $devis = Devis::findOrFail($id);
        $client = Client::find($devis->client_id,['id','raison_sociale']);
        //$stocks = Stock::latest()->cursor();

        $devisbc = Devisbc::where('devis_id',$id)->first();

        $max = \App\UserProfile::where('user_id',Auth::user()->id)->first()->remise_max; 
        $min = \App\UserProfile::where('user_id',Auth::user()->id)->first()->remise_min; 


        return view('devis.edit2', compact('devis', 'client','min','max','devisbc'));
    }



    public function update(Request $request, $id){

        //$requestData = $request->all();


        $data = $this->getData($request);

        $devis = Devis::findOrFail($id);

        if($devis->etat == 'Termine'){
            $devis->update($data);
        }else{

                


                if ($devis->user_id == Auth::id()) {
                    // Same User
                    
                    if($devis->valider == false){
                        // Non valide
                        // sauvegarde normal
                        $devis->update($data);
                    }else{
                        // valide
                        //created new devis with new user id
                        // lock old devis
                        //copy ventes with the new devis id

                        $data['user_id'] = Auth::id();
                        $data['version'] = $devis->version + 1;
                        $data['ref_devis'] = $devis->ref_devis;
                        $data['parentdevis_id'] = $devis->id;
                        $devis2 = Devis::create($data);
                        //Usersallow::setPermissions($devis2->id,'devis');
                        Usersallow::copyPermissions($devis->id,$devis2->id,'devis');
                        $devis->locked = true;
                        $devis->save();

                        $ventes = Vente::where('devis_id','=',$devis->id)->get();
                        foreach($ventes as $vente){
                            $newVente = $vente->replicate();
                            $newVente->devis_id = $devis2->id;
                            $newVente->save();
                        }
                    }

                }else{
                        // Different Users
                        // valide
                        // created new devis with new user id
                        // lock old devis
                        // copy ventes with the new devis id

                        $data['user_id'] = Auth::id();
                        $data['version'] = $devis->version + 1;
                        $data['ref_devis'] = $devis->ref_devis;
                        $data['parentdevis_id'] = $devis->id;
                        $devis2 = Devis::create($data);
                        //Usersallow::setPermissions($devis2->id,'devis');
                        Usersallow::copyPermissions($devis->id,$devis2->id,'devis');
                        //Usersallow::addPermissions($devis2->id,'devis',$devis->user_id);

                        $devis->locked = true;
                        $devis->save();

                        $ventes = Vente::where('devis_id','=',$devis->id)->get();
                        foreach($ventes as $vente){
                            $newVente = $vente->replicate();
                            $newVente->devis_id = $devis2->id;
                            $newVente->save();
                        }

                }

        }
        
       /* $data = $this->getData($request);
        $data['user_id'] = Auth::id();
        $data2 = $data;

        $devis = Devis::findOrFail($id);
  
        if ($devis->user_id == $data['user_id']) {

            $devis->update($data);
        } else {
            $data2['locked'] = 1;
            $devis->update($data2);
            if ($devis->parentdevis_id == null) {
                $data['parentdevis_id'] = $devis->id;
            } else {
                $data['parentdevis_id'] = $devis->parentdevis_id;
            }

            Devis::create($data);
        }*/


        

        return redirect('clients/' . $data['client_id'])->with('flash_message', 'devis updated!');
    }


    

    protected function getData(Request $request)
    {
        $rules = [
            'parentdevis_id' => 'nullable',
            'client_id' => 'required',
            'ref_devis' => 'nullable|string|min:0|max:191',
            'prix_total' => 'nullable',
            'garantie' => 'nullable',
            'objet' => 'nullable',
            //'etat' => 'nullable',
            'delai_livraison' => 'nullable',
            'locked' => 'nullable',
        ];

        $data = $request->validate($rules);


        return $data;
    }


    private function getParent($devis){
        //info($devis->id);
        $list=array();
        if( $devis->parentdevis_id != null){
            $parentDevis = Devis::where('devis.id',$devis->parentdevis_id)
                                 ->join('userprofiles','userprofiles.user_id','devis.user_id')
                                 ->get(['devis.id as id','devis.ref_devis','devis.objet','devis.created_at','devis.prix_total','devis.version','devis.locked','devis.parentdevis_id','devis.valider','userprofiles.nom','userprofiles.prenom'])
                                 ->first();
            $list = $this->getParent($parentDevis);
            array_push($list,$devis);
            return $list;
            
        }else{
            array_push($list,$devis);
            return $list;
        }
         
        
    }

    public function getOldDevis($devis_id){

        //info('call');
        
        $devis = Devis::find($devis_id);
        $list = $this->getParent($devis);
        //info($list);
        //$devis = Devis::whereRaw('(id=' . $devis_id . ' or parentdevis_id = ' . $devis_id . ') and locked = 1')->latest()->get();
        return $list;
        //return $list;
    }

    public function printdevis($id){

        $devis = Devis::findOrFail($id);
        $echeances = Echeance::where('devis_id',$id)->get();

        $ventes = Vente::join('produits','produits.id','=','ventes.produit_id')
        ->where('ventes.devis_id',$devis->id)
        ->get(['produits.id','produits.nom','produits.marque','produits.description','produits.ref_interne','ventes.quantite','ventes.prix_unitaire','ventes.prix_total','ventes.remise']);
        return view('prints.printdevis',compact('devis','ventes','echeances'));
    }

     
    
    //------------------------ API --------------------------//


    public function SearchProduct($nomProduit){

        /*$product = DB::select("SELECT stocks.produit_id as id, produits.nom , 
            produits.ref_interne ,produits.marque, produits.description, produits.prix_vente, count(*) as quantite,
            (SELECT SUM(ventes.quantite) FROM ventes WHERE ventes.produit_id=produits.id ) as reserve
            FROM stocks
            join produits on produits.id = stocks.produit_id
            WHERE stocks.etat = 'Disponible' AND
            (produits.nom like '%".$nomProduit."%' OR
            produits.description like '%".$nomProduit."%' OR
            produits.marque like '%".$nomProduit."%' OR
            produits.ref_interne like '%".$nomProduit."%')
            group by stocks.produit_id;");*/


        $product = DB::select("SELECT id , produits.nom,produits.ref_interne ,produits.marque, produits.description, produits.prix_vente, 
            (select count(*) from stocks where stocks.produit_id = produits.id and stocks.etat = 'Disponible' ) as quantite ,
            IFNULL( ( (SELECT SUM(ventes.quantite) FROM ventes WHERE ventes.produit_id= produits.id) - (SELECT  count(*)  FROM  bl_ventes
            join stocks on stocks.id = bl_ventes.stock_id
            where stocks.produit_id = produits.id
            group by produits.id) ),0) as reserve,
            IFNULL((select max(prix_unitaire) from stocks where stocks.produit_id = produits.id and stocks.etat = 'Disponible' ),0) as prix_max
            FROM  produits
            WHERE produits.nom like '%".$nomProduit."%' OR
            produits.description like '%".$nomProduit."%' OR
            produits.marque like '%".$nomProduit."%' OR
            produits.ref_interne like '%".$nomProduit."%';");

        /*info($product);

        foreach($product as $p ){
            info(gettype($p));
        }*/

       /* $tags = array_map(function($tag) {
            return array(
                'id' => $tag['prod'],
            );
        }, $product);

        info($tags);*/


        /*SELECT produits.id, produits.nom,produits.ref_interne ,produits.marque, produits.description, produits.prix_vente, 
        (select count(*) from stocks where stocks.produit_id = produits.id and stocks.etat = 'Disponible' ) as quantite ,
        IFNULL ((SELECT SUM(ventes.quantite) FROM ventes  join stocks on stocks.produit_id = ventes.produit_id 
        WHERE ventes.produit_id=produits.id and stocks.etat = 'Disponible' ) ,0) as reserve
        FROM  produits; */

        return $product;

    }




    public function validerDevis($id ,Request $request) {

       
        $devis = Devis::findOrFail($id);
        $devis->prix_total = $request->prix_total;
        $devis->objet = $request->objet;
        $devis->delai_livraison = $request->delai_livraison;
        $devis->garantie = $request->garantie;
        $devis->valider = true;
        $devis->save();

        return redirect('clients/' . $devis['client_id'])->with('flash_message', 'devis confirmer!');

    }

}
