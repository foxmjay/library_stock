<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Fournisseur;
use App\Ville;
use App\Pays;
use App\Region;
use App\Langue;
use App\Devise;
use App\Boncommand;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class FournisseurController extends Controller
{

    /**
     * Display a listing of the Fournisseurs.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request){

        $keyword = $request->get('search');

        $fournisseurs = Collection::make(new Fournisseur);

        if (!empty($keyword)) {
            //$produits = Produit::where('ref_constructeur','like', '%'.$keyword.'%')->get();

            $fournisseurs = Fournisseur::where('raison_sociale','like', '%'.$keyword.'%')
            ->orwhere('secteur_activite','like','%'.$keyword.'%')
            ->orwhere('forme_juridique','like','%'.$keyword.'%')
            ->orwhere('identifient_fiscal','like','%'.$keyword.'%')
            ->orwhere('n_patente','like','%'.$keyword.'%')
            ->get();
        }else{

            $fournisseurs = Fournisseur::take(10)->get();

        }
        return view('fournisseurs.index',compact('fournisseurs'));

    }

    /**
     * Show the form for creating a new Fournisseur.
     *
     * @return Illuminate\View\View
     */
    public function create(){
        $villes = Ville::latest()->cursor();
        $pays = Pays::latest()->cursor();
        $regions = Region::latest()->cursor();
        $langues = Langue::latest()->cursor();
        $devises = Devise::latest()->cursor();

        return view('fournisseurs.create',compact('villes','pays','regions','langues','devises'));
    }

    /**
     * Store a new Fournisseur in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request){

            $data = $this->getData($request);
            Fournisseur::create($data);

            return redirect('fournisseurs/')
                             ->with('success_message', 'Fournisseur ajoute avec succes');

    }


    public function edit($id){

        $fournisseur = Fournisseur::findOrFail($id);
        $villes = Ville::latest()->cursor();
        $pays = Pays::latest()->cursor();
        $regions = Region::latest()->cursor();
        $langues = Langue::latest()->cursor();
        $devises = Devise::latest()->cursor();
        return view('fournisseurs.edit', compact('fournisseur','villes','pays','regions','langues','devises'));
    }

    /**
     * Update the specified Fournisseur in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $data = $this->getData($request);
        $fournisseur = Fournisseur::findOrFail($id);
        $fournisseur->update($data);

        return redirect('fournisseurs/')
                            ->with('success_message', 'Sauvgarde');


    }


    public function show($id) {

        $userProfile = UserProfile::where('user_id',Auth::user()->id)->first();

        $fournisseur = Fournisseur::findOrFail($id);
        /*$villes = Ville::findOrFail($fournisseur->ville_id);
        $pays = Pays::findOrFail($fournisseur->pays_id);
        $regions = Region::findOrFail($fournisseur->region_id);
        $langues = Langue::findOrFail($fournisseur->langue_id);*/
        //$devises = Devise::findOrFail($fournisseur->devise_id);

        if ($userProfile->type  == "Admin") {
            $boncommands = Boncommand::join('userprofiles','userprofiles.user_id','boncommands.user_id')
            ->where('fournisseur_id',$fournisseur->id)->latest()
            ->get(['boncommands.id','boncommands.ref_bc','boncommands.ref_devis','boncommands.date_bc','boncommands.created_at','boncommands.prix_total','boncommands.date_livraison','boncommands.etat','boncommands.fournisseur_id','userprofiles.nom','userprofiles.prenom']);

            $total = DB::select("select SUM(boncommandbls.prix_total) as som from boncommandbls
            join boncommands on boncommands.id = boncommandbls.bc_id
            where boncommands.fournisseur_id = ".$fournisseur->id);

        }else {

            $boncommands = Boncommand::join('userprofiles','userprofiles.user_id','boncommands.user_id')
            ->where('userprofiles.user_id',Auth::id())
            ->where('fournisseur_id',$fournisseur->id)->latest('boncommands.created_at')
            ->get(['boncommands.id','boncommands.ref_bc','boncommands.ref_devis','boncommands.date_bc','boncommands.created_at','boncommands.prix_total','boncommands.date_livraison','boncommands.etat','boncommands.fournisseur_id','userprofiles.nom','userprofiles.prenom']);


            //$boncommands = Boncommand::where('fournisseur_id',$fournisseur->id)->where('user_id',Auth::id())->latest()->get();

            $total = DB::select("select SUM(boncommandbls.prix_total) as som from boncommandbls
            join boncommands on boncommands.id = boncommandbls.bc_id
            where boncommands.fournisseur_id = ".$fournisseur->id." and boncommands.user_id = ".Auth::id());
        }

        //$factures = Boncommand::where('fournisseur_id',$fournisseur->id)->where('etat',"Termine")->latest()->get();

        return view('fournisseurs.show', compact('fournisseur','boncommands','total'));
    }

    /**
     * Remove the specified Fournisseur from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $fournisseur = Fournisseur::findOrFail($id);
            $fournisseur->delete();

            return redirect('fournisseurs/')
                             ->with('success_message', 'fournisseur supprime');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'raison_sociale' => 'required|string|min:0|max:191' ,
            'categorie' => 'nullable|string|min:0|max:191',
            'secteur_activite' => 'nullable|string|min:0|max:191',
            'forme_juridique'=> 'nullable|string|min:0|max:191',
            'identifient_fiscal'=> 'nullable|string|min:0|max:191',
            'n_patente'=> 'nullable|string|min:0|max:191',
            'n_reg_commerce'=> 'nullable|string|min:0|max:191',
            'ville_reg_commerce'=> 'nullable|string|min:0|max:191',
            'ice'=> 'nullable|string|min:0|max:191',
            'adresse'=> 'nullable|string|min:0|max:191',
            'adresse_facturation'=> 'nullable|string|min:0|max:191',
            'telephone'=> 'nullable|string|min:0|max:191',
            'gsm'=> 'nullable|string|min:0|max:191',
            'email'=> 'nullable|string|min:0|max:191',
            'ville_id'=> 'nullable',
            'pays_id'=> 'nullable',
            'region_id'=> 'nullable',
            'langue_id'=> 'nullable',
            'devise_id'=> 'nullable',
            'note'=>'nullable',
            //'mode_paiement'=> 'nullable|string|min:0|max:191',
            //'echeance_paiement' => 'nullable',

        ];

        $data = $request->validate($rules);


        return $data;
    }

}
