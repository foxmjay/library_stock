<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Bonretour;
use App\Stock;
use App\Devis;
use App\Produit;
use App\Fournisseur;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

use Illuminate\Support\Str;


class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request){

        $keyword = $request->get('search');

        $stock = Collection::make(new Stock);

        if (!empty($keyword)) {
            /*$stock = Stock::join('produits','produits.id','=','stocks.produit_id')
                            ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')
                            ->where('stocks.ref_fournisseur','like', '%'.$keyword.'%')
            ->orwhere('produits.nom','like','%'.$keyword.'%')
            ->orwhere('produits.marque','like','%'.$keyword.'%')
            ->orwhere('produits.description','like','%'.$keyword.'%')
            ->orwhere('produits.ref_interne','like','%'.$keyword.'%')
            ->orwhere('stocks.num_serie','like','%'.$keyword.'%')
            ->orwhere('fournisseurs.raison_sociale','like','%'.$keyword.'%')
            ->get(['stocks.id','produits.nom','fournisseurs.raison_sociale','produits.marque','produits.ref_interne','stocks.num_serie']);*/

            $stock = DB::select("SELECT stocks.id as id, produits.nom,fournisseurs.raison_sociale,produits.marque,produits.ref_interne,stocks.num_serie
            FROM stocks
            join produits on produits.id = stocks.produit_id
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            WHERE  stocks.etat = 'Disponible' AND (
            produits.nom like '%".$keyword."%' OR
            produits.description like '%".$keyword."%' OR
            produits.marque like '%".$keyword."%' OR
            produits.ref_interne like '%".$keyword."%' OR
            stocks.num_serie like '%".$keyword."%' OR
            fournisseurs.raison_sociale like '%".$keyword."%' );");

        }else{

            $stock = Stock::join('produits','produits.id','=','stocks.produit_id')
            ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')
            ->where('stocks.etat','=','Disponible')->take(10)
            ->get(['stocks.id','produits.nom','fournisseurs.raison_sociale','produits.marque','produits.ref_interne']);

        }

        return view('stocks.index', compact('stock'));
    }



    public function etatStock(Request $request){

        $keyword = $request->get('search');

        $stock = Collection::make(new Stock);

        if (!empty($keyword)) {
           /* $stock = DB::select("SELECT stocks.produit_id as id,produits.nom ,
            produits.ref_interne ,produits.marque, produits.description, produits.prix_vente, count(*) as quantite ,
            (SELECT SUM(ventes.quantite) FROM ventes WHERE ventes.produit_id=produits.id ) as reserve
            FROM stocks
            join produits on produits.id = stocks.produit_id
            WHERE  stocks.etat = 'Disponible' AND (
            produits.nom like '%".$keyword."%' OR
            produits.description like '%".$keyword."%' OR
            produits.marque like '%".$keyword."%' OR
            produits.ref_interne like '%".$keyword."%')
            group by  stocks.produit_id;");*/

            $stock = DB::select("SELECT id , produits.nom,produits.ref_interne ,produits.marque, produits.description, produits.prix_vente,
            (select count(*) from stocks where stocks.produit_id = produits.id and stocks.etat = 'Disponible' ) as quantite ,
            IFNULL( ( (SELECT SUM(ventes.quantite) FROM ventes WHERE ventes.produit_id= produits.id) - (SELECT  count(*)  FROM  bl_ventes
            join stocks on stocks.id = bl_ventes.stock_id
            where stocks.produit_id = produits.id
            group by produits.id) ),0) as reserve
            FROM  produits
            WHERE produits.nom like '%".$keyword."%' OR
            produits.description like '%".$keyword."%' OR
            produits.marque like '%".$keyword."%' OR
            produits.ref_interne like '%".$keyword."%';");


        }else{

            /*$stock = DB::select("SELECT stocks.produit_id as id, produits.nom ,
            produits.ref_interne ,produits.marque, produits.description, produits.prix_vente, count(*) as quantite,
            (SELECT SUM(ventes.quantite) FROM ventes WHERE ventes.produit_id=produits.id ) as reserve
            FROM stocks
            join produits on produits.id = stocks.produit_id
            WHERE  stocks.etat = 'Disponible'
            group by  stocks.produit_id limit 20;");*/

            $stock = DB::select("SELECT id , produits.nom,produits.ref_interne ,produits.marque, produits.description, produits.prix_vente,
            (select count(*) from stocks where stocks.produit_id = produits.id and stocks.etat = 'Disponible' ) as quantite ,
            IFNULL( ( (SELECT SUM(ventes.quantite) FROM ventes WHERE ventes.produit_id= produits.id) - (SELECT  count(*)  FROM  bl_ventes
            join stocks on stocks.id = bl_ventes.stock_id
            where stocks.produit_id = produits.id
            group by produits.id) ),0) as reserve
            FROM  produits limit 20;");



        }

        return view('stocks.etat', compact('stock'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $fournisseurs = Fournisseur::all();
        return view('stocks.create', compact('fournisseurs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'prix_unitaire' => 'nullable|numeric',
		]);
        $requestData = $request->all();
        $requestData['etat']="Disponible";

        if(DB::table('stocks')->where('num_serie', $requestData['num_serie'])->exists()){
            return redirect('stock')->with('success_message', 'Numero de serie exist deja . Stock non ajoute !');
        }

        Stock::create($requestData);

        return redirect('stock')->with('success_message', 'Stock added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stock = Stock::findOrFail($id);
        $produits = Produit::latest()->cursor();
        return view('stocks.show', compact('stock','produits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stock = Stock::findOrFail($id);
        $produits = Produit::latest()->cursor();
        $fournisseurs = Fournisseur::latest()->cursor();


        return view('stocks.edit', compact('stock','produits','fournisseurs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			//'produit_id' => 'required',
            'prix_unitaire' => 'nullable|numeric',
		]);
        $requestData = $request->all();

        $stock = Stock::findOrFail($id);
        $stock->update($requestData);

        return redirect('stock')->with('success_message', 'Stock updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Stock::destroy($id);

        return redirect('stock')->with('success_message', 'Stock deleted!');
    }



    public function importcsv(){

        $produits = Collection::make(new Produit);
        return view('stocks.importcsv', compact('produits'));
    }


    public function storecsv(Request $request){


      $path = $request->file('csv_file')->getRealPath();
      $rows = array_map('str_getcsv',file($path));
      $cols=[] ;
      foreach($rows[0] as $index =>$col){
        if(strpos($col,"#") !== false)
            $cols += array($col => $index);
      }

      if(count($cols) < 5){
        return redirect('stocks/importcsv')->with('error_message', 'La structure du fichier est invalide !');
      }else{

        foreach($rows as $row){

            if(strpos($row[0],"#") !==false){
                // do nothing
                info($row[0]);
            }
            else{
                $stock = Stock::create(['produit_id'=>$row[$cols['#produit_id']],
                                            'fournisseur_id'=>$row[$cols['#fournisseur_id']],
                                            'num_serie'=>$row[$cols['#num_serie']],
                                            'ref_fournisseur'=>$row[$cols['#ref_fournisseur']],
                                            'prix_unitaire'=>$row[$cols['#prix_unitaire']],
                                            'etat'=>'Disponible'
                ]);
            }
        }
      }


      //return count($cols);
      return redirect('stocks/importcsv')->with('success_message', 'Produits importes avec succes');

    }



    //-------------------- API ----------------------//


    public function SearchStockRetour(Request $request){

        $data = json_decode($request->data,true);
        $nomProduit = $data['nomproduit'];
        $client_id = $data['client_id'];


        $stocks = DB::select("SELECT stocks.id as id, fournisseurs.raison_sociale, produits.nom ,
            produits.ref_interne ,produits.marque, produits.description, stocks.num_serie, stocks.etat, bl_ventes.bl_id
            FROM stocks
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            join produits on produits.id = stocks.produit_id
            join bl_ventes on bl_ventes.stock_id = stocks.id
            join bonlivraisons on bonlivraisons.id = bl_ventes.bl_id
            join devis on devis.id = bonlivraisons.devis_id
            join clients on clients.id = devis.client_id
            Where clients.id = ".$client_id." AND (
            produits.nom like '%".$nomProduit."%' OR
            produits.description like '%".$nomProduit."%' OR
            produits.marque like '%".$nomProduit."%' OR
            produits.ref_interne like '%".$nomProduit."%' OR
            stocks.num_serie like '%".$nomProduit."%');");

        //return $stocks;



        return response()->json(['result' => "OK",'stocks' => $stocks]);


    }




    public function storeItems(Request $request){


        $data = json_decode($request->data,true);

        if( $data['type'] == 'nonserialisable'){  // To be optimaized !!!!!!!!


            for($i=0; $i< intval($data['quantite']) ; $i++){

                do{
                   $num_serie = "Non_Serialisable_".Str::random(20);
                }while(DB::table('stocks')->where('num_serie', $num_serie)->exists());

                info($num_serie);
                $stock = Stock::create(['produit_id'=>$data['produit_id'],
                                        'fournisseur_id'=>$data['fournisseur_id'],
                                        'prix_unitaire'=>$data['prix_unitaire'],
                                        'ref_fournisseur'=>$data['ref_fournisseur'],
                                        'num_serie'=>$num_serie]);
            }

            return response()->json(['result' => "OK"]);

        }else{

            $stocks = $data['stocks'];

            //------------------- if the num serie exists already -------------//

            foreach($stocks as $v){
                //$stock = Stock::findOrFail($v['id']);
                if(DB::table('stocks')->where('num_serie', $v)->exists()){
                    return response()->json(['result' => "KO",'stock' => $v]);
                }

            }

            //------------------------------------------------------------------------//

            foreach($stocks as $stock){
                $stock = Stock::create(['produit_id'=>$data['produit_id'],
                                        'fournisseur_id'=>$data['fournisseur_id'],
                                        'prix_unitaire'=>$data['prix_unitaire'],
                                        'ref_fournisseur'=>$data['ref_fournisseur'],
                                        'num_serie'=>$stock]);
            }
        }

        return response()->json(['result' => "OK"]);


    }




}


