<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Achat;
use App\Produit;
use App\Fournisseur;
use App\Stock;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;


class AchatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $achat = Achat::where('fournisseur_id', 'LIKE', "%$keyword%")
                ->orWhere('produit_id', 'LIKE', "%$keyword%")
                ->orWhere('quantite', 'LIKE', "%$keyword%")
                ->orWhere('prix_unitaire', 'LIKE', "%$keyword%")
                ->orWhere('prix_total', 'LIKE', "%$keyword%")
                ->orWhere('date_achat', 'LIKE', "%$keyword%")
                ->orWhere('image_facture_id', 'LIKE', "%$keyword%")
                ->orWhere('type_paiment', 'LIKE', "%$keyword%")
                ->orWhere('date_cheque', 'LIKE', "%$keyword%")
                ->orWhere('image_cheque_id', 'LIKE', "%$keyword%")
                ->orWhere('avance', 'LIKE', "%$keyword%")
                ->orWhere('etat', 'LIKE', "%$keyword%")
                ->orWhere('date_complete', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $achat = Achat::with(['Produit','fournisseur'])->latest()->paginate($perPage);
        }

        return view('achat.index', compact('achat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      $produit = produit::latest()->cursor();
      $fournisseur = Fournisseur::latest()->cursor();
        return view('achat.create', compact('fournisseur','produit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $image_facture=null;
        $image_cheque=null;

        $requestData = $request->all();

        if($request->hasFile('image_facture')){
            $path = $request->image_facture->store('uploads','public');
            $requestData['image_facture'] = $path;
        }
        if($request->hasFile('image_cheque')){
            $path = $request->image_cheque->store('uploads','public');
            $requestData['image_cheque'] = $path;
        }

        $this->validate($request, [
			'fournisseur_id' => 'required',
			'produit_id' => 'required',
			'etat' => 'required'
		]);

        Achat::create($requestData);

        return redirect('achat')->with('flash_message', 'Achat added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $achat = Achat::findOrFail($id);

        return view('achat.show', compact('achat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $achat = Achat::findOrFail($id);
        $produit = produit::latest()->cursor();
        $fournisseur = Fournisseur::latest()->cursor();

        return view('achat.edit', compact('achat','fournisseur','produit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        //$image_facture=null;
        //$image_cheque=null;

        $requestData = $request->all();

        if($request->hasFile('image_facture')){
            $path = $request->image_facture->store('uploads','public');
            $requestData['image_facture'] = $path;
        }
        if($request->hasFile('image_cheque')){
            $path = $request->image_cheque->store('uploads','public');
            $requestData['image_cheque'] = $path;
        }

        $this->validate($request, [
			'fournisseur_id' => 'required',
			'produit_id' => 'required',
			'etat' => 'required'
		]);

        $achat = Achat::findOrFail($id);
        $achat->update($requestData);

        return redirect('achat')->with('flash_message', 'Achat updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Achat::destroy($id);

        return redirect('achat')->with('flash_message', 'Achat deleted!');
    }



    /*--------------------------- API  for VueJS ------------------------------*/

    /*public function getItems($bc_id){

        $achats = Achat::join('stocks','stocks.id','=','achats.stock_id')
        ->join('produits','produits.id','=','stocks.produit_id')
        ->where('achats.bc_id',$bc_id)->get(['achats.id','produits.nom','produits.ref_interne','achats.prix_unitaire','stocks.num_serie']);

        return $achats;
    }


    public function storeItem(Request $request){

        $stock = Stock::where('num_serie',$request->num_serie)->get();
        
        if(count($stock) <=0){

            $stock = new Stock();
            $stock->produit_id = $request->produit_id;
            $stock->fournisseur_id = $request->fournisseur_id;
            $stock->num_serie = $request->num_serie;
            $stock->ref_fournisseur = $request->ref_fournisseur;
            $stock->prix_unitaire = $request->prix_unitaire;
            $stock->etat = "BC";
            $stock->save();


            $achat = new Achat();
            $achat->bc_id = $request->bc_id;
            $achat->stock_id = $stock->id;
            $achat->prix_unitaire = $request->prix_unitaire;
            $achat->save();

            return response()->json(['result' => "OK"]);

        }

        return response()->json(['result' => "Exists"]);

    }*/


    public function getItems($bc_id){

        /*$achats = Achat::join('produits','produits.id','=','achats.produit_id')
        ->where('achats.bc_id',$bc_id)->get(['achats.id','achats.produit_id','produits.nom','produits.ref_interne','achats.quantite','achats.prix_total','achats.prix_unitaire']);
        */
        
        $achats = DB::select("SELECT achats.id,achats.produit_id, produits.nom,produits.ref_interne,achats.quantite,achats.prix_total,achats.prix_unitaire, COUNT(bl_achats.achat_id) AS left_quantity
        FROM bl_achats 
        RIGHT JOIN achats ON achats.id = bl_achats.achat_id
        join produits on produits.id = achats.produit_id
        where achats.bc_id = ".$bc_id."
        GROUP BY achats.id;");

        return $achats;
    }



    public function storeItem(Request $request){

        $achat = new Achat();
        $achat->bc_id = $request->bc_id;
        $achat->produit_id = $request->produit_id;
        $achat->prix_unitaire = $request->prix_unitaire;
        $achat->quantite = $request->quantite;
        $achat->prix_total = $request->prix_unitaire * $request->quantite;
        $achat->save();

        return response()->json(['result' => "OK"]);

    }

    public function deleteItem(Request $request, $id){
        
        $achat = Achat::findOrFail($id);
        $achat->delete();
    }
}
