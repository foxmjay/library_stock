<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;


use Illuminate\Support\Facades\DB;
use App\Devis;

class ComptaController extends Controller
{
    public function GainParMoisByUser($id){

        $GainParMois = DB::select("Select mois,SUM(prix_total) as prix  from (SELECT MONTH(created_at) AS mois,prix_total,created_at,etat FROM devis where user_id = ".$id.") AS T
        where etat = 'Termine' and YEAR(created_at) = YEAR(CURDATE())  GROUP BY mois");
       // dd($GainParMois);
        return $GainParMois;

        //return view('clients.edit', compact('GainParMois'));
    }

    public function backup(){
        $userProfile = \App\UserProfile::where('user_id',Auth::user()->id)->first();
        if ($userProfile->type  == "SuperAdmin") {
            return response()->download(storage_path("backups/backup.sql"));
        }

        //return response()->download('http://gestionstock.stepone.ma/storage/backups/backup.sql');



    }
}
