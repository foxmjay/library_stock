<?php

namespace App\Http\Controllers;

use App\Boncommand;
use App\Fournisseur;
use App\Produit;
use App\Achat;
use App\Echeance;
use App\Stock;
use App\Categorie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Exception;

use Illuminate\Support\Facades\DB;

class BoncommandsController extends Controller{

    public function index(){
        $boncommands = Boncommand::all()->sortByDesc('date_bc');
        return view('boncommands.index',compact('boncommands'));
    }

    public function create($f_id){

        $fournisseur = Fournisseur::find($f_id,['id','raison_sociale']);
        $bc = Boncommand::latest()->first(['id','ref_bc']);
        $ref_bc="";
        $date=date("Y");
        
        if(empty($bc)){
            $ref_bc = $date.sprintf('-%05d',1);
        }else{
            $ref_bc = $date.sprintf('-%05d',$bc->id);   
        }
        return view('boncommands.create',compact('fournisseur','ref_bc'));
    }


    public function store(Request $request){ 
   
            $data = $this->getDataStore($request);

            $data['user_id']=Auth::id();


            if($request->hasFile('image_devis')){
                $path = $request->image_devis->store('uploads','public');
                $data['image_devis'] = $path;
            }

            $bc=Boncommand::create($data);
            return redirect('boncommands/'.$bc->id.'/edit')
                             ->with('success_message', 'Boncommand ajoute avec succes');


    }

    public function edit($id){

        $boncommand = Boncommand::findOrFail($id);
        $fournisseurs = Fournisseur::latest()->cursor();
        //$produits = Produit::latest()->cursor();
        $categories = Categorie::latest()->cursor();

       
        return view('boncommands.edit', compact('boncommand','fournisseurs','categories'));
    }


    public function createBl($bc_id){

        $boncommand = Boncommand::findOrFail($bc_id);
        $fournisseur = Fournisseur::find($boncommand->fournisseur_id,['id','raison_sociale']);
        
        return view('boncommands.boncommandbl_create',compact('fournisseur','boncommand'));
    }


    public function produit($id){

        $boncommand = Boncommand::findOrFail($id);
        $fournisseurs = Fournisseur::latest()->cursor();
        //$produits = Produit::latest()->cursor();
        $categories = Categorie::latest()->cursor();

       
        return view('boncommands.produit', compact('boncommand','fournisseurs','categories'));
    }


    public function update(Request $request, $id) {


        $data = $this->getData($request);

        if($request->hasFile('image_devis')){
            $path = $request->image_devis->store('uploads','public');
            $data['image_devis'] = $path;
        }


        $boncommand = Boncommand::findOrFail($id);

        $boncommand->update($data);

        

        return redirect('fournisseurs/'.$boncommand->fournisseur_id)->with('success_message', 'Boncommand sauvgarde succes');

        //return redirect('boncommands')->with('flash_message', 'boncommand updated!');
    }



    public function confirmer(Request $request){

            
        $bc_id = $request->bc_id;
        $bc = Boncommand::findOrFail($bc_id);
        $bc->etat = "Termine";
        $bc->date_bc = $request->date_bc;
        $bc->tva = $request->tva;
        $bc->prix_total = $request->prix_total;
        $bc->date_livraison = $request->date_livraison;
        $bc->ref_devis = $request->ref_devis;

        if($request->hasFile('image_devis')){
            $path = $request->image_devis->store('uploads','public');
            $bc->image_devis= $path;
        }

        $bc->save();

        /*$achats = Achat::where('bc_id',$bc_id)->get();

        foreach($achats as $achat){
                $stock = Stock::findOrFail($achat->stock_id);
                $stock->etat = "Disponible";
                $stock->save();
        }
        */
        return response()->json(['result' => "OK",'fournisseur_id' => $bc->fournisseur_id]);

    }

    public function destroy($id){
        $fournisseur_id = Boncommand::find($id)->fournisseur_id;
        Boncommand::destroy($id);

        return redirect('fournisseurs/'.$fournisseur_id)->with('flash_message', 'Achat deleted!');
    }


    public function print($id){
        $boncommand = Boncommand::findOrFail($id);
        $echeances = Echeance::where('bc_id',$id)->get();
        /*$achats = Achat::join('stocks','stocks.id','=','achats.stock_id')
        ->join('produits','produits.id','=','stocks.produit_id')
        ->where('bc_id',$boncommand->id)
        ->get(['produits.id','produits.nom','produits.marque','produits.description','achats.prix_unitaire']);*/

        /*$achats = DB::select("SELECT produits.id, produits.nom, produits.description, produits.id ,produits.nom, produits.description, count(*) as quantite , sum(bonretour_stocks.prix) as prix_total
        FROM bonretour_stocks
        join ventes on ventes.id = bonretour_stocks.vente_id
        join stocks on stocks.id = bonretour_stocks.stock_id
        join produits on produits.id = stocks.produit_id
        where bonretour_stocks.br_id = ".$br_id."
        Group by produits.id;");*/

        $achats = DB::select("SELECT produits.id, produits.nom, produits.marque, produits.description, achats.quantite, achats.prix_unitaire, achats.prix_total
        FROM achats
        join produits on produits.id = achats.produit_id
        where achats.bc_id = ".$boncommand->id.";");


        //return $achats;
        return view('prints.printbc',compact('boncommand','achats','echeances'));
    }


    protected function getDataStore(Request $request){
        $rules = [
            'fournisseur_id' => 'required',
            'ref_bc' => 'nullable|string|min:0|max:191',
            'ref_devis' => 'nullable|string|min:0|max:191',
            'tva'=> 'nullable|numeric',
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

    protected function getData(Request $request){
        $rules = [
            'prix_total'=> 'nullable|numeric',
            'etat'=> 'nullable',
            'date_bc'=> 'nullable',
            'ref_devis' => 'nullable|string|min:0|max:191',
            'date_livraison'=> 'nullable',
            'tva'=> 'nullable|numeric',
        ];
        
        $data = $request->validate($rules);


        return $data;
    }




}
