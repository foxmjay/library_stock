<?php

namespace App\Http\Controllers;

use App\Boncommand;
use App\BoncommandBl;
use App\BlAchat;
use App\Stock;
use App\Fournisseur;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BoncommandBlController extends Controller{


    public function show($bl_id){

        $bl = BoncommandBl::findOrFail($bl_id);
        $boncommand = Boncommand::findOrFail($bl->bc_id);
        $fournisseur = Fournisseur::find($boncommand->fournisseur_id,['id','raison_sociale']);

        $stocks = BlAchat::join('achats','achats.id','bl_achats.achat_id')
                        ->join('stocks','stocks.id','bl_achats.stock_id')
                        ->join('produits','produits.id','stocks.produit_id')
                        ->where('bl_achats.bl_id',$bl_id)->get(['bl_achats.stock_id as id','produits.nom','produits.ref_interne','stocks.num_serie','stocks.ref_fournisseur','achats.prix_unitaire']);

        return view('boncommands.boncommandbl_show',compact('bl','boncommand','stocks','fournisseur'));
    }
    

    public function store(Request $request){



        $data = json_decode($request->jsondata,true);


                
        
        //------------------- if the num serie exists already -------------//
            
        foreach($data['stocks'] as $v){
            //$stock = Stock::findOrFail($v['id']);
            if(DB::table('stocks')->where('num_serie', $v['num_serie'])->exists()){
                return response()->json(['result' => "KO",'stock' => $v]);
            }

        }

        //------------------------------------------------------------------------//

        

        //-------- set type of boncommand ----//
        $bc = Boncommand::findOrFail($data['bc_id']);
        if($data['isNewBl']){
            $bc->type = $data['bl_type'];
            $bc->save();
        }

        //-------------------------------------------------------------//


        //-------------------------  create  new boncommandbl ------------//
        $image_bl='';
        if ($request->hasFile('image_bl')) {
            $path = $request->image_bl->store('uploads', 'public');
            $image_bl = $path;
        }


        $bl = BoncommandBl::create(['bc_id'=>$data['bc_id'],
                                    'ref_bl'=>$data['ref_bl'],
                                    'image_bl'=>$image_bl
                                    ]);

        //-----------------------------------------------------------//

                  

        //------------  create Stock & create ventes_bl  &  calculate prix total -------------------------//
        $prix_total=0;

        foreach($data['stocks'] as $v){
            $stock = Stock::create(['produit_id'=>$v['produit_id'],
                                    'fournisseur_id' => $data['fournisseur_id'],
                                    'num_serie' => $v['num_serie'],
                                    'ref_fournisseur' => $v['ref_fournisseur'],
                                    'prix_unitaire'=> $v['prix_unitaire'],
                                    'etat'=> 'Disponible'
                                    ]);
            

            $bl_v = BlAchat::create(['bl_id'=>$bl->id,'achat_id'=>$v['id'],'stock_id' => $stock->id]);
            $prix_total +=$v['prix_unitaire'] ;
        }
        $bl->prix_total = $prix_total;
        $bl->save();

        //------------------------------------------------------------------------------------------------------//


        //--------------------- check if boncommand is complete ------------------------------//

       /* $ventes = Vente::where('devis_id',$devis_id)->get(['id','quantite']);
        $complet = 1;
        foreach($ventes as $vente){

            $bl_vente = DB::select("SELECT bl_ventes.vente_id, count(*) as quantite
            FROM bl_ventes
            where bl_ventes.vente_id = ".$vente->id."
            Group by bl_ventes.vente_id;");
            $quantite = 0;
            if(empty($bl_vente))
                $quantite = 0;
            else
                $quantite = $bl_vente[0]->quantite;
            
            
            if($quantite < $vente->quantite){
                $complet = 0;
            }
        }

        if($complet == 1){
            $devisbc = Devisbc::where('devis_id',$devis_id)->first();
            $devisbc->etat = "Complet";
            $devisbc->save();

            if($devisbc->type_facture == 'Complet'){
                $facture = Facture::latest()->first(['id','ref_facture']);
                $ref_facture="";
                if(empty($facture)){
                    $ref_facture = $date.sprintf('-%05d',1);
                }else{
                    $ref_facture = $date.sprintf('-%05d',$facture->id + 1);   
                }
                $facture = Facture::create(['devis_id'=>$devis_id,'ref_facture'=>$ref_facture]);
            }
        }*/
        //------------------------------------------------------------------------------------//
        
        return response()->json(['result' => "OK"]);

    }


        
    public function getItemsPerBC($bc_id){

        $bls = BoncommandBl::join('boncommands','boncommands.id','=','boncommandbls.bc_id')
        ->where('boncommandbls.bc_id',$bc_id)->get(['ref_bl','boncommandbls.created_at','boncommandbls.id as id','boncommands.type','boncommandbls.prix_total']);

        return $bls;

    }
}
