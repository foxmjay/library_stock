<?php

namespace App\Http\Controllers;

use App\Vente;
use App\Devis;
use App\Devisbc;
use App\Stock;
use App\Bonlivraison;
use App\Bl_vente;
use App\Echeance;
use App\Facture;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;


class BonlivraisonsController extends Controller
{
    
    public function getItemsPerBl($devis_id){

        $bls = Bonlivraison::join('devisbcs','devisbcs.devis_id','=','bonlivraisons.devis_id')
        ->where('bonlivraisons.devis_id',$devis_id)->get(['ref_bl','bonlivraisons.prix_total','devisbcs.etat','bonlivraisons.created_at','bonlivraisons.id as id','devisbcs.type_facture']);

        return $bls;

    }

    public function create($devis_id){

        $devis = Devis::findOrFail($devis_id);
        $dbc = Devisbc::where('devis_id',$devis->id)->first();
        $num_bc = $dbc->num_bc;
        $bc_type = $dbc->type_facture;
        $client_id = $devis->client_id;

        $bls = Bonlivraison::where('devis_id',$devis_id)->get();
        $isNewBl=true;
        if(count($bls) > 0)
            $isNewBl = false;
        //$devis_id = Devis::where()->first();
        //return view('bonlivraisons.index',compact('devisbc_id','devisbc'));
     
        return view('bonlivraisons.create',compact('devis_id','client_id','num_bc','isNewBl','bc_type'));
    }

    public function edit($bl_id){
        //$devis_id = Devisbc::findOrFail($devisbc_id,['devis_id'])->devis_id;
        //$devis_id = Devis::where()->first();
        //return view('bonlivraisons.index',compact('devisbc_id','devisbc'));

        //$ventes = Ventes::where('bl_id',$bl_id)->get();

        $bl = Bonlivraison::findOrFail($bl_id);
        $devis_id = $bl->devis_id;
        $devis = Devis::findOrFail($devis_id);
        $client_id = $devis->client_id;
        $num_bc = Devisbc::where('devis_id',$devis->id)->first()->num_bc;



        $ventes = DB::select("SELECT bl_ventes.stock_id, produits.nom, produits.id, produits.marque, stocks.num_serie, produits.ref_interne,
            produits.description, fournisseurs.raison_sociale, produits.prix_vente, bl_ventes.vente_id, ventes.remise
            FROM bl_ventes
            join ventes on ventes.id = bl_ventes.vente_id
            join produits on produits.id = ventes.produit_id
            join stocks on stocks.id = bl_ventes.stock_id
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            where bl_ventes.bl_id = ".$bl_id.";");

        /*$ventes = Vente::join('stocks','stocks.id','=','ventes.stock_id')
        ->join('produits','produits.id','=','stocks.produit_id')
        ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')
        ->join('bl_ventes','bl_ventes.vente_id','ventes.id')
        ->join('bonlivraisons','bonlivraisons.id','bl_ventes.bl_id')
        ->where('bl_ventes.bl_id',$bl_id)->get(['ventes.id','produits.nom','produits.ref_interne' ,'stocks.num_serie','fournisseurs.raison_sociale','ventes.remise','ventes.prix_final','bonlivraisons.prix_total']);

        //return view('bonlivraisons.edit',compact('bl','devis_id','ventes'));*/

        return view('bonlivraisons.edit',compact('bl','devis_id','ventes','client_id','num_bc'));
    }



    public function store($devis_id, Request $request){


        $data = json_decode($request->data,true);
        $date=date("Y");


        //---------------- Verif ref facture if doesn't exists already -------------------//
        if($data['type_ref_facture'] == 'Manuel' && $data['bc_type'] == 'Partiel'){
            $facture = Facture::where('ref_facture','like',$data['ref_facture'])->get();        
            if(count($facture)>=1){
                return response()->json(['result' => "KO",'facture' => $data['ref_facture']]);
            }
        }
        //---------------------------------------------------------------------------------//

        
        //-------- set facture type if no bl of devis was created ----//
        $devisbc = Devisbc::where('devis_id',$devis_id)->first();
        if($data['isNewBl']){
            $devisbc->type_facture = $data['bc_type'];
            $devisbc->save();
        }

        //-------------------------------------------------------------//
        
        
        //------------------- if the selected Stock is not avaialble -------------//
        foreach($data['stocks'] as $v){
            $stock = Stock::findOrFail($v['id']);
            if($stock->etat != "Disponible"){
                return response()->json(['result' => "KO",'stock' => $v]);
            }

        }

        //------------------------------------------------------------------------//


        //-------------------------  generate refrence bl & create  new BL ------------//
        $bl = Bonlivraison::latest()->first(['id','ref_bl']);
        $ref_bl="";
        if(empty($bl)){
            $ref_bl = $date.sprintf('-%05d',1);
        }else{
            $ref_bl = $date.sprintf('-%05d',$bl->id +1);   
        }

        $bl = Bonlivraison::create(['devis_id'=>$devis_id,
                                    'ref_bl'=>$ref_bl,
                                    'type'=>$data['bl_type'],
                                    ]);

        //-----------------------------------------------------------//

        //----------------------- generate ref facture & create new facture if type is partial --------------------//
        
        $facture = new Facture();
        if($devisbc->type_facture == 'Partiel'){
            if($data['type_ref_facture'] == 'Manuel'){
                $ref_facture=$data['ref_facture'];
            }else{

                $facture = Facture::latest()->first(['id','ref_facture']);
                $ref_facture="";
                if(empty($facture)){
                    $ref_facture = $date.sprintf('-%05d',1);
                }else{
                    $ref_facture = $date.sprintf('-%05d',$facture->id + 1);   
                }
            }    
            
            $facture = Facture::create(['devis_id'=>$devis_id,'bl_id'=>$bl->id,'ref_facture'=>$ref_facture]);
        }
                  
        //------------------------------------------------------------------------------------------------------//

        //------------  update etat stock & create ventes_bl  &  calculate prix totoal -------------------------//
        $prix_total=0;
        foreach($data['stocks'] as $v){
            $stock = Stock::findOrFail($v['id']);
            $stock->etat = "Vendu";
            $stock->save();

            $bl_v = Bl_vente::create(['bl_id'=>$bl->id,'vente_id'=>$v['vente_id'],'stock_id' => $v['id']]);
            $prix_total +=$v['prix_vente'] - ($v['prix_vente'] * $v['remise']/ 100);
        }
        $bl->prix_total = $prix_total;
        $bl->save();

        if( $devisbc->type_facture == 'Partiel'){
            $facture->prix_total = $prix_total;
            $facture->save();
        }
        
        //------------------------------------------------------------------------------------------------------//


        //--------------------- check if devis is complete ------------------------------//
        $ventes = Vente::where('devis_id',$devis_id)->get(['id','quantite']);
        $complet = 1;
        foreach($ventes as $vente){

            $bl_vente = DB::select("SELECT bl_ventes.vente_id, count(*) as quantite
            FROM bl_ventes
            where bl_ventes.vente_id = ".$vente->id."
            Group by bl_ventes.vente_id;");
            $quantite = 0;
            if(empty($bl_vente))
                $quantite = 0;
            else
                $quantite = $bl_vente[0]->quantite;
            
            
            if($quantite < $vente->quantite){
                $complet = 0;
            }
        }

        if($complet == 1){
            $devisbc = Devisbc::where('devis_id',$devis_id)->first();
            $devisbc->etat = "Complet";
            $devisbc->save();

            if($devisbc->type_facture == 'Complet'){
                return response()->json(['result' => "OK",'complet' => $bl->id]);
            }    

            /*if($devisbc->type_facture == 'Complet'){
                $facture = Facture::latest()->first(['id','ref_facture']);
                $ref_facture="";
                if(empty($facture)){
                    $ref_facture = $date.sprintf('-%05d',1);
                }else{
                    $ref_facture = $date.sprintf('-%05d',$facture->id + 1);   
                }
                $facture = Facture::create(['devis_id'=>$devis_id,'ref_facture'=>$ref_facture,'prix_total'=>$prix_total]);
            }*/
        }
        //------------------------------------------------------------------------------------//
        
        return response()->json(['result' => "OK"]);

    }


    public function confirmationFacture($bl_id){
    
        $bl = Bonlivraison::findOrFail($bl_id);
        $client_id = Devis::findOrFail($bl->devis_id)->client_id;
        return view('bonlivraisons.confirmfacture',compact('bl','client_id'));
        //return redirect('bonlivraisons.confirmfacture')->with('devis_id','prix_total');
    }

    public function storeFacture(Request $request){


        $data = json_decode($request->data,true);
        $date=date("Y");

        info($data);
        //---------------- Verif ref facture if doesn't exists already -------------------//
        if($data['type_ref_facture'] == 'Manuel'){
            $facture = Facture::where('ref_facture','like',$data['ref_facture'])->get();        
            if(count($facture)>=1){
                return response()->json(['result' => "KO",'facture' => $data['ref_facture']]);
            }
        }
        //----------------------------------------------------------------------------------//

        $prix_total=$data['prix_total'];
        $devis_id=$data['devis_id'];

        if($data['type_ref_facture'] == 'Manuel'){
            $ref_facture=$data['ref_facture'];
        }else{
            $facture = Facture::latest()->first(['id','ref_facture']);
            $ref_facture="";
            if(empty($facture)){
                $ref_facture = $date.sprintf('-%05d',1);
            }else{
                $ref_facture = $date.sprintf('-%05d',$facture->id + 1);   
            }
        }

        $facture = Facture::create(['devis_id'=>$devis_id,'ref_facture'=>$ref_facture,'prix_total'=>$prix_total]);
        
        return response()->json(['result' => "OK"]);


    }

    //------------------------------------ not used --------------------------------------------------//

    public function update($bl_id, Request $request){
        
        $data = json_decode($request->data,true);
        

        $bl = Bonlivraison::findOrFail($bl_id);

        $devis = Devis::findOrFail($bl->devis_id);

        $bl->type = $data['bl_type'];
        $bl->num_bc = $data['num_bc'];
        $bl->save();


        /*foreach($data['stocks'] as $v){
            $stock = Stock::findOrFail($v['stock_id']);
            if($stock->etat != "Disponible"){
                return response()->json(['result' => "KO",'stock' => $v]);
            }
        }*/

        $stocks = DB::select("SELECT bl_ventes.stock_id
            FROM bl_ventes
            where bl_ventes.bl_id = ".$bl_id.";");


        return;

        /*$ventes = Vente::join('stocks','stocks.id','=','ventes.stock_id')
        ->join('produits','produits.id','=','stocks.produit_id')
        ->join('fournisseurs','fournisseurs.id','=','stocks.fournisseur_id')
        ->join('bl_ventes','bl_ventes.vente_id','ventes.id')
        ->where('bl_ventes.bl_id',$bl_id)->get(['ventes.id']);*/

        // ----------- Delete removed ventes ---------//
        foreach($stocks as $v){
            $exists = 0;
            foreach($data['stocks'] as $sv){
                if( $v->stock_id == $sv['stock_id'])
                    $exists = 1;
            }
            if($exists == 0){
                $bl_v = Bl_vente::where('stock_id',$v->stock_id)->first();
                $bl_v->delete();
                $stock = Stock::find($v->stock_id);
                $stock->etat = "Disponible";
                $stock->save();
            }
                
        }

        // -------------- Add new ventes -------//
        $prix_total=0;
        foreach($data['stocks'] as $sv){
            $exists = 0;
            foreach($stocks as $v){
                if( $v->stock_id == $sv['stock_id'])
                    $exists =1;
            }
            if($exists == 0)
                Bl_vente::create(['bl_id'=>$bl->id,'vente_id'=>$sv['id']]);

            $prix_total +=$sv['prix_final'];
        }

        $bl->prix_total = $prix_total;
        $bl->save();

        
        return response()->json(['result' => "OK",'client_id' => $devis->client_id]);

    }

    //----------------------------------------------------------------------------------------------//


    public function show($bl_id){
        //$devis_id = Devisbc::findOrFail($devisbc_id,['devis_id'])->devis_id;
        //$devis_id = Devis::where()->first();
        //return view('bonlivraisons.index',compact('devisbc_id','devisbc'));

        //$ventes = Ventes::where('bl_id',$bl_id)->get();

        $bl = Bonlivraison::findOrFail($bl_id);
        $devis_id = $bl->devis_id;
        $devis = Devis::findOrFail($devis_id);
        $client_id = $devis->client_id;
        $num_bc = Devisbc::where('devis_id',$devis->id)->first()->num_bc;



        $ventes = DB::select("SELECT bl_ventes.stock_id, produits.nom, produits.id, produits.marque, stocks.num_serie, produits.ref_interne,
            produits.description, fournisseurs.raison_sociale, produits.prix_vente, bl_ventes.vente_id, ventes.remise
            FROM bl_ventes
            join ventes on ventes.id = bl_ventes.vente_id
            join produits on produits.id = ventes.produit_id
            join stocks on stocks.id = bl_ventes.stock_id
            join fournisseurs on fournisseurs.id = stocks.fournisseur_id
            where bl_ventes.bl_id = ".$bl_id.";");


        return view('bonlivraisons.show',compact('bl','devis_id','ventes','client_id','num_bc'));
    }


    public function printbl($bl_id){

        $bl = Bonlivraison::findOrFail($bl_id);
        $devis = Devis::find($bl->devis_id);
        $devisbc = Devisbc::where('devis_id',$devis->id)->first();

        $ventes = DB::select("SELECT bl_ventes.vente_id, produits.nom, produits.description, count(*) as quantite
        FROM bl_ventes
        join ventes on ventes.id = bl_ventes.vente_id
        join produits on produits.id = ventes.produit_id
        where bl_ventes.bl_id = ".$bl_id."
        Group by bl_ventes.vente_id;");


        $stocks = Bl_vente::join('ventes','ventes.id','bl_ventes.vente_id')
                            ->join('stocks','stocks.id','bl_ventes.stock_id')
                            ->where('bl_ventes.bl_id',$bl_id)
                            ->get(['stocks.id', 'bl_ventes.vente_id', 'stocks.num_serie', 'stocks.ref_fournisseur']);


        //return $produits;

        
        /*$ventes = Vente::join('produits','produits.id','=','ventes.produit_id')
        ->where('ventes.devis_id',$devis->id)
        ->get(['produits.id','produits.nom','produits.marque','produits.description','ventes.quantite']);*/

        //return $ventes;


        
        /*$ventes = Vente::join('stocks','ventes.stock_id','=','stocks.id')
        ->join('produits','produits.id','=','stocks.produit_id')
        ->where('devis_id',$devis->id)
        ->get(['produits.id','produits.nom','produits.marque','produits.description','ventes.quantite','ventes.prix_unitaire','ventes.prix_total']);*/

        return view('prints.printbl',compact('bl','ventes','devis','devisbc','stocks'));
    }


    public function printfacture($bl_id){


        /*$ventes = DB::select("SELECT bl_ventes.stock_id, produits.nom, produits.id, produits.marque, stocks.num_serie, produits.ref_interne,
        produits.description, fournisseurs.raison_sociale, produits.prix_vente, bl_ventes.vente_id, ventes.remise
        FROM bl_ventes
        join ventes on ventes.id = bl_ventes.vente_id
        join produits on produits.id = ventes.produit_id
        join stocks on stocks.id = bl_ventes.stock_id
        join fournisseurs on fournisseurs.id = stocks.fournisseur_id
        where bl_ventes.bl_id = ".$bl_id.";");*/



        $bl = Bonlivraison::findOrFail($bl_id);
        $devis = Devis::find($bl->devis_id);
        $devisbc = Devisbc::where('devis_id',$devis->id)->first();
        $echeances = Echeance::where('devis_id',$devis->id)->get();
        $facture = Facture::where('bl_id',$bl->id)->first();


        $ventes = DB::select("SELECT bl_ventes.vente_id, produits.nom, produits.description, produits.ref_interne, ventes.prix_unitaire, ventes.prix_total , ventes.remise, count(*) as quantite
        FROM bl_ventes
        join bonlivraisons on bonlivraisons.id = bl_ventes.bl_id
        join ventes on ventes.id = bl_ventes.vente_id
        join produits on produits.id = ventes.produit_id
        where bl_ventes.bl_id = ".$bl_id."
        Group by bl_ventes.vente_id;");
        
        return view('prints.printfacture',compact('bl','ventes','devis','devisbc','echeances','facture'));
    }



    public function printfacturetotal($devis_id){


        /*$ventes = DB::select("SELECT bl_ventes.stock_id, produits.nom, produits.id, produits.marque, stocks.num_serie, produits.ref_interne,
        produits.description, fournisseurs.raison_sociale, produits.prix_vente, bl_ventes.vente_id, ventes.remise
        FROM bl_ventes
        join ventes on ventes.id = bl_ventes.vente_id
        join produits on produits.id = ventes.produit_id
        join stocks on stocks.id = bl_ventes.stock_id
        join fournisseurs on fournisseurs.id = stocks.fournisseur_id
        where bl_ventes.bl_id = ".$bl_id.";");*/

        

        $bls = Bonlivraison::where('devis_id',$devis_id)->get();
        $devis = Devis::find($devis_id);
        $devisbc = Devisbc::where('devis_id',$devis->id)->first();
        $echeances = Echeance::where('devis_id',$devis->id)->get();
        $facture = Facture::where('devis_id',$devis_id)->first();


        $ventes = DB::select("SELECT bl_ventes.vente_id, produits.nom, produits.description, produits.ref_interne,  ventes.prix_unitaire, ventes.prix_total , ventes.remise, count(*) as quantite
        FROM bl_ventes
        join bonlivraisons on bonlivraisons.id = bl_ventes.bl_id
        join ventes on ventes.id = bl_ventes.vente_id
        join produits on produits.id = ventes.produit_id
        join devis on devis.id = bonlivraisons.devis_id
        where devis.id = ".$devis_id."
        Group by bl_ventes.vente_id;");      

        return view('prints.printfacturetotal',compact('bls','ventes','devis','devisbc','echeances','facture'));
    }

    
}
