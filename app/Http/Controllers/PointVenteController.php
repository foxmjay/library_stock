<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Produit;
use App\PointVente;
//use App\VDStock;
use App\Stock;
use App\PVStock;
use App\Image;
use App\Client;
use App\User;
use App\UserProfile;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PointVenteController extends Controller
{
    public function index(Request $request){

        $keyword = $request->get('search');

        $pvs = Collection::make(new PointVente);

        if (!empty($keyword)) {
            $pvs = PointVente::where('ref','like','%'.$keyword.'%')
            //->orwhere('adresse','like','%'.$keyword.'%')
            //->orwhere('ref','like','%'.$keyword.'%')
            ->orwhere('objet','like','%'.$keyword.'%')
            ->get(['id','ref','objet','prix_total']);


        }else{

            $pvs = PointVente::take(10)
            ->get(['id','ref','objet','prix_total']);

        }

        return view('pointventes.index', compact('pvs'));
    }

    public function create(){
        return view('pointventes.create');
    }

    public function edit($id){

        return view('pointventes.edit',['pv_id'=>$id]);
    }

    public function getStock(Request $request){

        $data = json_decode($request->data,true);

        $stocks = DB::select("SELECT  produits.id , produits.nom, produits.marque, produits.ref_interne,
        produits.barcode, produits.description, produits.prix_vente ,
        ( SELECT count(produit_id) FROM stocks where stocks.produit_id = produits.id and stocks.etat='Disponible') as quantity
        FROM produits
        WHERE produits.barcode = '".$data['barcode']."';");

        if ( $stocks )
            return json_encode($stocks[0]);
        else
            return response()->json(['Error' => 'Not Found']);

    }


    public function getPV($id){


        info($id);

        $pv = PointVente::find($id);

        $pvstocks = PVStock::where('pv_id','=',$pv->id);
        $client=null;
        if($pv->client_id)
            $client = Client::find($pv->client_id);

        $listProduits = DB::select("SELECT produits.id as produit_id,produits.barcode, produits.ref_interne,  produits.prix_vente as price,
        produits.nom, stocks.produit_id,
        count(stocks.produit_id) as quantity,
        ( library_stock.produits.prix_vente * count(stocks.produit_id) ) as total
        FROM pv_stocks
        join stocks on stocks.id = pv_stocks.stock_id
        join produits on produits.id = stocks.produit_id
        where pv_stocks.pv_id=".$pv->id."
        group by stocks.produit_id;");


        $json_response=[
                'pv'=>$pv,
                'listProduits'=>$listProduits,
                'client' => $client
        ];

        return response()->json($json_response);

    }


    public function storePV(Request $request){

        $data = json_decode($request->data,true);
        $user = auth()->user();
        $json_response=[];


        $date=date("Y");
        $pv = PointVente::latest()->first(['id','ref']);
        $ref_pv="";
        if(empty($pv)){
            $ref_pv = "pv_".$date.sprintf('-%05d',1);
        }else{
            $ref_pv = "pv_".$date.sprintf('-%05d',$pv->id +1);
        }

        $image_id = null;

        if($data['image_cheque'] && $data['type_paiement']== 'Cheque'){

            $imageBase64 = $data['image_cheque'];
            $extension = explode('/', explode(':', substr($imageBase64, 0, strpos($imageBase64, ';')))[1])[1];

            if(preg_match('/^data:image\/(\w+);base64,/', $imageBase64)){
                $imageData = substr($imageBase64, strpos($imageBase64,',')+1);
                $imageData = base64_decode($imageData);
                $path='uploads/image_cheque_'.$ref_pv.'.'.$extension;
                Storage::disk('public')->put($path,$imageData);

                $image=Image::create(['image'=>$path,'type'=>'cheque']);
                $image_id=$image->id;
            }else {
                return response()->json(['error'=> 'Image Invalide']);

            }
        }


        $pv_new = PointVente::create(['client_id'=>$data['client_id'],
                                      'ref'=>$ref_pv,
                                      'prix_total'=>$data['prix_total'],
                                      'objet'=>'not used',
                                      'garantie'=> 0,
                                      'etat'=>'not used',
                                      'user_id'=>$user->id,
                                      'remise'=>$data['remise'],
                                      'rendu'=>$data['montant_rendu'],
                                      'recu'=>$data['montant_recu'],
                                      'type_paiement'=>$data['type_paiement'],
                                      'image_cheque_id'=>$image_id
                                    ]);


        foreach($data['listProduits'] as $produit){
            //info($produit);
            $stocks = Stock::where('produit_id','=',$produit['produit_id'])
                            ->where('etat','=','Disponible')
                            ->limit($produit['quantity'])->get();
            //info($produit['quantity']);
            //info(count($stocks));

            if( count($stocks) >= $produit['quantity'] ){
                foreach($stocks as $stock){
                    PVStock::create(['pv_id'=>$pv_new->id,
                                        'stock_id'=>$stock->id,
                                        'prix_unitaire'=>$produit['price']
                    ]);

                    $stock->etat="Vendu";
                    $stock->save();
                }

            }else {
                $json_response=['error'=> 'quantite insuffisante pour le produit '.$produit['nom']];
            }
        }

        if(collect($json_response)->has('error')){

            $pvstocks=PVStock::where('pv_id','=',$pv_new->id)->get();
            foreach($pvstocks as $pvstock){
                $stock = Stock::find($pvstock->stock_id);
                $stock->etat="Disponible";
                $stock->save();
                $pvstock->delete();
            }
            $pv_new->delete();
        }

        $json_response=['pv_new'=>$pv_new];
        return response()->json($json_response);

    }


    public function updatePV(Request $request){

        $data = json_decode($request->data,true);
        $user = auth()->user();

        $json_response=['result'=> 'OK'];

        $pv = PointVente::find($data['pv_id']);

        $image_id = null;


        if($data['image_cheque'] && $data['type_paiement']== 'Cheque'){

            $imageBase64 = $data['image_cheque'];
            $extension = explode('/', explode(':', substr($imageBase64, 0, strpos($imageBase64, ';')))[1])[1];

            if(preg_match('/^data:image\/(\w+);base64,/', $imageBase64)){
                $imageData = substr($imageBase64, strpos($imageBase64,',')+1);
                $imageData = base64_decode($imageData);
                $path='uploads/image_cheque_'.$pv->ref.'.'.$extension;
                Storage::disk('public')->put($path,$imageData);
                if(!$pv->image_cheque_id){
                    $image=Image::create(['image'=>$path,'type'=>'cheque']);
                    $image_id=$image->id;
                }else{
                    $image_id=$pv->image_cheque_id;
                }

            }else {
                return response()->json(['error'=> 'Image Invalide']);

            }
        }else{
            if($pv->image_cheque_id){
                Image::find($pv->image_cheque_id)->delete();
            }
        }


        $pv->client_id=$data['client_id'];
        $pv->prix_total=$data['prix_total'];
        $pv->user_id=$user->id;
        $pv->remise=$data['remise'];
        $pv->rendu=$data['montant_rendu'];
        $pv->recu=$data['montant_recu'];
        $pv->type_paiement=$data['type_paiement'];
        $pv->image_cheque_id=$image_id;
        $pv->save();

        // BACKUP OLD STOCKS FROM POINT VENTE //

        $pvstocks_backup=PVStock::where('pv_id','=',$pv->id)->get();

        // ADD NEW PRODUITs //

        $new_pvstocks= collect(new PVStock);
        foreach($data['listProduits'] as $produit){

            $stocks = Stock::where('produit_id','=',$produit['produit_id'])
                            ->where('etat','=','Disponible')
                            ->limit($produit['quantity'])->get();

            if( count($stocks) >= $produit['quantity'] ){
                foreach($stocks as $stock){
                    $pv_=PVStock::create(['pv_id'=>$pv->id,
                                        'stock_id'=>$stock->id,
                                        'prix_unitaire'=>$produit['price']
                    ]);
                    $new_pvstocks->add($pv_);

                    $stock->etat="Vendu";
                    $stock->save();
                }

            }else {
                $json_response=['error'=> 'quantite insuffisante pour le produit '.$produit['nom']];
            }
        }

        if(collect($json_response)->has('error')){

            // DELETE STOCKS FROM POINT VENTE //
            foreach($new_pvstocks as $pvstock){
                $stock = Stock::find($pvstock->stock_id);
                $stock->etat="Disponible";
                $stock->save();
                $pvstock->delete();
            }
        }else{

            // DELETE OLD STOCK FROM POINT VENTE //
            foreach($pvstocks_backup as $pvstock){
                $stock = Stock::find($pvstock->stock_id);
                $stock->etat="Disponible";
                $stock->save();
                $pvstock->delete();
            }
        }

        return response()->json($json_response);

    }


    public function printPVTicket($pv_id){


        $pv = PointVente::find($pv_id);
        $userProfile = UserProfile::where('user_id','=',$pv->user_id)->get(['nom','prenom'])->first();

        $listProduits = DB::select("SELECT produits.id as produit_id,produits.barcode, produits.ref_interne,  produits.prix_vente as price,
        produits.nom, stocks.produit_id,
        count(stocks.produit_id) as quantity,
        ( library_stock.produits.prix_vente * count(stocks.produit_id) ) as total
        FROM pv_stocks
        join stocks on stocks.id = pv_stocks.stock_id
        join produits on produits.id = stocks.produit_id
        where pv_stocks.pv_id=".$pv->id."
        group by stocks.produit_id;");

        return view('prints.printpv_ticket',compact('pv','listProduits','userProfile'));
    }


}
