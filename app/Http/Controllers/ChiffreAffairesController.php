<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ChiffreAffaire;
use DB;
use Illuminate\Http\Request;

class ChiffreAffairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $chiffreaffaires = ChiffreAffaire::where('chiffreannuel', 'LIKE', "%$keyword%")
                ->orWhere('objectif', 'LIKE', "%$keyword%")
                ->orWhere('commerciale_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $annuel = ChiffreAffaire::whereNotNull('chiffreannuel')->latest()->paginate($perPage);
            $chiffreaffaires = ChiffreAffaire::join('userprofiles','chiffreaffaires.commerciale_id','=','userprofiles.user_id')->get(['userprofiles.prenom','userprofiles.nom','chiffreaffaires.*']);
            // dd($chiffreaffaires);
        }

        return view('chiffre-affaires.index', compact('chiffreaffaires','annuel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
         $commerciales = DB::select("select  users.id,userprofiles.nom,userprofiles.prenom FROM userprofiles,users
                                        WHERE userprofiles.user_id = users.id and userprofiles.type = 'Commercial' ");

//                                          $commerciales = DB::select("SELECT DISTINCT userprofiles.user_id as id,userprofiles.nom,userprofiles.prenom
// FROM userprofiles
// INNER JOIN chiffreaffaires ON chiffreaffaires.commerciale_id <> userprofiles.user_id  and  (userprofiles.nom is not null or userprofiles.prenom is not null) and userprofiles.type="Commercial"
// ");
        return view('chiffre-affaires.create',compact('commerciales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        ChiffreAffaire::create($requestData);

        return redirect('chiffre-affaires')->with('flash_message', 'ChiffreAffaire added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $chiffreaffaire = ChiffreAffaire::findOrFail($id);

        return view('chiffre-affaires.show', compact('chiffreaffaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $chiffreaffaire = ChiffreAffaire::findOrFail($id);


         $commerciales = DB::select("select  users.id,userprofiles.nom,userprofiles.prenom FROM userprofiles,users
                                        WHERE userprofiles.user_id = users.id and userprofiles.type = 'Commercial'");

        return view('chiffre-affaires.edit', compact('chiffreaffaire','commerciales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $chiffreaffaire = ChiffreAffaire::findOrFail($id);

        $chiffreaffaire->update($requestData);

        return redirect('chiffre-affaires')->with('flash_message', 'ChiffreAffaire updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ChiffreAffaire::destroy($id);

        return redirect('chiffre-affaires')->with('flash_message', 'ChiffreAffaire deleted!');
    }
}
