<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserProfile;
use App\User;

class UserProfileController extends Controller{
    
    public function index(){

        $users = User::join('userprofiles','users.id','=','userprofiles.user_id')->get(['users.id','users.email','userprofiles.nom',
                                                                                        'userprofiles.prenom','userprofiles.tel',
                                                                                        'userprofiles.remise_min','userprofiles.remise_max']);

        
        return view('utilisateurs.index',compact('users'));
    }


    public function create(){

        return view('utilisateurs.create');
    }

    public function store(Request $request){ 

        $datausers = $this->getDataUsers($request);
        $datauserProfile = $this->getDataUserProfile($request);

        $datausers['password'] = bcrypt($datausers['password']);
        $user = User::create($datausers);

        $datauserProfile['user_id'] = $user->id;
        UserProfile::create($datauserProfile);

        return redirect('utilisateurs/')->with('success_message', 'User ajoute avec succes');

    }


    public function edit($id){

        $user = User::join('userprofiles','users.id','=','userprofiles.user_id')
                    ->where('users.id',$id)
                    ->get(['users.id','users.email','users.name','userprofiles.nom',
                          'userprofiles.prenom','userprofiles.tel','userprofiles.type','userprofiles.tel',
                          'userprofiles.remise_min','userprofiles.remise_max'])->first();
        //return  $user;
        return view('utilisateurs.edit', compact('user'));
    }


    public function update($id, Request $request){
            

        
        $datausers = $this->getDataUsers($request);
        $datauserProfile = $this->getDataUserProfile($request);

        if( $datausers['password'] != null)
            $datausers['password'] = bcrypt($datausers['password']);
        else
            unset($datausers['password']);


        $user = User::findOrFail($id);
        $user->update($datausers);

        $userProfile = UserProfile::where('user_id',$id)->get()->first();
        $userProfile->update($datauserProfile);

        return redirect('utilisateurs/')->with('success_message', 'Sauvgarde');

    }

    public function destroy($id)
    {

        $user = User::findOrFail($id);
        $userProfile = UserProfile::where('user_id',$id)->get()->first();
        $userProfile->delete();
        $user->delete();

        return redirect('utilisateurs/')->with('success_message', 'utilisateurs supprime');

    }




    protected function getDataUsers(Request $request){
        $rules = [
            'name' => 'required|string|min:0|max:191' ,
            'email' => 'required|string|min:0|max:191',
            'password' => 'required|string|min:0|max:191',
        ];
        
        $data = $request->validate($rules);


        return $data;
    }



    protected function getDataUserProfile(Request $request){
        $rules = [
            'nom' => 'required|string|min:0|max:191' ,
            'prenom' => 'nullable|string|min:0|max:191',
            'tel' => 'nullable|string|min:0|max:191',
            'type'=> 'nullable|string|min:0|max:191',
            'remise_min'=> 'nullable|string|min:0|max:191',
            'remise_max'=> 'nullable|string|min:0|max:191',    
        ];
        
        $data = $request->validate($rules);


        return $data;
    }
}
