<?php

namespace App\Http\Controllers;


use App\Depense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Collection;

class DepenseController extends Controller{


    public function index(Request $request){

        $keyword = $request->get('search');



        if (!empty($keyword)) {
            $depenses = Collection::make(new Depense);

            $depenses = Depense::where('nom','like', '%'.$keyword.'%')
            ->orwhere('prenom','like','%'.$keyword.'%')->orderby('created_at','desc')->paginate(15);
            $gain = [];
        }
        else {
            $depenses = Depense::where('type','Depense')->orderby('created_at','desc')->latest()->paginate(15);
            $gain = Depense::where('type','Gain')->orderby('created_at','desc')->latest()->paginate(15);
        }
        return view('depenses.index',compact('depenses','gain'));

    }


    public function create(){
        return view('depenses.create');
    }



    public function store(Request $request){


        $data = $this->getData($request);
        if($request->hasFile('imagesfacture')){
            $path = $request->imagesfacture->store('uploads','public');
            $data['imagesfacture']= $path;
        }
        Depense::create($data);
        return redirect('depenses/')->with('success_message', 'Depense ajoute avec succes');

    }

    public function edit($id){

        $depense = Depense::findOrFail($id);

        return view('depenses.edit', compact('depense'));
    }


    public function update($id, Request $request){


            $data = $this->getData($request);
            $depense = Depense::findOrFail($id);
            $depense->update($data);

            return redirect('depenses/')->with('success_message', 'Sauvgarde');

    }


    protected function getData(Request $request){
        $rules = [
            'nom' => 'required|string|min:0|max:191' ,
            'prenom' => 'nullable|string|min:0|max:191',
            'montant' => 'nullable|numeric',
            'type'=> 'nullable|string|min:0|max:191',
            'motif'=> 'nullable|string|min:0|max:191',
            'date' => 'nullable|date',
            'autremotif' => 'nullable|string|min:0|max:191',
            'justifie' => 'nullable|string|min:0|max:191',
            'factureref' => 'nullable|string|min:0|max:191',
            'imagesfacture' => 'nullable|image',
        ];

        $data = $request->validate($rules);

        return $data;
    }
}
