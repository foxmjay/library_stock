<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlAchat extends Model{

      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bl_achats';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'bl_id',
                'achat_id',
                'stock_id',
                'created_at',
                'updated_at',
              ];
}
