<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boncommand extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boncommands';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'parentbc_id',
                'fournisseur_id',
                'ref_bc',
                'ref_devis',
                'image_devis',
                'prix_total',
                'etat',
                'date_bc',
                'echeance_paiement',
                'date_livraison',
                'user_id',
                'tva',
                'type',
                'created_at',
                'updated_at',
                'deleted_at'
              ];
              
    public function fournisseur(){
        return $this->belongsTo('App\Fournisseur');
    }
}
