<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facture extends Model{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'factures';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'devis_id',
                'bl_id',
                'ref_facture',
                'prix_total',
                'created_at',
                'updated_at',
              ];

}
