<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PVStock extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pv_stocks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'pv_id',
                'stock_id',
                'prix_unitaire',
                'created_at',
                'updated_at',
              ];

}
