<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonretour extends Model{
    
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bonretours';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'client_id',
                'fournisseur_id',
                'bl_id',
                'devis_id',
                'ref_br',
                'date_br',
                'image_br',
                'prix_total',
                'created_at',
                'updated_at',
              ];
        
}
