<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devise extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devises';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'name',
                'sign',
                'created_at',
                'updated_at'
              ];

}
