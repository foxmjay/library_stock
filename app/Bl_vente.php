<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bl_vente extends Model{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bl_ventes';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bl_id',
        'vente_id',
        'stock_id',
        'created_at',
        'updated_at'
    ];
}
