<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoncommandBl extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boncommandbls';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'bc_id',
                'ref_bl',
                'image_bl',
                'prix_total',
                'created_at',
                'updated_at',
                'deleted_at'
              ];
              
}
