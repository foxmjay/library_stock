<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Langue extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'langues';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'name',
                'sign',
                'created_at',
                'updated_at'
              ];

}
