<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devis extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devis';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parentdevis_id',
        'client_id',
        'user_id',
        'ref_devis',
        'prix_total',
        'etat',
        'delai_livraison',
        'garantie',
        'objet',
        'locked',
        'valider',
        'version',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

}
