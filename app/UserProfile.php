<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userprofiles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'user_id',
                'nom',
                'prenom',
                'tel',
                'email',
                'type',
                'remise_min',
                'remise_max',
                'created_at',
                'updated_at',
                'deleted_at'
              ];


    public function stock(){
        return $this->hasone('App\User');
    }      
    
    public function rdv(){
        return $this->hasone('App\Rdv');
    }    

}
