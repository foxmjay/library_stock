<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Echange extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'echanges';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'client_id',
                'fournisseur_id',
                'devis_id',
                'bl_id',
                'vente_id',
                'stock_src',
                'stock_dst',
                'ref_echange',
                'date',
                'action',
                'created_at',
                'updated_at',
              ];
        
}
