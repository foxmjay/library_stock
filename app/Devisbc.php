<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devisbc extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devisbcs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'devis_id',
        'ref_devisbc',
        'num_bc',
        'date_bc',
        'image_bc',
        'etat',
        'type_facture',
        'created_at',
        'updated_at'
    ];

    /*public static function getTypeFactureByDevisId($id){
        $devisbc = Devisbc::where('devis_id',$id)->first();
        return $devisbc->type_facture;
    }*/

}
