<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VDStock extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vd_stocks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'vd_id',
                'stock_id',
                'remise',
                'prix_unitaire',
                'prix_total',
                'created_at',
                'updated_at',
              ];
        
}
