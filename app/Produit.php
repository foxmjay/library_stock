<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Produit extends Model
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'produits';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom','marque','barcode','description','categorie_id','garantie','ref_constructeur', 'ref_interne','prix_vente','prix_min','prix_max','created_at','updated_at','deleted_at'];



    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
     public function stock(){
         return $this->hasmany('App\Stock');
     }

}
