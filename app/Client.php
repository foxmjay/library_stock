<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'raison_sociale',
                'categorie',
                'secteur_activite',
                'forme_juridique',
                'identifient_fiscal',
                'n_patente',
                'n_reg_commerce',
                'ville_reg_commerce',
                'ice',
                'adresse',
                'adresse_facturation',
                'telephone',
                'gsm',
                'email',
                'ville_id',
                'pays_id',
                'region_id',
                'langue_id',
                'devise_id',
                //'mode_paiement',
                //'echeance_paiement',
                'note',
                'created_at',
                'updated_at',
                'deleted_at'
              ];


                      

}
