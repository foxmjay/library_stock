<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Echeance extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'echeances';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'devis_id',
                'bc_id',
                'jours',
                'pourcentage',
                'date',
                'type_paiement',
                'image_cheque',
                'date_cheque',
                'recu',
                'created_at',
                'updated_at',
              ];


    public function devis(){
        return $this->hasmany('App\Devis');
    }                     

    public function boncommand(){
        return $this->hasmany('App\Boncommand');
    }                     

}
