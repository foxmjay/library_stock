<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;

class EmailChequeCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EmailCheque:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $echeances = DB::select("select * from echeances_alert");

            $data['echeances']= $echeances;



        // dd($data);

          Mail::send('emails.mytestmail', $data, function($message) {

              $message->to('rgaguena.elmehdi@gmail.com')
                        ->to('aminebenhfid@gmail.com')
                      ->subject('Advanced-Cheque Alert - '.date("d/m/Y"));
          });

          if (Mail::failures()) {
            \Log::channel('daily')->info("Email fail!");
           }else{
            \Log::channel('daily')->info("email sent!");
           }
    }
}
