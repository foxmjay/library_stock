<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BonretourStock extends Model{
    

        
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bonretour_stocks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'br_id',
                'stock_id',
                'vente_id',
                'bl_id',
                'bc_id',
                'prix',
                'created_at',
                'updated_at',
              ];

}
