<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Souscategorie extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'souscategories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['categorie', 'created_at','updated_at'];

}
