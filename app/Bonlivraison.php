<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonlivraison extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bonlivraisons';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'devis_id',
        'ref_bl',
        'type',
        'prix_total',
        'created_at',
        'updated_at'
    ];

}
