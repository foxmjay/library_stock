<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'DashboardController@home')->middleware('auth');
Route::post('dashboard/filter', 'DashboardController@home')->middleware('auth');

Auth::routes(['register'=> false]);

Route::resource('clients', 'ClientsController')->middleware('auth');
Route::post('clients/search', 'ClientsController@index')->middleware('auth');
Route::post('clients/get_clients', 'ClientsController@getClients')->middleware('auth');

Route::resource('fournisseurs', 'FournisseurController')->middleware('auth');
Route::post('fournisseurs/search', 'FournisseurController@index')->middleware('auth');

Route::resource('utilisateurs', 'UserProfileController');

Route::resource('depenses', 'DepenseController')->middleware('auth');
Route::post('depenses/search', 'DepenseController@index')->middleware('auth');

Route::get('messages/sent', 'MessagesController@envoye')->middleware('auth');

Route::resource('messages', 'MessagesController')->middleware('auth');

Route::get('send-mail', 'DashboardController@sendEmail')->middleware('auth');

Route::resource('chiffre-affaires', 'ChiffreAffairesController')->middleware('auth');



/*Route::get('/contacts/{client}/client', 'ContactsController@indexClient')->middleware('auth');
Route::get('contacts/create/{client}/client','ContactsController@createWithClient')->middleware('auth');
Route::get('contacts/edit/{client}/client','ContactsController@editWithClient')->middleware('auth');
Route::post('contacts/store/{contact}/client', 'ContactsController@storeWithClient')->middleware('auth');

Route::get('/contacts/{fournisseur}/fournisseur', 'ContactsController@indexFournisseur')->middleware('auth');
Route::get('contacts/create/{fournisseur}/fournisseur','ContactsController@createWithFournisseur')->middleware('auth');
Route::get('contacts/edit/{fournisseur}/fournisseur','ContactsController@editWithFournisseur')->middleware('auth');
Route::post('contacts/store/{fournisseur}/fournisseur', 'ContactsController@storeWithFournisseur')->middleware('auth');


Route::put('contacts/{id}', 'ContactsController@update')->middleware('auth');
Route::delete('contacts/{id}', 'ContactsController@destroy')->middleware('auth');*/


Route::get ('/contacts/{client_id}/perclient', 'ContactsController@getItemsClient');
Route::get ('/contacts/{fournisseur_id}/perfournisseur', 'ContactsController@getItemsFournisseur');
Route::post ('/contacts/{id}/delete', 'ContactsController@deleteItem');
Route::post ('/contacts', 'ContactsController@storeItem');

Route::get('produits/importcsv', 'ProduitsController@importcsv')->middleware('auth');
Route::post('produits/storecsv', 'ProduitsController@storecsv')->middleware('auth');

Route::resource('produits', 'ProduitsController')->middleware('auth');
Route::post('produits/search', 'ProduitsController@index')->middleware('auth');

Route::post('produits/api/create', 'ProduitsController@addProduit')->middleware('auth');

Route::get('searchproduct/{productname}', 'ProduitsController@SearchProduct')->middleware('auth');
Route::get('getallproduct/', 'ProduitsController@getAllProducts')->middleware('auth');

Route::post('produits/getstock', 'ProduitsController@getStock')->middleware('auth');
Route::get('product_select2', 'ProduitsController@productSelect2')->middleware('auth');




//Route::resource('contacts', 'ContactsController')->middleware('auth');
Route::resource('achat', 'AchatController')->middleware('auth');
Route::resource('vente', 'VenteController')->middleware('auth');

Route::get('stocks/importcsv', 'StockController@importcsv')->middleware('auth');
Route::post('stocks/storecsv', 'StockController@storecsv')->middleware('auth');
Route::resource('stock', 'StockController')->middleware('auth');

Route::post('stock/search', 'StockController@index')->middleware('auth');
Route::post('etatstock/search', 'StockController@etatStock')->middleware('auth');
Route::get('etatstock', 'StockController@etatStock')->middleware('auth');
Route::post('stock/storeItems', 'StockController@storeItems')->middleware('auth');

Route::resource('categories', 'CategorieController')->middleware('auth');


Route::post('boncommands', 'BoncommandsController@store')->middleware('auth');
Route::get('boncommands/{id}/edit', 'BoncommandsController@edit')->middleware('auth');
Route::get('boncommands/{id}/produit', 'BoncommandsController@produit')->middleware('auth');
Route::put('boncommands/{id}', 'BoncommandsController@update')->middleware('auth');
Route::get('boncommands/{f_id}/create', 'BoncommandsController@create')->middleware('auth');
Route::get('boncommands/{id}/print', 'BoncommandsController@print')->middleware('auth');
Route::delete('boncommands/{id}', 'BoncommandsController@destroy')->middleware('auth');
Route::post('boncommands/confirmer', 'BoncommandsController@confirmer')->middleware('auth');
Route::get('boncommands/{bc_id}/createbl', 'BoncommandsController@createBl')->middleware('auth');

Route::post('boncommandbls/store', 'BoncommandBlController@store')->middleware('auth');
Route::get('boncommandbls/{bc_id}/perbc', 'BoncommandBlController@getItemsPerBc')->middleware('auth');
Route::get('boncommandbls/{bl_id}/show', 'BoncommandBlController@show')->middleware('auth');


Route::get('devis/{id}/printdevis', 'DevisController@printdevis')->middleware('auth');
Route::get('bonlivraison/{id}/printbl', 'BonlivraisonsController@printbl')->middleware('auth');
Route::get('bonlivraison/{id}/printfacture', 'BonlivraisonsController@printfacture')->middleware('auth');
Route::get('bonlivraison/{id}/printfacturetotal', 'BonlivraisonsController@printfacturetotal')->middleware('auth');
Route::post('devis/{id}/confirm', 'DevisbcsController@confirm')->middleware('auth');
Route::get('devis/{c_id}/create', 'DevisController@create')->middleware('auth');
Route::post('devis/{id}/valider', 'DevisController@validerDevis')->middleware('auth');



Route::get('/devis/{id}/edit2', 'DevisController@edit2')->middleware('auth');
Route::get('/devis/{id}/getolddevis', 'DevisController@getOldDevis')->middleware('auth');
Route::resource('devis', 'DevisController')->middleware('auth');

Route::get ('/achats/{bc_id}/perbc', 'AchatController@getItems')->middleware('auth');
Route::post ('/achats/{id}/delete', 'AchatController@deleteItem')->middleware('auth');
Route::post ('/achats', 'AchatController@storeItem')->middleware('auth');


Route::get('/ventes/{devis_id}/perdevis', 'VenteController@getItems')->middleware('auth');


Route::post ('/ventes/{id}/delete', 'VenteController@deleteItem')->middleware('auth');
Route::post ('/ventes', 'VenteController@storeItem')->middleware('auth');
Route::post('/ventes/getquantite', 'VenteController@getQuantite')->middleware('auth');


Route::get ('/ventes/{devis_id}/notSelected', 'VenteController@getVentesNotSelected')->middleware('auth');


Route::get ('/ventes/{id}/client/{cid}', 'VenteController@cid')->middleware('auth');

Route::get('getproduct/{productname}', 'DevisController@SearchProduct')->middleware('auth');

Route::post('getstock/retour', 'StockController@SearchStockRetour')->middleware('auth');
Route::post('/stockretour/confrimer', 'BonretoursController@confirmer')->middleware('auth');
Route::post('/stockretour/confrimerbl', 'BonretoursController@confirmerBL')->middleware('auth');
Route::get('bonretour/{id}/printbr', 'BonretoursController@printbr')->middleware('auth');
Route::get('bonretour/{id}/printavoir', 'BonretoursController@printavoir')->middleware('auth');



Route::get('parameters/edit', 'ParameterController@edit')->middleware('auth');
Route::put('parameters/update', 'ParameterController@update')->middleware('auth');


Route::get ('/echeances/{bc_id}/index', 'EcheanceController@index')->middleware('auth');
Route::get ('/echeances/{bc_id}/perbc', 'EcheanceController@getItemsBc')->middleware('auth');
Route::post ('/echeances', 'EcheanceController@storeItem')->middleware('auth');
Route::post ('/echeances/{id}/delete', 'EcheanceController@deleteItem')->middleware('auth');


Route::get ('/echeances/{devis_id}/indexdevis/{type}', 'EcheanceController@indexDevis')->middleware('auth');
Route::get ('/echeances/{devis_id}/perdevis', 'EcheanceController@getItemsDevis')->middleware('auth');


Route::post ('/validation/{id}', 'EcheanceController@ValidationEcheance')->middleware('auth');


Route::get('bonlivraison/{id}/create', 'BonlivraisonsController@create')->middleware('auth');
Route::get('bonlivraison/{id}/edit', 'BonlivraisonsController@edit')->middleware('auth');
Route::get('bonlivraison/{id}/show', 'BonlivraisonsController@show')->middleware('auth');
Route::post('bonlivraison/{id}/store', 'BonlivraisonsController@store')->middleware('auth');
Route::get('bonlivraison/{bl_id}/confirmfacture', 'BonlivraisonsController@confirmationFacture')->middleware('auth');
Route::post('bonlivraison/storeFacture', 'BonlivraisonsController@storeFacture')->middleware('auth');


Route::post('bonlivraison/{id}/update', 'BonlivraisonsController@update')->middleware('auth');
Route::get('bonlivraison/{id}/perbl', 'BonlivraisonsController@getItemsPerBl')->middleware('auth');
Route::get('bonretour/{id}/create', 'BonretoursController@create')->middleware('auth');
Route::get('bonretour/{id}/frombl', 'BonretoursController@frombl')->middleware('auth');
Route::get('echange/{id}/create', 'EchangeController@create')->middleware('auth');
Route::post('echange/store', 'EchangeController@store')->middleware('auth');
Route::get('echange/{id}/printbl', 'EchangeController@printbl')->middleware('auth');
Route::get('echange/fournisseur', 'EchangeController@indexFournisseur')->middleware('auth');
Route::post('echange/search', 'EchangeController@indexFournisseur')->middleware('auth');
Route::get('echange/fournisseur/{id}', 'EchangeController@echange_fournisseur')->middleware('auth');
Route::post('echange/fournisseur/store', 'EchangeController@store_fournisseur')->middleware('auth');






Route::resource('rdvs', 'RdvController')->middleware('auth');
Route::get('notifsrdv', 'RdvController@notifsRdv')->name('notifsrdv.notifs')->middleware('auth');
Route::get('notifsrdv/{id}', 'RdvController@notifsRdvShow')->name('notifsrdv.show')->middleware('auth');

/**********Compta**********/
Route::get ('/compta/{user_id}', 'ComptaController@GainParMoisByUser')->middleware('auth');

Route::get ('/backup', 'ComptaController@backup')->middleware('auth');

Route::get ('/usersallow/{id}', 'UsersallowController@index')->middleware('auth');
Route::post ('/usersallow/devis', 'UsersallowController@storeItem')->middleware('auth');
Route::get ('/usersallow/{id}/{type}', 'UsersallowController@getItems')->middleware('auth');
Route::post ('/usersallow/{id}/delete', 'UsersallowController@deleteItem')->middleware('auth');


Route::get('ventedirect', 'VenteDirectController@index')->middleware('auth');
Route::post('ventedirect/search', 'VenteDirectController@index')->middleware('auth');
Route::get('ventedirect/create', 'VenteDirectController@create')->middleware('auth');
Route::post('ventedirect/getstock', 'VenteDirectController@getStock')->middleware('auth');
Route::post('ventedirect/store', 'VenteDirectController@store')->middleware('auth');
Route::get('ventedirect/{id}/show', 'VenteDirectController@show')->middleware('auth');
Route::get('ventedirect/{id}/printbl', 'VenteDirectController@printbl')->middleware('auth');


Route::get('pointventes', 'PointVenteController@index')->middleware('auth');
Route::post('pointventes/search', 'PointVenteController@index')->middleware('auth');
Route::get('pointventes/create', 'PointVenteController@create')->middleware('auth');
Route::get('pointventes/{id}/edit', 'PointVenteController@edit')->middleware('auth');
Route::post('pointventes/getstock', 'PointVenteController@getStock')->middleware('auth');
Route::post('pointventes/store_pv', 'PointVenteController@storePV')->middleware('auth');
Route::post('pointventes/update_pv', 'PointVenteController@updatePV')->middleware('auth');
Route::get('pointventes/getpv/{id}', 'PointVenteController@getPV')->middleware('auth');
Route::get('pointventes/printpvticket/{pv_id}', 'PointVenteController@printPVTicket')->middleware('auth');

//Route::post('pointventes/store', 'PointVenteController@store')->middleware('auth');
//Route::get('pointventes/{id}/show', 'PointVenteController@show')->middleware('auth');
//Route::get('pointventes/{id}/printbl', 'PointVenteController@printbl')->middleware('auth');


/*Route::resource('/boncommands', 'BoncommandController', [
    'except' => ['edit', 'show', 'store']
  ]);*/

//Route::get('/home', 'HomeController@index')->name('home');
